<!DOCTYPE html>
<html lang="{{ $locale }}">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>{{ $seo['title']??null}} {{!empty($seo2['title'])?'- '.$seo2['title']:'' }}</title>
    @if(!empty($seo['keywords']))
        <meta name="keywords"
              content="{{ $seo['keywords']??null }}{{!empty($seo2['keywords'])?'- '.$seo2['keywords']:'' }}">
        <meta name="description"
              content="{{ $seo['description']??null }}{{!empty($seo2['description'])?'- '.$seo2['description']:'' }}">
    @endif
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="{{ aAdmin('img/avatar.jpg') }}" rel="shortcut icon" type="image/x-icon">
    @stack('meta')
    <link rel="stylesheet" href="{{ asset('f/site/css/style.css') }}">
    @css(aApp('select2/select2.css'))
    @css(aApp('font-awesome/css/all.css'))
    @stack('css')
    @if(false)
    <script>
        window.fbAsyncInit = function () {
            FB.init({
                appId: '1187896921575649',
                xfbml: true,
                version: 'v7.0'
            });
            FB.AppEvents.logPageView();
        };

        (function (d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) {
                return;
            }
            js = d.createElement(s);
            js.id = id;
            js.src = "https://connect.facebook.net/en_US/sdk.js";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    </script>
    @endif
</head>
<body>
<header id="header_container">
    <div id="header" style="            box-shadow: -1px 0px 6px 2px #8080804a;
">
        <input type="hidden" id="noResult" value="{{t('search.no results')}}">
        <div class="container header_container d_flex j_content_between a_items_center">
            <div style="">
                <div class="header-min d_flex a_items_center j_content_between a_items_center">
                    <div class="header-logo">
                        <a href="{{route('page')}}">
                            <img width="155" src="{{ $info->data->header_logo() }}" alt="">
                        </a>
                    </div>

                </div>
            </div>
            <div class="header_bar">
                <div class="d_flex j_content_end a_items_center cabinet-buttons h-full">
                    @if(!empty($info->contacts[0]->phone))
                        <a href="tel:{{$info->contacts[0]->phone}}" class="d_flex mr-2 font-gray cabinet-login-button j_content_center a_items_center"><i
                                class="fa fa-phone mr-1"></i> <span
                                class="font-segeo font-13 header_icon_text">{{ $info->contacts[0]->phone}} </span></a>
                    @endif
                    <a href="{{route('cabinet.login')}}" class="d_flex mr-2 font-gray cabinet-login-button j_content_center a_items_center"><i
                            class="fa fa-user mr-1"></i> <span
                            class="font-segeo font-13 header_icon_text">{{ t('app.Անձնական էջ') }}</span></a>
                    <a href="{{route('cabinet.addEstate',['step'=>1])}}" class="header_icon_text">
                        <button style="padding: 7px 10px;border-radius: 18px"
                                class="bg-default add_estate_button  d_flex a_items_center j_content_center">
                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 20.016 19.918">
                                <g id="XMLID_1072_" transform="translate(0 -1.25)">
                                    <path id="XMLID_980_"
                                          d="M0,21.168H15.1V1.25H0Zm2.937-4.038H5.724v1.176H2.937ZM2.823,5.6V4.426H7.548V5.6Zm.114,10.273V14.7h9.222v1.176Zm0-2.431V12.268h9.222v1.176Zm0-2.431V9.837h9.222v1.176Zm0-2.431V7.406h9.222V8.582Z"
                                          fill="#ffffff"/>
                                    <path id="XMLID_1073_" d="M438.5,108.933h2.823v10.656H438.5Z"
                                          transform="translate(-421.307 -103.461)"
                                          fill="#ffffff"/>
                                    <path id="XMLID_1076_" d="M445.7,413.627l1.175-2.928h-2.351Z"
                                          transform="translate(-427.092 -393.395)"
                                          fill="#ffffff"/>
                                    <path id="XMLID_1077_" d="M441.323,33.162a1.412,1.412,0,0,0-2.823,0V33.6h2.823Z"
                                          transform="translate(-421.307 -29.304)" fill="#ffffff"/>
                                </g>
                            </svg>
                            <span class="ml-1 font-white font-segeo font-13">
                            {{ t('app.Տեղադրել հայտարարություն') }}
                        </span>
                        </button>
                    </a>
                </div>
                <div class="d_flex a_items_center j_content_end" style="padding-top: 15px">
                    <div class="header-menu" style="padding-right: 25px">
                        <div class="open-menu">
                            <span></span>
                            <span></span>
                            <span></span>
                            <span></span>
                        </div>
                        <div class="for-mobile-bg"></div>
                        <div class="menu-cnt">
                            <div id="mySidenav" class="sidenav">
                                <div class="sidenav_mobile_language">
                                    <ul>
                                        @if(count($languages))
                                            @foreach($languages as $language)
                                                <li>
                                                    <a class="{{ ($current_language->id == $language->id)?'actived':'' }}"
                                                       style="color: #737373;"
                                                       href="{{ url(\LanguageManager::getUrlWithLocale($language->iso)) }}"><span>{{ $language->title }}</span>
                                                    </a>{{(($loop->index + 1) == count($languages))?'':' /'}}
                                                </li>
                                            @endforeach
                                        @endif
                                    </ul>
                                </div>
                                <div class="d_flex j_content_center a_items_end f_direction_column mr-3 cabinet-mobile-buttons">

                                    <a href="">
                                        <button style="padding: 7px 10px;border-radius: 18px"
                                                class="bg-default add_estate_button  d_flex a_items_center j_content_end">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 20.016 19.918">
                                                <g id="XMLID_1072_" transform="translate(0 -1.25)">
                                                    <path id="XMLID_980_"
                                                          d="M0,21.168H15.1V1.25H0Zm2.937-4.038H5.724v1.176H2.937ZM2.823,5.6V4.426H7.548V5.6Zm.114,10.273V14.7h9.222v1.176Zm0-2.431V12.268h9.222v1.176Zm0-2.431V9.837h9.222v1.176Zm0-2.431V7.406h9.222V8.582Z"
                                                          fill="#ffffff"/>
                                                    <path id="XMLID_1073_" d="M438.5,108.933h2.823v10.656H438.5Z"
                                                          transform="translate(-421.307 -103.461)"
                                                          fill="#ffffff"/>
                                                    <path id="XMLID_1076_" d="M445.7,413.627l1.175-2.928h-2.351Z"
                                                          transform="translate(-427.092 -393.395)"
                                                          fill="#ffffff"/>
                                                    <path id="XMLID_1077_" d="M441.323,33.162a1.412,1.412,0,0,0-2.823,0V33.6h2.823Z"
                                                          transform="translate(-421.307 -29.304)" fill="#ffffff"/>
                                                </g>
                                            </svg>
                                            <span class="ml-1 font-white font-segeo font-13">
                            {{ t('app.Տեղադրել հայտարարություն') }}
                        </span>
                                        </button>
                                    </a>
                                </div>
                                <ul>
                                    @if(!empty($menu_pages))
                                        @foreach($menu_pages as $page)
                                            <li>
                                                <a class="{{ (isset($current_page)  && ($current_page == $page->id))?'actived':null }}"
                                                   href="{{ route('page', ['url'=>$page->url]) }}">{{ $page->title }}</a>
                                            </li>
                                        @endforeach
                                    @endif
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="header-language">
                        <ul>
                            @if(count($languages))
                                @foreach($languages as $language)
                                    <li>
                                        <a class="{{ ($current_language->id == $language->id)?'actived':'' }}"
                                           style="color: #737373;"
                                           href="{{ url(\LanguageManager::getUrlWithLocale($language->iso)) }}"><span>{{ $language->title }}</span>
                                        </a>{{(($loop->index + 1) == count($languages))?'':' /'}}
                                    </li>
                                @endforeach
                            @endif
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>
<style>
    .slick-list.draggable{
        z-index: 11111111!important;
    }
    .error-notify, .success-notify {
        animation-fill-mode: forwards;
        animation-name: closett;
        animation-duration: 5s;
        -moz-animation-timing-function: linear;
    }

    @keyframes closett {
        0% {
            opacity: 1;
        }
        100% {
            opacity: 0;
        }
    }

    /*body {*/
    /*    background-color: white;*/
    /*}*/
</style>

@yield('content')
{{--<script src="{{ asset('f/site/js/jquery-3.2.1.min.js') }}"></script>--}}
{{--<script src="{{ asset('f/site/js/jquery.cookie.min.js') }}"></script>--}}
{{--<script src="{{ asset('f/site/slick/slick.min.js') }}"></script>--}}
<script src="{{ asset('f/site/js/script.js') }}"></script>
@if(auth()->check())
    <script>
        $('.best-offers__heart').click(function () {
            var id = $(this).data('id');
            $.ajax({
                url: '{!! route('auth.favorites') !!}',
                type: 'post',
                dataType: 'json',
                data: {
                    _token: '{!! csrf_token() !!}',
                    _method: 'post',
                    item_id: id,
                },
                error: function (e) {
                    alert("Error");
                },
                success: function (e) {
                    if (e.success) {
                        if(e.success){
                            if (e.deleted){
                                $(document).find('.best-offers__heart[data-id='+e.estate_id+']').removeClass('active-heart')
                            } else {
                                $(document).find('.best-offers__heart[data-id='+e.estate_id+']').addClass('active-heart')
                            }
                        }else alert("Error");
                    } else alert("Error");
                }
            });

        });
        $('.product-det__heart').click(function () {
            var id = $(this).data('id');
            $.ajax({
                url: '{!! route('auth.favorites') !!}',
                type: 'post',
                dataType: 'json',
                data: {
                    _token: '{!! csrf_token() !!}',
                    _method: 'post',
                    item_id: id,
                },
                error: function (e) {
                    alert("Error");
                },
                success: function (e) {
                    if (e.success) {
                        if(e.success){
                            if (e.deleted){
                                $(document).find('.product-det__heart[data-id='+e.estate_id+']').removeClass('active-heart')
                            } else {
                                $(document).find('.product-det__heart[data-id='+e.estate_id+']').addClass('active-heart')
                            }
                        }else alert("Error");
                    } else alert("Error");
                }
            });

        });
    </script>
@else
    <script>
        $('.best-offers__heart').click(function () {
            $.cookie.defaults = {path: '/', expires: 14};
            var favorites = $.cookie("favorite");
            var id = '' + $(this).data('id') + '';
            $.removeCookie("favorite");
            // console.log( favorites,id,$.inArray(id, favorites))
            if (favorites == null) {
                favorites = [];
            } else {
                favorites = favorites.split(',')
            }
            if ($(this).hasClass('active-heart')) {
                if ($.inArray(id, favorites) != -1) {
                    favorites.splice($.inArray(id, favorites), 1);
                }
                $(this).removeClass('active-heart')
            } else {
                if (!($.inArray(id, favorites) != -1)) {
                    favorites.push(id)
                }
                $(this).addClass('active-heart')
            }
            $.cookie("favorite", favorites);
        });
        $('.product-det__heart').click(function () {
            $.cookie.defaults = {path: '/', expires: 14};
            var favorites = $.cookie("favorite");
            var id = '' + $(this).data('id') + '';
            $.removeCookie("favorite");
            // console.log( favorites,id,$.inArray(id, favorites))
            if (favorites == null) {
                favorites = [];
            } else {
                favorites = favorites.split(',')
            }
            if ($(this).hasClass('active-heart')) {
                if ($.inArray(id, favorites) != -1) {
                    favorites.splice($.inArray(id, favorites), 1);
                }
                $(this).removeClass('active-heart')
            } else {
                if (!($.inArray(id, favorites) != -1)) {
                    favorites.push(id)
                }
                $(this).addClass('active-heart')
            }
            $.cookie("favorite", favorites);
        });
    </script>
@endif


<script>
    $(document).ready(function () {
        function placeFooter() {
            if ($(document.body).height() < $(window).height()) {
                $("footer").css({position: "absolute", bottom: "0px", width: "100%"});
            } else {
                $("footer").css({position: ""});
            }
        }

        placeFooter();
    })

</script>

@js(aApp('select2/select2.js'))

{{--<script src="{{ asset('f/site/js/multirange.js') }}"></script>--}}

@stack('js')
</body>
</html>
