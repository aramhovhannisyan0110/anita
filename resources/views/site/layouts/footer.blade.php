<footer>
    <div class="container">
        <div class="footer-min">
            <div class="footer-block d_flex a_items_start j_content_between">
                <div class="footer-street">
                    <strong class="footer-title">{{t('app.Հասցե')}}</strong>
                    <ul>
                        <li>
                            <img src="{{asset('f/site/img/Group10.png')}}">
                            @foreach($info->contacts as $address)
                                @if($address->address!='')
                                    <a href="javascript:void(0)">{{$address->address}}</a>
                                @endif
                            @endforeach
                        </li>
                        <li>
                            <img src="{{asset('f/site/img/Group11.png')}}">
                            @foreach($info->contacts as $phone)
                                @if($phone->phone!='')
                                   <a href="tel:{{$phone->phone}}">{{$phone->phone}} <br></a>
                                @endif
                            @endforeach
                        </li>
                        <li>
                            <img src="{{asset('f/site/img/Group12.png')}}">
                            @foreach($info->contacts as $email)
                                @if($email->email!='')
                                    <a href="mailto:{{$email->email}}">{{$email->email}} <br></a>
                                @endif
                            @endforeach
                        </li>
                    </ul>
                </div>
                <div class="footer-href">
                    <strong class="footer-title">{{t('app.Տեղեկատվություն')}}</strong>
                    <ul>
                        @if(!empty($menu_pages))
                            @foreach($menu_pages as $page)
                                <li>
                                    <a class="{{ (isset($current_page)  && ($current_page == $page->id))?'actived':null }}"
                                       href="{{ route('page', ['url'=>$page->url]) }}">{{ $page->title }}</a>
                                </li>
                            @endforeach
                        @endif
                    </ul>
                </div>
                <div class="footer-href">
                    <strong class="footer-title">{{t('app.Օգտակար հղումներ')}}</strong>
                    <div class="footer-href__last d_flex">
                        <ul>
                            @if(!empty($footer_links))
                                @foreach($footer_links as $link)
                                    @if($loop->index <= 4)
                                    <li>
                                        <a href="{{ $link->url }}" target="_blank">{{ $link->title }}</a>
                                    </li>
                                    @else
                                        <?php break ?>
                                    @endif
                                @endforeach
                            @endif
                        </ul>
                        <ul>
                            @if(!empty($footer_links))
                                @foreach($footer_links as $link)
                                    @if($loop->index > 4)
                                        <li>
                                            <a href="{{ $link->url }}" target="_blank">{{ $link->title }}</a>
                                        </li>
                                    @else
                                        <?php continue ?>
                                    @endif
                                @endforeach
                            @endif
                        </ul>
                    </div>
                </div>
                <div class="footer-form">
                    <strong class="footer-title">{{t('app.Բաժանորդագրվեք մեզ')}}</strong>
                    <div class="footer-form__info">
                        <form id="subscribe" action="{{ route('subscribe') }}#subscribe" method="post">
                            @csrf
                            <div class="footer-form__inp d_flex a_items_center">
                                <label>
                                    <input type="text" name="email" placeholder="{{t('app.Մուտքագրեք էլ. հասցե')}}">
                                </label>
                                <div class="footer-form-btn">
                                    <button>
                                        <img src="{{asset('f/site/img/paper-plane.png')}}">
                                    </button>
                                </div>
                            </div>
                            @if($errors->has('email'))
                                <div style="color: #ff6563;font: 16px Segoe UI, HelveticaNeueCyr-Bold, Helvetica,Roboto, sans-serif;font-weight: 700;line-height: 1.3;">{{$errors->messages()['email'][0]}}</div>
                            @endif
                            @if(!empty(old('success')))
                               <div style="color: #26AF61;font: 16px Segoe UI, HelveticaNeueCyr-Bold, Helvetica,Roboto, sans-serif;font-weight: 700;line-height: 1.3;"> {{ t('subscribe.success') }} </div>
                            @endif
                        </form>
                    </div>
                    <div class="footer-icon">
                        <ul>
                            @foreach($socials as $social)
                                <li><a target="_blank" href="{{$social->url}}">
                                        <img src="{{$social->icon()}}">
                                    </a></li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
            <div class="footer-last-block d_flex j_content_between a_items_end">
                <div class="footer-last-text d_flex a_items_end">
                    <div class="footer-logo">
                        <a href="{{ route('page') }}" class="d_flex">
                            <img src="{{$info->data->menu_logo()}}">
                        </a>
                    </div>
                    <p>©{{date('Y')}} Atlass @lang('app.All rights reserved')</p>
                </div>
                <div class="footer-name d_flex a_items_center">
                    <a target="_blank" href="https://astudio.am/"> @lang('app.design and development')</a>
                    <p>ASTUDIO</p>
                </div>
            </div>
        </div>
    </div>
</footer>

