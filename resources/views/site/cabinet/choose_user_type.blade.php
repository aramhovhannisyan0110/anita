@extends('site.layouts.header')
@section('content')
    {{--    @dd($errors)--}}
    <div class="login"><p class="container">{{t('register.Ընտրեք օգտատիրոջ տեսակը')}}</p></div>
    <section class="d_flex justify-content-center container a_items_center f_direction_column login_container">
        <form action="{{route('chooseUserType.post')}}" method="post">
            @csrf
            <div class="login_text d_flex">
                <input type="hidden" name="user_id" value="{{$user->id}}">
                <div class="user_type d_flex a_items_center">
                    <input type="radio" name="user_type"
                           {{ (empty(old('user_type')) || old('user_type') == '1')?'checked':'' }}  value="1"
                           class="criteria filter_check_inp"
                           id="check-2" data-id="1">
                    <label class="cursor-pointer filter_check_label" for="check-2"
                           data-content="{{t('register.Անհատ')}}">{{t('register.Անհատ')}} </label>
                </div>
                <div class="user_type d_flex a_items_center">
                    <input type="radio" name="user_type"
                           {{ (!empty(old('user_type')) && old('user_type') == '2')?'checked':'' }} value="2"
                           class="criteria filter_check_inp" id="check-3"
                           data-id="1">
                    <label class="cursor-pointer filter_check_label" for="check-3"
                           data-content="{{t('register. Գործակալ կամ Կառուցապատող')}}">{{t('register. Գործակալ կամ Կառուցապատող')}} </label>
                </div>
            </div>
            <button type="submit">
                <svg style="margin-right: 5px" height="12pt" viewBox="0 -46 417.81333 417" width="12pt" xmlns="http://www.w3.org/2000/svg">
                    <path
                        d="m159.988281 318.582031c-3.988281 4.011719-9.429687 6.25-15.082031 6.25s-11.09375-2.238281-15.082031-6.25l-120.449219-120.46875c-12.5-12.5-12.5-32.769531 0-45.246093l15.082031-15.085938c12.503907-12.5 32.75-12.5 45.25 0l75.199219 75.203125 203.199219-203.203125c12.503906-12.5 32.769531-12.5 45.25 0l15.082031 15.085938c12.5 12.5 12.5 32.765624 0 45.246093zm0 0"
                        fill="#ffffff"/>
                </svg>
                <span>
                    {{t('register.Հաստատել')}}
                    </span>
            </button>
        </form>
    </section>
    @include('site.layouts.footer')
@endsection
@push('css')
    <style>
        body {
            background-color: #f8f6f6 !important;
        }

    </style>
@endpush
