@extends('site.layouts.header')
@section('content')
    <div class="main w-full">
        <div class="w-full bg-white">
            <div class="container">
                <div class="login row"><p class="col-12 py-1">{{t('cabinet.Ընտրեք օգտատիրոջ տեսակը')}}</p></div>
            </div>
        </div>
        <div class="container mt-4">
            <div class="row">
                <div class="col-12 col-md-5 col-lg-4 col-xl-3">
                    @component('site.components.cabinet_link',['active'=>$active??null])@endcomponent
                </div>
                <div class="col-12 col-md-7 col-lg-8 col-xl-9">
                    <div>
                        @if (session('personal.success'))
                            <div class="cabinet-block-success text-center text-2xl mb-5 "
                                 style="color: green">{{ t('cabinet.information updated') }}</div>
                        @endif
                        @if (session('personal.count_error'))
                                <style>
                                    .cabinet-form{
                                        display: none!important;
                                    }
                                </style>
                            <div class="cabinet-block-success text-center text-2xl mb-5 d_flex j_content_center f_direction_column a_items_center"
                                 style="color:  #aa2222;text-align: center; min-height: 400px"><span>{{ t('cabinet.count error') }}</span></div>
                        @endif
                        <div class="cabinet-form font-segeo w-full">
                            <form action="{{ route('cabinet.personal') }}" method="post" class="w-full"
                                  autocomplete="off">
                                @csrf
                                <div class="info_card p-2">
                                    <div class="row">
                                        <div
                                            class="col-12 text-center text-3xl mb-5 font-segeo font-bold font-default font-16">{{ t('cabinet.Անձնական տվյալներ') }}
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-12">
                                            <table>
                                                <tr class="font-segeo font-bold font-default font-16 " style="border: 1px solid darkslategrey">
                                                    <td class="p-3 text-center" style="text-align: center; border: 1px solid darkslategrey">{{t('cabinet.Added')}}</td>
                                                    <td class="p-3 text-center" style="text-align: center; border: 1px solid darkslategrey">{{t('cabinet.Can add')}}</td>
                                                    <td class="p-3 text-center" style="text-align: center; border: 1px solid darkslategrey">{{t('cabinet.Active')}}</td>
                                                    <td class="p-3 text-center" style="text-align: center; border: 1px solid darkslategrey">{{t('cabinet.Pending')}}</td>
                                                    <td class="p-3 text-center" style="text-align: center; border: 1px solid darkslategrey">{{t('cabinet.Archive')}}</td>
                                                    <td class="p-3 text-center" style="text-align: center; border: 1px solid darkslategrey">{{t('cabinet.Declined')}}</td>
                                                </tr>
                                                <tr>
                                                    <td class="p-3" style="text-align: center; border: 1px solid darkslategrey">{{ $user->added }}</td>
                                                    <td class="p-3" style="text-align: center; border: 1px solid darkslategrey">{{ (((int)$user->can_add - (int)$user->added) <= 0) ? 0 : ((int)$user->can_add - (int)$user->added)}}</td>
                                                    <td class="p-3" style="text-align: center; border: 1px solid darkslategrey">{{ count($announcements_list['active']) }}</td>
                                                    <td class="p-3" style="text-align: center; border: 1px solid darkslategrey">{{ count($announcements_list['pending']) }}</td>
                                                    <td class="p-3" style="text-align: center; border: 1px solid darkslategrey">{{ count($announcements_list['archive']) }}</td>
                                                    <td class="p-3" style="text-align: center; border: 1px solid darkslategrey">{{ count($announcements_list['declined']) }}</td>
                                                </tr>
                                            </table>

                                        </div>
                                        <div class="col-12 col-lg-6">
                                            <div>
                                                <div class="d_flex j_content_between a_items_ mt-2 f_direction_column">
                                                    <input type="text" id="form-name"
                                                           class="px-2 py-3 border w-full cabinet-input {{($errors->has('name')?'border-red':'border-cardBorder')}}"
                                                           name="name" placeholder="{{ t('cabinet.Անուն Ազգանուն') }}"
                                                           maxlength="255"
                                                           value="{{ old('name',$user->name) }}">
                                                </div>
                                                @if ($errors->has('name'))
                                                    <div class="flex justify-end text-red">
                                                        <span class="err-field">{{ $errors->first('name') }}</span>
                                                    </div>
                                                @endif
                                            </div>
                                            <div>
                                                <div
                                                    class="d_flex j_content_between a_items_center mt-2">
                                                    <input type="text" id="form-address"
                                                           class="px-2 py-3 border w-full cabinet-input {{($errors->has('address')?'border-red':'border-cardBorder')}} "
                                                           name="address" placeholder="{{ t('cabinet.Հասցե') }}"
                                                           maxlength="255" value="{{ old('address', $user->address) }}">
                                                </div>
                                                @if ($errors->has('address'))
                                                    <div class="flex justify-end text-red">
                                                        <span class="err-field">{{ $errors->first('address') }}</span>
                                                    </div>
                                                @endif
                                            </div>
                                            <div>
                                                <div
                                                    class="d_flex j_content_between a_items_center mt-2">
                                                    <input type="text" id="form-phone"
                                                           class="px-2 py-3 border w-full cabinet-input {{($errors->has('phone')?'border-red':'border-cardBorder')}}"
                                                           name="phone" placeholder="{{ t('cabinet.Հեռ․') }}"
                                                           maxlength="255"
                                                           value="{{ old('phone', $user->phone) }}">
                                                </div>
                                                @if ($errors->has('phone'))
                                                    <div class="flex justify-end text-red">
                                                        <span class="err-field">{{ $errors->first('phone') }}</span>
                                                    </div>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-12 col-lg-6">
                                            <div>
                                                <div
                                                    class="d_flex j_content_between a_items_center mt-2">
                                                    <input type="text" id="form-address"
                                                           class="px-2 py-3 border w-full cabinet-input {{($errors->has('phone_2')?'border-red':'border-cardBorder')}} "
                                                           name="phone_2" placeholder="{{ t('cabinet.Հեռ․') }}"
                                                           maxlength="255" value="{{ old('phone_2', $user->phone_2) }}">
                                                </div>
                                                @if ($errors->has('phone_2'))
                                                    <div class="flex justify-end text-red">
                                                        <span class="err-field">{{ $errors->first('phone_2') }}</span>
                                                    </div>
                                                @endif
                                            </div>
                                            <div>
                                                <div
                                                    class="d_flex j_content_between a_items_center mt-2">
                                                    <input type="text" id="form-address"
                                                           class="px-2 py-3  border w-full cabinet-input {{($errors->has('phone_3')?'border-red':'border-cardBorder')}} "
                                                           name="phone_3" placeholder="{{ t('cabinet.Հեռ․') }}"
                                                           maxlength="255" value="{{ old('phone_3', $user->phone_3) }}">
                                                </div>
                                                @if ($errors->has('phone_3'))
                                                    <div class="flex justify-end text-red">
                                                        <span class="err-field">{{ $errors->first('phone_3') }}</span>
                                                    </div>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-12 col-lg-6 mt-2">
                                            <div class="d_flex j_content_start a_items_center">
                                                <svg id="Group_5546" data-name="Group 5546"
                                                     xmlns="http://www.w3.org/2000/svg" width="32" height="32"
                                                     viewBox="0 0 32 32">
                                                    <rect id="Rectangle_1930" data-name="Rectangle 1930" width="32"
                                                          height="32" fill="#7f40bd"/>
                                                    <g id="_001-viber" data-name="001-viber"
                                                       transform="translate(6.857 6.857)">
                                                        <path id="Path_11317" data-name="Path 11317"
                                                              d="M269.406,70.718a5.577,5.577,0,0,0-4.937-5.314c-.055-.006-.114-.016-.177-.027a2.565,2.565,0,0,0-.418-.046.7.7,0,0,0-.752.631.729.729,0,0,0,.109.572,1.081,1.081,0,0,0,.777.334c.077.011.15.021.211.034,2.466.551,3.3,1.417,3.7,3.862.01.06.014.133.019.211.018.292.055.9.708.9h0a1.158,1.158,0,0,0,.174-.014c.609-.092.59-.648.58-.915a1.351,1.351,0,0,1,0-.194A.267.267,0,0,0,269.406,70.718Z"
                                                              transform="translate(-253.871 -62.979)" fill="#fcfcfc"/>
                                                        <path id="Path_11318" data-name="Path 11318"
                                                              d="M256.006,1.471c.073.005.142.01.2.019,4.049.623,5.911,2.541,6.427,6.621a2.4,2.4,0,0,1,.012.244c.005.319.016.982.728,1h.022a.7.7,0,0,0,.528-.2,1.115,1.115,0,0,0,.194-.853c0-.068-.006-.132-.005-.188a8.252,8.252,0,0,0-7.73-8.1.29.29,0,0,0-.05,0,.35.35,0,0,1-.049,0c-.042,0-.093,0-.147-.007s-.14-.01-.215-.01a.733.733,0,0,0-.806.753C255.074,1.4,255.7,1.449,256.006,1.471Z"
                                                              transform="translate(-246.162)" fill="#fcfcfc"/>
                                                        <path id="Path_11319" data-name="Path 11319"
                                                              d="M22.983,37.948c-.086-.066-.175-.134-.258-.2-.442-.356-.912-.684-1.367-1l-.283-.2a2.822,2.822,0,0,0-1.6-.608,2.075,2.075,0,0,0-1.727,1.094.918.918,0,0,1-.788.478,1.565,1.565,0,0,1-.634-.159,7.638,7.638,0,0,1-3.991-3.886c-.371-.834-.251-1.379.4-1.823a1.845,1.845,0,0,0,1.013-1.618c-.055-1.019-2.3-4.086-3.252-4.435a1.846,1.846,0,0,0-1.255,0,3.633,3.633,0,0,0-2.261,1.86,3.457,3.457,0,0,0,.049,2.79,22.511,22.511,0,0,0,4.94,7.52,23.945,23.945,0,0,0,7.494,4.976,4.15,4.15,0,0,0,.741.22c.069.015.128.029.171.04a.287.287,0,0,0,.073.01h.023A4.251,4.251,0,0,0,24,40.319C24.445,39.066,23.635,38.446,22.983,37.948Z"
                                                              transform="translate(-6.709 -24.563)" fill="#fcfcfc"/>
                                                        <path id="Path_11320" data-name="Path 11320"
                                                              d="M271.411,132.859a.852.852,0,0,0-.9.517.692.692,0,0,0,.029.6.9.9,0,0,0,.679.359,1.605,1.605,0,0,1,1.49,1.6.759.759,0,0,0,.723.749h0a.709.709,0,0,0,.086-.005c.459-.055.681-.392.661-1a2.861,2.861,0,0,0-.893-1.935A2.664,2.664,0,0,0,271.411,132.859Z"
                                                              transform="translate(-260.96 -128.072)" fill="#fcfcfc"/>
                                                    </g>
                                                </svg>
                                                <p class="font-16 font-default font-segeo font-bold ml-2">{{ t('cabinet.Viber') }}</p>
                                            </div>
                                            <div>
                                                <div
                                                    class="d_flex j_content_between a_items_center mt-2">
                                                    <input type="text" id="form-address"
                                                           class="px-2 py-3 border w-full cabinet-input {{($errors->has('viber')?'border-red':'border-cardBorder')}} "
                                                           name="viber" placeholder="{{ t('cabinet.Viber') }}"
                                                           maxlength="255" value="{{ old('viber', $user->viber) }}">
                                                </div>
                                                @if ($errors->has('viber'))
                                                    <div class="flex justify-end text-red">
                                                        <span class="err-field">{{ $errors->first('viber') }}</span>
                                                    </div>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-12 col-lg-6">
                                            <div class="d_flex j_content_start a_items_center mt-2">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="32" height="32"
                                                     viewBox="0 0 32 32">
                                                    <g id="Group_5545" data-name="Group 5545"
                                                       transform="translate(0.429)">
                                                        <rect id="Rectangle_1931" data-name="Rectangle 1931" width="32"
                                                              height="32" transform="translate(-0.429)" fill="#4caf50"/>
                                                        <g id="Group_5544" data-name="Group 5544"
                                                           transform="translate(6.857 6.857)">
                                                            <path id="Path_11321" data-name="Path 11321"
                                                                  d="M15.634,2.628A9.251,9.251,0,0,0,9.177,0,9.076,9.076,0,0,0,1.292,13.591L0,18.286l4.827-1.259a9.571,9.571,0,0,0,4.355,1.1A9.074,9.074,0,0,0,15.623,2.658Z"
                                                                  transform="translate(0)" fill="#f8f8f8"/>
                                                            <path id="Path_11322" data-name="Path 11322"
                                                                  d="M9.7,17.04H9.684a7.677,7.677,0,0,1-4.126-1.214L2.7,16.568l.766-2.777L3.285,13.5A7.552,7.552,0,1,1,9.7,17.04Z"
                                                                  transform="translate(-0.507 -0.468)" fill="#4caf50"/>
                                                            <path id="Path_11323" data-name="Path 11323"
                                                                  d="M14.762,12.413l-.007.057c-.229-.114-1.346-.661-1.554-.736-.467-.173-.335-.027-1.232,1-.133.149-.266.16-.492.057A6.184,6.184,0,0,1,9.645,11.66a6.912,6.912,0,0,1-1.265-1.577c-.223-.386.244-.44.669-1.245a.419.419,0,0,0-.019-.4c-.057-.114-.512-1.234-.7-1.681s-.371-.389-.512-.389a1.134,1.134,0,0,0-1.042.262c-1.23,1.352-.92,2.746.133,4.229,2.068,2.706,3.17,3.2,5.184,3.9a3.152,3.152,0,0,0,1.432.092,2.345,2.345,0,0,0,1.536-1.086,1.886,1.886,0,0,0,.137-1.086c-.056-.1-.206-.16-.434-.263Z"
                                                                  transform="translate(-1.423 -1.512)" fill="#fafafa"/>
                                                        </g>
                                                    </g>
                                                </svg>
                                                <p class="font-16 font-default font-segeo font-bold ml-2">{{ t('cabinet.WhatsApp') }}</p>
                                            </div>
                                            <div>
                                                <div
                                                    class="d_flex j_content_between a_items_center mt-2">
                                                    <input type="text" id="form-address"
                                                           class="px-2 py-3 border w-full cabinet-input {{($errors->has('whatsapp')?'border-red':'border-cardBorder')}} "
                                                           name="whatsapp" placeholder="{{ t('cabinet.WhatsApp') }}"
                                                           maxlength="255"
                                                           value="{{ old('whatsapp', $user->whatsapp) }}">
                                                </div>
                                                @if ($errors->has('whatsapp'))
                                                    <div class="flex justify-end text-red">
                                                        <span class="err-field">{{ $errors->first('whatsapp') }}</span>
                                                    </div>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @if(!auth()->user()->from_social)
                                <div class="info_card p-2">
                                    <div class="row">
                                        <div
                                            class="col-12 text-center text-3xl mb-5 font-segeo font-bold font-default font-16">{{ t('cabinet.Փոփոխել գաղտնաբառը') }}
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-12 col-lg-6">
                                            <div>
                                                <div class="d_flex j_content_between a_items_ mt-2 f_direction_column">
                                                    <input type="password" id="form-name"
                                                           class="px-2 py-3 border w-full cabinet-input {{($errors->has('current_password')?'border-red':'border-cardBorder')}}"
                                                           name="current_password" placeholder="{{ t('cabinet.Ընթացիկ գաղտնաբառ') }}"
                                                           maxlength="255"
                                                           >
                                                </div>
                                                @if ($errors->has('current_password'))
                                                    <div class="flex justify-end text-red">
                                                        <span class="err-field">{{ $errors->first('current_password') }}</span>
                                                    </div>
                                                @endif
                                            </div>
                                            <div>
                                                <div
                                                    class="d_flex j_content_between a_items_center mt-2">
                                                    <input type="password" id="form-address"
                                                           class="px-2 py-3 border w-full cabinet-input {{($errors->has('address')?'border-red':'border-cardBorder')}} "
                                                           name="password" placeholder="{{ t('cabinet.Նոր գաղտնաբառ') }}"
                                                           maxlength="255">
                                                </div>
                                                @if ($errors->has('password'))
                                                    <div class="flex justify-end text-red">
                                                        <span class="err-field">{{ $errors->first('password') }}</span>
                                                    </div>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-12 col-lg-6">
                                            <div>
                                                <div
                                                    class="d_flex j_content_between a_items_center mt-2">
                                                    <input type="password" id="form-address"
                                                           class="px-2 py-3 border w-full cabinet-input {{($errors->has('phone_2')?'border-red':'border-cardBorder')}} "
                                                           name="password_confirmation" placeholder="{{ t('cabinet.Կրկնել գաղտնաբառը․') }}"
                                                           maxlength="255">
                                                </div>
                                                @if ($errors->has('password_confirmation'))
                                                    <div class="flex justify-end text-red">
                                                        <span class="err-field">{{ $errors->first('password_confirmation') }}</span>
                                                    </div>
                                                @endif
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                @endif
                                <div class="send_button d_flex j_content_end a_items_center md:justify-end">
                                    <button style="margin-top: 25px" type="submit"
                                            class="bg-red rounded px-4 py-2 text-white"><i class="far fa-save"></i> {{ t('cabinet.Պահպանել') }}</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @push('css')
        <style>
            .send_button button {
                font: 13px "Segoe UI", HelveticaNeueCyr-Bold, Helvetica, Roboto, sans-serif;
                border: 2px solid #FE980F;
                transition: .3s;
                background: #FE980F;
                color: #fff;
            }

            .send_button button:hover {
                transition: .3s;
                background: #ffffff;
                color: #FE980F;
            }
            .border-red {
                border: 1px solid darkred;
            }

            .err-field {
                color: #aa2222;
                font-size: 13px;
            }

            .cabinet-input {
                border: 1px solid #D5CFCF;
                border-radius: 5px;
            }

            .info_card {
                box-shadow: 0px 3px 4px rgba(0, 95, 162, .16);
                margin-bottom: 15px;

            }

            .info_card:hover {
                box-shadow: 0px 5px 20px rgba(0, 95, 162, .16);
            }

        </style>
    @endpush
    @push('js')
        <script>
            {{--url='{!! route("cabinet.sms.verification") !!}'--}}
            $(document).on('click', '#send_sms', function () {
                var phone = $('#form-phone').val()
                $.ajax({
                    headers: {

                        'X-CSRF-TOKEN': token
                    },
                    type: "POST",
                    url: url,
                    data: {
                        'phone': phone,
                    },

                }).done(function (response) {

                    $.each(response.message, function (key, value) {
                        $('body').append('<div class="' + key + '"><p>' + value + '</p></div>')
                    });
                    if (typeof response.append !== 'undefined') {
                        $('#append_send_code').html('');
                        $('#append_send_code').append('<div class="flex justify-between items-center mt-2 mt-2 flex-col md:flex-row"> <label for="accept_code" class="login-form-label"></label> <input type="text" id="accept_code" class="p-2 border "  style="width: 400px;" name="accept_code" placeholder="Введите код" maxlength="255" value=""> </div> <div class="flex justify-center md:justify-end" > <a id="send_code" href="javascript:void(0)"   class="bg-red rounded mt-2 px-4 py-2 text-white">Отправить код</a> </div>\n')

                    }

                });
            })

            {{--            var checkUrl='{!! route('cabinet.sms.check') !!}'--}}

            $(document).on('click', '#send_code', function () {
                var code = $('#accept_code').val()
                $.ajax({
                    headers: {

                        'X-CSRF-TOKEN': token
                    },
                    type: "POST",
                    url: checkUrl,
                    data: {
                        'code': code,
                    },

                }).done(function (response) {
                    $.each(response.message, function (key, value) {
                        $('body').append('<div class="' + key + '"><p>' + value + '</p></div>')
                    });
                    console.log(typeof response.remove !== 'undefined')
                    if (typeof response.remove !== 'undefined') {
                        $(document).find('#append_send_code').remove();
                        $(document).find('#send_sms').remove();
                    }

                });
            })
        </script>
    @endpush
@endsection






