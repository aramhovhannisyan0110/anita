@extends('site.layouts.header')
@section('content')
    <div class="login"><p class="container">{{t('login.Մուտք')}}</p></div>
    <section class="d_flex justify-content-center container a_items_center f_direction_column">
        <div class="login_text">
            {{t('login.Մուտք սոցիալական ցանցերի միջոցով')}}
        </div>
        <div class="socials d_flex justify-content-center  a_items_center">

            <ul class="d_flex">
                <li><a href="{{route('cabinet.facebook.login')}}">
                        <svg xmlns="http://www.w3.org/2000/svg" width="14.807" height="28.689"
                             viewBox="0 0 14.807 28.689">
                            <path id="Path_11329" data-name="Path 11329"
                                  d="M159.065,71.453v4.441s-5.187-.74-5.187,2.549v3.289h4.685l-.585,5.016h-4.1V99.822h-5.187V86.747l-4.434-.082V81.731h4.35V77.949s-.287-5.627,5.354-6.66A15.328,15.328,0,0,1,159.065,71.453Zm0,0"
                                  transform="translate(-144.258 -71.133)" fill="#4267b2"/>
                        </svg>
                    </a></li>
                <li><a href="{{route('cabinet.gmail.login')}}">
                        <svg xmlns="http://www.w3.org/2000/svg" width="33.418" height="33.418"
                             viewBox="0 0 43.418 43.418">
                            <g id="_001-google" data-name="001-google" transform="translate(0 0)">
                                <path id="Path_11340" data-name="Path 11340"
                                      d="M68.989,308.478A21.468,21.468,0,0,1,36.4,302.957l1.41-6.5,6.248-1.16a11.529,11.529,0,0,0,10.965,8.115,11.284,11.284,0,0,0,6.716-2.188l6,.916Z"
                                      transform="translate(-33.313 -270.258)" fill="#59c36a"/>
                                <path id="Path_11341" data-name="Path 11341"
                                      d="M269.967,372.45l-1.247-6.335-6-.916A11.284,11.284,0,0,1,256,367.388v10.261A21.676,21.676,0,0,0,269.967,372.45Z"
                                      transform="translate(-234.291 -334.231)" fill="#00a66c"/>
                                <g id="Connected_Home_1_" transform="translate(0 10.719)">
                                    <g id="Group_5648" data-name="Group 5648">
                                        <g id="Group_5647" data-name="Group 5647">
                                            <g id="Group_5646" data-name="Group 5646">
                                                <path id="Path_11342" data-name="Path 11342"
                                                      d="M10.261,137.39a11.856,11.856,0,0,0,.483,3.333L3.087,148.38a21.108,21.108,0,0,1,0-21.98l6.146,1.058,1.512,6.6A11.854,11.854,0,0,0,10.261,137.39Z"
                                                      transform="translate(0 -126.4)" fill="#ffda2d"/>
                                            </g>
                                        </g>
                                    </g>
                                </g>
                                <path id="Path_11343" data-name="Path 11343"
                                      d="M263.981,201.088a21.675,21.675,0,0,1-7.742,16.511l-7.25-7.25a11.257,11.257,0,0,0,3.536-4.172H242.272A1.26,1.26,0,0,1,241,204.9v-7.632A1.26,1.26,0,0,1,242.272,196h20.081a1.257,1.257,0,0,1,1.247,1.043A21.572,21.572,0,0,1,263.981,201.088Z"
                                      transform="translate(-220.563 -179.379)" fill="#4086f4"/>
                                <path id="Path_11344" data-name="Path 11344"
                                      d="M266.252,206.176a11.258,11.258,0,0,1-3.536,4.172l7.25,7.25a21.5,21.5,0,0,0,7.361-20.556A1.257,1.257,0,0,0,276.081,196H256v10.176Z"
                                      transform="translate(-234.291 -179.379)" fill="#4175df"/>
                                <path id="Path_11345" data-name="Path 11345"
                                      d="M69.345,6.063A1.284,1.284,0,0,1,68.989,7l-5.444,5.419a1.246,1.246,0,0,1-1.654.127,11.379,11.379,0,0,0-6.869-2.29,11.529,11.529,0,0,0-10.965,8.115L36.4,10.719a21.454,21.454,0,0,1,32.487-5.6,1.338,1.338,0,0,1,.458.941Z"
                                      transform="translate(-33.313)" fill="#ff641a"/>
                                <path id="Path_11346" data-name="Path 11346"
                                      d="M262.869,12.55a1.246,1.246,0,0,0,1.654-.127L269.967,7a1.283,1.283,0,0,0,.356-.941,1.338,1.338,0,0,0-.458-.941A21.644,21.644,0,0,0,256,0V10.261A11.379,11.379,0,0,1,262.869,12.55Z"
                                      transform="translate(-234.291 0)" fill="#f03800"/>
                            </g>
                        </svg>
                    </a></li>
                <li><a href="{{route('cabinet.mailru.login')}}">
                        <svg xmlns="http://www.w3.org/2000/svg" width="30.664" height="30.664"
                             viewBox="0 0 36.664 36.664">
                            <path id="Path_11351" data-name="Path 11351"
                                  d="M158,268.664c-2.213,0-3.333-1.667-3.333-5a8.433,8.433,0,1,0-2.03,5.39A5.943,5.943,0,0,0,158,272c6.666,0,6.666-6.273,6.666-8.333a18.332,18.332,0,1,0-5.586,13.176,1.672,1.672,0,1,0-2.333-2.4h0a15,15,0,1,1,4.586-10.779C161.331,267.4,160.491,268.664,158,268.664Zm-11.666,0a5,5,0,1,1,5-5A5,5,0,0,1,146.332,268.664Z"
                                  transform="translate(-128 -245.332)" fill="#365ff5"/>
                        </svg>
                    </a></li>
            </ul>
        </div>
        <div class="login_text">
            {{t('login.Կամ օգտագործեք ձեր էլ փոստի Հասցեն')}}
        </div>
        <form action="{{route('login.post')}}" method="post">
            @csrf
        <div class="login_container">
                <input placeholder="{{t('login.Էլ հասցե՝ ')}}" id="email" type="email" name="email">
                @if($errors->has('email'))
                    <p class="validation_error">{{ $errors->first('email') }}</p>
                @endif

                <input placeholder="{{t('login.Գաղտնաբառ՝ ')}}" id="password" type="password" name="password">
                @if($errors->has('password'))
                    <p class="validation_error">{{ $errors->first('password') }}</p>
                @endif
          <button type="submit">
              <svg style="margin-right: 10px" xmlns="http://www.w3.org/2000/svg" width="18.251" height="18.251" viewBox="0 0 18.251 18.251">
                  <g id="Group_5669" data-name="Group 5669" transform="translate(-910 -590.154)">
                      <g id="Group_5668" data-name="Group 5668">
                          <g id="enter" transform="translate(910 590.154)">
                              <path id="Path_11219" data-name="Path 11219" d="M181.179.011c-.019,0-.035-.011-.055-.011h-8.175a2.284,2.284,0,0,0-2.281,2.281v.76a.76.76,0,1,0,1.521,0v-.76a.762.762,0,0,1,.761-.76h3.543l-.232.078a1.529,1.529,0,0,0-1.03,1.443V14.449h-2.281a.762.762,0,0,1-.761-.76V12.167a.76.76,0,1,0-1.521,0v1.521a2.284,2.284,0,0,0,2.281,2.281h2.281v.76a1.522,1.522,0,0,0,1.521,1.521,1.6,1.6,0,0,0,.484-.075l4.569-1.523a1.528,1.528,0,0,0,1.03-1.443V1.521a1.524,1.524,0,0,0-1.656-1.51Zm0,0" transform="translate(-164.584)" fill="#fcfbfb"/>
                              <path id="Path_11220" data-name="Path 11220" d="M8.142,109.927,5.1,106.886a.76.76,0,0,0-1.3.538V109.7H.76a.76.76,0,0,0,0,1.521H3.8v2.281a.76.76,0,0,0,1.3.538L8.142,111A.759.759,0,0,0,8.142,109.927Zm0,0" transform="translate(0 -102.861)" fill="#fcfbfb"/>
                          </g>
                      </g>
                  </g>
              </svg>

              <span>
                  {{t('login.Մուտք')}}
              </span></button>
            @if($errors->has('global'))
                <p class="validation_error">{{ $errors->first('global') }}</p>
            @endif

            <div class="reset_container">
                <a class="reset_href" href="">{{t('login.Մոռացել եք գաղտնաբառը ?')}}</a>
                <a class="register_href" href="{{route('register')}}">{{t('login.Գրանցվել')}}</a>
            </div>
        </div>
        </form>

    </section>
    @include('site.layouts.footer')
@endsection
@push('css')
    <style>
        body {
            background-color: #f8f6f6 !important;
        }
        .register_href{
            color: black;font-size: 20px;font-weight: bold;
        }
        .reset_href{
            color: #5B5B5B;
        }
        .reset_container{
            display: flex;
            justify-content: space-between;
            flex-wrap: wrap;
            margin-top: 20px;
        }
.login_container{
    display: flex;
    flex-direction: column;
}
.login_container button{
    width: 100%;
    display: flex;
    justify-content: center;
    align-items: center;
    padding:10px 15px;
    background: #FE980F;
    color: white;
    margin-top: 15px;
}
.validation_error{
    color: red;
    margin: 5px 0;
}
.login_container input {
    margin-top: 15px;
    background: white;
    padding:10px;
    border-radius: 5px;
    border: 1px solid #D5CFCF;
    min-width: 350px;
    max-width: 100vw;
}
        .login_text {
            font: 16px "Segoe UI", HelveticaNeueCyr-Bold, Helvetica, Roboto, sans-serif;
            font-style: normal;
            font-weight: 400;
            color: #042A3D;
            margin: 15px 0 ;

        }

        .socials ul > li {
            background-color: white;
            box-sizing: border-box;
            box-shadow: 0 0 2px 3px #77889924;
            border-radius: 50%;
            display: flex;
            justify-content: center;
            align-items: center;
            height: 50px;
            width: 50px;
            margin: 5px;
            padding: 7px;
        }

        .socials ul > li > a {
            display: flex;
            justify-content: center;
            align-items: center;
        }

        .login {
            margin-top: 3px;
            font: 16px "Segoe UI", HelveticaNeueCyr-Bold, Helvetica, Roboto, sans-serif;
            font-style: normal;
            font-weight: 600;
            color: #042A3D;
            background: white;
        }
    </style>
@endpush
