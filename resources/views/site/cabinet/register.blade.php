@extends('site.layouts.header')
@section('content')
{{--    @dd($errors)--}}
    <div class="login"><p class="container">{{t('login.Գրանցում')}}</p></div>
    <section class="d_flex justify-content-center container a_items_center f_direction_column">
        <form action="{{route('register.post')}}" method="post">
            <div class="login_text d_flex">
                <div class="user_type d_flex a_items_center">
                    <input type="radio" name="user_type" {{ (empty(old('user_type')) || old('user_type') == '1')?'checked':'' }}  value="1" class="criteria filter_check_inp"
                           id="check-2" data-id="1">
                    <label class="cursor-pointer filter_check_label" for="check-2"
                           data-content="{{t('register.Անհատ')}}">{{t('register.Անհատ')}} </label>
                </div>
                <div class="user_type d_flex a_items_center">
                    <input type="radio" name="user_type" {{ (!empty(old('user_type')) && old('user_type') == '2')?'checked':'' }} value="2" class="criteria filter_check_inp" id="check-3"
                           data-id="1">
                    <label class="cursor-pointer filter_check_label" for="check-3"
                           data-content="{{t('register. Գործակալ կամ Կառուցապատող')}}">{{t('register. Գործակալ կամ Կառուցապատող')}} </label>
                </div>
            </div>
            @csrf
            <div class="login_container">
                <input placeholder="{{t('login.Անուն ')}}" id="name" type="text" name="name" value="{{ old('name',null) }}">
                @if($errors->has('name'))
                    <p class="validation_error">{{ $errors->first('name') }}</p>
                @endif
                <input placeholder="{{t('login.Էլ հասցե՝ ')}}" id="email" type="email" name="email" value="{{ old('email',null) }}">
                @if($errors->has('email'))
                    <p class="validation_error">{{ $errors->first('email') }}</p>
                @endif
                <input placeholder="{{t('login.Հեռ՝ ')}}" id="phone" type="text" name="phone" value="{{ old('phone',null) }}">
                @if($errors->has('phone'))
                    <p class="validation_error">{{ $errors->first('phone') }}</p>
                @endif
{{--                <input placeholder="{{t('login.Հասցե՝ ')}}" id="phone" type="text" name="phone" value="{{ old('phone',null) }}">--}}
{{--                @if($errors->has('phone'))--}}
{{--                    <p class="validation_error">{{ $errors->first('phone') }}</p>--}}
{{--                @endif--}}
                <input placeholder="{{t('login.Գաղտնաբառ՝ ')}}" id="password" type="password" name="password">
                @if($errors->has('password'))
                    <p class="validation_error">{{ $errors->first('password') }}</p>
                @endif
                <input placeholder="{{t('login.Կրկնել գաղտնաբառը՝ ')}}" id="password_confirmation" type="password"
                       name="password_confirmation">
                @if($errors->has('password_confirmation'))
                    <p class="validation_error">{{ $errors->first('password_confirmation') }}</p>
                @endif
                <div class="privacy_policy flex items-center justify-end mb-1 flex-wrap">
                    <input type="checkbox" name="accept" class="criteria filter_check_inp" id="check-1" data-id="1">
                    <label class="cursor-pointer filter_check_label" for="check-1"
                           data-content="{{t('register.Համաձայն եմ')}}">{{t('register.Համաձայն եմ')}} </label>
                    <a
                        href="javascript:void(0)"> {{t('register.գաղտնիության և օգտագործման պայմաններին')}}</a>
                </div>
                @if($errors->has('accept'))
                    <p class="validation_error">{{ $errors->first('accept') }}</p>
                @endif
                <button type="submit">
                    <svg id="stamped" xmlns="http://www.w3.org/2000/svg" width="18.818" height="18.818"
                         viewBox="0 0 18.818 18.818">
                        <path id="Path_11221" data-name="Path 11221"
                              d="M18.367,18.449H16.014a.588.588,0,0,1-.588-.588V16.719a1.111,1.111,0,0,0-.349-.746A2.911,2.911,0,0,1,14.3,13.4a2.94,2.94,0,0,1,4.781-1.716,2.932,2.932,0,0,1,1.051,2.253,2.9,2.9,0,0,1-.872,2.078.922.922,0,0,0-.3.634v1.209a.588.588,0,0,1-.588.587Z"
                              transform="translate(-3.077 -2.375)" fill="#fcfbfb"/>
                        <path id="Path_11222" data-name="Path 11222"
                              d="M20.821,22.974H12.588A.588.588,0,0,1,12,22.386V20.922a1.374,1.374,0,0,1,1.2-1.361l2.45-.307,2.033,0,2.522.311a1.374,1.374,0,0,1,1.2,1.362v1.464a.588.588,0,0,1-.588.588Z"
                              transform="translate(-2.591 -4.156)" fill="#fcfbfb"/>
                        <path id="Path_11223" data-name="Path 11223"
                              d="M10.469,1.568H9.606a2.161,2.161,0,0,0-4.156,0H4.588A.592.592,0,0,0,4,2.156V3.332A1.379,1.379,0,0,0,5.372,4.7H9.685a1.379,1.379,0,0,0,1.372-1.372V2.156A.592.592,0,0,0,10.469,1.568Z"
                              transform="translate(-0.864)" fill="#fcfbfb"/>
                        <path id="Path_11224" data-name="Path 11224"
                              d="M11.173,3h-.2v.98A2.155,2.155,0,0,1,8.821,6.136H4.509A2.155,2.155,0,0,1,2.352,3.98V3h-.2A2.155,2.155,0,0,0,0,5.156v9.8a2.155,2.155,0,0,0,2.156,2.156h5.7a2.946,2.946,0,0,1,2.391-2.588,3.628,3.628,0,0,1-.282-.549H2.94a.588.588,0,1,1,0-1.176h6.7a4.483,4.483,0,0,1,.024-1.372H2.94a.588.588,0,0,1,0-1.176h7.12a4.549,4.549,0,0,1,3.246-2.478h.024V5.156A2.155,2.155,0,0,0,11.173,3Zm-.784,5.881H2.94a.588.588,0,0,1,0-1.176h7.449a.588.588,0,1,1,0,1.176Z"
                              transform="translate(0 -0.648)" fill="#fcfbfb"/>
                    </svg>
                    <span>
                    {{t('register.Գրանցում')}}
                    </span>
                </button>

                @if($errors->has('global'))
                    <p class="validation_error">{{ $errors->first('global') }}</p>
                @endif

                {{--                <div class="reset_container">--}}
                {{--                    <a class="reset_href" href="">{{t('login.Մոռացել եք գաղտնաբառը ?')}}</a>--}}
                {{--                    <a class="register_href" href="{{route('register')}}">{{t('login.Գրանցվել')}}</a>--}}
                {{--                </div>--}}
            </div>
        </form>

    </section>
    @include('site.layouts.footer')
@endsection
@push('css')
    <style>
        body {
            background-color: #f8f6f6 !important;
        }

        .register_href {
            color: black;
            font-size: 20px;
            font-weight: bold;
        }

        .reset_href {
            color: #5B5B5B;
        }

        .reset_container {
            display: flex;
            justify-content: space-between;
            flex-wrap: wrap;
            margin-top: 20px;
        }

        .login_container {
            display: flex;
            flex-direction: column;
        }

        .login_container button {
            width: 100%;
            display: flex;
            justify-content: center;
            align-items: center;
            padding: 10px 15px;
            background: #FE980F;
            color: white;
            margin-top: 15px;
        }

        .validation_error {
            color: #ef4743;
            margin: 5px 0;
            font: 12px "Segoe UI",HelveticaNeueCyr-Bold,Helvetica,Roboto,sans-serif;
        }

        .login_container input {
            margin-top: 15px;
            background: white;
            padding: 10px;
            border-radius: 5px;
            border: 1px solid #D5CFCF;
            min-width: 350px;
            max-width: 100vw;
        }

        .login_text {
            font: 16px "Segoe UI", HelveticaNeueCyr-Bold, Helvetica, Roboto, sans-serif;
            font-style: normal;
            font-weight: 400;
            color: #042A3D;
            margin: 15px 0;

        }

        .socials ul > li {
            background-color: white;
            box-sizing: border-box;
            box-shadow: 0 0 2px 3px #77889924;
            border-radius: 50%;
            display: flex;
            justify-content: center;
            align-items: center;
            height: 50px;
            width: 50px;
            margin: 5px;
            padding: 7px;
        }

        .socials ul > li > a {
            display: flex;
            justify-content: center;
            align-items: center;
        }

        .login {
            margin-top: 3px;
            font: 16px "Segoe UI", HelveticaNeueCyr-Bold, Helvetica, Roboto, sans-serif;
            font-style: normal;
            font-weight: 600;
            color: #042A3D;
            background: white;
        }

        .filter_check_label {
            position: relative;
            cursor: pointer;
            font-size: 14px;
            padding: 0 0.25em 0;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
        }

        .filter_check_inp:checked {
            color: #fff;
            border-color: #FE980F;
            background: #FE980F;
        }

        .filter_check_inp::before {
            position: absolute;
            content: "";
            display: block;
            top: 2px;
            left: 5px;
            width: 6px;
            height: 10px;
            border-style: solid;
            border-color: #fff;
            border-width: 0 2px 2px 0;
            -webkit-transform: rotate(45deg);
            transform: rotate(45deg);
            opacity: 0;
        }

        .filter_check_inp:checked ~ label::before {
            -webkit-clip-path: polygon(0 0, 100% 0, 100% 100%, 0 100%);
            clip-path: polygon(0 0, 100% 0, 100% 100%, 0 100%);
        }

        .filter_check_label::before {
            position: absolute;
            content: attr(data-content);
            color: #FE980F;
            -webkit-clip-path: polygon(0 0, 0 0, 0% 100%, 0 100%);
            clip-path: polygon(0 0, 0 0, 0% 100%, 0 100%);
            -webkit-transition: -webkit-clip-path 200ms cubic-bezier(0.25, 0.46, 0.45, 0.94);
            transition: -webkit-clip-path 200ms cubic-bezier(0.25, 0.46, 0.45, 0.94);
            transition: clip-path 200ms cubic-bezier(0.25, 0.46, 0.45, 0.94);
            transition: clip-path 200ms cubic-bezier(0.25, 0.46, 0.45, 0.94), -webkit-clip-path 200ms cubic-bezier(0.25, 0.46, 0.45, 0.94);
        }

        .filter_check_inp:checked::before {
            opacity: 1;
        }

        .filter_check_inp::before {
            position: absolute;
            content: "";
            display: block;
            top: 1px;
            left: 4px;
            width: 4px;
            height: 7px;
            border-style: solid;
            border-color: #fff;
            border-width: 0 2px 2px 0;
            -webkit-transform: rotate(45deg);
            transform: rotate(45deg);
            opacity: 0;
        }

        .filter_check_inp {
            position: relative;
            padding: 0 !important;
            width: 15px;
            height: 15px;
            color: #363839;
            border: 1px solid #bdc1c6;
            -webkit-appearance: none;
            -moz-appearance: none;
            appearance: none;
            outline: 0;
            cursor: pointer;
            margin-top: 0 !important;
            -webkit-transition: background 175ms cubic-bezier(0.1, 0.1, 0.25, 1);
            transition: background 175ms cubic-bezier(0.1, 0.1, 0.25, 1);
            min-width: unset !important;
            border-radius: 0 !important;
        }

        .privacy_policy {
            margin-top: 15px;
            display: flex;
            align-items: center;
            justify-content: flex-end;
        }

        .privacy_policy label {
            font-size: 13px;
        }

        .privacy_policy a {
            font-size: 13px;
            text-decoration: underline;
        }

        .user_type .filter_check_inp {
            border-radius: 50% !important;
        }

        .user_type .filter_check_inp {
            width: 14px;
            height: 14px;
        }

        .user_type .filter_check_inp::before {
            top: 1px;
            left: 1px;
            width: 6px;
            height: 6px;
            border-style: solid;
            border-color: #fff;
            border-radius: 50%;
            background: #cc8202;
            border-width: 2px;
        }

        .user_type .filter_check_inp:checked {
            border-color: #cc8202;
            background: #cc8202;
        }

        .user_type .filter_check_label::before {
            color: #cc8202;
        }
    </style>
@endpush
