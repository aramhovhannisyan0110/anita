@extends('site.layouts.header')
@section('content')
    <div class="main w-full">
        <div class="w-full bg-white">
            <div class="container">
                <div class="login row"><p class="col-12 py-1">{{t('cabinet.Ընտրեք օգտատիրոջ տեսակը')}}</p></div>
            </div>
        </div>
        <div class="container mt-4">
            <div class="row">
                <div class="col-12 col-md-5 col-lg-4 col-xl-3">
                    @component('site.components.cabinet_link',['active'=>$active??null])@endcomponent
                </div>
                <div class="col-12 col-md-7 col-lg-8 col-xl-9">
                    <div class="container-fluid">
                        <div class="row">
                            @if(!empty($count_packages) && count($count_packages))
                                @foreach($count_packages as $package)
                                    <div class="col-12 col-md-6 col-lg-4 col-xl-3 mb-6">
                                        <div class="shadowed bg-white p-2 ">
                                            <div class="font-segeo">
                                                <p class="font-10 font-gray">{{t('cabinet.Հայտարարությունների քանակ')}}</p>
                                                <p class="font-default font-22 font-bolder">{{$package->count}}</p>
                                            </div>
                                            <div class="font-segeo">
                                                <p class="font-10 font-gray">{{t('cabinet.Արժեք')}}</p>
                                                <p class="font-default font-22 font-bolder">{{$package->price}}<sup
                                                        class="font-13 font-logo-blue font-bold ml-1"
                                                        style="vertical-align: super">Դ</sup></p>
                                            </div>
                                            <div>
                                                <a href="#ex{{$package->id}}" rel="modal:open" class="w-full mt-2">
                                                    <button
                                                        class="w-full py-2 bg-default font-white font-bold">{{t('cabinet.Ձեռք բերել փաթեթը')}}</button>
                                                </a>
                                                <div id="ex{{$package->id}}" class="modal" style="height: auto">
                                                    <div class="mt-3">
                                                        <p class="font-default font-bolder font-segeo font-16">
                                                            {{ t('cabinet.Փաթեթի գնում') }}
                                                        </p>
                                                        <p class="font-logo-blue font-bold font-segeo font-13 py-2">
                                                            {!! $package->desc !!}
                                                        </p>
                                                    </div>
                                                    <div class="font-segeo">
                                                        <p class="font-13 font-gray">{{t('cabinet.Հայտարարությունների քանակ')}}</p>
                                                        <p class="font-default font-22 font-bolder">{{$package->count}}</p>
                                                    </div>
                                                    <div class="font-segeo">
                                                        <p class="font-13 font-gray">{{t('cabinet.Արժեք')}}</p>
                                                        <p class="font-default font-22 font-bolder">{{$package->price}}<sup
                                                                class="font-13 font-logo-blue font-bold ml-1"
                                                                style="vertical-align: super">Դ</sup></p>
                                                    </div>
                                                    <form action="{{ route('cabinet.buy_package') }}">
                                                        @csrf
                                                        <input type="hidden" name="package_id" value="{{$package->id}}">
                                                        <input type="hidden" name="price" value="{{$package->price}}">
                                                        <input type="hidden" name="count" value="{{$package->count}}">
                                                        <button type="submit" class="w-full py-2 bg-default font-white font-bold mt-2">{{t('cabinet.Ձեռք բերել փաթեթը')}}</button>
                                                    </form>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @if ($errors->has('global'))
        <div class="error-notify">
            {{ t('auth.oop`s') }}
        </div>
    @elseif(session('payment') == 'success')
        <div class="success-notify">
            {{ t('payment.success') }}
        </div>
    @endif
@endsection
@push('css')
    @css(aSite('css/jquery.modal.min.css'))
@endpush
@push('js')
    @js(aSite('js/jquery.modal.min.js'))
@endpush
