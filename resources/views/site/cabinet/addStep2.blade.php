@extends('site.layouts.header')
@section('content')
    <div class="main w-full">
        <div class="w-full bg-white">
            <div class="container">
                <div class="login row"><p class="col-12 py-1">{{t('cabinet.Ընտրեք օգտատիրոջ տեսակը')}}</p></div>
            </div>
        </div>
        <div class="container mt-4">
            <div class="row">
                <div class="col-12 col-md-5 col-lg-4 col-xl-3">
                    @component('site.components.cabinet_link',['active'=>$active??null])@endcomponent
                </div>
                <div class="col-12 col-md-7 col-lg-8 col-xl-9">
                    <div class="w-full dash-container">
                        <div class="dash active">
                            <div
                                class="border-rounded-full border-3 border-default d_flex j_content_center a_items_center">
                                <svg xmlns="http://www.w3.org/2000/svg" width="25" height="25"
                                     viewBox="0 0 21.233 21.293">
                                    <g id="home" transform="translate(-1.876 -1.583)">
                                        <path id="Path_11249" data-name="Path 11249"
                                              d="M21.3,10.044l.463.315a.352.352,0,0,0,.49-.094L23.048,9.1a.352.352,0,0,0-.094-.49L12.691,1.644a.352.352,0,0,0-.4,0L2.031,8.609a.352.352,0,0,0-.094.49l.792,1.167a.352.352,0,0,0,.49.094l.464-.315V13.57a.876.876,0,0,0-.464.476,16.7,16.7,0,0,0-1.3,4.248,2.322,2.322,0,0,0,1.762,2.433v1.444H1.92v.7H23.066v-.7H21.3ZM3.114,9.578l-.4-.584,9.774-6.632,9.774,6.632-.4.584-9.18-6.23a.352.352,0,0,0-.4,0Zm-.49,8.717a15.721,15.721,0,0,1,1.247-3.984.176.176,0,0,1,.326,0,15.721,15.721,0,0,1,1.247,3.984c0,1.021-.593,1.762-1.41,1.762s-1.41-.741-1.41-1.762Zm1.762,2.433a2.322,2.322,0,0,0,1.762-2.433,16.7,16.7,0,0,0-1.3-4.248.876.876,0,0,0-.464-.476v-4l8.106-5.5,8.106,5.5v6.86l-.059-.089a.366.366,0,0,0-.586,0l-.7,1.057a.352.352,0,0,0-.059.2v.7h-.7v-.7a.354.354,0,0,0-.059-.2l-.7-1.057a.366.366,0,0,0-.586,0l-.7,1.057a.354.354,0,0,0-.059.2v.7h-.7v-.7a.354.354,0,0,0-.059-.2l-.7-1.057a.366.366,0,0,0-.586,0l-.7,1.057a.354.354,0,0,0-.059.2v4.582H11.436v-6.7a.352.352,0,0,0-.352-.352H7.206a.352.352,0,0,0-.352.352v6.7H4.387ZM14.96,22.171h-.7V17.7l.352-.529.352.529Zm.7-3.172h.7v.7h-.7Zm0,1.41h.7v1.762h-.7Zm2.115,1.762h-.7V17.7l.352-.529.352.529Zm.7-3.172h.7v.7h-.7Zm0,1.41h.7v1.762h-.7Zm-7.753,1.762H7.559V15.827h3.172Zm9.163,0V17.7l.352-.529.352.529v4.475Z"
                                              transform="translate(0 0)" fill="#FE980F"/>
                                        <path id="Path_11250" data-name="Path 11250"
                                              d="M26.41,23.934h2.115a1.411,1.411,0,0,0,1.41-1.41V20.41A1.411,1.411,0,0,0,28.524,19H26.41A1.411,1.411,0,0,0,25,20.41v2.115A1.411,1.411,0,0,0,26.41,23.934Zm-.7-1.41v-.7h1.41v1.41h-.7A.706.706,0,0,1,25.7,22.524Zm2.819.7h-.7v-1.41h1.41v.7A.706.706,0,0,1,28.524,23.229Zm.7-2.819v.7h-1.41V19.7h.7A.706.706,0,0,1,29.229,20.41Zm-2.819-.7h.7v1.41H25.7v-.7A.706.706,0,0,1,26.41,19.7Z"
                                              transform="translate(-14.974 -11.278)" fill="#FE980F"/>
                                        <path id="Path_11251" data-name="Path 11251" d="M23,50h.7v.7H23Z"
                                              transform="translate(-13.679 -31.353)" fill="#FE980F"/>
                                    </g>
                                </svg>
                            </div>
                        </div>
                        <div class="dash dash_for_step2">
                            <div
                                class="border-rounded-full border-3 border-default  d_flex j_content_center a_items_center">
                                <svg xmlns="http://www.w3.org/2000/svg" width="28.318" height="27.53"
                                     viewBox="0 0 27.318 26.53">
                                    <g id="hotel" transform="translate(-0.081 0.493)">
                                        <path id="Path_11252" data-name="Path 11252"
                                              d="M20.338,11.916a7.016,7.016,0,0,0-2.857.6V6.006a.5.5,0,0,0-.5-.5h-.641V3.964a.5.5,0,0,0-.5-.5H14.707v-.6a.5.5,0,0,0-1.008,0v.6H12.543a.5.5,0,0,0-.5.5V5.5h-.673v-2a.5.5,0,0,0-.5-.5h-1.3V1.436a.5.5,0,0,0-.5-.5h-2.9V.011a.5.5,0,0,0-.5-.5.5.5,0,0,0-.5.5V.933H2.384a.5.5,0,0,0-.5.5V3H.585a.5.5,0,0,0-.5.5V22a.5.5,0,0,0,.5.5H14.223a7.06,7.06,0,1,0,6.115-10.587ZM2.888,1.94H8.56V3H2.888ZM7.275,21.5H6.168V18.411H7.275Zm-2.115,0H4.172V18.411H5.16Zm5.2,0H8.283V17.907a.5.5,0,0,0-.5-.5H3.668a.5.5,0,0,0-.5.5V21.5H1.089V4.01h9.27ZM13.047,4.468h2.282V5.5H13.047ZM11.367,21.5V6.51h5.107v6.561a7.109,7.109,0,0,0-1.021.813.506.506,0,0,0-.153-.024H12.565a.5.5,0,0,0,0,1.008H14.6a7.068,7.068,0,0,0-.744,1.313h-1.29a.5.5,0,0,0,0,1.008h.942a7.021,7.021,0,0,0-.223,1.5h-.719a.5.5,0,0,0,0,1.008h.748a7,7,0,0,0,.429,1.8Zm8.972,3.534a6.053,6.053,0,1,1,6.053-6.053,6.053,6.053,0,0,1-6.053,6.053Zm0,0"
                                              fill="#FE980F"/>
                                        <path id="Path_11253" data-name="Path 11253"
                                              d="M47.074,196.5h6.107a.5.5,0,0,0,0-1.008H47.074a.5.5,0,0,0,0,1.008Zm0,0"
                                              transform="translate(-44.403 -187.186)" fill="#FE980F"/>
                                        <path id="Path_11254" data-name="Path 11254"
                                              d="M47.074,259.1h6.107a.5.5,0,0,0,0-1.008H47.074a.5.5,0,0,0,0,1.008Zm0,0"
                                              transform="translate(-44.403 -246.983)" fill="#FE980F"/>
                                        <path id="Path_11255" data-name="Path 11255"
                                              d="M47.074,321.71h6.107a.5.5,0,0,0,0-1.008H47.074a.5.5,0,0,0,0,1.008Zm0,0"
                                              transform="translate(-44.403 -306.782)" fill="#FE980F"/>
                                        <path id="Path_11256" data-name="Path 11256"
                                              d="M47.074,133.89h6.107a.5.5,0,0,0,0-1.008H47.074a.5.5,0,0,0,0,1.008Zm0,0"
                                              transform="translate(-44.403 -127.39)" fill="#FE980F"/>
                                        <path id="Path_11257" data-name="Path 11257"
                                              d="M267.554,264.55h2.626a.5.5,0,0,0,0-1.008h-2.626a.5.5,0,0,0,0,1.008Zm0,0"
                                              transform="translate(-254.989 -252.187)" fill="#FE980F"/>
                                        <path id="Path_11258" data-name="Path 11258"
                                              d="M267.554,210.106h2.626a.5.5,0,0,0,0-1.008h-2.626a.5.5,0,0,0,0,1.008Zm0,0"
                                              transform="translate(-254.989 -200.185)" fill="#FE980F"/>
                                        <path id="Path_11259" data-name="Path 11259"
                                              d="M374.268,342.363h-6.557a.5.5,0,0,0-.5.5V344.4s0,.009,0,.013,0,.009,0,.013a.5.5,0,0,0,.247.433l2.2,2.281v3.981a.5.5,0,0,0,.729.451l1.658-.829a.5.5,0,0,0,.279-.451V347.14l2.2-2.281a.5.5,0,0,0,.247-.433s0-.009,0-.013,0-.009,0-.013v-1.532A.5.5,0,0,0,374.268,342.363Zm-.5,1.008v.551h-5.549v-.551Zm-2.308,3.216a.5.5,0,0,0-.141.35v3.044l-.65.325v-3.369a.5.5,0,0,0-.141-.35l-1.6-1.657h4.132Zm0,0"
                                              transform="translate(-350.652 -327.47)" fill="#FE980F"/>
                                    </g>
                                </svg>

                            </div>
                        </div>
                        <div class="dash">
                            <div
                                class="border-rounded-full border-3 border-passive  d_flex j_content_center a_items_center">
                                <svg xmlns="http://www.w3.org/2000/svg" width="25.868" height="25.868"
                                     viewBox="0 0 25.868 25.868">
                                    <g id="eye" transform="translate(0 -0.001)">
                                        <g id="Group_5452" data-name="Group 5452" transform="translate(0 4.598)">
                                            <g id="Group_5451" data-name="Group 5451">
                                                <path id="Path_11287" data-name="Path 11287"
                                                      d="M25.69,98.849a33.024,33.024,0,0,0-4.009-3.889C18.467,92.332,15.524,91,12.934,91S7.4,92.332,4.187,94.96A33.019,33.019,0,0,0,.178,98.849a.758.758,0,0,0,0,.975,33.024,33.024,0,0,0,4.009,3.889c3.214,2.628,6.157,3.96,8.747,3.96s5.533-1.332,8.747-3.96a33.025,33.025,0,0,0,4.009-3.889A.758.758,0,0,0,25.69,98.849Zm-12.756,7.308c-4.562,0-9.668-5.187-11.163-6.821,2.653-2.907,7.245-6.82,11.163-6.82,4.562,0,9.667,5.187,11.163,6.821C21.445,102.244,16.852,106.157,12.934,106.157Z"
                                                      transform="translate(0 -91)" fill="#AAAFB8"/>
                                            </g>
                                        </g>
                                        <g id="Group_5454" data-name="Group 5454" transform="translate(7.629 7.629)">
                                            <g id="Group_5453" data-name="Group 5453">
                                                <path id="Path_11288" data-name="Path 11288"
                                                      d="M160.852,155.547a.758.758,0,0,0-.758.758,3.789,3.789,0,1,1-3.789-3.789.758.758,0,0,0,0-1.516,5.3,5.3,0,1,0,5.3,5.3A.758.758,0,0,0,160.852,155.547Z"
                                                      transform="translate(-151 -151)" fill="#AAAFB8"/>
                                            </g>
                                        </g>
                                        <g id="Group_5456" data-name="Group 5456" transform="translate(0 20.241)">
                                            <g id="Group_5455" data-name="Group 5455">
                                                <path id="Path_11289" data-name="Path 11289"
                                                      d="M5.406,400.844a.758.758,0,0,0-1.072,0L.222,404.955a.758.758,0,0,0,1.072,1.072l4.111-4.112A.758.758,0,0,0,5.406,400.844Z"
                                                      transform="translate(-0.001 -400.622)" fill="#AAAFB8"/>
                                            </g>
                                        </g>
                                        <g id="Group_5458" data-name="Group 5458" transform="translate(20.241 20.241)">
                                            <g id="Group_5457" data-name="Group 5457">
                                                <path id="Path_11290" data-name="Path 11290"
                                                      d="M406.027,404.955l-4.112-4.112a.758.758,0,1,0-1.072,1.072l4.112,4.112a.758.758,0,0,0,1.072-1.072Z"
                                                      transform="translate(-400.622 -400.622)" fill="#AAAFB8"/>
                                            </g>
                                        </g>
                                        <g id="Group_5460" data-name="Group 5460" transform="translate(0 0.001)">
                                            <g id="Group_5459" data-name="Group 5459" transform="translate(0)">
                                                <path id="Path_11291" data-name="Path 11291"
                                                      d="M5.406,4.334,1.294.222A.758.758,0,0,0,.222,1.294L4.334,5.406A.758.758,0,0,0,5.406,4.334Z"
                                                      transform="translate(-0.001 -0.001)" fill="#AAAFB8"/>
                                            </g>
                                        </g>
                                        <g id="Group_5462" data-name="Group 5462" transform="translate(20.241 0.001)">
                                            <g id="Group_5461" data-name="Group 5461">
                                                <path id="Path_11292" data-name="Path 11292"
                                                      d="M406.027.222a.758.758,0,0,0-1.072,0l-4.112,4.111a.758.758,0,1,0,1.072,1.072l4.112-4.111A.758.758,0,0,0,406.027.222Z"
                                                      transform="translate(-400.622 -0.001)" fill="#AAAFB8"/>
                                            </g>
                                        </g>
                                        <g id="Group_5464" data-name="Group 5464" transform="translate(10.661 10.661)">
                                            <g id="Group_5463" data-name="Group 5463">
                                                <path id="Path_11293" data-name="Path 11293"
                                                      d="M213.274,211a2.274,2.274,0,1,0,2.274,2.274A2.276,2.276,0,0,0,213.274,211Zm0,3.031a.758.758,0,1,1,.758-.758A.759.759,0,0,1,213.274,214.031Z"
                                                      transform="translate(-211 -211)" fill="#AAAFB8"/>
                                            </g>
                                        </g>
                                    </g>
                                </svg>
                            </div>
                        </div>
                        <div class="dash">
                            <div
                                class="border-rounded-full border-3 border-passive  d_flex j_content_center a_items_center">
                                <svg xmlns="http://www.w3.org/2000/svg" width="23.5" height="17.671"
                                     viewBox="0 0 23.5 17.671">
                                    <g id="_001-check" data-name="001-check" transform="translate(0 -63.498)">
                                        <g id="Group_5668" data-name="Group 5668" transform="translate(0 63.498)">
                                            <path id="Path_11352" data-name="Path 11352"
                                                  d="M23.157,63.7a.918.918,0,0,0-1.291.141L9.652,79.043a.918.918,0,0,1-1.3.041l-6.8-6.517a.918.918,0,0,0-1.27,1.325l6.8,6.52a2.73,2.73,0,0,0,1.894.758l.114,0a2.732,2.732,0,0,0,1.941-.915l.032-.037L23.3,64.991A.918.918,0,0,0,23.157,63.7Z"
                                                  transform="translate(0 -63.498)" fill="#AAAFB8"/>
                                        </g>
                                    </g>
                                </svg>
                            </div>
                        </div>
                    </div>
                    <div class="row" style="margin: 0">
                        <div class="col-3 font-segeo font-default font-13"
                             style="text-align: center;padding:5px 25px">{{ t('cabinet.Ընտրել անշարժ գույքի տեսակը') }}</div>
                        <div class="step2_title col-3 font-segeo font-default font-13"
                             style="text-align: center;padding:5px 25px">{{ t('cabinet.Ավելացնել մանրամասներ') }}</div>
                        <div class="step3_title col-3 font-segeo font-passive font-13"
                             style="text-align: center;padding:5px 25px">{{ t('cabinet.Տեսնել Վերջնակն Արդյունքը') }}</div>
                        <div class="col-3 font-segeo font-passive font-13"
                             style="text-align: center;padding:5px 25px">{{ t('cabinet.Գույքը ավելացված է') }}</div>
                    </div>
                    <div class="w-full" id="step2">
                        <div>
                            <a href="{{ route('cabinet.addEstate',['step'=>1]) }}" class="font-segeo font-13 go_back "
                               style="cursor: pointer;color: #FE980F"><i
                                    class="fas fa-arrow-left"></i> {{t('cabinet.Հետ')}}</a>
                        </div>
                        <form
                            id="estate_form"
                            action="{!! $edit?route('cabinet.estates.edit', ['id'=>$item->id]):route('cabinet.estates.add',['category' => $selected_cat,'type' => $selected_tp]) !!}"
                            method="post" enctype="multipart/form-data">
                            @csrf @method($edit?'patch':'put')
                            {{--                        @dd($errors)--}}
                            {{--                        @if ($errors->any())--}}
                            {{--                            <div class="alert alert-danger" role="alert">--}}
                            {{--                                @foreach ($errors->any() as $error)--}}
                            {{--                                    <p>{{ $error }}</p>--}}
                            {{--                                @endforeach--}}
                            {{--                            </div>--}}
                            {{--                        @endif--}}
                            <div class="row cstm-input " style="display: none">
                                <div class="col-12 p-b-5">
                                    <input class="labelauty-reverse toggle-bottom-input on-unchecked" type="checkbox"
                                           name="generate_url"
                                           data-labelauty="Вставить ссылку вручную" {!! oldCheck('generate_url', $edit ?false : true) !!}>
                                    <div class="bottom-input">
                                        <input type="text" style="margin-top:3px;" name="url" class="form-control"
                                               id="form_url"
                                               placeholder="Ссылка" value="{{ old('url', $item->url??null) }}">
                                    </div>
                                </div>
                            </div>
                            <div class="row mt-5">
                                <div class="col-12 col-lg-6  col-xl-4">
                                    <div class="card">
                                        <div class="address">
                                            @if(!empty($regions) && count($regions))
                                                <div class="card ">
                                                    <div
                                                        class="c-title mb-2 font-bold font-13 font-segeo">{{t('cabinet.Ընտրեք տարածաշրջանը')}}</div>
                                                    <div class="c-body  {{$errors->has('region')?'error_bordered':''}}">
                                                        <select id="demo-3b" name="region[0]"
                                                                class="{{$errors->has('region')?'error_border':''}}">
                                                            @foreach($regions as $region)
                                                                <optgroup label="{{$region->title}}">
                                                                    @foreach($region->childes as $cat)
                                                                        <option value="" disabled
                                                                                data-level="1"> {{$cat->title}}  </option>
                                                                        @foreach($cat->childes as $c)
                                                                            <option
                                                                                {{ (old('region.0')==$c->id) ? 'selected' : ((!empty($connected_regions) &&  in_array($c->id,$connected_regions)) ? 'selected' : '')  }} value="{{$c->id}}"
                                                                                data-level="2"> {{$c->title}}  </option>
                                                                        @endforeach
                                                                    @endforeach
                                                                </optgroup>
                                                            @endforeach
                                                        </select>
                                                        <div style="color: #aa2222;font-size: 12px"
                                                             class=" font-segeo  font-bold mt-1">
                                                            {{$errors->has('region')?$errors->first('region'):''}}
                                                        </div>
                                                    </div>
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 col-lg-6 col-xl-4">
                                    <div class="card">
                                        <div class="c-body">
                                            {{--                                        <div class="w-100 ">--}}
                                            {{--                                            @labelauty(['id'=>'price_filter', 'label'=>'Договорная|Договорная', 'checked'=>oldCheck('price_filter', ($edit &&  ($item->price != -1))?false:true)])@endlabelauty--}}
                                            {{--                                        </div>--}}
                                            @if($edit)
                                                @if(count($item->estate_filter))
                                                    @foreach($item->estate_filter as $val)
                                                        @if($val->filter_id == $price_filter->id)
                                                            @php($value = $val->value)
                                                        @endif
                                                    @endforeach
                                                @endif
                                                <div class="card" id="for_price_filter">
                                                    <div
                                                        class="c-title mb-2 font-bold font-13 font-segeo">{{$price_filter->title.' ('.$price_filter->metric.')'}}</div>
                                                    <div class="c-body"
                                                         style="display: flex; flex-direction:column;justify-content: space-between">
                                                        <input id="price_filter_inp" type="text"
                                                               name="interval_filter[{{$price_filter->id}}]"
                                                               class="form-control  {{$errors->has('interval_filter.'.$price_filter->id)?'error_border':''}}"
                                                               placeholder="{{$price_filter->title}}"
                                                               value="{{ old('interval_filter.'.$price_filter->id, $value??null) }}"
                                                               data-old="{{ old('interval_filter.'.$price_filter->id, $value??null) }}">
                                                    </div>
                                                    <div style="color: #aa2222;font-size: 12px"
                                                         class=" font-segeo  font-bold mt-1">
                                                        {{$errors->has('interval_filter.'.$price_filter->id)?$errors->first('interval_filter.'.$price_filter->id):''}}
                                                    </div>
                                                </div>
                                            @else
                                                <div class="card">
                                                    <div
                                                        class="c-title mb-2 font-bold font-13 font-segeo">{{$price_filter->title.' ('.$price_filter->metric.')'}}</div>
                                                    <div class="c-body"
                                                         style="display: flex; flex-direction:column;justify-content: space-between">
                                                        <input type="text" id="price_filter_inp"
                                                               name="interval_filter[{{$price_filter->id}}]"
                                                               class="form-control   {{$errors->has('interval_filter.'.$price_filter->id)?'error_border':''}}"
                                                               placeholder="{{$price_filter->title}}"
                                                               value="{{ old('interval_filter.'.$price_filter->id, $value??null) }}"
                                                               data-old="{{ old('interval_filter.'.$price_filter->id, $value??null) }}">
                                                        <div style="color: #aa2222;font-size: 12px"
                                                             class=" font-segeo  font-bold mt-1">
                                                            {{$errors->has('interval_filter.'.$price_filter->id)?$errors->first('interval_filter.'.$price_filter->id):''}}
                                                        </div>
                                                    </div>
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                @if(!empty($global_filters))
                                    @foreach($global_filters as $filter)
                                        @component('admin.components.admin_filters',['filter'=>$filter,'edit'=>$edit,'item'=>$item??null]) @endcomponent
                                    @endforeach
                                @endif
                                @if(!empty($local_filters))
                                    @foreach($local_filters as $filter)
                                        @if(!($filter->price_filter))
                                            @component('admin.components.admin_filters',['filter'=>$filter,'edit'=>$edit,'item'=>$item??null]) @endcomponent
                                        @endif
                                    @endforeach
                                @endif
                                <div class="col-12 mt-5">
                                    <div class="card">
                                        @bylang(['id'=>'form_title1', 'tp_classes'=>'little-p'])
                                        <div class="mb-4">

                                            @if($errors->has('title.'.$iso))
                                                <style>
                                                    a[href = '{!! '#form_title1_'.$iso !!}'] .error_note {
                                                        display: inline !important;
                                                    }
                                                </style>
                                            @endif

                                            <label for="" class="font-segeo font-13 font-bold">
                                                {{t('cabinet.Հայտարարության անվանում')}}
                                                <input type="text" name="title[{!! $iso !!}]"
                                                       class="form-control mt-2 {{$errors->has('title.'.$iso)?'error_border':''}}"
                                                       placeholder="{{t('cabinet.Հայտարարության անվանում')}}"
                                                       value="{{ old('title.'.$iso, tr($item, 'title', $iso)) }}">
                                            </label>
                                            <div style="color: #aa2222;font-size: 12px"
                                                 class=" font-segeo  font-bold mt-1">
                                                {{$errors->has('title.'.$iso)?$errors->first('title.'.$iso):''}}
                                            </div>
                                        </div>
                                        <div>
                                            <label for="" class="font-segeo font-13 font-bold">
                                                {{t('cabinet.Նկարագրություն')}}
                                                <textarea type="text" rows="7" name="description[{!! $iso !!}]"
                                                          class="mt-2 form-control ckeditor {{$errors->has('description.'.$iso)?'error_border':''}}"
                                                          placeholder="{{t('cabinet.Նկարագրություն')}}">{{ old('description.'.$iso, tr($item, 'description', $iso)) }}</textarea>
                                            </label>
                                            <div style="color: #aa2222;font-size: 12px"
                                                 class=" font-segeo  font-bold mt-1">
                                                {{$errors->has('description.'.$iso)?$errors->first('description.'.$iso):''}}
                                            </div>
                                        </div>
                                        @endbylang
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="card features__min">
                                        <div
                                            class="c-title font-segeo font-13 font-bold mb-3">{{ t('cabinet.Ընտրել տեղանքը') }}</div>
                                        <div class="c-body">
                                            <div class="little-p">
                                                <div id="map" style="width: 100%; height: 400px"></div>
                                                <div class="mt-2" style="display: none">
                                                    <input type="text" name="lat" class="form-control map-inp lat"
                                                           placeholder="Широта" maxlength="20"
                                                           value="{{ old('lat', $item->lat??null) }}">
                                                    <input type="text" name="lng" class="form-control map-inp lng mt-2"
                                                           placeholder="Долгота" maxlength="20"
                                                           value="{{ old('lng', $item->lng??null) }}">
                                                    <button type="button" class="btn btn-secondary mt-2 show-on-map">
                                                        Показать
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="card features__min">
                                        <div class="c-title pb-1 pt-2 font-segeo font-13 font-bold mb-3">
                                            {{ t('cabinet.Բեռնել տեսանյութ (Տեղադրել Youtube - ի հղում)') }}
                                        </div>
                                        <div class="c-body">
                                            <input type="text" id="for_iframe" name="youtube" class="form-control my-2"
                                                   placeholder="YouTube"
                                                   value="{{ old('youtube', $item->youtube??null) }}">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card my-5 features__min">
                                <div
                                    class="c-title font-segeo font-13 font-bold ">{{ t('cabinet.Ավելացնել լուսանկար') }}</div>
                                <div class="c-body ">
                                    <div class="form-group"
                                         style="margin-top:15px;margin-bottom:0;position: relative;min-height: 65px;">
                                        {{--            <label for="exampleInputFile">File input</label>--}}
                                        <input type="file" name="images[]" class="demo" multiple
                                               data-jpreview-container="#demo-1-container">
                                        <div id="demo-1-container" class="jpreview-container"></div>
                                    </div>
                                    <input type="hidden" name="gallery" value="estates">
                                </div>
                            </div>
                        </form>
                        @if($edit)
                            <div class="row">
                                @if(!empty($gallery_images) && !empty($gallery_images->where('poster',1)->first()) && count($gallery_images->where('poster',1)))
                                    <div class="col-12 col-lg-4">
                                        <div class="card">
                                            <div class="c-title">Главное изображение</div>
                                            <div class="c-body"><img id="for_home" class="w-100"
                                                                     src="{{ $gallery_images->where('poster',1)->first()->image(true) }}"
                                                                     alt=""></div>
                                        </div>

                                    </div>
                                @elseif(!empty($gallery_images) && count($gallery_images))
                                    <div class="col-12 col-lg-4">

                                        <div class="card">
                                            <div class="c-title">Главное изображение</div>
                                            <div class="c-body"><img id="for_home" class="w-100"
                                                                     src="{{ $gallery_images->first()->image(true) }}"
                                                                     alt=""></div>
                                        </div>

                                    </div>
                                @else
                                    <div class="col-12 col-lg-4">
                                        <div class="card">
                                            <div class="c-title">Главное изображение</div>
                                            <div class="c-body"><img id="for_home" class="w-100"
                                                                     src="{{ asset('f/site/img/default.jpg') }}" alt="">
                                            </div>
                                        </div>
                                    </div>
                                @endif
                                @if(!empty($gallery_images))
                                    <div class="col-12 col-lg-8">
                                        @component('site.components.cabinetGallery',['gallery_images'=>$gallery_images,'show_home'=>true])@endcomponent
                                    </div>
                                @endif
                            </div>
                        @endif
                        <div class="send_button" style="margin: 0;margin-top: 10px">
                            <button type="button" id="to_step3" class="font-13 px-2 py-2"
                                    style="width: unset;max-width: unset"><i
                                    class="fas fa-paper-plane"></i> {{ t('cabinet.Առաջ') }}</button>
                        </div>
                    </div>
                    <div class="w-full" id="step3" style="display: none">
                        <div>
                            <p class="font-segeo font-13 go_back " style="cursor: pointer;color: #FE980F"><i
                                    class="fas fa-arrow-left"></i> {{t('cabinet.Հետ')}}</p>
                        </div>
                        <div class="product-det__title" style="display: none">
                            <h1 id="for_title">

                            </h1>
                        </div>
                        <div class="product-det__title" style="display: none">
                            <p>
                                <i style="color: #cc8202" class="fas fa-map-marker-alt"></i> <span
                                    id="for_region"></span>
                            </p>
                        </div>
                        <div class="product-det__price">
                            <strong id="for_price"></strong>
                        </div>
                        <div id="container2" class="row">
                        </div>
                        <div class="features__min" style="display: none">
                            <h2>{{ t('cabinet.Մանրամասներ') }}  </h2>
                            <div class="param_table">
                                <div class="features__min__info best-offers__info block1"
                                     style="margin-top: 0!important;margin-bottom: 0!important;">
                                    <table>
                                        <tbody id="for_params">

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="features__min" style="display: none">
                            <h2>{{ t('cabinet.Նկարագրություն') }} </h2>
                            <p id="for_desc">
                            </p>
                        </div>
                        <div class="features__min" style="display: none">
                            <h2>{{ t('cabinet.Տեսանյութ') }} </h2>
                            <div class="features__img" id="for_youtube_iframe">
                            </div>
                        </div>
                        <div class="features__min">
                            <h2>{{ t('cabinet.Քարտեզ') }} </h2>
                            <div class="card">
                                <div class="little-p">
                                    <div id="map2" style="width: 100%; height: 400px"></div>
                                    <div class="mt-2" style="max-width: 300px">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="send_button send_button_submit" style="margin: 0;margin-top: 10px">
                            <button type="button" id="to_step3" class="bg-default font-13 px-2 py-2"
                                    style="width: unset;max-width: unset"><i
                                    class="fas fa-paper-plane"></i> {{ t('cabinet.Ուղարկել մոդեռացիայի') }}</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('site.layouts.footer')
@endsection
@css(aApp('jquery-ui/jquery-ui.css'))

<link href="{{asset('f/admin/smartSelect/smartSelect.min.css')}}"
      data-href="{{asset('f/admin/smartSelect/smartSelect.min.css')}}" id="smartSelect" media="all" rel="stylesheet"
      type="text/css">
@css(aApp('fancybox/fancybox.css'))
@css(aApp('multiupload/jpreview.css'))
@push('css')
    <style>
        .del_gallery.show {
            padding-right: 0px;
            justify-content: center;
            align-items: center;
            display: flex !important;
        }

        .del_gallery.show .modal-dialog {
            background: white;
        }

        .del_gallery.show .modal-footer {
            display: flex;
            justify-content: flex-end;
        }

        .gallery-grid {
            position: relative;
            max-width: 350px;
            margin: 0 auto;
        }

        .gallery-absolute {
            display: flex;
            align-items: center;
            justify-content: center;
            position: absolute;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            background-color: rgba(0, 0, 0, .7);
            opacity: 0;
            pointer-events: none;
            -webkit-transition: opacity .3s ease;
            -moz-transition: opacity .3s ease;
            -o-transition: opacity .3s ease;
            transition: opacity .3s ease;
        }

        .gallery-item-actions {
            position: absolute;
            top: 10px;
            right: 10px;
        }

        .gallery-image {
            width: 100%;
            height: auto;
            min-height: 120px;
            object-fit: cover;
        }

        .gallery-grid:hover .gallery-absolute {
            pointer-events: all;
            opacity: 1;
        }

        a.gallery-item-action:not(.gallery-item-move) {
            -webkit-transition: color .3s ease;
            -moz-transition: color .3s ease;
            -o-transition: color .3s ease;
            transition: color .3s ease;
        }

        a.gallery-item-action {
            line-height: 1;
            margin-left: 5px;
            color: #fff;
            font-size: 20px;
        }

        a.gallery-item-action.gallery-item-move {
            cursor: move;
        }

        a.gallery-item-action:not(.gallery-item-move):hover {
            color: #bbb;
        }

        a.gallery-item-show {
            line-height: 1;
            font-size: 40px;
            color: #fff;
            -webkit-transition: color .3s ease;
            -moz-transition: color .3s ease;
            -o-transition: color .3s ease;
            transition: color .3s ease;
        }

        .send_button {
            display: flex;
            justify-content: flex-end;
        }

        .send_button button {
            font: 13px "Segoe UI", HelveticaNeueCyr-Bold, Helvetica, Roboto, sans-serif;
            border: 2px solid #FE980F;
            transition: .3s;
            background: #FE980F;
            color: #fff;
        }

        .send_button button:hover {
            transition: .3s;
            background: #ffffff;
            color: #FE980F;
        }

        .form-control {
            height: 40px;
            padding: 6px 10px;
            font-size: 13px;
            color: #042a3d;
            border-radius: 2px;
        }

        .smartselect button {
            width: 100% !important;
            text-align: left !important;
            border: 1px solid #cccccc;
            border-radius: 2px;
            color: #042a3d;
            height: 40px;
            background: white;
            font-family: "Segoe UI", "HelveticaNeueCyr-Bold", "Helvetica", "Roboto", sans-serif;
        }

        .nav-link {
            background-color: #F0F1F2;
            color: #042a3d;
        }

        .nav-link.active {
            background-color: #ffffff;
            color: #cd8304;
        }

        .nav > li > a {
            padding: 5px 30px;
            font-family: "Segoe UI", "HelveticaNeueCyr-Bold", "Helvetica", "Roboto", sans-serif;
            font-size: 13px;
            border-radius: 0;
            margin-right: 10px;
        }

        .nav > li > a.active {
            box-shadow: 0px 0px 4px 2px #8484842e;
            border-radius: 5px 5px 0 0;
        }

        .nav > li > a:hover, .nav > li > a:focus {
            background-color: #ffffff;
            color: #cd8304;
        }

        .tab-content.tabcontent-border {
            background-color: white;
            padding: 15px;
            position: relative;
        }

        input[type="file"] {
            display: block;
        }

        .imageThumb {
            max-height: 75px;
            border: 2px solid;
            padding: 1px;
            cursor: pointer;
        }

        .header_container:before, .header_container:after {
            display: none;
        !important;
        }

        .pip {
            display: inline-block;
            margin: 10px 10px 0 0;
        }

        .remove {
            display: block;
            background: #444;
            border: 1px solid black;
            color: white;
            text-align: center;
            cursor: pointer;
        }

        .remove:hover {
            background: white;
            color: black;
        }

        table * {
            cursor: pointer
        }

        table td {
            padding: 5px;
        }

        .error_border {
            border: 1px solid #aa2222 !important;
        }

        .error_bordered .smartselect.dropdown {
            border: 1px solid #aa2222 !important;
        }

        input[type="radio"] {
            background-color: #fff;
            background-image: -webkit-linear-gradient(0deg, transparent 20%, hsla(0, 0%, 100%, .7), transparent 80%), -webkit-linear-gradient(90deg, transparent 20%, hsla(0, 0%, 100%, .7), transparent 80%);
            border-radius: 10px;
            box-shadow: inset 0 1px 1px hsla(0, 0%, 100%, .8), 0 0 0 1px hsl(108, 50%, 49%), 0 2px 3px hsla(108, 50%, 49%, 0.6), 0 4px 3px hsla(108, 50%, 49%, 0.45), 0 6px 6px hsla(0, 8%, 62%, 0), 0 10px 6px hsla(0, 0%, 0%, 0);
            cursor: pointer;
            display: inline-block;
            height: 15px;
            margin-right: 15px;
            position: relative;
            width: 15px;
            -webkit-appearance: none;
        }

        input[type="radio"]:after {
            border-radius: 25px;
            box-shadow: inset 0 0 0 1px hsl(108, 100%, 31%), 0 1px 1px hsla(0, 0%, 100%, 0.8);
            content: '';
            display: block;
            height: 9px;
            left: 3px;
            position: relative;
            top: 3px;
            width: 9px;
        }

        input[type="radio"]:checked:after {
            background-color: #56ba3e;
            box-shadow: inset 0 0 0 1px hsla(108, 6%, 13%, 0.32), inset 0 2px 2px hsla(0, 0%, 100%, .4), 0 1px 1px hsla(0, 0%, 100%, .8), 0 0 2px 2px hsl(108, 100%, 50%);
        }
    </style>
    @css(aApp('labelauty/labelauty.css'))

@endpush
@push('js')

    @js(aApp('bootstrap/js/bootstrap.js'))
    @js(aApp('fancybox/fancybox.js'))
    @js(aApp('multiupload/bootstrap-prettyfile.js'))
    @js(aApp('multiupload/jpreview.js'))
    @js(aApp('multiupload/index.js'))
    @js(aAdmin('smartSelect/smartSelect.min.js'))
    {{--    @ckeditor--}}
    <script src="https://api-maps.yandex.ru/2.1/?apikey=ce752946-5050-4a17-9d27-624b2dc71d8b&lang=ru_RU"></script>
    <script>
        (function () {
            ymaps.ready(init);
            var myMap, tempPlacemark = null,
                latInput = $('.map-inp.lat'), longInput = $('.map-inp.lng'),
                myPlacemark = {}, placemarkData = {};

            function showOnMap(position) {
                if (tempPlacemark !== null) {
                    myMap.geoObjects.remove(tempPlacemark);
                    tempPlacemark = null;
                    latInput.val('');
                    longInput.val('');
                }
                tempPlacemark = new ymaps.Placemark(position)
                myMap.geoObjects.add(tempPlacemark);
                latInput.val(position[0]);
                longInput.val(position[1]);
            }

            function init() {
                myMap = new ymaps.Map("map", {
                    center: [40.2, 44.5],
                    zoom: 10,
                });
                @if($edit)
                    tempPlacemark = new ymaps.Placemark([{!! $item->lat !!}, {!! $item->lng !!}])
                myMap.geoObjects.add(tempPlacemark);
                @endif

                myMap.events.add('click', function (e) {
                    showOnMap(e.get('coords'));
                    showOnMap2(e.get('coords'));
                });
                $('.show-on-map').on('click', function () {
                    var lat = $.trim(latInput.val()),
                        long = $.trim(longInput.val());
                    if (lat != '' && long != '' && !isNaN(lat) && !isNaN(long)) {
                        showOnMap([lat, long]);
                        myMap.setCenter([lat, long], myMap.getZoom(), {duration: 300});
                        showOnMap2([lat, long]);
                        myMap2.setCenter([lat, long], myMap2.getZoom(), {duration: 300});
                    }
                });
            }

            ymaps.ready(init2);
            var myMap2, tempPlacemark2 = null,
                latInput2 = $('.map-inp.lat'), longInput2 = $('.map-inp.lng'),
                myPlacemark2 = {}, placemarkData2 = {};

            function showOnMap2(position) {
                if (tempPlacemark2 !== null) {
                    myMap2.geoObjects.remove(tempPlacemark2);
                    tempPlacemark2 = null;
                    latInput.val('');
                    longInput.val('');
                }
                tempPlacemark2 = new ymaps.Placemark(position)
                myMap2.geoObjects.add(tempPlacemark2);
                latInput2.val(position[0]);
                longInput2.val(position[1]);
            }

            function init2() {
                myMap2 = new ymaps.Map("map2", {
                    center: [40.2, 44.5],
                    zoom: 10,
                });
                @if($edit)
                    tempPlacemark2 = new ymaps.Placemark([{!! $item->lat !!}, {!! $item->lng !!}])
                myMap2.geoObjects.add(tempPlacemark2);
                @endif
            }
        })();
    </script>
    <script>
        $('.demo').prettyFile();
        $('.demo').jPreview();
        $('.send_button_submit').click(
            function () {
                $('#estate_form').submit();
            }
        )
        $('#to_step3').click(function () {
            $('#step2').hide();
            $('#step3').show();
            $('.dash_for_step2').addClass('active');
            $('body,html').stop(true, false).animate({
                scrollTop: 0
            }, 200);

        });
        $('.go_back').click(function () {
            $('#step2').show();
            $('#step3').hide();
            $('.dash_for_step2').removeClass('active');
            $('body,html').stop(true, false).animate({
                scrollTop: 0
            }, 200);

        });
        $(document).ready(function () {
            var table = $('#for_params');
            $('.interval_filter').keyup(function () {
                var filter = $(this).data('filter');
                var title = $(this).attr('placeholder');
                var value = $(this).val();
                $('#for_params').closest('.features__min').show();

                if ($(document).find('tr[data-forfilter=' + filter + ']').length) {
                    $(document).find('tr[data-forfilter=' + filter + ']').find('.for_val').html(value);
                } else {
                    table.append(
                        '<tr data-forfilter ="' + filter + '" >' +
                        '<td>' +
                        '<div><span class="opt" title="' + title + '">' + title + ' <span class="delimeter">  </span> <span style="">..........................................................................................................</span> </span> </div>' +
                        '</td>' +
                        '<td>' +
                        '<div><b class="for_val">' + value + '</b><span class="delimeter"><b>;</b>&nbsp;&nbsp;&nbsp; </span></div>' +
                        '</td>' +
                        '</tr>'
                    )
                }
            })
            $('.without_interval_filter').on('change', function () {
                var filter = $(this).data('filter');
                var title = $(this).data('title');
                var value = $(this).find('option:selected').html();
                $('#for_params').closest('.features__min').show();
                if ($(document).find('tr[data-forfilter=' + filter + ']').length) {
                    $(document).find('tr[data-forfilter=' + filter + ']').find('.for_val').html(value);
                } else {
                    table.append(
                        '<tr data-forfilter ="' + filter + '" >' +
                        '<td>' +
                        '<div><span class="opt" title="' + title + '">' + title + ' <span class="delimeter">  </span> <span style="">..........................................................................................................</span> </span> </div>' +
                        '</td>' +
                        '<td>' +
                        '<div><b class="for_val">' + value + '</b><span class="delimeter"><b>;</b>&nbsp;&nbsp;&nbsp; </span></div>' +
                        '</td>' +
                        '</tr>'
                    )
                }
            })
            $('#price_filter_inp').keyup(function () {
                $('#for_price').html($(this).val() + '$')
            })
            $('textarea[name="description[{!! app()->getLocale() !!}]"]').keyup(function () {
                $('#for_desc').closest('.features__min').show();
                $('#for_desc').html($(this).val())
            })
            $('input[name="title[{!! app()->getLocale() !!}]"]').keyup(function () {
                $('#for_title').closest('.product-det__title').show();
                $('#for_title').html("{!! $part.'. ' !!}" + $(this).val())
            })


            $('#for_iframe').change(function () {
                $('#for_youtube_iframe').closest('.features__min').show();

                function getId(url) {
                    const regExp = /^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|&v=)([^#&?]*).*/;
                    const match = url.match(regExp);
                    return (match && match[2].length === 11)
                        ? match[2]
                        : null;
                }

                const videoId = getId($(this).val());
                const iframeMarkup = '<iframe width="100%" height="100%" src="//www.youtube.com/embed/'
                    + videoId + '" frameborder="0" allowfullscreen></iframe>';
                $('#for_youtube_iframe').html(iframeMarkup)
            })
        })
    </script>
    <script>
        $(document).ready(function () {
            $('input[type="file"]').change(function (e) {
                console.log($('input[type="file"]'));
                var fileName = e.target.files.name;
                $('input[type="file"] ').each(function (index, field) {
                    $('.file-name').html('');
                    console.log(this);
                    for (var i = 0; i < field.files.length; i++) {
                        $('.file-name').append(`<div class="p-1 i${i}" >${field.files[i].name} <sup><span class="btn-red"  onclick="deletefile('${field.files[i].name}', ${i})">x</span></sup></span></div>`);
                    }
                });
            });
        });

        function deletefile(x, i) {
            $('.i' + i).remove();
            let input = $('#exampleFormControlFile1')[0];
            let dt = new DataTransfer()
            for (let file of input.files) {
                if (file.name != x) {
                    dt.items.add(file);
                }
            }
            input.files = dt.files;
            console.log(input.files);
        }


        function checkPrice() {
            var price_filter = parseInt($('#for_price_filter').find('input').data('old'));
            if ($('#price_filter').is(':checked')) {
                $('#for_price_filter').hide();
                $('#for_price_filter').find('input').val(-1)
            } else {
                $('#for_price_filter').show();
                $('#for_price_filter').find('input').val((!isNaN(price_filter)) ? price_filter : '')
            }
        }

        var selectRegion = function ($target, event) {
            $('#for_region').closest('.product-det__title').show();

            var value = $target.find('.ss-label').html();
            $('#for_region').html(value)
            return true;
        };
        $("select#demo-3b").smartselect({
            toolbar: false,
            defaultView: 'root+selected',
            multiple: false,
            text: {
                selectLabel: 'Выбрать...'
            },
            callback: {
                onOptionSelected: [selectRegion],
            }
        });
        $(document).ready(function () {
            checkPrice();
            $('#price_filter').on('click', function () {
                checkPrice()
            })
        })
    </script>
@endpush
