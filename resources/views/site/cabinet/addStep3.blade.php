@extends('site.layouts.header')
@section('content')
    <div class="main w-full">
        <div class="w-full bg-white">
            <div class="container"><div class="login row"><p class="col-12 py-1">{{t('cabinet.Ընտրեք օգտատիրոջ տեսակը')}}</p></div></div>
        </div>
        <div class="container mt-4">
            <div class="row">
                <div class="col-12 col-md-5 col-lg-4 col-xl-3">
                    @component('site.components.cabinet_link',['active'=>$active??null])@endcomponent
                </div>
                <div class="col-12 col-md-7 col-lg-8 col-xl-9">
                    <div class="w-full dash-container">
                        <div class="dash">
                            <div class="border-rounded-full border-3 border-default d_flex j_content_center a_items_center">
                                <svg xmlns="http://www.w3.org/2000/svg" width="25" height="25" viewBox="0 0 21.233 21.293">
                                    <g id="home" transform="translate(-1.876 -1.583)">
                                        <path id="Path_11249" data-name="Path 11249" d="M21.3,10.044l.463.315a.352.352,0,0,0,.49-.094L23.048,9.1a.352.352,0,0,0-.094-.49L12.691,1.644a.352.352,0,0,0-.4,0L2.031,8.609a.352.352,0,0,0-.094.49l.792,1.167a.352.352,0,0,0,.49.094l.464-.315V13.57a.876.876,0,0,0-.464.476,16.7,16.7,0,0,0-1.3,4.248,2.322,2.322,0,0,0,1.762,2.433v1.444H1.92v.7H23.066v-.7H21.3ZM3.114,9.578l-.4-.584,9.774-6.632,9.774,6.632-.4.584-9.18-6.23a.352.352,0,0,0-.4,0Zm-.49,8.717a15.721,15.721,0,0,1,1.247-3.984.176.176,0,0,1,.326,0,15.721,15.721,0,0,1,1.247,3.984c0,1.021-.593,1.762-1.41,1.762s-1.41-.741-1.41-1.762Zm1.762,2.433a2.322,2.322,0,0,0,1.762-2.433,16.7,16.7,0,0,0-1.3-4.248.876.876,0,0,0-.464-.476v-4l8.106-5.5,8.106,5.5v6.86l-.059-.089a.366.366,0,0,0-.586,0l-.7,1.057a.352.352,0,0,0-.059.2v.7h-.7v-.7a.354.354,0,0,0-.059-.2l-.7-1.057a.366.366,0,0,0-.586,0l-.7,1.057a.354.354,0,0,0-.059.2v.7h-.7v-.7a.354.354,0,0,0-.059-.2l-.7-1.057a.366.366,0,0,0-.586,0l-.7,1.057a.354.354,0,0,0-.059.2v4.582H11.436v-6.7a.352.352,0,0,0-.352-.352H7.206a.352.352,0,0,0-.352.352v6.7H4.387ZM14.96,22.171h-.7V17.7l.352-.529.352.529Zm.7-3.172h.7v.7h-.7Zm0,1.41h.7v1.762h-.7Zm2.115,1.762h-.7V17.7l.352-.529.352.529Zm.7-3.172h.7v.7h-.7Zm0,1.41h.7v1.762h-.7Zm-7.753,1.762H7.559V15.827h3.172Zm9.163,0V17.7l.352-.529.352.529v4.475Z" transform="translate(0 0)" fill="#FE980F"/>
                                        <path id="Path_11250" data-name="Path 11250" d="M26.41,23.934h2.115a1.411,1.411,0,0,0,1.41-1.41V20.41A1.411,1.411,0,0,0,28.524,19H26.41A1.411,1.411,0,0,0,25,20.41v2.115A1.411,1.411,0,0,0,26.41,23.934Zm-.7-1.41v-.7h1.41v1.41h-.7A.706.706,0,0,1,25.7,22.524Zm2.819.7h-.7v-1.41h1.41v.7A.706.706,0,0,1,28.524,23.229Zm.7-2.819v.7h-1.41V19.7h.7A.706.706,0,0,1,29.229,20.41Zm-2.819-.7h.7v1.41H25.7v-.7A.706.706,0,0,1,26.41,19.7Z" transform="translate(-14.974 -11.278)" fill="#FE980F"/>
                                        <path id="Path_11251" data-name="Path 11251" d="M23,50h.7v.7H23Z" transform="translate(-13.679 -31.353)" fill="#FE980F"/>
                                    </g>
                                </svg>
                            </div>
                        </div>
                        <div class="dash">
                            <div class="border-rounded-full border-3 border-passive  d_flex j_content_center a_items_center">
                                <svg xmlns="http://www.w3.org/2000/svg" width="28.318" height="27.53" viewBox="0 0 27.318 26.53">
                                    <g id="hotel" transform="translate(-0.081 0.493)">
                                        <path id="Path_11252" data-name="Path 11252" d="M20.338,11.916a7.016,7.016,0,0,0-2.857.6V6.006a.5.5,0,0,0-.5-.5h-.641V3.964a.5.5,0,0,0-.5-.5H14.707v-.6a.5.5,0,0,0-1.008,0v.6H12.543a.5.5,0,0,0-.5.5V5.5h-.673v-2a.5.5,0,0,0-.5-.5h-1.3V1.436a.5.5,0,0,0-.5-.5h-2.9V.011a.5.5,0,0,0-.5-.5.5.5,0,0,0-.5.5V.933H2.384a.5.5,0,0,0-.5.5V3H.585a.5.5,0,0,0-.5.5V22a.5.5,0,0,0,.5.5H14.223a7.06,7.06,0,1,0,6.115-10.587ZM2.888,1.94H8.56V3H2.888ZM7.275,21.5H6.168V18.411H7.275Zm-2.115,0H4.172V18.411H5.16Zm5.2,0H8.283V17.907a.5.5,0,0,0-.5-.5H3.668a.5.5,0,0,0-.5.5V21.5H1.089V4.01h9.27ZM13.047,4.468h2.282V5.5H13.047ZM11.367,21.5V6.51h5.107v6.561a7.109,7.109,0,0,0-1.021.813.506.506,0,0,0-.153-.024H12.565a.5.5,0,0,0,0,1.008H14.6a7.068,7.068,0,0,0-.744,1.313h-1.29a.5.5,0,0,0,0,1.008h.942a7.021,7.021,0,0,0-.223,1.5h-.719a.5.5,0,0,0,0,1.008h.748a7,7,0,0,0,.429,1.8Zm8.972,3.534a6.053,6.053,0,1,1,6.053-6.053,6.053,6.053,0,0,1-6.053,6.053Zm0,0" fill="#AAAFB8"/>
                                        <path id="Path_11253" data-name="Path 11253" d="M47.074,196.5h6.107a.5.5,0,0,0,0-1.008H47.074a.5.5,0,0,0,0,1.008Zm0,0" transform="translate(-44.403 -187.186)" fill="#AAAFB8"/>
                                        <path id="Path_11254" data-name="Path 11254" d="M47.074,259.1h6.107a.5.5,0,0,0,0-1.008H47.074a.5.5,0,0,0,0,1.008Zm0,0" transform="translate(-44.403 -246.983)" fill="#AAAFB8"/>
                                        <path id="Path_11255" data-name="Path 11255" d="M47.074,321.71h6.107a.5.5,0,0,0,0-1.008H47.074a.5.5,0,0,0,0,1.008Zm0,0" transform="translate(-44.403 -306.782)" fill="#AAAFB8"/>
                                        <path id="Path_11256" data-name="Path 11256" d="M47.074,133.89h6.107a.5.5,0,0,0,0-1.008H47.074a.5.5,0,0,0,0,1.008Zm0,0" transform="translate(-44.403 -127.39)" fill="#AAAFB8"/>
                                        <path id="Path_11257" data-name="Path 11257" d="M267.554,264.55h2.626a.5.5,0,0,0,0-1.008h-2.626a.5.5,0,0,0,0,1.008Zm0,0" transform="translate(-254.989 -252.187)" fill="#AAAFB8"/>
                                        <path id="Path_11258" data-name="Path 11258" d="M267.554,210.106h2.626a.5.5,0,0,0,0-1.008h-2.626a.5.5,0,0,0,0,1.008Zm0,0" transform="translate(-254.989 -200.185)" fill="#AAAFB8"/>
                                        <path id="Path_11259" data-name="Path 11259" d="M374.268,342.363h-6.557a.5.5,0,0,0-.5.5V344.4s0,.009,0,.013,0,.009,0,.013a.5.5,0,0,0,.247.433l2.2,2.281v3.981a.5.5,0,0,0,.729.451l1.658-.829a.5.5,0,0,0,.279-.451V347.14l2.2-2.281a.5.5,0,0,0,.247-.433s0-.009,0-.013,0-.009,0-.013v-1.532A.5.5,0,0,0,374.268,342.363Zm-.5,1.008v.551h-5.549v-.551Zm-2.308,3.216a.5.5,0,0,0-.141.35v3.044l-.65.325v-3.369a.5.5,0,0,0-.141-.35l-1.6-1.657h4.132Zm0,0" transform="translate(-350.652 -327.47)" fill="#AAAFB8"/>
                                    </g>
                                </svg>

                            </div>
                        </div>
                        <div class="dash">
                            <div class="border-rounded-full border-3 border-passive  d_flex j_content_center a_items_center">
                                <svg xmlns="http://www.w3.org/2000/svg" width="25.868" height="25.868" viewBox="0 0 25.868 25.868">
                                    <g id="eye" transform="translate(0 -0.001)">
                                        <g id="Group_5452" data-name="Group 5452" transform="translate(0 4.598)">
                                            <g id="Group_5451" data-name="Group 5451">
                                                <path id="Path_11287" data-name="Path 11287" d="M25.69,98.849a33.024,33.024,0,0,0-4.009-3.889C18.467,92.332,15.524,91,12.934,91S7.4,92.332,4.187,94.96A33.019,33.019,0,0,0,.178,98.849a.758.758,0,0,0,0,.975,33.024,33.024,0,0,0,4.009,3.889c3.214,2.628,6.157,3.96,8.747,3.96s5.533-1.332,8.747-3.96a33.025,33.025,0,0,0,4.009-3.889A.758.758,0,0,0,25.69,98.849Zm-12.756,7.308c-4.562,0-9.668-5.187-11.163-6.821,2.653-2.907,7.245-6.82,11.163-6.82,4.562,0,9.667,5.187,11.163,6.821C21.445,102.244,16.852,106.157,12.934,106.157Z" transform="translate(0 -91)" fill="#AAAFB8"/>
                                            </g>
                                        </g>
                                        <g id="Group_5454" data-name="Group 5454" transform="translate(7.629 7.629)">
                                            <g id="Group_5453" data-name="Group 5453">
                                                <path id="Path_11288" data-name="Path 11288" d="M160.852,155.547a.758.758,0,0,0-.758.758,3.789,3.789,0,1,1-3.789-3.789.758.758,0,0,0,0-1.516,5.3,5.3,0,1,0,5.3,5.3A.758.758,0,0,0,160.852,155.547Z" transform="translate(-151 -151)" fill="#AAAFB8"/>
                                            </g>
                                        </g>
                                        <g id="Group_5456" data-name="Group 5456" transform="translate(0 20.241)">
                                            <g id="Group_5455" data-name="Group 5455">
                                                <path id="Path_11289" data-name="Path 11289" d="M5.406,400.844a.758.758,0,0,0-1.072,0L.222,404.955a.758.758,0,0,0,1.072,1.072l4.111-4.112A.758.758,0,0,0,5.406,400.844Z" transform="translate(-0.001 -400.622)" fill="#AAAFB8"/>
                                            </g>
                                        </g>
                                        <g id="Group_5458" data-name="Group 5458" transform="translate(20.241 20.241)">
                                            <g id="Group_5457" data-name="Group 5457">
                                                <path id="Path_11290" data-name="Path 11290" d="M406.027,404.955l-4.112-4.112a.758.758,0,1,0-1.072,1.072l4.112,4.112a.758.758,0,0,0,1.072-1.072Z" transform="translate(-400.622 -400.622)" fill="#AAAFB8"/>
                                            </g>
                                        </g>
                                        <g id="Group_5460" data-name="Group 5460" transform="translate(0 0.001)">
                                            <g id="Group_5459" data-name="Group 5459" transform="translate(0)">
                                                <path id="Path_11291" data-name="Path 11291" d="M5.406,4.334,1.294.222A.758.758,0,0,0,.222,1.294L4.334,5.406A.758.758,0,0,0,5.406,4.334Z" transform="translate(-0.001 -0.001)" fill="#AAAFB8"/>
                                            </g>
                                        </g>
                                        <g id="Group_5462" data-name="Group 5462" transform="translate(20.241 0.001)">
                                            <g id="Group_5461" data-name="Group 5461">
                                                <path id="Path_11292" data-name="Path 11292" d="M406.027.222a.758.758,0,0,0-1.072,0l-4.112,4.111a.758.758,0,1,0,1.072,1.072l4.112-4.111A.758.758,0,0,0,406.027.222Z" transform="translate(-400.622 -0.001)" fill="#AAAFB8"/>
                                            </g>
                                        </g>
                                        <g id="Group_5464" data-name="Group 5464" transform="translate(10.661 10.661)">
                                            <g id="Group_5463" data-name="Group 5463">
                                                <path id="Path_11293" data-name="Path 11293" d="M213.274,211a2.274,2.274,0,1,0,2.274,2.274A2.276,2.276,0,0,0,213.274,211Zm0,3.031a.758.758,0,1,1,.758-.758A.759.759,0,0,1,213.274,214.031Z" transform="translate(-211 -211)" fill="#AAAFB8"/>
                                            </g>
                                        </g>
                                    </g>
                                </svg>

                            </div>
                        </div>
                        <div class="dash">
                            <div class="border-rounded-full border-3 border-passive  d_flex j_content_center a_items_center">
                                <svg xmlns="http://www.w3.org/2000/svg" width="23.5" height="17.671" viewBox="0 0 23.5 17.671">
                                    <g id="_001-check" data-name="001-check" transform="translate(0 -63.498)">
                                        <g id="Group_5668" data-name="Group 5668" transform="translate(0 63.498)">
                                            <path id="Path_11352" data-name="Path 11352" d="M23.157,63.7a.918.918,0,0,0-1.291.141L9.652,79.043a.918.918,0,0,1-1.3.041l-6.8-6.517a.918.918,0,0,0-1.27,1.325l6.8,6.52a2.73,2.73,0,0,0,1.894.758l.114,0a2.732,2.732,0,0,0,1.941-.915l.032-.037L23.3,64.991A.918.918,0,0,0,23.157,63.7Z" transform="translate(0 -63.498)" fill="#AAAFB8"/>
                                        </g>
                                    </g>
                                </svg>

                            </div>
                        </div>
                    </div>
                    <div class="row" style="margin: 0">
                        <div class="col-3 font-segeo font-default font-13" style="text-align: center;padding:5px 25px">{{ t('cabinet.Ընտրել անշարժ գույքի տեսակը') }}</div>
                        <div class="col-3 font-segeo font-passive font-13" style="text-align: center;padding:5px 25px">{{ t('cabinet.Ավելացնել մանրամասներ') }}</div>
                        <div class="col-3 font-segeo font-passive font-13" style="text-align: center;padding:5px 25px">{{ t('cabinet.Տեսնել Վերջնակն Արդյունքը') }}</div>
                        <div class="col-3 font-segeo font-passive font-13" style="text-align: center;padding:5px 25px">{{ t('cabinet.Գույքը ավելացված է') }}</div>
                    </div>
                    <div class="">
                        <div class="real-estate-sale__block">

                        </div>
                    </div>
                </div>
            </div>

        </div>

    </div>
    @include('site.layouts.footer')

@endsection
@push('js')
    <script src="{{ asset('f/site/js/jquery.formstyler.min.js') }}"></script>
    <script>
        $('select').styler();
    </script>
@endpush
