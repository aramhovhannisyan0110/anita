@extends('site.layouts.header')
@section('content')
    <div class="main w-full">
        <div class="w-full bg-white">
            <div class="container">
                <div class="login row"><p class="col-12 py-1">{{t('cabinet.Ընտրեք օգտատիրոջ տեսակը')}}</p></div>
            </div>
        </div>
        <div class="container mt-4">
            <div class="row">
                <div class="col-12 col-md-5 col-lg-4 col-xl-3">
                    @component('site.components.cabinet_link',['active'=>$active??null])@endcomponent
                </div>
                <div class="col-12 col-md-7 col-lg-8 col-xl-9">
                    <div class="container-fluid">
                        <div class="row">
                            @if(!empty($user->orders) && count($user->orders))
                                <div class="col-12 d_flex j_content_center a_items_center text-center font-segeo font-bolder font-logo-orange font-24 pb-4">
                                    <span>
                                        {{ t('cabinet.Գնումների պատմություն') }}
                                    </span>
                                </div>
                                <div class="col-12  ">
                                    <div class="row bg-white ">
                                        <div class="col-3" style="padding: 0!important;">
                                            <div class="shadowed bg-white p-2 font-logo-orange font-bold font-segeo">
                                                {{t('cabinet.ID')}}
                                            </div>
                                        </div>
                                        <div class="col-3" style="padding: 0!important;">
                                            <div class="shadowed bg-white p-2 font-logo-orange font-bold font-segeo">
                                                {{t('cabinet.Amount')}}
                                            </div>
                                        </div>
                                        <div class="col-3" style="padding: 0!important;">
                                            <div class="shadowed bg-white p-2 font-logo-orange font-bold font-segeo">
                                                {{t('cabinet.Price')}}
                                            </div>
                                        </div>
                                        <div class="col-3" style="padding: 0!important;">
                                            <div class="shadowed bg-white p-2 font-logo-orange font-bold font-segeo">
                                                {{t('cabinet.Date')}}
                                            </div>
                                        </div>
                                        @foreach($user->orders as $order)
                                            <div class="col-3" style="padding: 0">
                                                <div class="shadowed bg-white p-2 font-default font-default font-segeo">
                                                    {{$order->id}}
                                                </div>
                                            </div>
                                            <div class="col-3" style="padding: 0">
                                                <div class="shadowed bg-white p-2 font-default font-default font-segeo">
                                                    {{$order->amount}}
                                                </div>
                                            </div>
                                            <div class="col-3" style="padding: 0">
                                                <div class="shadowed bg-white p-2 font-default font-default font-segeo">
                                                    {{$order->status}}
                                                </div>
                                            </div>
                                            <div class="col-3" style="padding: 0">
                                                <div class="shadowed bg-white p-2 font-default font-default font-segeo">
                                                    {{$order->created_at}}
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            @else
                                <div class="col-12 d_flex j_content_center a_items_center text-center font-segeo font-bolder font-logo-orange font-24">
                                    <span>
                                        {{ t('cabinet.Դատարկ պատմություն') }}
                                    </span>
                                </div>
                            @endif

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @if ($errors->has('global'))
        <div class="error-notify">
            {{ t('auth.oop`s') }}
        </div>
    @elseif(session('payment') == 'success')
        <div class="success-notify">
            {{ t('payment.success') }}
        </div>
    @endif
@endsection
@push('css')
    @css(aSite('css/jquery.modal.min.css'))
@endpush
@push('js')
    @js(aSite('js/jquery.modal.min.js'))
@endpush

