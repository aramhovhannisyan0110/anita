@extends('site.layouts.header')
@section('content')
    <div class="main w-full">
        <div class="w-full bg-white">
            <div class="container">
                <div class="login row"><p class="col-12 py-1">{{t('cabinet.Ընտրեք օգտատիրոջ տեսակը')}}</p></div>
            </div>
        </div>
        <div class="container mt-4">
            <div class="row">
                <div class="col-12 col-md-5 col-lg-4 col-xl-3">
                    @component('site.components.cabinet_link',['active'=>$active??null])@endcomponent
                </div>
                <div class="col-12 col-md-7 col-lg-8 col-xl-9 font-segeo font-13">
                    <div class="tabbedPanels w-full">
                        <ul class="tabs">
                            <li><a href="#panel1">{{t('cabinet.Ակտիվ Հայտարարություններ')}}</a></li>
                            <li><a href="#panel2">{{t('cabinet.Մոդեռացիայի Հայտարարություններ')}}</a></li>
                            <li><a href="#panel3">{{t('cabinet.Հայտարարությունների Արխիվ')}}</a></li>
                            <li><a href="#panel4">{{t('cabinet.Արգելափակված Հայտարարություններ')}}</a></li>
                        </ul>
                        <div class="panelContainer shadowed">
                            <div id="panel1" class="panel">
                                @if(!empty($active_announcements) && count($active_announcements))
                                    <div class="sale-apartment__info">
                                        @foreach($active_announcements as $item)
                                            <div class="items"  id="item-{{$item->id}}" style="background-color: #f7f7f7;">
                                                @component('site.components.cabinet_estate_card', ['item'=>$item,'favorites'=>$favorites,'top'=>($item->top)?true:false])@endcomponent
                                            </div>
                                            <div id="block-{{$item->id}}" class="container-fluid pb-3 pack_block">
                                                <div class="row">
                                                    @if(!empty($tops) && count($tops))
                                                        <div class="col-12 col-lg-4">
                                                            <div
                                                                class="font-16 font-bold font-default my-2">{{ t('cabinet.Բարձրացնել թոփ') }}</div>
                                                            @if($item->top)
                                                                <div
                                                                    class="privacy_policy d_flex a_items_center j_content_start mb-1 flex-wrap">
                                                                    <p>{!! t('cabinet.Ակտիվ է մինչև՝ &nbsp') . (array_key_exists($item->id, $top) ? $top[$item->id] : '' )  !!} </p>
                                                                </div>
                                                            @else
                                                                @foreach($tops as $top)
                                                                    <div
                                                                        class="privacy_policy d_flex a_items_center j_content_start mb-1 flex-wrap">
                                                                        <input type="radio" name="to_top[{{$item->id}}]"
                                                                               class="criteria filter_check_inp"
                                                                               id="to_top_{{$top->id}}_{{$item->id}}"
                                                                               data-id="{{$top->id}}"
                                                                               data-day="{{$top->day}}"
                                                                               data-price="{{$top->price}}"
                                                                               data-for="top">
                                                                        <label class="cursor-pointer filter_check_label"
                                                                               for="to_top_{{$top->id}}_{{$item->id}}"
                                                                               data-content="{{ $top->day.t('cabinet.օր') . '-'  .$top->price. t('cabinet.դր․')}}">{{ $top->day.t('cabinet.օր') . '-'  .$top->price. t('cabinet.դր․')}}</label>
                                                                    </div>
                                                                @endforeach
                                                            @endif
                                                        </div>
                                                    @endif
                                                    @if(!empty($homes) && count($homes))
                                                        <div class="col-12 col-lg-4">
                                                            <div
                                                                class="font-16 font-bold font-default my-2">{{ t('cabinet.Ցույց տալ գլխավոր էջում') }}</div>
                                                            @if($item->block_1)
                                                                <div
                                                                    class="privacy_policy d_flex a_items_center j_content_start mb-1 flex-wrap">
                                                                    <p>{!! t('cabinet.Ակտիվ է մինչև՝ &nbsp') . (array_key_exists($item->id, $home) ? $home[$item->id] : '' )  !!} </p>
                                                                </div>
                                                            @else
                                                                @foreach($homes as $top)
                                                                    <div
                                                                        class="privacy_policy d_flex a_items_center j_content_start mb-1 flex-wrap">
                                                                        <input type="radio"
                                                                               name="to_home[{{$item->id}}]"
                                                                               class="criteria filter_check_inp"
                                                                               id="to_home_{{$top->id}}_{{$item->id}}"
                                                                               data-id="{{$top->id}}"
                                                                               data-day="{{$top->day}}"
                                                                               data-price="{{$top->price}}"
                                                                               data-for="home">
                                                                        <label class="cursor-pointer filter_check_label"
                                                                               for="to_home_{{$top->id}}_{{$item->id}}"
                                                                               data-content="{{ $top->day.t('cabinet.օր') . '-'  .$top->price. t('cabinet.դր․')}}">{{ $top->day.t('cabinet.օր') . '-'  .$top->price. t('cabinet.դր․')}}</label>
                                                                    </div>
                                                                @endforeach
                                                            @endif
                                                        </div>
                                                    @endif
                                                    @if(!empty($urgents) && count($urgents))
                                                        <div class="col-12 col-lg-4">
                                                            <div
                                                                class="font-16 font-bold font-default my-2">{{ t('cabinet.Կցել շտապ սթիքեր ') }}</div>
                                                            @if($item->urgent)
                                                                <div
                                                                    class="privacy_policy d_flex a_items_center j_content_start mb-1 flex-wrap">
                                                                    <p>{!! t('cabinet.Ակտիվ է մինչև՝ &nbsp') . (array_key_exists($item->id, $urgent) ? $urgent[$item->id] : '' )  !!} </p>
                                                                </div>
                                                            @else
                                                                @foreach($urgents as $top)
                                                                    <div
                                                                        class="privacy_policy d_flex a_items_center j_content_start mb-1 flex-wrap">
                                                                        <input type="radio"
                                                                               name="to_urgent[{{$item->id}}]"
                                                                               class="criteria filter_check_inp"
                                                                               id="to_urgent_{{$top->id}}_{{$item->id}}"
                                                                               data-id="{{$top->id}}"
                                                                               data-day="{{$top->day}}"
                                                                               data-price="{{$top->price}}"
                                                                               data-for="urgent">
                                                                        <label class="cursor-pointer filter_check_label"
                                                                               for="to_urgent_{{$top->id}}_{{$item->id}}"
                                                                               data-content="{{ $top->day.t('cabinet.օր') . '-'  .$top->price. t('cabinet.դր․')}}">{{ $top->day.t('cabinet.օր') . '-'  .$top->price. t('cabinet.դր․')}}</label>
                                                                    </div>
                                                                @endforeach
                                                            @endif
                                                        </div>
                                                    @endif
                                                </div>
                                                <div class="send_button d_flex j_content_end"
                                                     style="margin: 0;margin-top: 10px">
                                                    <div>
                                                        <a href="#ex{{$item->id}}" rel="modal:open"
                                                           class="w-full mt-2">
                                                            <button data-val="{{$item->id}}"
                                                                    class="w-full p-2 bg-default font-white font-bold buy_pack">
                                                                <i
                                                                    class="fas fa-paper-plane"></i> {{t('cabinet.Ձեռք բերել նշված փաթեթները')}}
                                                            </button>
                                                        </a>
                                                        <div id="ex{{$item->id}}" class="modal" style="height: auto">
                                                            <div class="row">
                                                                <div class="col-12">
                                                                    <p class="font-16 font-default font-segeo font-bold">{{t('cabinet.Պատվերի Վերանայում')}}</p>
                                                                    <p class="font-13 font-gray font-segeo font-bold my-2">{{t('cabinet.Խնդրում ենք ուշադիր դիտել ձեր գնվող ծառայություները')}}</p>
                                                                </div>
                                                                <div
                                                                    class="col-12 col-md-4 cont_for_top  f_direction_column j_content_between">
                                                                    <p class="font-16 font-default font-segeo font-bold">{{ t('cabinet.Բարձրացնել թոփ') }}</p>
                                                                    <p class="for_top font-13 font-logo-orange font-segeo font-bold"></p>
                                                                </div>
                                                                <div
                                                                    class="col-12 col-md-4 cont_for_home  f_direction_column j_content_between">
                                                                    <p class="font-16 font-default font-segeo font-bold">{{ t('cabinet.Ցույց տալ գլխավոր էջում') }}</p>
                                                                    <p class="for_home font-13 font-logo-orange font-segeo font-bold"></p>
                                                                </div>
                                                                <div
                                                                    class="col-12 col-md-4 cont_for_urgent  f_direction_column j_content_between">
                                                                    <p class="font-16 font-default font-segeo font-bold">{{ t('cabinet.Կցել շտապ սթիքեր ') }}</p>
                                                                    <p class="for_urgent font-13 font-logo-orange font-segeo font-bold"></p>
                                                                </div>
                                                            </div>
                                                            <form action="{{route('cabinet.estates.get_packages')}}"
                                                                  method="POST">
                                                                @csrf
                                                                <input type="hidden" name="home_package"
                                                                       id="home_package{{$item->id}}">
                                                                <input type="hidden" name="top_package"
                                                                       id="top_package{{$item->id}}">
                                                                <input type="hidden" name="urgent_package"
                                                                       id="urgent_package{{$item->id}}">
                                                                <input type="hidden" name="estate_id"
                                                                       id="estate_id{{$item->id}}">
                                                                <button type="submit"
                                                                        class="w-full py-2 bg-default font-white font-bold mt-2">{{t('cabinet.Ձեռք բերել փաթեթը')}}</button>
                                                            </form>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                @else
                                    <p class="font-bold font-16 font-segeo py-5 font-default"
                                       style="text-align: center">{{t('cabinet.Հայտարարություններ չկան')}}</p>
                                @endif
                            </div>
                            <div id="panel2" class="panel">
                                @if(!empty($pending_announcements) && count($pending_announcements))
                                    <div class="sale-apartment__info">
                                        @foreach($pending_announcements as $item)
                                            <div class="cabinet_annc">
                                                <div class="items"  id="item-{{$item->id}}" style="background-color: #f7f7f7;">
                                                    @component('site.components.cabinet_estate_card', ['item'=>$item,'favorites'=>$favorites,'top'=>($item->top)?true:false])@endcomponent
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                @else
                                    <p class="font-bold font-16 font-segeo py-5 font-default"
                                       style="text-align: center">{{t('cabinet.Հայտարարություններ չկան')}}</p>
                                @endif
                            </div>
                            <div id="panel3" class="panel">
                                @if(!empty($archive_announcements) && count($archive_announcements))
                                    <div class="sale-apartment__info">
                                        @foreach($archive_announcements as $item)
                                            <div class="items"  id="item-{{$item->id}}" style="background-color: #f7f7f7;">
                                                @component('site.components.cabinet_estate_card', ['item'=>$item,'favorites'=>$favorites,'top'=>true])@endcomponent
                                            </div>
                                        @endforeach
                                        @else
                                            <p class="font-bold font-16 font-segeo py-5 font-default"
                                               style="text-align: center">{{t('cabinet.Հայտարարություններ չկան')}}</p>
                                        @endif
                                    </div>  <!-- end panel 3 -->
                                    <div id="panel4" class="panel">
                                        @if(!empty($declined_announcements) && count($declined_announcements))
                                            <div class="sale-apartment__info">
                                                @foreach($declined_announcements as $item)
                                                    <div class="items" id="item-{{$item->id}}" style="background-color: #f7f7f7;">
                                                        @component('site.components.cabinet_estate_card', ['item'=>$item,'favorites'=>$favorites,'top'=>true])@endcomponent
                                                    </div>
                                                @endforeach
                                            </div>
                                        @else
                                            <p class="font-bold font-16 font-segeo py-5 font-default"
                                               style="text-align: center">{{t('cabinet.Հայտարարություններ չկան')}}</p>
                                        @endif
                                    </div>  <!-- end panel 4 -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @if ($errors->has('global'))
            <div class="error-notify">
                {{ t('auth.oop`s') }}
            </div>
        @elseif(session('payment') == 'success')
            <div class="success-notify">
                {{ t('payment.success') }}
            </div>
        @endif
        @endsection
        @push('css')
            @css(aSite('css/jquery.modal.min.css'))
            <style>
                .verticalCard {
                    flex-direction: column !important;
                    flex-wrap: nowrap !important;
                }

                .verticalCard .items {
                    padding: 0 !important;

                }

                .cont_for_top, .cont_for_home, .cont_for_urgent {
                    display: none;
                    height: auto;
                    min-width: 33.3%;
                }

                .modal {
                    width: fit-content;
                    max-width: 90%;
                }

                .apartment-info__text table td .opt, .best-offers__info table td .opt {
                    z-index: 1;
                }

                .send_button button {
                    font: 13px "Segoe UI", HelveticaNeueCyr-Bold, Helvetica, Roboto, sans-serif;
                    border: 2px solid #FE980F;
                    transition: .3s;
                    background: #FE980F;
                    color: #fff;
                }

                .send_button button:hover {
                    transition: .3s;
                    background: #ffffff;
                    color: #FE980F;
                }

                .cabinet_annc {
                    box-shadow: 2px 3px 4px rgba(0, 95, 162, .16);
                    margin-bottom: 15px;

                }

                .cabinet_annc:hover {
                    box-shadow: 5px 5px 20px rgba(0, 95, 162, .16);
                }

                .best-offers__block {
                    box-shadow: unset !important;
                }

                .best-offers__info {
                    margin-top: 0;
                    border-top: 1px solid #70707026;
                    border-bottom: 1px solid #70707026;
                }

                .filter_check_label::before {
                    color: #D39831 !important;
                }

                .filter_check_inp:checked {
                    border-color: #D39831;
                    background: #D39831;
                }

                .panelContainer {
                    background-color: #fff;
                    padding: 10px;
                }

                .tabs {
                    margin: 0;
                    padding: 0;
                    list-style: none;
                }

                .tabs {
                    display: flex;
                    justify-content: flex-start;
                    padding: 0;
                    margin: 0;
                    text-align: center;
                }

                .tabs a {
                    display: block;
                    text-decoration: none;
                    color: #042A3D;
                    padding: 8px;
                    margin-right: 4px; /* spaces out the tabs  */
                    background-color: #F0F1F2;
                }

                .tabs a.active {
                    background-color: #fff;
                    border-top-left-radius: 5px;
                    border-top-right-radius: 5px;
                    color: #cc8202;
                    /*font-weight: bold;*/
                    box-shadow: 0 -2px 5px 1px #d3d3d36e;
                }

                /*.panel img {*/
                /*    margin-top: 10px;*/
                /*}*/

                .panel p {
                    margin-bottom: 0px;
                }

                element.style {
                }
                .sale-apartment__info:not(.verticalCard) .best-offers__box a {
                    min-height: unset;
                }
                .best-offers__block:hover a {
                    color: #cc8200 !important;
                }
                .best-offers__heart1 a {
                    width: 35px;
                    height: 35px;
                    border-radius: 50%;
                    background: #fff;
                    box-shadow: 2px 3px 4px rgba(0, 95, 162, .16);
                    display: flex;
                    justify-content: center;
                    align-items: center;
                }
            </style>
            @css(aSite('css/estate_list.css'))
        @endpush
        @push('js')
            @js(aSite('js/jquery.modal.min.js'))
            <script>
                $(document).ready(function () {
                    function cardPosition() {
                        if ($(window).width() < 992) {
                            $('.sale-apartment__info').addClass('verticalCard')
                        } else {
                            $('.sale-apartment__info').removeClass('verticalCard')
                        }
                    }

                    cardPosition();
                    $(window).resize(function () {
                        cardPosition();
                    })


                    $('.buy_pack').click(function () {
                        var button = $(this);
                        var estate = button.data('val');
                        $('.for_top, .for_home, .for_urgent').html('')
                        $('.cont_for_top, .cont_for_home, .cont_for_urgent').hide()
                        var inputs = $(this).closest('.pack_block').find('input:checked');
                        inputs.each(function () {
                            $('#estate_id' + estate + '').val(button.data('val'));
                            if ($(this).data('for') == 'top') {
                                $('.cont_for_top').css('display', "flex");
                                $('.for_top').html($(this).data('day') + "{{t('cabinet.օր') . '-' }}" + $(this).data('price') + "{{t('cabinet.դր․')}}")
                                $('#top_package' + estate + '').val($(this).data('id'));
                            } else if ($(this).data('for') == 'home') {
                                $('.cont_for_home').css('display', "flex");
                                $('.for_home').html($(this).data('day') + "{{t('cabinet.օր') . '-' }}" + $(this).data('price') + "{{t('cabinet.դր․')}}")
                                $('#home_package' + estate + '').val($(this).data('id'));
                            } else if ($(this).data('for') == 'urgent') {
                                $('.cont_for_urgent').css('display', "flex");
                                $('.for_urgent').html($(this).data('day') + "{{t('cabinet.օր') . '-' }}" + $(this).data('price') + "{{t('cabinet.դր․')}}")
                                $('#urgent_package' + estate + '').val($(this).data('id'));

                            }
                        })
                    })
                    $('.tabs a').click(function () {
                        $('.panel').hide();
                        $('.tabs a.active').removeClass('active');
                        $(this).addClass('active');
                        var panel = $(this).attr('href');
                        $(panel).fadeIn(1000);
                        return false;  // prevents link action
                    });  // end click
                    $('.tabs li:first a').click();
                    $('#delete').on('click', function () {
                        var id = $(this).data('id')
                        $.ajax({
                            url: '{!! route('cabinet.delete') !!}',
                            type: 'delete',
                            dataType: 'json',
                            data: {
                                _token: "{!! csrf_token() !!}",
                                id: id,
                            },
                            error: function (e) {
                                console.log('error');
                            },
                            success: function (e) {
                                if (e.success){
                                    $('#item-'+ e.id +'').fadeOut();
                                    $('#block-'+ e.id +'' ).fadeOut();
                                }
                            }
                        });
                    })
                }); // end ready
            </script>
    @endpush
