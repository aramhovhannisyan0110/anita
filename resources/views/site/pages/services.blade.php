@extends('site.layouts.header')
@section('content')
    <section style="padding: 7px 0;
    background: white;
    border-top: 3px solid #f8f6f6;">
        <div class="container">
            <div class="product-det__title">
                <h1>{{t('app.Մեր ծառայությունները')}}</h1>
            </div>
        </div>
    </section>
    <section>
        <div class="container">
            <div class="services-block-fon">
                <div class="services-bg" style="position: relative;">
                    <img style="position: relative" src="{{$content->header->image()}}" alt="">
                    <div class="d_flex align-items-center " style="position: absolute;width: 100%;height: 100%;align-items: center;
    justify-content: center;">

                        <img src="{{$content->header->image2()}}" alt="png">
                    </div>
                </div>
                <div class="about-our-company__title">

                    <h2>{{$content->content->title}}</h2>
                    <p>
                        {!! $content->content->desc !!}
                    </p>
                </div>

                @if(!empty($items))
                    @foreach($items as $item)
                        <div class="name-of-services">
                            <div class="name-of-services__title d_flex a_items_center">
                                <div class="name-of-services__key">
                                    <img src="{{asset('u/services/'.$item->image)}}" alt="png">
                                </div>
                                <strong>{{$item->title}}</strong>
                            </div>
                            <p>
                                {!! $item->desc !!}
                            </p>
                            <div class="name-services__gallery">
                                @if(count($item->galleries))
                                    @foreach($item->galleries as $gallery)
                                        <div class="name-services__img">
                                            <a href="{{asset('u/gallery/'.$gallery->image)}}" data-fancybox="gallery">
                                                <img src="{{asset('u/gallery/thumbs/'.$gallery->image)}}" alt="jpg">
                                            </a>
                                        </div>
                                    @endforeach
                                @endif
                            </div>
                        </div>
                    @endforeach
                @endif
            </div>
        </div>
    </section>
    @include('site.layouts.footer')
    @push('css')
        <link rel="stylesheet" href="{{ asset('f/site/css/jquery.fancybox.css') }}">
    @endpush
    @push('js')
        <script src="{{ asset('f/site/js/jquery.fancybox.js') }}"></script>
    @endpush
@endsection
