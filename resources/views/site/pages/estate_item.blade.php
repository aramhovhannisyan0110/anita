@extends('site.layouts.header')
@section('content')
    <section style="padding: 7px 0;
    background: white;
    border-top: 3px solid #f8f6f6;">
        <div class="container">
            <div class="product-det__title">
                <h1>{{$part.'. '}}  {{$item->title}}</h1>
            </div>
        </div>
    </section>
    <section>

        <div class="container">
            <div class="product-det">
                <div class="product-det__min d_flex j_content_between">
                    <div class="product-det__left">
                        <div class="product-det__main d_flex j_content_between a_items_end">
                            <div class="product-det__title">
                                @if(!empty(\App\Models\Location::getLocationTitle($item->locations->locations_id??null)))
                                    <p>
                                        <i style="color: #cc8202"
                                           class="fas fa-map-marker-alt"></i> {{ \App\Models\Location::getLocationTitle($item->locations->locations_id) }}
                                    </p>
                                @endif
                            </div>
                            <div class="product-det__heart {{ (in_array($item->id,$favorites))?'active-heart':'' }}"
                                 data-id="{{$item->id}}">
                                <a href="javascript:;">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24.716" height="21.99"
                                         viewBox="0 0 24.716 21.99">
                                        <path id="heart"
                                              d="M22.756,2.152A6.646,6.646,0,0,0,17.812,0a6.218,6.218,0,0,0-3.884,1.341,7.945,7.945,0,0,0-1.57,1.639,7.941,7.941,0,0,0-1.57-1.639A6.217,6.217,0,0,0,6.9,0,6.647,6.647,0,0,0,1.961,2.152,7.726,7.726,0,0,0,0,7.428,9.2,9.2,0,0,0,2.452,13.45a52.273,52.273,0,0,0,6.136,5.76c.85.725,1.814,1.546,2.815,2.421a1.451,1.451,0,0,0,1.911,0c1-.875,1.965-1.7,2.816-2.422a52.244,52.244,0,0,0,6.136-5.759,9.2,9.2,0,0,0,2.451-6.022,7.725,7.725,0,0,0-1.961-5.276Zm0,0"
                                              transform="translate(0)" fill="#d9e2e9"/>
                                    </svg>
                                </a>
                            </div>
                        </div>
                        <div class="product-det__price">
                            <strong>{{($item->price == -1)? t('app.Պայմանագրային')  :number_format($item->price??null ,0, ',', ' ').' $'}} </strong>
                        </div>
                        <div class="product-det__slid">
                            <div class="slider-for-photo">
                                <div class="slider-for-photo__box ">
                                    <div class="for-box__photo">
                                        @if(!empty($item->galleries->where('poster',1)->first()) && count($item->galleries->where('poster',1)))
                                            <img width="100%"
                                                 src="{{ $item->galleries->where('poster',1)->first()->image(false) }}"
                                                 alt="">
                                        @elseif(count($item->galleries))
                                            <img width="100%"
                                                 src="{{ $item->galleries->first()->image(false) }}" alt="">
                                        @else
                                            <img width="100%"
                                                 src="{{ asset('f/site/img/default.jpg') }}" alt="">
                                        @endif
                                    </div>
                                </div>
                                @foreach($galleries as $gallery)
                                    @if(empty($item->galleries->where('poster',1)->first()) && !count($item->galleries->where('poster',1)) && $loop->first)
                                    @elseif($gallery->poster === '0')
                                        <div class="slider-nav-photo__box ">
                                            <div class="for-box__photo">
                                                <img width="100%" src="{{asset($gallery->image(false))}}" alt="png">
                                            </div>
                                        </div>
                                    @endif
                                @endforeach
                            </div>
                            @if(count($galleries))
                                <div class="slider-nav-photo ">
                                    <div class="slider-nav-photo__box  small_gallery_block">
                                        <div class="nav-box__photo">
                                            @if(!empty($item->galleries->where('poster',1)->first()) && count($item->galleries->where('poster',1)))
                                                <img width="100%"
                                                     src="{{ $item->galleries->where('poster',1)->first()->image(true) }}"
                                                     alt="">
                                            @elseif(count($item->galleries))
                                                <img width="100%"
                                                     src="{{ $item->galleries->first()->image(true) }}" alt="">
                                            @else
                                                <img width="100%"
                                                     src="{{ asset('f/site/img/default.jpg') }}" alt="">
                                            @endif
                                        </div>
                                    </div>
                                    @foreach($galleries as $gallery)
                                        @if(empty($item->galleries->where('poster',1)->first()) && !count($item->galleries->where('poster',1)) && $loop->first)
                                        @elseif($gallery->poster === '0')
                                            <div class="slider-nav-photo__box small_gallery_block">
                                                <div class="nav-box__photo">
                                                    <img src="{{asset('u/gallery/'.$gallery->image)}}" alt="png">
                                                </div>
                                            </div>
                                        @endif
                                    @endforeach
                                </div>
                            @endif
                        </div>
                        <div
                            style="display: flex; justify-content: flex-end;color: #FE980F;;font: 15px Segoe UI, HelveticaNeueCyr-Bold, Helvetica,Roboto, sans-serif;font-weight: 700;line-height: 1.3;">{{ t('app.publication_date') }}
                            :
                            <i style="font-size: 15px;margin: 2px 7px; margin-right: 2px"
                               class="fas fa-calendar-alt"></i>
                            {{date('d',strtotime($item->created_at))}} {{t('months.'.date('M',strtotime($item->created_at)).'')}} {{date('Y',strtotime($item->created_at))}}
                        </div>
                        <div class="features__min">
                            <h2>{{t('app.Մանրամասներ')}} </h2>
                            <div class="param_table">
                                <div class="features__min__info best-offers__info block1"
                                     style="margin-top: 0!important;margin-bottom: 0!important;">
                                    <table>
                                        @if(!empty($item->estate_filter) && count($item->estate_filter))
                                            @foreach($item->estate_filter as $filter)
                                                @if($filter->filter->have_interval)
                                                    @if(!empty($filter->value))
                                                        <tr>
                                                            <td>
                                                                <div><span class="opt"
                                                                           title="{{$filter->filter->title}}">{{$filter->filter->title}} <span
                                                                            class="delimeter">  </span> <span style="">..........................................................................................................</span> </span>
                                                                </div>
                                                            </td>
                                                            <td>
                                                                <div>
                                                                    <b>{!! ($filter->filter->price_filter && $filter->value == -1)? t('app.Պայմանագրային') : $filter->value .( (!empty($filter->filter->metric) && (int)substr($filter->filter->metric, -1)!=0)?substr($filter->filter->metric, 0, -1).'<sup>'.intval(substr($filter->filter->metric, -1)).'</sup>':((!empty($filter->filter->metric))?$filter->filter->metric:'')) !!}</b><span
                                                                        class="delimeter"><b>;</b>&nbsp;&nbsp;&nbsp; </span>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    @endif
                                                @else
                                                    @if(!empty($filter->filter->option) && count($filter->filter->option))
                                                        {{--                                                                            @dd($filter->filter->option)--}}
                                                        @foreach($filter->filter->option as $option)
                                                            @if($filter->option_id == $option->id)
                                                                <tr>
                                                                    <td data-val="{{$filter->option_id}}">
                                                                        <div><span class="opt"
                                                                                   title="{{$filter->filter->title}}">{{$filter->filter->title}} <span
                                                                                    class="delimeter">  </span> <span
                                                                                    style="">..........................................................................................................</span> </span>
                                                                        </div>
                                                                    </td>
                                                                    <td>
                                                                        <div>
                                                                            <b>{{(json_decode($option->options)->{app()->getLocale()})??null}}{!! (!empty($filter->filter->metric) && (int)substr($filter->filter->metric, -1)!=0)?substr($filter->filter->metric, 0, -1).'<sup>'.intval(substr($filter->filter->metric, -1)).'</sup>':((!empty($filter->filter->metric))?$filter->filter->metric:'')  !!}</b><span
                                                                                class="delimeter"><b>;</b>&nbsp;&nbsp;&nbsp;  </span>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                            @endif
                                                        @endforeach
                                                    @endif
                                                @endif
                                            @endforeach
                                        @endif
                                        <tr>
                                            <td>
                                                <div><span class="opt" title="{{ t('app.Կոդ:') }} "> {{ t('app.Կոդ:') }}  <span
                                                            class="delimeter">  </span> <span style="">..........................................................................................................</span>  </span>
                                                </div>
                                            </td>
                                            <td><b>{{$item->code??null}}</b><span class="delimeter"><b>;</b> </span>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <div class="best-offers__info block2" style="margin-top: 0!important;">
                                    <table></table>
                                </div>
                            </div>
                        </div>
                        @if(!empty($item->description))
                            <div class="features__min">
                                <h2>{{ t('app.Նկարագրություն') }} </h2>
                                <p>
                                    {!! $item->description !!}
                                </p>
                            </div>
                        @endif

{{--                        <div class="features__min">--}}
{{--                            <h2>{{ t('app.Տվյալներ') }} </h2>--}}
{{--                            <div class="features-form__data">--}}
{{--                                <form action="{{ route('contacts.send_request') }}" method="post" id="contact-form">--}}
{{--                                    @csrf--}}
{{--                                    <input type="hidden" name="code" value="{{$item->code}}">--}}
{{--                                    <input type="hidden" name="block" value="{{$part}}">--}}
{{--                                    <div class="features-form d_flex f_wrap">--}}
{{--                                        <div class="features-form__inp">--}}
{{--                                            <input name="name" type="text" placeholder="{{ t('app.Անուն') }}"--}}
{{--                                                   class="name">--}}
{{--                                            @if($errors->has('name'))--}}
{{--                                                <span class="err-text text-left w-100 d-inline-block" id="error"--}}
{{--                                                      style="color: red">* {{$errors->first('name')}}</span>--}}
{{--                                                <style>.name {--}}
{{--                                                        border: 1px solid red !important;--}}
{{--                                                    }</style>--}}
{{--                                            @endif--}}
{{--                                        </div>--}}
{{--                                        <div class="features-form__inp">--}}
{{--                                            <input name="phone" class="phone" type="text"--}}
{{--                                                   placeholder="{{ t('app.Բջջ․՝') }}">--}}
{{--                                            @if($errors->has('phone'))--}}
{{--                                                <span class="err-text text-left w-100 d-inline-block" id="error"--}}
{{--                                                      style="color: red">* {{$errors->first('phone')}}</span>--}}
{{--                                                <style>.phone {--}}
{{--                                                        border: 1px solid red !important;--}}
{{--                                                    }</style>--}}
{{--                                            @endif--}}
{{--                                        </div>--}}
{{--                                        <div class="features-form__inp">--}}
{{--                                            <input name="alt_phone" type="text" class="alt_phone"--}}
{{--                                                   placeholder="{{ t('app.Քաղ․՝') }}">--}}
{{--                                            @if($errors->has('alt_phone'))--}}
{{--                                                <span class="err-text text-left w-100 d-inline-block" id="error"--}}
{{--                                                      style="color: red">* {{$errors->first('alt_phone')}}</span>--}}
{{--                                                <style>.alt_phone {--}}
{{--                                                        border: 1px solid red !important;--}}
{{--                                                    }</style>--}}
{{--                                            @endif--}}
{{--                                        </div>--}}
{{--                                        <div class="features-form__inp">--}}
{{--                                            <input name="mail" type="text" class="email"--}}
{{--                                                   placeholder="{{ t('app.Email:') }}">--}}
{{--                                            @if($errors->has('mail'))--}}
{{--                                                <span class="err-text text-left w-100 d-inline-block"--}}
{{--                                                      id="error"--}}
{{--                                                      style="color: red">* {{$errors->first('mail')}}</span>--}}
{{--                                                <style>.email {--}}
{{--                                                        border: 1px solid red !important;--}}
{{--                                                    }</style>--}}
{{--                                            @endif--}}
{{--                                        </div>--}}
{{--                                        <div class="contact-us__btn">--}}
{{--                                            <button><i class="fas fa-paper-plane"--}}
{{--                                                       style="margin-right: 5px;"></i> {{ t('app.Ուղարկել') }}</button>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                </form>--}}
{{--                            </div>--}}
{{--                        </div>--}}
                        @if(!empty($item->youtube))
                            <div class="features__min">
                                <h2>{{ t('app.Տեսանյութ') }} </h2>
                                <div class="features__img">
                                    <iframe width="100%" height="100%" src="https:{{$item->youtube}}?rel=0"
                                            frameborder="0"
                                            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                                            allowfullscreen></iframe>
                                </div>
                            </div>
                        @endif
                        @if(!empty($item->iframe))
                            <div class="features__min">
                                <h2>{{ t('app.Քարտեզ') }} </h2>
                                <div class="features__map">
                                    <iframe src="{{$item->iframe}}" width="100%" height="100%" frameborder="0"
                                            style="border:0;" allowfullscreen=""></iframe>
                                </div>
                            </div>
                        @endif
                    </div>
                    <div class="product-det__right">
                        {{--                        @if(!empty($item->user) && !($item->user->admin))--}}
                        {{--                            <div class="product-det__right__title">--}}
                        {{--                                <h2>{{ t('app.Օգտատիրոջ տվյալներ') }}</h2>--}}
                        {{--                            </div>--}}
                        {{--                            <div class="product-det__right__box verticalCard">--}}
                        {{--                                <div>--}}
                        {{--                                    {{$item->user->name}}--}}
                        {{--                                </div>--}}


                        {{--                                @if(!empty($alts) && count($alts))--}}
                        {{--                                    @foreach($alts as $alt)--}}
                        {{--                                        <div class="items">--}}
                        {{--                                            @component('site.components.estate_card', ['item'=>$alt,'favorites'=>$favorites])@endcomponent--}}
                        {{--                                        </div>--}}
                        {{--                                    @endforeach--}}
                        {{--                                @endif--}}
                        {{--                            </div>--}}
                        {{--                        @endif--}}
                        @if(empty($item->user) || ($item->user->admin))
                            <div class="product-det__right__title">
                                <h2>{{ t('app.Օգտատիրոջ տվյալներ') }}</h2>
                            </div>
                            <div class="product-det__right__box verticalCard features__min">
                                <div class="text-3xl mb-5 font-segeo font-bold font-gray font-16">
                                    <table>
                                        @if(!empty($item->user_name))
                                            <tr  class="d_flex a_items_start">
                                                <td><i class="fa fa-user font-logo-orange mr-2"></i></td>
                                                <td>{{$item->user_name}}</td>
                                            </tr>
                                        @endif
                                        @if(!empty($item->user_address))
                                            <tr  class="d_flex a_items_start">
                                                <td><i class="fa fa-map-marker font-logo-orange mr-2 "></i></td>
                                                <td>{{$item->user_address}}</td>
                                            </tr>
                                        @endif

                                        @if(!empty($item->user_phones) && ($phones = (array)json_decode($item->user_phones)))
                                            @if(!empty($phones) && count($phones))
                                                @foreach($phones as $k=>$phone)
                                                    @if(($k === 0 || $k === 1 || $k === 2) && !empty($phone))
                                                        <tr class="d_flex a_items_start ">
                                                            <td>
                                                                <a href="tel:{{$phone}}">
                                                                <i class="fa fa-phone font-logo-orange mr-2 "></i></a>
                                                            </td>
                                                            <td>
                                                                <a class="font-gray" href="tel:{{$phone}}">{{$phone}}</a>
                                                            </td>
                                                        </tr>
                                                    @elseif($k === 'Viber'  && !empty($phone))
                                                        <tr class="d_flex a_items_start">
                                                            <td>
                                                                <a href="viber://add?number={{$phone}}"  class="font-gray">
                                                                <img height="20px" width="20px" class="mr-2"
                                                                     src="{{asset('f/site/img/viber.png')}}" alt="">
                                                                </a>
                                                            </td>
                                                            <td>
                                                                <a href="viber://add?number={{$phone}}"  class=" font-gray">
                                                                {{$phone}}
                                                                </a>
                                                            </td>
                                                        </tr>
                                                    @elseif($k === 'WhatsApp'  && !empty($phone))
                                                        <tr class="d_flex a_items_start mt-2">
                                                            <td>
                                                                <a href="whatsapp://send?phone={{$phone}}"
                                                                   class=" font-gray">
                                                                    <img height="20px" width="20px" class="mr-2"
                                                                         src="{{asset('f/site/img/whatsapp.png')}}"
                                                                         alt="">
                                                                </a>
                                                            </td>
                                                            <td class="">
                                                                <a href="whatsapp://send?phone={{$phone}}"
                                                                   class=" a_items_center font-gray">
                                                                    {{$phone}}
                                                                </a>
                                                            </td>
                                                        </tr>
                                                    @else
                                                    @endif

                                                @endforeach
                                            @endif
                                        @endif
                                    </table>

                                </div>
                                @if(!empty($alts) && count($alts))
                                    @foreach($alts as $alt)
                                        <div class="items">
                                            @component('site.components.estate_card', ['item'=>$alt,'favorites'=>$favorites])@endcomponent
                                        </div>
                                    @endforeach
                                @endif
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </section>
    @if ($errors->has('global'))
        <div class="error-notify">
            {{ t('auth.oop`s') }}
        </div>
    @elseif(session('message_sent'))
        <div class="success-notify">
            {{ t('auth.success') }}
        </div>
    @endif
    @include('site.layouts.footer')
@endsection
@push('css')
    <style>
        .items {
            display: block !important;
        }

        .verticalCard .apartment-info__cnt {
            display: flex;
            flex-direction: column;
            height: 100%;
        }

        .verticalCard .apartment-info__text .opt span {
            display: inline !important;
        }

        .verticalCard .items {
            margin-bottom: 20px;
            margin-top: 0 !important;
        }

        .verticalCard .items {
            width: 33.3333%;
            min-width: 280px;
            padding: 0 10px;
        }

        .verticalCard {
            display: flex;
            flex-wrap: wrap;
        }
    </style>
@endpush
@push('js')
    <script>
        $('.block2 table').append($('.block1').find('tr').slice(Math.ceil($('.block1').find('tr').length / 2), parseInt($('.block1').find('tr').length)).remove())

        function initSlickByAram() {
            var list = $(document).find('.slick-list')[1];
            if (!$('.slick-next').length) {

                $(list).css('background', '#F8F6F6')
                $(list).css('z-index', '99999999999999999999999999')
            } else {
                $(list).css('z-index', '0')

            }
        }

        $(document).ready(function () {
            initSlickByAram()
        })
        $(window).resize(function () {
            initSlickByAram()
        })
    </script>
@endpush
