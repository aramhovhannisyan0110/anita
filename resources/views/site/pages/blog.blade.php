@extends('site.layouts.header')
@section('content')
    <section style="padding: 7px 0;
    background: white;
    border-top: 3px solid #f8f6f6;">
        <div class="container">
            <div class="product-det__title">
                <h1>{{t('app.Նորություններ, հուշումներ և հոդվածներ')}}</h1>
            </div>
        </div>
    </section>
    <section>
        <div class="container">
            <div class="news-tips-articles blog-pages">
                <div class="tips-articles__block d_flex f_wrap ">
                    @if(!empty($items))
                        @foreach($items as $item)
                            @component('site.components.news_card', ['item'=>$item,'parent_news'=>$page])@endcomponent
                        @endforeach
                    @endif
                </div>
                @if(!empty($items) && count($items))
                    <div class="pagination_buttons">
                        {{ $items->links() }}
                    </div>
                @endif
            </div>
        </div>
    </section>
    @include('site.layouts.footer')
@endsection
@push('css')
        @css(aSite('css/estate_list.css'))
    <style>
        .tips-articles__info {
            height: 100%;
            display: flex;
            flex-direction: column;
            justify-content: space-between;
        }
    </style>
@endpush
