@extends('site.layouts.header')
@section('content')
    <section style="padding: 7px 0;
    background: white;
    border-top: 3px solid #f8f6f6;">
        <div class="container">
            <div class="product-det__title">
                <h1>{{ t('app.իմ ընտրածները') }}</h1>
            </div>
        </div>
    </section>
    <section>
        <div class="container">
            <div class="favorites__list">
                <div class="about-offers__block favorites__block d_flex f_wrap">
                    @if(!empty($items) && count($items))
                        @foreach($items as $item)
                            <div class="about-offers__box">
                            @component('site.components.estate_card', ['item'=>$item,'favorites'=>$favorites])@endcomponent
                            </div>
                        @endforeach
                    @else
                        <div class="d_flex f_direction_column j_content_center a_items_center w-full">
                            <span  class = "font-segeo font-bold font-default font-24">{{t('favorites.empty')}}</span>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </section>
    @include('site.layouts.footer')
@endsection
@push('js')
    <script>
        $(document).ready(function () {
            $('select').styler();
        })
        $(function () {
            console.log($(".items__min:hidden"))
            $(".items__min").slice(0, 8).show();
            $("#loadMore").on('click', function (e) {
                e.preventDefault();
                $(".items__min:hidden").slice(0, 8).slideDown();
                if ($(".items__min:hidden").length == 0) {
                    $("#loadMore" + "").fadeOut('slow');
                }

            });
        });
        $(function () {
            console.log($(".items:hidden"))
            $(".items").slice(0, 8).show();
            $("#loadmach").on('click', function (e) {
                e.preventDefault();
                $(".items:hidden").slice(0, 8).slideDown();
                if ($(".items:hidden").length == 0) {
                    $("#loadmach" + "").fadeOut('slow');
                }

            });


        });

        $('.card_heart').click(function () {
            $(this).closest('.about-offers__box').fadeOut();
        });
        </script>
@endpush
