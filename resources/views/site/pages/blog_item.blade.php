@extends('site.layouts.header')
@section('content')
    @push('meta')
        <meta property="og:title" content="{{$item->title}}" />
        <meta property="og:image" content="{{asset('u/blog/'.$item->image)}}" />
        <meta property="og:type" content="Blog" />
    @endpush
    <section style="padding: 7px 0;
    background: white;
    border-top: 3px solid #f8f6f6;">
        <div class="container">
            <div class="product-det__title">
                <h1>{{$item->title}}</h1>
            </div>
        </div>
    </section>
    <section>

        <div class="container">
            <div class="blog-single-pages">


                <div class="blog-single__main d_flex">
                    <div class="blog-single__left">

                        <div class="tips-articles__href ">
                            <p><i style="font-size: 18px;margin-right: 5px;" class="far fa-clock"></i>{{date('d',strtotime($item->date))}} {{t('months.'.date('M',strtotime($item->date)).'')}} {{date('Y',strtotime($item->date))}}</p>
                        </div>
                        <div class="blog-single_img">
                            <img src="{{asset('u/blog/'.$item->image)}}"   alt="{{ $item->image_alt }}" title="{{ $item->image_title }}">
                        </div>
                        <div style="display: flex;justify-content: flex-end">





{{--                            <div style="font-size: 26px;color: #4267b2;margin: 5px" class="fb-share-button"--}}
{{--                                 data-href="{{ url()->current() }}"--}}
{{--                                 data-layout="icon_link">--}}
{{--                            </div>--}}
                            <a href="{{ url()->current() }}" data-image="article-1.jpg" data-title="Article Title" data-desc="Some description for this article" class="btnShare">Sharessss</a>
                            <a style="font-size: 26px;color: #4267b2;margin: 5px" href="http://www.facebook.com/sharer.php?u={{route('page_item',['parent'=>$page->url,'current'=>$item->url])}}" target="_blank"><i class="fab fa-facebook-square"></i></a>
                            <a style="font-size: 26px;color: #0073b1;margin: 5px" href="https://www.linkedin.com/sharing/share-offsite/?url={{route('page_item',['parent'=>$page->url,'current'=>$item->url])}}" target="_blank"><i class="fab fa-linkedin"></i></a>
                            <a style="color: #ffffff;margin: 5px" href=" https://vk.com/share.php?url={{route('page_item',['parent'=>$page->url,'current'=>$item->url])}}" target="_blank"><p style="background: #4267b2;padding: 3px 2px; display: flex;justify-content: center;align-items: center; border-radius: 2px;margin-top: 2px;"><i class="fab fa-vk"></i></p></a>
                            <a style="font-size: 26px;color: #1da1f2;margin: 5px" class="twitter-share-button" href="https://twitter.com/intent/tweet?url={{route('page_item',['parent'=>$page->url,'current'=>$item->url])}}" data-size="large"><i class="fab fa-twitter-square"></i></a>

                        </div>
                        <div class="blog-single__text about-our-company__title ">
                            {!! $item->desc !!}
                        </div>
                    </div>
                    <div class="blog-single__dubscribe">
                        <div class="news-tips-articles blog-pages ">
                            <h2 style="color: #FE980F;padding: 11px 0px;">{{t('app.այլ նորություններ')}}</h2>
                            <div class="tips-articles__block d_flex f_wrap ">
                                @if(!empty($alts))
                                    @foreach($alts as $alt)
                                        <div class="tips-articles__box">
                                            <div class="tips-articles__info">
                                                <div>
                                                    <a href="{{route('page_item',['parent'=>$page->url,'current'=>$alt->url])}}" class="tips-articles__img">
                                                        <img src="{{asset('u/blog/'.$alt->image)}}"   alt="{{ $alt->image_alt }}" title="{{ $alt->image_title }}">
                                                    </a>
                                                    <div class="tips-articles__text">
                                                        <a href="{{route('page_item',['parent'=>$page->url,'current'=>$alt->url])}}">{{$alt->title}}</a>
                                                        <p>
                                                            {!! $alt->short_desc !!}
                                                        </p>
                                                    </div>
                                                </div>

                                                <div class="tips_footer">
                                                    <div class="tips-articles__href d_flex a_items_center j_content_between">
                                                        <p><i style="font-size: 16px;margin-right: 5px;" class="far fa-clock"></i>{{date('d',strtotime($alt->date))}} {{t('months.'.date('M',strtotime($alt->date)).'')}} {{date('Y',strtotime($alt->date))}}</p>
                                                        <a href="{{route('page_item',['parent'=>$page->url,'current'=>$alt->url])}}">{{t('app.Դիտել ավելին')}}</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
{{--                <div class="news-tips-articles blog-pages">--}}
{{--                    <h2>{{t('app.Առնչվող նյութեր')}}</h2>--}}
{{--                    <div class="tips-articles__block d_flex f_wrap j_content_center">--}}
{{--                        @if(!empty($alts))--}}
{{--                            @foreach($alts as $alt)--}}
{{--                                <div class="tips-articles__box">--}}
{{--                                    <div class="tips-articles__info">--}}
{{--                                        <div>--}}
{{--                                            <a href="javascript:;" class="tips-articles__img">--}}
{{--                                                <img src="{{asset('u/blog/'.$alt->image)}}">--}}
{{--                                            </a>--}}
{{--                                            <div class="tips-articles__text">--}}
{{--                                                <a href="javascript:;">{{$alt->title}}</a>--}}
{{--                                                <p>--}}
{{--                                                    {!! $alt->short_desc !!}--}}
{{--                                                </p>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}

{{--                                        <div class="">--}}
{{--                                            <div class="tips-articles__href d_flex a_items_center j_content_between">--}}
{{--                                                <p><img style="padding-top: 2px" src="{{asset('f/site/img/clock.png')}}">{{date('d',strtotime($alt->date))}} {{t('months.'.date('M',strtotime($alt->date)).'')}} {{date('Y',strtotime($alt->date))}}</p>--}}
{{--                                                <a href="{{route('page_item',['parent'=>$page->url,'current'=>$alt->url])}}">{{t('app.Դիտել ավելին')}}</a>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            @endforeach--}}
{{--                        @endif--}}
{{--                     </div>--}}
{{--                </div>--}}
            </div>

        </div>
    </section>
    <!-- Load Facebook SDK for JavaScript -->
{{--    <div id="fb-root"></div>--}}
{{--    <script>(function(d, s, id) {--}}
{{--            var js, fjs = d.getElementsByTagName(s)[0];--}}
{{--            if (d.getElementById(id)) return;--}}
{{--            js = d.createElement(s); js.id = id;--}}
{{--            js.src = "https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v3.0";--}}
{{--            fjs.parentNode.insertBefore(js, fjs);--}}
{{--        }(document, 'script', 'facebook-jssdk'));</script>--}}

    <!-- Your share button code -->

    @include('site.layouts.footer')
@endsection
@push('js')
    <script>
        $('.btnShare').click(function(){
            elem = $(this);
            postToFeed(elem.data('title'), elem.data('desc'), elem.prop('href'), elem.data('image'));
            return false;
        });
    </script>
@endpush
@push('css')
    <style>
        .blog-pages .tips-articles__box{
            width: 100% !important;
        }
        .tips_footer{
            padding: 0 15px;
            border-top: 1px solid #EEEEEE;        }
        .tips-articles__info {
            height: 100%;
            display: flex;
            flex-direction: column;
            justify-content: space-between;
        }
    </style>
@endpush

