@extends('site.layouts.header')
@section('content')
    <section style="padding: 7px 0;
    background: white;
    border-top: 3px solid #f8f6f6;">
        <div class="container">
            <div class="product-det__title">
                <h1>{{ $part??'' }}</h1>
            </div>
        </div>
    </section>
    <section>
        <div class="container">
            <div class="product-list">
                <div class="product-list__min d_flex j_content_between">
                    <div class="product-list__left">
                        <div class="popup-request__min d_none">
                            <button class="btn-modal" data-val="popup-request-call">
                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16"
                                     fill="none">
                                    <path
                                        d="M0.689133 1.20473e-06H15.3107C15.5688 -0.000474686 15.8052 0.140071 15.9231 0.363897C16.0426 0.590737 16.0216 0.863897 15.8689 1.07107L10.5122 8.4329C10.5104 8.43544 10.5084 8.43782 10.5067 8.44036C10.312 8.6967 10.2067 9.0073 10.2062 9.3263V15.3284C10.2073 15.506 10.1358 15.6769 10.0075 15.803C9.87902 15.9289 9.70438 16 9.52227 16C9.42975 15.9998 9.33804 15.9819 9.25252 15.9472L6.24244 14.8276C5.97285 14.7471 5.79383 14.4989 5.79383 14.2V9.3263C5.79334 9.0073 5.68798 8.6967 5.49351 8.44036C5.49172 8.43782 5.48977 8.43544 5.48798 8.4329L0.131087 1.07091C-0.021595 0.863897 -0.042572 0.590896 0.0769405 0.364056C0.194663 0.140071 0.431248 -0.000474686 0.689133 1.20473e-06Z"
                                        fill="#E7272D"/>
                                </svg>
                            </button>
                        </div>
                        <div class="sale-apartment__min sale-apartment__sel d_flex j_content_end a_items_center pl-3">
                            <p style="flex: 1">{{$total_count.' '.t('search.Արդյունք')}}</p>

                            <div class="sale-apartment__sel d_flex"
                                 style="z-index:0; display: {{ (!empty($items) && count($items)) || (!empty($tops) && count($tops))?'':'none!important' }}">
                                <p>{{t('search.Դասակարգել։')}}</p>
                                <div class="sale-apartment__select ">
                                    <select class="for_select2_sort" name="sort" style="cursor: pointer"
                                            onchange="location = this.value;">
                                        <option
                                            {{(!empty($sort) && $sort == 0)?'selected':'' }} style="cursor: pointer;padding: 10px"
                                            value="{{ url()->full().'&sort=date_asc' }}">{{t('search.Ըստ ամսաթվի')}} ⇧
                                        </option>
                                        <option
                                            {{(!empty($sort) && $sort == 3)?'selected':'' }} style="cursor: pointer;padding: 10px"
                                            value="{{ url()->full().'&sort=date_desc' }}">{{t('search.Ըստ ամսաթվի')}} ⇩
                                        </option>
                                        <option
                                            {{(!empty($sort) && $sort == 1)?'selected':'' }} style="cursor: pointer;padding: 10px"
                                            value="{{ url()->full().'&sort=price_asc' }}">{{t('search.Ըստ գնի')}} ⇧
                                        </option>
                                        <option
                                            {{(!empty($sort) && $sort == 2)?'selected':'' }} style="cursor: pointer;padding: 10px"
                                            value="{{ url()->full().'&sort=price_desc' }}">{{t('search.Ըստ գնի')}} ⇩
                                        </option>
                                    </select>
                                </div>
                            </div>
                            <div class="reviews-min-text-hrefs"
                                 style="display: {{ (!empty($items) && count($items)) || (!empty($tops) && count($tops))?'':'none!important' }}">
                                <ul class="d_flex a_items_center">
                                    <li class="card_view country-city-min active-href" data-catalog="pick-up">
                                        <div class="sale-apartment__line">
                                            <span></span>
                                            <span></span>
                                            <span></span>
                                        </div>
                                    </li>
                                    <li class="card_view country-city-min" data-catalog="shipment">
                                        <div class="sale-apartment__box d_flex f_wrap">
                                            <span></span>
                                            <span></span>
                                            <span></span>
                                            <span></span>
                                            <span></span>
                                            <span></span>
                                            <span></span>
                                            <span></span>
                                            <span></span>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <style>

                        </style>
                        <div class="reviews-min-info card_view_container">
                            <div class="reviews-min-block-js" data-catalog="pick-up">
                                <div class="sale-apartment__info">
                                    @if(!empty($tops) && count($tops))
                                        @foreach($tops as $item)
                                            <div class="items" style="background-color: #f7f7f7;">
                                                @component('site.components.estate_card', ['item'=>$item,'favorites'=>$favorites,'top'=>true])@endcomponent
                                            </div>
                                        @endforeach
                                        <section class="banner_block" style="margin: 20px 0;position: relative">
                                            <img src="{{$info->search_page[0]->image()}}" alt="">
                                            <a href="{{$info->search_page[0]->url??'javascript:void(0)'}}" style="font: 20px Segoe UI, HelveticaNeueCyr-Bold, Helvetica, Roboto, sans-serif;
                                            color: #ffffff;font-weight: 700;position: absolute;top: 0;left: 0; display: flex;justify-content: flex-start;align-items: center;height: 100%;width: 100%">
                                                <p style="padding: 15px">
                                                    {{$info->search_page[0]->title}}
                                                </p>
                                            </a>
                                        </section>
                                    @endif
                                    @if(!empty($items) && count($items))
                                        @php($key = 1 )
                                        @foreach($items as $item)
                                            <div class="items">
                                                @component('site.components.estate_card', ['item'=>$item,'favorites'=>$favorites])@endcomponent
                                            </div>
                                            @if((($loop->index+1) % 6 == 0) && $loop->index != 0)
                                                @if($key>4)@php($key=0)@endif
                                                @if(!empty($info->search_page[$key]))
                                                    <section class="banner_block"
                                                             style="margin: 20px 0;position: relative">
                                                        <img src="{{$info->search_page[$key]->image()}}" alt="">
                                                        <a href="{{$info->search_page[$key]->url??'javascript:void(0)'}}"
                                                           style="font: 20px Segoe UI, HelveticaNeueCyr-Bold, Helvetica, Roboto, sans-serif; color: #ffffff;font-weight: 700;position: absolute;top: 0;left: 0; display: flex;justify-content: flex-start;align-items: center;height: 100%;width: 100%">
                                                            <p style="padding: 15px">
                                                                {{$info->search_page[$key]->title}}
                                                            </p>
                                                        </a>
                                                    </section>
                                                @endif
                                                @php($key++)
                                            @endif
                                        @endforeach
                                    @elseif(empty($tops) && !count($tops))
                                        <div class="empty_result">
                                            <p>{{ t('search.Արդյունքներ չեն գտնվել') }}</p>
                                        </div>
                                    @endif

{{--                                    <div class="download-more btn">--}}
{{--                                        <button--}}
{{--                                            {{ empty($items) || count($items) <= 8? 'style = display:none!important' : '' }} id="loadmach">--}}
{{--                                            <img src="{{asset('f/site/img/refresh.png')}}"--}}
{{--                                                 alt="png"> {{t('app.Բեռնել ավելին')}}--}}
{{--                                        </button>--}}
{{--                                    </div>--}}
                                </div>
                            </div>
                        </div>
                        @if(!empty($items) && count($items))
                            <div class="pagination_buttons">
                                {{ $items->appends(request()->input())->links() }}
                            </div>
                        @endif
                    </div>
                    <div class="product-list__right">
                        <div class="product-list__rent">
                            <div class="product-reviews-min-text-hrefs">
                                <ul class="d_flex j_content_between a_items_center">
                                    @if(!empty($types))
                                        @foreach($types as $type)
                                            <li class="product-click-reviews product-country-city-min  {{($type->id == $selected_tp)?'product-active-href':''}}"
                                                data-catalog="{{$type->id}}">
                                                <div class="product-rent__text">
                                                    <p>{{$type->title}}</p>
                                                </div>
                                            </li>
                                        @endforeach
                                    @endif
                                </ul>
                            </div>
                            @if(!empty($types))
                                @foreach($types as $type)
                                    <div class="product-reviews-min-info ">
                                        <div class="product-reviews-min-block-js" data-catalog="{{$type->id}}"
                                             style="padding-bottom: 1px">
                                            <div class="product-rent__min">
                                                <form method="get" action="{{ route('search') }}">
                                                    <div class="search_container">
                                                        <input type="hidden" name="type" value="{{ $type->id }}">
                                                        <div class="search" data-type="{{$type->id}}">
                                                            <div class="product-rent__info">
                                                                <label for="">{{ t('app.Տեսակ։') }}</label>
                                                                <select onchange="getFilters(this)" name="category"
                                                                        data-type="{{$type->id}}"
                                                                        class="for_select2_closable">
                                                                    @foreach($categories as $category)
                                                                        <option class="locations"
                                                                                {{ $category->id == $selected_cat ? 'selected':'' }}
                                                                                value="{{$category->id}}">{{$category->title}}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                            <div class="product-rent__info">
                                                                <div class="address" data-type="0"
                                                                     style="margin-top: 4px">
                                                                    <label for="">{{ t('app.region') }}:</label>
                                                                    <select data-type="0" onchange="getLocations(this)"
                                                                            name="region"
                                                                            data-id="0"
                                                                            class="form-control new_region for_select2_closable">
                                                                        <option>{{ t("search.select") }}</option>
                                                                        @if(!empty($regions) && count($regions))
                                                                            @foreach($regions as $region)
                                                                                <option
                                                                                    {{ ($region->id == $selected_region && ($type->id == $selected_tp))?'selected':'' }}
                                                                                    value="{{  $region->id  }}">{{ $region->title }}
                                                                                </option>
                                                                            @endforeach
                                                                        @endif
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="product-rent__info">
                                                                <label for="">{{ t('app.region1') }}:</label>
                                                                <select data-type="0"
                                                                        multiple="multiple"
                                                                        name="regions[]"
                                                                        data-id="0"
                                                                        class="form-control new_region for-opt for_select2_closable">
                                                                </select>
                                                            </div>
                                                            @if(!empty($global_filters))
                                                                @foreach($global_filters as $filter)
                                                                    @component('site.components.filter_list', ['filter'=>$filter,'selected_with'=>$selected_with,'selected_without'=>$selected_without,'selected_with_min'=>$selected_with_min,'selected_with_max'=>$selected_with_max,'type'=>$type,'selected_tp'=>$selected_tp])@endcomponent
                                                                @endforeach
                                                            @endif
                                                            @if(!empty($local_filters))
                                                                @foreach($local_filters as $filter)
                                                                    @component('site.components.filter_list', ['filter'=>$filter,'selected_with'=>$selected_with,'selected_without'=>$selected_without,'selected_with_min'=>$selected_with_min,'selected_with_max'=>$selected_with_max,'type'=>$type,'selected_tp'=>$selected_tp])@endcomponent
                                                                @endforeach
                                                            @endif
                                                        </div>
                                                        <div class="product-rent__info">
                                                            <label for="">{{ t('search.status') }}</label>
                                                            <select data-type="0"
                                                                    multiple="multiple"
                                                                    name="status[]"
                                                                    data-id="0"
                                                                    class="form-control   for_select2">
                                                                <option
                                                                    {{ (in_array('0',$status) && $selected_tp == $type->id)?'selected':'' }} value="0">{{ t('search.Свободно') }}</option>
                                                                <option
                                                                    {{ (in_array('1',$status) && $selected_tp == $type->id)?'selected':'' }} value="1">{{ t('search.Продано') }}</option>
                                                                <option
                                                                    {{ (in_array('2',$status) && $selected_tp == $type->id)?'selected':'' }} value="2">{{ t('search.Сдано в аренду') }}</option>
                                                            </select>
                                                        </div>
                                                        <div class="product-rent__info">
                                                            <label for="">{{ t('search.search by code') }}</label>
                                                            <input
                                                                {{ !empty($code) && $selected_tp == $type->id?'value='.$code.'':'' }}
                                                                class="form-control "
                                                                type="text" name="code"
                                                                placeholder="{{ t('search.enter code') }}"
                                                                style="width: 100%;padding: 13px 20px;font: 13px Segoe UI, HelveticaNeueCyr-Bold, Helvetica, Roboto, sans-serif;  border: 1px solid #e6e5e5!important;">
                                                        </div>
                                                    </div>
                                                    <div class="new_building" style="display: {{(!$new)?'none':''}}">
                                                        <label for="new" class="new_build">
                                                            <input type="checkbox" id="new" name="new">
                                                            <span class="checkmark"></span>{{ t('search.новостройка') }}
                                                        </label>
                                                    </div>
                                                    <div class="surface-area__btn" style="margin-bottom: 12px">
                                                        <button><i class="fas fa-search"></i> {{t('app.Որոնել')}}
                                                        </button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <input type="hidden" id="lang" value="{{app()->getLocale()}}">
    </section>

    <div id="popup-request-call" class="modal">
        <div class="modal-content">

            <div class="popup-request-call__info">
                <div class="close">
                </div>

            </div>
        </div>
    </div>
    @include('site.layouts.footer')
@endsection
@push('css')
    @css(aSite('css/estate_list.css'))
@endpush
@push('js')
    <script>
        window.token = '{!! @csrf_token() !!}';
        window.selected_region = {!!(int)$selected_region!!}
        window.selected_regions = {!!json_encode($selected_regions)!!}
        window.get_filters = '{!!route('getFilters') !!}'
        window.get_regions = '{!!route('getRegions') !!}'
        window.get_building_type = '{!!route('getBuildingType') !!}'
        window.select_place = '{{ t("search.select") }}'
        window.no_min = '{{ t("search.no_min") }}'
        window.no_max = '{{ t("search.no_max") }}'
    </script>
    @js(aSite('js/estate_list.js'))

@endpush
