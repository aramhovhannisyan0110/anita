@extends('site.layouts.header')
@section('content')
    <div class="main">


        <div class="home_slider_content">
            <div class="b1 ">
                <div class="" style="transition-duration: 0ms;">

                        <div class="container">
                            <div class="home_slider_text_div">
                                <div style="margin-top:40px;max-width: 450px;display:flex;align-items:center;justify-content:center;flex-direction:column;color: black;padding: 25px" class="container error-container pt-s pb-2s">
                                    @if ($error['title'])
                                        <div style="font: 26px Segoe UI, HelveticaNeueCyr-Bold, Helvetica, Roboto, sans-serif;text-align: end" class="error-title">{{ $error['title'] }}</div>
                                    @endif
                                    <img style="position:relative;"
                                        src="{{ asset('f/site/img/404.png') }}" alt="">
                                        <div style="font: 22px Segoe UI, HelveticaNeueCyr-Bold, Helvetica, Roboto,  sans-serif;background-color: white;font-weight: 100;margin-top:-5px;text-align: center" class="error-title">{{ t('error.sorry, the page was not found') }}</div>

                                    <a href="{{route('page')}}" class="real-sale__btn" style="max-width: 100%;">
                                        <button style="transition: .3s;border:2px solid #FE980F;!important;color: white;height: unset!important;padding: 10px 0;margin-top: 25px" class="head_button"><i class="fas fa-home"></i> {{t('error.home')}}</button>
                                    </a>
                                </div>

                            </div>
                        </div>
                </div>
            </div>
            <!-- Add Pagination -->

        </div>
    </div>
    <style>
        .head_button:hover{
            color: #FE980F!important;
            background: white!important;
            transition: .3s;
        }
    </style>
    @include('site.layouts.footer')
@endsection
@push('js')

    {{--    @include('ckfinder::setup')--}}
@endpush
