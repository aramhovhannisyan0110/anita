@extends('site.layouts.header')
@section('content')
    <div class="container pb-s">
        <div class="dynamic-page">
            @if ($page->image && $page->show_image)
                <div class="dynamic-page-banner">
                    <img src="{{ asset('u/pages/'.$page->image) }}" alt="{{ $page->title }}">
                </div>
            @endif
            <div class="global-page-title pt-s">
                <h1>{{ $page->title }}</h1>
            </div>
            <div class="dynamic-page-content dynamic-text ">{!! $page->content !!}</div>
        </div>
        @component('site.components.gallery', ['gallery'=>$gallery])@endcomponent
    @component('site.components.video_gallery', ['gallery'=>$video_gallery])@endcomponent
    </div>
    <style>
        .dynamic-page{
            /*padding: 10px;*/
            margin-top: 15px;
        }
        .dynamic-page-content{
            font-family:  "Segoe UI", "HelveticaNeueCyr-Bold", "Helvetica","Roboto", sans-serif;
            padding: 15px 0;
        }
        .global-page-title{
            font:22px  "Segoe UI", "HelveticaNeueCyr-Bold", "Helvetica","Roboto", sans-serif;
            padding: 15px 0 0;
        }
        .photo-gallery{
            box-shadow: 0px 3px 4px rgba(0, 95, 162, 0.16);
            background: white;
        }
        .video-gallery{
            box-shadow: 0px 3px 4px rgba(0, 95, 162, 0.16);
            background: white;
            margin-top: 30px;

        }
        .video-player {
            position: absolute;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            cursor: pointer;
            background-color: #000;
        }

        .video-player .video-thumbnail {
            position: absolute;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
            background-size: cover;
        }

        .video-player button.video-play-button {
            position: absolute;
            top: 50%;
            left: 50%;
            width: 68px;
            height: 48px;
            z-index: 1;
            -webkit-transform: translate(-50%, -50%);
            -moz-transform: translate(-50%, -50%);
            -ms-transform: translate(-50%, -50%);
            -o-transform: translate(-50%, -50%);
            transform: translate(-50%, -50%);
            border: none;
            background: transparent;
            box-shadow: none;
            outline: none;
            cursor: pointer;
        }

        .video-player .video-loader {
            position: absolute;
            top: calc(50% - 30px);
            left: calc(50% - 30px);
            z-index: 1;
            width: 65px;
            height: 65px;
            border-width: 5px;
            border-style: solid;
            border-color: #ddd #ddd transparent #ddd;
            border-radius: 50%;
            -webkit-animation: spin 1s linear infinite;
            -moz-animation: spin 1s linear infinite;
            -o-animation: spin 1s linear infinite;
            animation: spin 1s linear infinite;
        }

        .video-player .video-data.loaded + .video-loader {
            display: none;
        }

        .force-4-3 {
            display: block;
            position: relative;
            width: 100%;
            padding-top: 75%;
            overflow: hidden;
        }

        .video-player:hover button.video-play-button .play-icon {
            fill: red;
            fill-opacity: 1;
        }

        .gallery-video iframe {
            border: none;
            position: absolute;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            width: 100%;
            height: 100%;
            z-index: 2;
        }
        .video-gallery h2,.photo-gallery h2{
            width: 100%;
            padding: 15px 10px 5px;
            color: #465660;
            font: 20px "Segoe UI", "HelveticaNeueCyr-Bold", "Helvetica", "Roboto", sans-serif;
            font-weight: bold;
        }
    </style>
    @include('site.layouts.footer')
    @push('css')
        <link rel="stylesheet" href="{{ asset('f/site/css/jquery.fancybox.css') }}">
    @endpush
    @push('js')
        <script src="{{ asset('f/site/js/jquery.fancybox.js') }}"></script>
    @endpush
@endsection
