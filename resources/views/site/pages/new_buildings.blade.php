@extends('site.layouts.header')
@section('content')
    <section style="padding: 7px 0;
    background: white;
    border-top: 3px solid #f8f6f6;">
        <div class="container">
            <div class="product-det__title">
                <h1>{{t('app.Նորություններ, հուշումներ և հոդվածներ')}}</h1>
            </div>
        </div>
    </section>
    <section>
        <div class="container">
            <div class="row">
                @if(!empty($items))
                    @foreach($items as $item)
                        <div class="new_building_card col-12 col-md-6 col-lg-4 col-xl-3 mt-3">
                            <a href="{{ route('page_item',['parent'=>$page->url,'current'=>$item->url]) }}">
                                <div class="p-2" style="box-shadow: 0 0 8px 5px #00000014;">
                                    <img src="{{asset('u/gallery/thumbs/'.(count($item->gallery)?$item->gallery->first()->image:''))}}" alt="">
                                </div>
                                <div class="new_building_card_title font-segeo font-16 font-logo-blue px-3 font-bold" style="margin-top: -25px;position: relative">
                                    <div class="p-2">
                                        {{$item->title}}
                                    </div>
                                </div>
                            </a>
                        </div>
                    @endforeach
                @endif
            </div>

        </div>
    </section>
    @include('site.layouts.footer')
@endsection
@push('css')
    <style>
        .tips-articles__info {
            height: 100%;
            display: flex;
            flex-direction: column;
            justify-content: space-between;
        }
    </style>
@endpush

