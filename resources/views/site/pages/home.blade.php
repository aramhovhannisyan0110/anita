@extends('site.layouts.header')
@section('content')
    <section class="real-estate-sale__fon" style="background-image: url({{$home->search->image()}})">
        <div class="container">
            <div class="real-estate-sale__min">
                <div class="real-estate-sale__title">
                    <h1>{{ $home->search->title }}</h1>
                </div>
                <div class="real-estate-sale__block">
                    <form method="get" action="{{ route('estateSearch') }}">
                        <div class="real-sale__form d_flex j_content_between a_items_center">
                            <div class="real-sale__sel">
                                @if(!empty($categories) && count($categories))
                                    <select name="categories">
                                        @foreach($categories as $category)
                                            <option value="{{$category->id}}">- {{ $category->title }} -</option>
                                        @endforeach
                                    </select>
                                @endif
                            </div>
                            @if(!empty($types) && count($types))
                                @foreach($types as $type)
                                    <div class="real-sale__btn btn">
                                        <button type="submit" name="types"
                                                value="{{ $type->id }}">{{ $type->title }}</button>
                                    </div>
                                @endforeach
                            @endif
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
    <section>
        @if(count($new_buildings))
            <div class="container bbest mt-5">
                <h2>{{t('app.Նորակառույցներ')}}</h2>
                <div class="best-offers">
                    <div class="best-offers__title">
                        <p>{{ t("app.Նորակառույցների լավագույն առաջարկներ") }}</p>
                    </div>
                    <div class="best-offers-slider best-offers-slidered ">
                        @foreach($new_buildings as $item)
                            <div class="best-offers-slider-box new_building_card ">
                                <a href="{{ route('page_item',['parent'=>$new_buildings_parents,'current'=>$item->url]) }}">
                                    <div class="p-2" style="box-shadow: 0 0 8px 5px #00000014;">
                                        <img
                                            src="{{asset('u/gallery/thumbs/'.(count($item->gallery)?$item->gallery->first()->image:''))}}"
                                            alt="">
                                    </div>
                                    <div
                                        class="new_building_card_title font-segeo font-16 font-logo-blue px-3 font-bold"
                                        style="margin-top: -25px;position: relative">
                                        <div class="p-2">
                                            {{$item->title}}
                                        </div>
                                    </div>
                                </a>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        @endif
    </section>
    <section>
        <div class="container d_flex" style="margin-top: 40px;flex-wrap: wrap;justify-content: center;">
            @if($home->services && count($home->services))
                @foreach($home->services as $service)
                    <div class="home_service_card">
                        <a href="{{$service->url}}">
                            <div style="position: relative;">
                                <div class="home_service_card_text"
                                     style="background-image: url({{asset('f/site/img/fone.png')}});">
                                    <h2>{{ $service->line1 }}</h2>
                                    <h2>{{ $service->line2 }}</h2>
                                </div>
                                <img src="{{$service->image()}}" style="width: 100%" alt="{{ $service->img_alt }}"
                                     title="{{ $service->img_title }}">
                            </div>
                        </a>
                    </div>
                @endforeach
            @endif
        </div>
    </section>
    @if($home->adv_title->title != '')
        <section>
            <div class="container">
                <div class="call-us-free">
                    <h2 class="text-center">{{ $home->adv_title->title }}</h2>
                    @if(count($home->adv))
                        <div class="call-us-free__href">
                            <ul>
                                @foreach($home->adv as $phone)
                                    <li>{{$phone->title}} <a href="tel: {{$phone->phone}}"> {{$phone->phone}}</a></li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                </div>
            </div>
        </section>
    @endif
    <section>
        <div class="container bbest">
            <h2>{{ $home->suggestions->title??null }}</h2>
            @foreach($types as $k=>$tp)
                <div class="best-offers">
                    <div class="best-offers__title">
                        <p>{{ t("app.block_1_$k") }}</p>
                    </div>
                    <div class="best-offers-slider">
                        @foreach($block_1 as $item)
                            @if($item->type_id == $tp->id)
                                <div class="best-offers-slider-box">
                                    {{--                                    @component('site.components.estate_card', ['item'=>$item,'favorites'=>$favorites])@endcomponent--}}
                                </div>
                            @endif
                        @endforeach
                    </div>
                </div>
            @endforeach
        </div>
    </section>
    @if(!empty($slides))
        <section>
            @foreach($slides as $slide)
                <div class="estate-passport-slid">
                    <div class="estate-passport-slid-box">
                        <div class="estate-passport-fon"
                             style="background-image: url({{ 'u/main_slider/'.$slide->image }})">
                            <div class="container">
                                <div class="estate-passport-block">
                                    <div class="estate-passport-title">
                                        <h2>{!! $slide->title !!}</h2>
                                        <p>{!! $slide->line_1 !!}<br>
                                            {!! $slide->line_2 !!}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @if($loop->index == 1)
                    <style>
                        .slick-dots {
                            display: block !important;
                        }
                    </style>
                @endif
            @endforeach
        </section>
    @endif
    <section>
        <div class="container bbest">
            <h2>{{ $home->suggestions2->title??null }}</h2>
            @foreach($types as $k=>$tp)
                <div class="best-offers">
                    <div class="best-offers__title">
                        <p>{{ t("app.block_2_$k") }}</p>
                    </div>
                    <div class="best-offers-slider">
                        @foreach($block_2 as $item)

                            @if($item->type_id == $tp->id)
                                <div class="best-offers-slider-box">
                                    @component('site.components.estate_card', ['item'=>$item,'favorites'=>$favorites])@endcomponent
                                </div>
                            @endif
                        @endforeach
                    </div>
                </div>
            @endforeach

        </div>
    </section>
    <section>
        <div class="container">
            <div class="about-our-company d_flex j_content_between a_items_center"
                 style="background-image: url({{$home->about->image()}});">
                <div class="about-our-company__block">
                    <div class="about-our-company__text">
                        <h2>{{$home->about->title}}</h2>
                        <p>
                            {!! $home->about->desc !!}
                        </p>
                    </div>
                    <div class="about-our-company__href">
                        <a href="{{$home->about->url??"javascript:void(0)"}}">{{$home->about->button_text}}</a>
                    </div>
                </div>
                <div class="about-our-company__logo">
                    <img src="{{$home->about->image2()}}">
                </div>
            </div>
        </div>
    </section>
    <section>
        <div class="container bbest">
            <h2>{{ $home->suggestions3->title??null }}</h2>
            @foreach($types as $k=>$tp)
                <div class="best-offers">
                    <div class="best-offers__title">
                        <p>{{ t("app.block_3_$k") }}</p>
                    </div>
                    <div class="best-offers-slider">
                        @foreach($block_3 as $item)
                            @if($item->type_id == $tp->id)
                                <div class="best-offers-slider-box">
                                    @component('site.components.estate_card', ['item'=>$item,'favorites'=>$favorites])@endcomponent
                                </div>
                            @endif
                        @endforeach
                    </div>
                </div>
            @endforeach

        </div>
    </section>
    <section>
        <div class="container">
            <div class="download-attachment d_flex  j_content_between">
                <div class="download-attachment__title">
                    <h2>{{ $home->app->title }}</h2>
                    <p>
                        {!! $home->app->desc !!}
                    </p>
                    <div class="download-attachment__href d_flex">
                        <a href="{{ $home->app->url1 }}" target="_blank">
                            <img src="{{ $home->app->button_img1() }}">
                        </a>
                        <a href="{{ $home->app->url2 }}" target="_blank">
                            <img src="{{ $home->app->button_img2() }}">
                        </a>
                    </div>
                </div>
                <div class="mobile-app__img">
                    <img src="{{ $home->app->image() }}" alt="{{ $home->app->img_alt }}"
                         title="{{ $home->app->img_title }}">
                </div>
            </div>
        </div>
    </section>
    <section>
        <div class="container">
            <div class="news-tips-articles blog-pages ">
                <h2 style="color: #FE980F">{{t('app.Նորություններ, հուշումներ և հոդվածներ')}}</h2>
                <div class="tips-articles__block d_flex f_wrap ">
                    @if(!empty($news))
                        @foreach($news as $item)
                            @component('site.components.news_card', ['item'=>$item,'parent_news'=>$parent_news])@endcomponent
                        @endforeach
                    @endif
                </div>
            </div>
        </div>
    </section>
    @include('site.layouts.footer')
@endsection
@push('css')
    <style>
        .slick-dots {
            display: none;
        }

        .tips-articles__info {
            height: 100%;
            display: flex;
            flex-direction: column;
            justify-content: space-between;
        }
    </style>
@endpush
@push('js')
    <script src="{{ asset('f/site/js/jquery.formstyler.min.js') }}"></script>
    <script>
        $('select').styler();
    </script>
@endpush
