@extends('site.layouts.header')
@section('content')
    <section style="padding: 7px 0;
    background: white;
    border-top: 3px solid #f8f6f6;">
        <div class="container">
            <div class="product-det__title">
                <h1>{{$item->title}}</h1>
            </div>
        </div>
    </section>
    <section>
        <div class="container">
            <div class="product-det">
                <div class="product-det__min d_flex j_content_between">
                    <div class="product-det__left">
                        <div class="product-det__slid">
                            <div class="slider-for-photo">
                                @if(count($item->gallery))
                                    @foreach($item->gallery as $gallery)
                                        <div class="slider-nav-photo__box ">
                                            <div class="for-box__photo">
                                                <img width="100%" src="{{asset($gallery->image(false))}}" alt="png">
                                            </div>
                                        </div>
                                    @endforeach
                                @endif
                            </div>
                            @if(count($item->gallery))
                                <div class="slider-nav-photo ">
                                    @foreach($item->gallery as $gallery)
                                        <div class="slider-nav-photo__box small_gallery_block">
                                            <div class="nav-box__photo">
                                                <img src="{{asset('u/gallery/'.$gallery->image)}}" alt="png">
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            @endif
                        </div>
                        @if(!empty($item->description))
                            <div class="features__min">
                                <h2>{{ t('app.Նկարագրություն') }} </h2>
                                <p>
                                    {!! $item->description !!}
                                </p>
                            </div>
                        @endif
                        @if(!empty($item->youtube))
                            <div class="features__min">
                                <h2>{{ t('app.Տեսանյութ') }} </h2>
                                <div class="features__img">
                                    <iframe width="100%" height="100%"
                                            src="https://www.youtube.com/embed/{{$item->youtube}}" frameborder="0"
                                            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                                            allowfullscreen></iframe>
                                </div>
                            </div>
                        @endif
                        <div class="features__min">
                            <h2>{{ t('cabinet.Քարտեզ') }} </h2>
                            <div class="card">
                                <div class="little-p">
                                    <div id="map2" style="width: 100%; height: 400px"></div>
                                    <div class="mt-2" style="max-width: 300px">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="sale-apartment__info verticalCard mt-5">
                            @if(!empty($item->estates) && count($item->estates))
                                @foreach($item->estates as $alt)
                                    <div class="items">
                                        @component('site.components.estate_card', ['item'=>$alt,'favorites'=>$favorites])@endcomponent
                                    </div>
                                @endforeach
                            @endif
                        </div>
                    </div>
                    <div class="product-det__right" style="margin-top: 0!important;">
                        <div class="product-det__right__box">
                            <div style="margin-top: 0!important; " class="features__min">
                                <h2>{{ $item->owner }} </h2>
                                <div class="d_flex f_direction_column">
                                    @if(!empty($item->phone))
                                        <p class="d_flex a_items_center">
                                            <svg xmlns="http://www.w3.org/2000/svg"
                                                 xmlns:xlink="http://www.w3.org/1999/xlink" width="20" height="20"
                                                 viewBox="0 0 20 20">
                                                <defs>
                                                    <clipPath id="clip-path1">
                                                        <rect id="Rectangle_1987" data-name="Rectangle 1987" width="20"
                                                              height="20" transform="translate(1264 322)" fill="#a7a7a7"
                                                              stroke="#707070" stroke-width="1"/>
                                                    </clipPath>
                                                </defs>
                                                <g id="Mask_Group_152" data-name="Mask Group 152"
                                                   transform="translate(-1264 -322)" clip-path="url(#clip-path1)">
                                                    <g id="_001-telephone" data-name="001-telephone"
                                                       transform="translate(1264 322)">
                                                        <g id="Group_5691" data-name="Group 5691"
                                                           transform="translate(0)">
                                                            <path id="Path_11371" data-name="Path 11371"
                                                                  d="M15.205,13.806a1.4,1.4,0,0,0-2.118,0c-.5.491-.991.982-1.477,1.482a.292.292,0,0,1-.408.075c-.32-.175-.662-.316-.97-.508a15.364,15.364,0,0,1-3.7-3.371A8.775,8.775,0,0,1,5.2,9.357a.3.3,0,0,1,.075-.391c.5-.479.978-.97,1.465-1.461a1.407,1.407,0,0,0,0-2.168c-.387-.391-.774-.774-1.161-1.165s-.795-.8-1.2-1.2a1.41,1.41,0,0,0-2.118,0c-.5.491-.978.995-1.486,1.477A2.412,2.412,0,0,0,.015,6.082,6.894,6.894,0,0,0,.548,9.049a18.031,18.031,0,0,0,3.2,5.331,19.805,19.805,0,0,0,6.559,5.132,9.486,9.486,0,0,0,3.633,1.057,2.663,2.663,0,0,0,2.285-.87c.425-.474.9-.907,1.353-1.361a1.416,1.416,0,0,0,.008-2.156Q16.4,14.99,15.205,13.806Z"
                                                                  transform="translate(0 -0.576)" fill="#a7a7a7"/>
                                                            <path id="Path_11372" data-name="Path 11372"
                                                                  d="M17.38,11.112l1.536-.262a6.893,6.893,0,0,0-5.831-5.656l-.216,1.544a5.327,5.327,0,0,1,4.511,4.374Z"
                                                                  transform="translate(-2.97 -1.199)" fill="#a7a7a7"/>
                                                            <path id="Path_11373" data-name="Path 11373"
                                                                  d="M19.843,3.238A11.324,11.324,0,0,0,13.35,0l-.216,1.544a9.881,9.881,0,0,1,8.361,8.1l1.536-.262A11.407,11.407,0,0,0,19.843,3.238Z"
                                                                  transform="translate(-3.031)" fill="#a7a7a7"/>
                                                        </g>
                                                    </g>
                                                </g>
                                            </svg>
                                            {{$item->phone}}
                                        </p>
                                    @endif
                                    @if(!empty($item->email))
                                        <p class="d_flex a_items_center">
                                            <svg xmlns="http://www.w3.org/2000/svg"
                                                 xmlns:xlink="http://www.w3.org/1999/xlink" width="20" height="20"
                                                 viewBox="0 0 20 20">
                                                <defs>
                                                    <clipPath id="clip-path">
                                                        <rect id="Rectangle_1988" data-name="Rectangle 1988" width="20"
                                                              height="20" transform="translate(1264 380)" fill="#a7a7a7"
                                                              stroke="#707070" stroke-width="1"/>
                                                    </clipPath>
                                                </defs>
                                                <g id="Mask_Group_153" data-name="Mask Group 153"
                                                   transform="translate(-1264 -380)" clip-path="url(#clip-path)">
                                                    <g id="_001-email" data-name="001-email"
                                                       transform="translate(1264 382.875)">
                                                        <path id="Path_11374" data-name="Path 11374"
                                                              d="M24.154,5.581c0-.023.016-.045.015-.068l-6.1,5.881,6.1,5.693c0-.041-.007-.082-.007-.123V5.581Z"
                                                              transform="translate(-4.169 -4.147)" fill="#a7a7a7"/>
                                                        <path id="Path_11375" data-name="Path 11375"
                                                              d="M13.252,14.335l-2.492,2.4a.652.652,0,0,1-.9.007L7.38,14.424,1.244,20.338a1.414,1.414,0,0,0,.477.1H18.9a1.413,1.413,0,0,0,.686-.189Z"
                                                              transform="translate(-0.287 -6.183)" fill="#a7a7a7"/>
                                                        <path id="Path_11376" data-name="Path 11376"
                                                              d="M10.174,12.919l9.315-8.973a1.412,1.412,0,0,0-.719-.209H1.594a1.416,1.416,0,0,0-.9.336Z"
                                                              transform="translate(-0.16 -3.737)" fill="#a7a7a7"/>
                                                        <path id="Path_11377" data-name="Path 11377"
                                                              d="M0,5.885V17.05a1.417,1.417,0,0,0,.061.37l6.06-5.835Z"
                                                              transform="translate(0 -4.233)" fill="#a7a7a7"/>
                                                    </g>
                                                </g>
                                            </svg>
                                            {{$item->email}}
                                        </p>
                                    @endif
                                    @if(!empty($item->address))
                                        <p class="d_flex a_items_center">
                                            <svg xmlns="http://www.w3.org/2000/svg"
                                                 xmlns:xlink="http://www.w3.org/1999/xlink" width="20" height="20"
                                                 viewBox="0 0 20 20">
                                                <defs>
                                                    <clipPath id="clip-path3">
                                                        <rect id="Rectangle_1988" data-name="Rectangle 1988" width="20"
                                                              height="20" transform="translate(1244 376)" fill="#a7a7a7"
                                                              stroke="#707070" stroke-width="1"/>
                                                    </clipPath>
                                                </defs>
                                                <g id="Mask_Group_154" data-name="Mask Group 154"
                                                   transform="translate(-1244 -376)" clip-path="url(#clip-path3)">
                                                    <g id="_001-pin" data-name="001-pin"
                                                       transform="translate(1244 376)">
                                                        <g id="Group_5694" data-name="Group 5694">
                                                            <path id="Path_11378" data-name="Path 11378"
                                                                  d="M10,0A6.667,6.667,0,0,0,4.134,9.835l5.5,9.95a.417.417,0,0,0,.729,0l5.5-9.954A6.668,6.668,0,0,0,10,0Zm0,10a3.333,3.333,0,1,1,3.333-3.333A3.337,3.337,0,0,1,10,10Z"
                                                                  fill="#a7a7a7"/>
                                                        </g>
                                                    </g>
                                                </g>
                                            </svg>
                                            {{$item->address}}
                                        </p>
                                    @endif
                                </div>
                            </div>
                            @if(!empty($item->conditions))
                                <div class="features__min">
                                    <h2>{{ t('app.Պայմաններ') }} </h2>
                                    <div>
                                        {!! $item->conditions !!}
                                    </div>
                                </div>
                            @endif
                            @if(!empty($item->banner_image))
                                <div class="features__min">
                                    <a class="w-full" href="{{$item->banner_url}}">
                                        <div style="position: relative">
                                            <div
                                                style="position: absolute;left: 0;right: 0; height: 100%;width: 100%;z-index: 4444"
                                                class="p-5">
                                                <h3 class="font-22 font-white font-bold font-segeo"
                                                    style="color: white!important;">{{ $item->banner_title }}</h3>
                                                <div class="edited font-13 font-white font-bold font-segeo"
                                                     style="color: white!important;">
                                                    {!! $item->banner_text !!}
                                                </div>
                                            </div>
                                            <img style="width: 100%;filter: brightness(0.5);"
                                                 src="{{asset('u/new_buildings/'.$item->banner_image)}}" alt="">
                                        </div>
                                    </a>
                                </div>
                            @endif
                            @if(count($alts))
                                <div class="features__min">
                                    <h2>{{ t('app.Առաջարկվող նորակառույցներ') }} </h2>
                                    @foreach($alts as $alt)
                                        <div class="new_building_card">
                                            <a href="{{ route('page_item',['parent'=>$page->url,'current'=>$alt->url]) }}">
                                                <div class="p-2" style="box-shadow: 0 0 8px 5px #00000014;">
                                                    <img
                                                        src="{{asset('u/gallery/thumbs/'.(count($alt->gallery)?$alt->gallery->first()->image:''))}}"
                                                        alt="">
                                                </div>
                                                <div
                                                    class="new_building_card_title font-segeo font-16 font-logo-blue px-3 font-bold"
                                                    style="margin-top: -25px;position: relative">
                                                    <div class="p-2">
                                                        {{$alt->title}}
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                    @endforeach
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    @if ($errors->has('global'))
        <div class="error-notify">
            {{ t('auth.oop`s') }}
        </div>
    @elseif(session('message_sent'))
        <div class="success-notify">
            {{ t('auth.success') }}
        </div>
    @endif
    @include('site.layouts.footer')
@endsection
@push('css')
    <style>
        .edited * {
            color: white !important;
        }

        .items {
            display: block !important;
        }

        .verticalCard .apartment-info__cnt {
            display: flex;
            flex-direction: column;
            height: 100%;
        }

        .verticalCard .apartment-info__text .opt span {
            display: inline !important;
        }

        .verticalCard .items {
            margin-bottom: 20px;
            margin-top: 0 !important;
        }

        .verticalCard .items {
            width: 33.3333%;
            min-width: 280px;
            padding: 0 10px;
        }

        .verticalCard {
            display: flex;
            flex-wrap: wrap;
        }
    </style>
@endpush
@push('js')
    <script src="https://api-maps.yandex.ru/2.1/?apikey=ce752946-5050-4a17-9d27-624b2dc71d8b&lang=ru_RU"></script>
    <script>
        (function () {
            ymaps.ready(init2);
            var myMap2, tempPlacemark2 = null,
                latInput2 = $('.map-inp.lat'), longInput2 = $('.map-inp.lng'),
                myPlacemark2 = {}, placemarkData2 = {};

            function showOnMap2(position) {
                if (tempPlacemark2 !== null) {
                    myMap2.geoObjects.remove(tempPlacemark2);
                    tempPlacemark2 = null;
                    latInput.val('');
                    longInput.val('');
                }
                tempPlacemark2 = new ymaps.Placemark(position)
                myMap2.geoObjects.add(tempPlacemark2);
                latInput2.val(position[0]);
                longInput2.val(position[1]);
            }

            function init2() {
                myMap2 = new ymaps.Map("map2", {
                    center: [40.2, 44.5],
                    zoom: 10,
                });
                tempPlacemark2 = new ymaps.Placemark([{!! $item->lat !!}, {!! $item->lng !!}])
                myMap2.geoObjects.add(tempPlacemark2);
            }
        })();
    </script>
    <script>
        $('.block2 table').append($('.block1').find('tr').slice(Math.ceil($('.block1').find('tr').length / 2), parseInt($('.block1').find('tr').length)).remove())

        function initSlickByAram() {
            var list = $(document).find('.slick-list')[1];
            if (!$('.slick-next').length) {

                $(list).css('background', '#F8F6F6')
                $(list).css('z-index', '99999999999999999999999999')
            } else {
                $(list).css('z-index', '0')

            }
        }

        $(document).ready(function () {
            initSlickByAram()
        })
        $(window).resize(function () {
            initSlickByAram()
        })
    </script>
@endpush
