@extends('site.layouts.header')
@section('content')
    <section style="padding: 7px 0;
    background: white;
    border-top: 3px solid #f8f6f6;">
        <div class="container">
            <div class="product-det__title">
                <h1>{{t('app.Կապ մեզ հետ')}}</h1>
            </div>
        </div>
    </section>
    <div class="for-contacts__map">
        <iframe src="{{ $info->map->source }}" width="100%" height="100%" frameborder="0" style="border:0;"
                allowfullscreen=""></iframe>
    </div>
    <section>
        <div class="container">
            <div class="contact-us">
                <div class="contact-us__title">
                    <h2>{{t('app.Գրեք մեզ')}}</h2>
                </div>
                <div class="contact-us__info d_flex a_items_center j_content_between">
                    <div class="contact-us__href">
                        <ul>
                            <li>
                                <img src="{{asset('f/site/img/Group10.png')}}">
                                @foreach($info->contacts as $address)
                                    @if($address->address!='')
                                        <a href="javascript:void(0)">{{$address->address}}</a>
                                    @endif
                                @endforeach
                            <li>
                                <img src="{{asset('f/site/img/Group11.png')}}">
                                @foreach($info->contacts as $phone)
                                    @if($phone->phone!='')
                                        <a href="tel:{{$phone->phone}}">{{$phone->phone}}{{!($loop->last) && !empty($info->contacts[$loop->index+1]->phone)?',':''}}
                                            <br></a>
                                    @endif
                                @endforeach
                            </li>
                            <li>
                                <img src="{{asset('f/site/img/Group12.png')}}">
                                @foreach($info->contacts as $email)
                                    @if($email->email!='')
                                        <a href="mailto:{{$email->email}}">{{$email->email}}{{!($loop->last) && !empty($info->contacts[$loop->index+1]->email)?',':''}}
                                            <br></a>
                                    @endif
                                @endforeach
                            </li>
                        </ul>
                    </div>
                    <div class="contact-us__icon">
                        <ul>
                            @foreach($socials as $social)
                                <li><a target="_blank" href="{{$social->url}}">
                                        <img src="{{$social->icon()}}">
                                    </a></li>
                            @endforeach
                        </ul>
                    </div>
                </div>
                <div class="contact-us__block">
                    <form action="{{ route('contacts.send_mail') }}" method="post" id="contact-form">
                        @csrf
                        <div class="contact-us__form">
                            <div class="contact-us__cnt d_flex j_content_between f_wrap ">
                                <div
                                    class="contact-us__inp  mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                    <input name="name" class=" mdl-textfield__input name"
                                           placeholder="{{ t('app.Անուն:') }}"
                                           type="text">
                                    @if($errors->has('name'))
                                        <span class="err-text text-left w-100 d-inline-block" id="error"
                                              style="color: red">* {{$errors->first('name')}}</span>
                                        <style>.name {
                                                border: 1px solid red !important;
                                            }</style>
                                    @endif
                                </div>
                                <div
                                    class="contact-us__inp  mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                    <input name="theme" class=" mdl-textfield__input theme"
                                           placeholder="{{ t('app.Թեմա։') }}"
                                           type="text">
                                    @if($errors->has('theme'))
                                        <span class="err-text text-left w-100 d-inline-block " id="error"
                                              style="color: red">* {{$errors->first('theme')}}</span>
                                        <style>.theme {
                                                border: 1px solid red !important;
                                            }</style>
                                    @endif
                                </div>
                                <div
                                    class="contact-us__inp  mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                    <input name="mail" class=" mdl-textfield__input email" type="text"
                                           placeholder="{{ t('app.Էլ-հասցե։') }}">
                                    @if($errors->has('mail'))
                                        <span class="err-text text-left w-100 d-inline-block"
                                              id="error"
                                              style="color: red">* {{$errors->first('mail')}}</span>
                                        <style>.email {
                                                border: 1px solid red !important;
                                            }</style>
                                    @endif
                                </div>
                                <div
                                    class="contact-us__inp  mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                    <input class=" mdl-textfield__input phone" name="phone" type="text"
                                           placeholder="{{ t('app.Հեռ․՝') }}">
                                    @if($errors->has('phone'))
                                        <span class="err-text text-left w-100 d-inline-block" id="error"
                                              style="color: red">* {{$errors->first('phone')}}</span>
                                        <style>.phone {
                                                border: 1px solid red !important;
                                            }</style>
                                    @endif
                                </div>
                            </div>
                            <div
                                class="contact-us__textarea  mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                <textarea rows="6" style="width: 100%" class=" mdl-textfield__input messagess"
                                          name="message"
                                          placeholder="{{ t('app.Հաղորդագրություն։') }}"></textarea>
                                @if($errors->has('message'))
                                    <span class="err-text text-left w-100 d-inline-block" id="error"
                                          style="color: red">* {{$errors->first('message')}}</span>
                                    <style>.messagess {
                                            border: 1px solid red !important;
                                        }</style>
                                @endif
                            </div>

                            <div class="surface-area__btn" style="margin: 0;margin-top: 10px">
                                <button><i class="fas fa-paper-plane"></i> {{ t('app.Ուղարկել') }}</button>
                            </div>

                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
    @if ($errors->has('global'))
        <div class="error-notify">
            {{ t('auth.oop`s') }}
        </div>
    @elseif(session('message_sent'))
        <div class="success-notify">
            {{ t('auth.success') }}
        </div>
    @endif
    @include('site.layouts.footer')
@endsection
@push('css')
@endpush
