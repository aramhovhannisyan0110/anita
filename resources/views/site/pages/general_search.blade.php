@extends('site.layouts.header')
@section('content')
    <section style="padding: 7px 0;
    background: white;
    border-top: 3px solid #f8f6f6;">
        <div class="container">
            <div class="product-det__title">
                <h1>{{ $part??'' }}</h1>
            </div>
        </div>
    </section>
    <section>
        <div class="container">
            <div class="">
                <div class="">
                    <div class="">
                        <div class="">
                            <div class="">
                                <div class="" data-catalog="">
                                    <div class="">
                                        <form method="get" action="{{ route('search') }}">
                                            <input type="hidden" name="type" value="{{ $selected_tp }}">
                                            <input type="hidden" name="category" value="{{ $selected_cat }}">
                                            <div class="search" data-type="" style="display: flex;flex-wrap: wrap">
                                                <div class="product-rent__info">
                                                    <div class="address" data-type="0" style="margin-top: 4px">
                                                        <label for="">{{ t('search.region') }}</label>
                                                        <select data-type="0" onchange="getLocations(this)"
                                                                {{--                                                                multiple="multiple"--}}
                                                                name="region"
                                                                data-id="0"
                                                                class="form-control new_region for_select2_closable">
                                                            <option value="">----</option>
                                                            @if(!empty($regions) && count($regions))
                                                                @foreach($regions as $region)
                                                                    <option
                                                                        value="{{  $region->id  }}">{{ $region->title }}
                                                                    </option>
                                                                @endforeach
                                                            @endif
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="product-rent__info">
                                                    <label for="">{{ t('search.region1') }}</label>
                                                    <select data-type="0"
                                                            multiple="multiple"
                                                            name="regions[]"
                                                            data-id="0"
                                                            class="form-control new_region for-opt for_select2">
                                                    </select>
                                                </div>

                                                @if(!empty($global_filters))
                                                    @foreach($global_filters as $filter)
                                                        @if($filter->have_interval)
                                                            <div class="product-rent__info">
                                                                <label
                                                                    for="">{{$filter->title}} {!! (!empty($filter->metric) && (int)substr($filter->metric, -1)!=0)?'('.substr($filter->metric, 0, -1).'<sup>'.intval(substr($filter->metric, -1)).'</sup>)':(!empty($filter->metric))?'('.$filter->metric.')':'' !!} </label>


                                                                <select multiple="multiple"
                                                                        style="display: none"
                                                                        data-filter={{$filter->id}}
                                                                            name="with_interval_filter[{{$filter->id}}][]"
                                                                        id="">
                                                                    @if(count($filter->option))
                                                                        @foreach($filter->option->sortBy('sort')  as $option)
                                                                            <option
                                                                                value="{{$option->id}}">{{(!empty($option->options))?json_decode($option->options)->min:null}}
                                                                                - {{(!empty($option->options))?  ((json_decode($option->options)->max == -1)? t('search.more') : json_decode($option->options)->max):null}}</option>
                                                                        @endforeach
                                                                    @endif
                                                                </select>
                                                                <input type="hidden" class="equal"
                                                                       name="equal[{{$filter->id}}]">
                                                                <div class="rendered">
                                                                    <div>
                                                                        <select class="rendered_min for_select2"
                                                                                name="rendered_min[{{ $filter->id }}]"
                                                                                id=""
                                                                                data-for="{{$filter->id}}">
                                                                            @if(count($filter->option))
                                                                                @foreach($filter->option->sortBy('sort') as $option)
                                                                                    <option
                                                                                        data-value="{{(!empty($option->options))?json_decode($option->options)->min:null}}"
                                                                                        value="{{json_decode($option->options)->min}}">{{(!empty($option->options))?  ((json_decode($option->options)->min == 0)? t('search.no_min') : json_decode($option->options)->min):null}}
                                                                                    </option>
                                                                                @endforeach
                                                                            @endif
                                                                        </select>
                                                                    </div>
                                                                    <div>
                                                                        <select class="rendered_max for_select2"
                                                                                name="rendered_max[{{ $filter->id }}]"
                                                                                id=""
                                                                                data-for="{{$filter->id}}">
                                                                            @if(count($filter->option))
                                                                                <option
                                                                                    data-value="-1"
                                                                                    value="-1">
                                                                                    {{t('search.no_max')}}
                                                                                </option>
                                                                                @foreach($filter->option->sortBy('sort') as $option)
                                                                                    <option
                                                                                        data-value="{{(!empty($option->options))?json_decode($option->options)->max:null}}"
                                                                                        value="{{json_decode($option->options)->max}}">{{(!empty($option->options))?  ((json_decode($option->options)->max == -1)? t('search.more') : json_decode($option->options)->max):null}}</option>
                                                                                @endforeach
                                                                            @endif
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        @else
                                                            <div class="product-rent__info">
                                                                <label
                                                                    for="">{{$filter->title}} {!! (!empty($filter->metric) && (int)substr($filter->metric, -1)!=0)?'('.substr($filter->metric, 0, -1).'<sup>'.intval(substr($filter->metric, -1)).'</sup>)':(!empty($filter->metric))?'('.$filter->metric.')':'' !!} </label>
                                                                <select multiple="multiple"
                                                                        name="without_interval_filter[{{$filter->id}}][]"
                                                                        class="for_select2"
                                                                        id="">
                                                                    @if(count($filter->option))
                                                                        @foreach($filter->option as $option)
                                                                            <option
                                                                                value="{{$option->id}}">{{(!empty($option->options))?json_decode($option->options)->{app()->getLocale()}:null}}</option>
                                                                        @endforeach
                                                                    @endif
                                                                </select>
                                                            </div>
                                                        @endif
                                                    @endforeach
                                                @endif
                                                @if(!empty($local_filters))
                                                    @foreach($local_filters as $filter)
                                                        @if( $filter->have_interval)
                                                            <div class="product-rent__info">
                                                                <label
                                                                    for="">{{$filter->title}} {!! (!empty($filter->metric) && (int)substr($filter->metric, -1)!=0)?'('.substr($filter->metric, 0, -1).'<sup>'.intval(substr($filter->metric, -1)).'</sup>)':((!empty($filter->metric))?'('.$filter->metric.')':'') !!} </label>

                                                                <select multiple="multiple"
                                                                        style="display: none"
                                                                        data-filter="{{$filter->id}}"
                                                                        name="with_interval_filter[{{$filter->id}}][]"
                                                                        id="">
                                                                    @if(count($filter->option))
                                                                        @foreach($filter->option->sortBy('sort') as $option)
                                                                            <option
                                                                                value="{{$option->id}}">{{(!empty($option->options))?json_decode($option->options)->min:null}}
                                                                                -{{(!empty($option->options))?  ((json_decode($option->options)->max == -1)? t('search.more') : json_decode($option->options)->max):null}}</option>
                                                                        @endforeach
                                                                    @endif
                                                                </select>
                                                                <input type="hidden" class="equal"
                                                                       name="equal[{{$filter->id}}]">
                                                                <div class="rendered">
                                                                    <div>
                                                                        <select class="rendered_min for_select2"
                                                                                name="rendered_min[{{ $filter->id }}]"
                                                                                id=""
                                                                                data-for="{{$filter->id}}">
                                                                            @if(count($filter->option))
                                                                                @foreach($filter->option->sortBy('sort') as $option)
                                                                                    <option
                                                                                        data-value="{{(!empty($option->options))?json_decode($option->options)->min:null}}"
                                                                                        value="{{json_decode($option->options)->min}}">{{(!empty($option->options))?  ((json_decode($option->options)->min == 0)? t('search.no_min') : json_decode($option->options)->min):null}}
                                                                                    </option>
                                                                                @endforeach
                                                                            @endif
                                                                        </select>
                                                                    </div>
                                                                    <div>
                                                                        <select class="rendered_max for_select2"
                                                                                name="rendered_max[{{ $filter->id }}]"
                                                                                id=""
                                                                                data-for="{{$filter->id}}">
                                                                            <option
                                                                                data-value="-1"
                                                                                value="-1">
                                                                                {{t('search.no_max')}}
                                                                            </option>
                                                                            @if(count($filter->option))
                                                                                @foreach($filter->option->sortBy('sort') as $option)
                                                                                    <option
                                                                                        data-value="{{(!empty($option->options))?json_decode($option->options)->max:null}}"
                                                                                        value="{{json_decode($option->options)->max}}">{{(!empty($option->options))?  ((json_decode($option->options)->max == -1)? t('search.more') : json_decode($option->options)->max):null}}</option>
                                                                                @endforeach
                                                                            @endif
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        @else
                                                            <div class="product-rent__info">
                                                                <label
                                                                    for="">{{$filter->title}} {!! (!empty($filter->metric) && (int)substr($filter->metric, -1)!=0)?'('.substr($filter->metric, 0, -1).'<sup>'.intval(substr($filter->metric, -1)).'</sup>)':(!empty($filter->metric))?'('.$filter->metric.')':'' !!} </label>

                                                                <select multiple="multiple"
                                                                        name="without_interval_filter[{{$filter->id}}][]"
                                                                        class="for_select2"
                                                                        id="">
                                                                    @if(count($filter->option))
                                                                        @foreach($filter->option as $option)
                                                                            <option
                                                                                value="{{$option->id}}">{{(!empty($option->options))?json_decode($option->options)->{app()->getLocale()}:null}}</option>
                                                                        @endforeach
                                                                    @endif
                                                                </select>
                                                            </div>
                                                        @endif
                                                    @endforeach
                                                @endif
                                                <div class="product-rent__info">
                                                    <label for="">{{ t('search.status') }}</label>
                                                    <select data-type="0"
                                                            multiple="multiple"
                                                            name="status[]"
                                                            data-id="0"
                                                            class="form-control   for_select2">
                                                        <option value="0">{{ t('search.Свободно') }}</option>
                                                        <option value="1">{{ t('search.Продано') }}</option>
                                                        <option value="2">{{ t('search.Сдано в аренду') }}</option>
                                                    </select>
                                                </div>
                                                <div class="product-rent__info">
                                                    <label for="">{{ t('search.search by code') }}</label>
                                                    <input
                                                        class="form-control "
                                                        type="text" name="code"
                                                        placeholder="{{ t('search.enter code') }}"
                                                        style="width: 100%;padding: 13px 20px;font: 13px Segoe UI, HelveticaNeueCyr-Bold, Helvetica, Roboto, sans-serif;">
                                                </div>
                                            </div>
                                            @if($new)
                                            <div class="new_building">
                                                <label for="new" class="new_build">
                                                    <input type="checkbox" id="new" name="new">
                                                    <span class="checkmark"></span>{{ t('search.новостройка') }}
                                                </label>
                                            </div>
                                            @endif
                                            <div class="surface-area__btn">
                                                <button><i class="fas fa-search"></i> {{t('app.Որոնել')}}</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <input type="hidden" id="lang" value="{{app()->getLocale()}}">
        <div class="container passp" style="">
            <img src="{{$info->search->image()}}" style="width: 100%" alt="">
            <div style="">
                <h2 style="font-size: 35px">{{$info->search->title}}</h2>
                <p style="font-size: 22px">{{$info->search->desc}}</p>
            </div>
        </div>
    </section>
    @include('site.layouts.footer')
    <style>
        .product-rent__info label > sup {

            font-size: 9px;
            vertical-align: super;
        }

        .passp {
            position: relative;
            margin-top: 40px
        }

        .passp div {
            display: flex;
            padding: 40px 30px;
            position: absolute;
            height: 100%;
            top: 0;
            flex-direction: column;
            justify-content: flex-end;
            font-family: Segoe UI, HelveticaNeueCyr-Bold, Helvetica, Roboto, sans-serif;
            color: white
        }

        @media (max-width: 992px) {
            .passp div {
                padding: 20px 10px;
            }

            .passp h2 {
                font-size: 25px !important;
            }

            .passp p {
                font-size: 16px !important;
            }
        }

        @media (max-width: 576px) {
            .passp div {
                padding: 10px 5px;
            }

            .passp h2 {
                font-size: 19px !important;
            }

            .passp p {
                font-size: 13px !important;
            }
        }

        /* The container */
        .new_build {
            display: block;
            position: relative;
            padding-right: 30px;
            margin-bottom: 3px;
            cursor: pointer;
            font: 13px "Segoe UI", "HelveticaNeueCyr-Bold", "Helvetica", "Roboto", sans-serif;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
            font-weight: 700;
            color: #042a3d;
        }
        .select2-selection.select2-selection--multiple:after{
            content: '';
            position: absolute;
            right: 0;
            top: 50%;
            transform: translate(-50%, -50%);
            border-left: 7px solid transparent;
            border-right: 7px solid transparent;
            border-top: 7px solid #888888;
            cursor: pointer;
        }
        .select2-selection.select2-selection--multiple,.select2-search__field{
            cursor: pointer;
        }
        /* Hide the browser's default checkbox */
        .new_build input {
            position: absolute;
            opacity: 0;
            cursor: pointer;
            height: 0;
            width: 0;
        }

        /* Create a custom checkbox */
        .checkmark {
            position: absolute;
            top: 0;
            right: 0;
            height: 20px;
            width: 20px;
            border: 2px solid #FE980F;

        }

        /* On mouse-over, add a grey background color */
        .new_build:hover input ~ .checkmark {
            background-color: #FE980F4f;
        }

        /* When the checkbox is checked, add a blue background */
        .new_build input:checked ~ .checkmark {
            background-color: #FE980F;
        }

        /* Create the checkmark/indicator (hidden when not checked) */
        .checkmark:after {
            content: "";
            position: absolute;
            display: none;
        }

        /* Show the checkmark when checked */
        .new_build input:checked ~ .checkmark:after {
            display: block;
        }

        /* Style the checkmark/indicator */
        .new_build .checkmark:after {
            left: 4px;
            top: 0px;
            width: 5px;
            height: 10px;
            border: solid white;
            border-width: 0 3px 3px 0;
            -webkit-transform: rotate(45deg);
            -ms-transform: rotate(45deg);
            transform: rotate(45deg);
        }

        .new_building {
            width: 100%;
            display: flex;
            justify-content: flex-end;
            padding: 10px 11px 0;
        }

        .rendered {
            display: flex;
            justify-content: space-between;
        }

        .rendered div {
            width: 49%;
        }

        .rendered .select2.select2-container:first-child {
            margin-right: 30px;
        }

        .added {
            margin-top: 4px;
        }

        .select2-dropdown {
            border: 1px solid #ebe5e5;
        }

        .select2-selection.select2-selection--single {
            background-color: white;
            border: 1px solid #e6e5e5;
            border-radius: 0;
            cursor: text;
            /*color: #e6e5e5;*/
            font: 13px "Segoe UI", "HelveticaNeueCyr-Bold", "Helvetica", "Roboto", sans-serif;
            font-weight: 700;
            padding: 3px 12px;
            height: 100%;
            min-height: 44px;
        }

        .product-rent__info label {
            color: #565656;
            font: 13px "Segoe UI", "HelveticaNeueCyr-Bold", "Helvetica", "Roboto", sans-serif;
            font-weight: 700;
            display: block;
            padding: 5px 0px;
        }

        .select2-container--default .select2-selection--single .select2-selection__rendered {
            color: #757575;
            line-height: 20px;
            font: 13px "Segoe UI", "HelveticaNeueCyr-Bold", "Helvetica", "Roboto", sans-serif;
            margin-top: 8px;
        }

        .select2-container--default .select2-selection--single .select2-selection__arrow b {
            top: 80% !important;
            border-color: #888 transparent transparent transparent;
            border-style: solid;
            border-width: 7px 7px 0 7px;
            height: 7px;
            right: 0;
            transform: translateX(-50%);
            margin-left: -4px;
            margin-top: -2px;
            position: absolute;
            /*top: 50%;*/
            width: 0;
        }
        .select2-container--default.select2-container--open .select2-selection--single .select2-selection__arrow b {
            border-color: transparent transparent #888 transparent;
            border-width: 0 7px 7px 7px;
        }
        .product-rent__info {
            width: 25%;
            min-width: 254px;
        }

        .select2-container--default .select2-selection--multiple {
            background-color: white;
            border: 1px solid #e6e5e5;
            border-radius: 0px;
            cursor: text;
            /*color: #e6e5e5;*/
            font: 13px "Segoe UI", "HelveticaNeueCyr-Bold", "Helvetica", "Roboto", sans-serif;
            font-weight: 700;
            padding: 3px 12px;
            height: 100%;
            min-height: 44px;
        }

        .select2-selection__rendered {
            display: flex !important;
        }

        .select2-container .select2-search--inline .select2-search__field {
            margin-top: 10px !important;
        }

        .select2-container {
            width: 100% !important;
        }

        .select2-container--default .select2-results > .select2-results__options {

            font: 13px "Segoe UI", "HelveticaNeueCyr-Bold", "Helvetica", "Roboto", sans-serif;
        }

        .select2-container--default.select2-container--focus .select2-selection--multiple {
            border: solid lightgrey 1px !important;

        }

        .select2-container--default .select2-selection--multiple .select2-selection__choice {
            background-color: #ebe5e5;
            cursor: default;
            margin-right: 5px;
            margin-top: 7px;
            font: 13px "Segoe UI", "HelveticaNeueCyr-Bold", "Helvetica", "Roboto", sans-serif;
            padding: 0 8px;
            padding-bottom: 3px;
            border-radius: 4px;
            color: #00152c;
        }

        .select2-selection.select2-selection--multiple {
            max-height: 175px !important;
            overflow: auto !important;
        }

        .select2-selection.select2-selection--multiple::-webkit-scrollbar {
            width: 6px;
        }

        .select2-selection.select2-selection--multiple::-webkit-scrollbar-track {
            box-shadow: inset 0 0 6px rgba(0, 0, 0, 0.1)
        }

        .select2-selection.select2-selection--multiple::-webkit-scrollbar-thumb {
            background-color: #C1C1C1;
            outline: 1px solid #C1C1C1;
        }

        .select2-container--default .select2-selection--multiple .select2-selection__choice__remove {
            color: #00152c !important;
            font-weight: lighter;
        }

        .select2-results__options--nested > li:hover {
            background-color: #FE980F !important;
        }

        .select2-container--default .select2-results__option--highlighted[aria-selected] {
            background-color: #A3ABB1 !important;

        }

        .select2-container--default .select2-results__option[aria-selected=true] {
            background-color: #A3ABB1 !important;
            color: white !important;
        }

        .select2-results__options::-webkit-scrollbar {
            width: 6px;
        }

        .select2-results__options::-webkit-scrollbar-track {
            box-shadow: inset 0 0 6px rgba(0, 0, 0, 0.3);
        }

        .select2-results__options::-webkit-scrollbar-thumb {
            background-color: #C1C1C1;
            outline: 1px solid #C1C1C1;
        }

        .select2-results__option[aria-disabled = true] {
            display: none;
        }
    </style>
@endsection
@push('js')
    <script>
        (function () {
            var parent = document.querySelector(".range-slider");
            if (!parent) return;
            var
                rangeS = parent.querySelectorAll("input[type=range]"),
                numberS = parent.querySelectorAll("input[type=number]");

            rangeS.forEach(function (el) {
                el.oninput = function () {
                    var slide1 = parseFloat(rangeS[0].value),
                        slide2 = parseFloat(rangeS[1].value);

                    if (slide1 > slide2) {
                        [slide1, slide2] = [slide2, slide1];
                        // var tmp = slide2;
                        // slide2 = slide1;
                        // slide1 = tmp;
                    }

                    numberS[0].value = slide1;
                    numberS[1].value = slide2;
                }
            });

            numberS.forEach(function (el) {
                el.oninput = function () {
                    var number1 = parseFloat(numberS[0].value),
                        number2 = parseFloat(numberS[1].value);

                    if (number1 > number2) {
                        var tmp = number1;
                        numberS[0].value = number2;
                        numberS[1].value = tmp;
                    }

                    rangeS[0].value = number1;
                    rangeS[1].value = number2;

                }
            });

        })();


            // $('select').styler();
            $('.for_select2').select2(
                {
                    placeholder: '{{ t("search.select") }}',
                    closeOnSelect: false,
                }
            ).on('select2:not(selected)', e => $(e.currentTarget).data('scrolltop', $('.select2-results__options').scrollTop()))
                .on('select2:not(selected)', e => $('.select2-results__options').scrollTop($(e.currentTarget).data('scrolltop')));
            $('.rendered select').select2(
                {
                    placeholder: '{{ t("search.select") }}',
                    closeOnSelect: true,
                }
            )
            $('.for_select2_closable').select2(
                {
                    placeholder: '{{ t("search.select") }}',
                    closeOnSelect: true,
                }
            )
        $(function () {
            $(".items__min").slice(0, 8).show();
            $("#loadMore").on('click', function (e) {
                e.preventDefault();
                $(".items__min:hidden").slice(0, 8).slideDown();
                if ($(".items__min:hidden").length == 0) {
                    $("#loadMore" + "").fadeOut('slow');
                }

            });
        });
        $(function () {
            $(".items").slice(0, 8).show();
            $("#loadmach").on('click', function (e) {
                e.preventDefault();
                $(".items:hidden").slice(0, 8).slideDown();
                if ($(".items:hidden").length == 0) {
                    $("#loadmach" + "").fadeOut('slow');
                }

            });
        });

        function getLocations(a) {
            var data_type = $(a).data('type');
            var parent = $(a).val();
            var data_id = $(a).data('id') + 1;
            var lang = $('#lang').val();
            var options = '';
            var optgroup = '';
            $(".for-opt").find('optgroup').remove();
            $.ajax({
                url: '{{ route('getRegions') }}',
                type: "GET",
                data: {
                    parent_id: parent
                },
                headers: {
                    'X-CSRF-Token': '@csrf'
                },
                success: function (data) {
                    data = JSON.parse(data);
                    if (data != '') {
                        for (index in data) {
                            options = '';
                            for (opt in data[index].childes) {
                                options += '<option value="' + data[index].childes[opt].id + '">' + data[index].childes[opt].title[lang] + '</option>'
                            }
                            // console.log(data[index].childes)
                            optgroup += '<optgroup label="' + data[index].title[lang] + '">' + options + '</optgroup>'
                        }
                        $(".for-opt").append(optgroup)
                    }
                },
            });
        }

        $(document).on('click', '.select2-results__group', function () {
            var item = $(this).parent().attr('aria-label');
            var opt = $(document).find('optgroup[label=' + item + ']');
            if ($(this).next().find("li[aria-selected='true']").length == $(this).next().find('li').length) {
                $(this).next().find('li').each(function () {
                    $(this).attr('aria-selected', false)
                })
            } else {
                $(this).next().find('li').each(function () {
                    $(this).attr('aria-selected', true)
                })
            }
            if (opt.find("option:selected").length == opt.find('option').length) {
                opt.find('option').each(function () {
                    $(this).prop('selected', false)
                })
            } else {
                opt.find('option').each(function () {
                    $(this).prop('selected', true)
                })
            }
            opt.parent().trigger('change.select2');
        });
        $('.rendered select').change(function () {
            var min = parseInt($(this).parent().parent().find('.rendered_min').find('option:selected').data('value'));
            var max = parseInt($(this).parent().parent().find('.rendered_max').find('option:selected').data('value'));
            if (min == max) {
                $(this).closest('.product-rent__info').find('.equal').val(min);
                var opt = $(("select[data-filter=" + $(this).data('for') + "]")).find('option');
                opt.each(function () {
                    var vals = $(this).text().split('-');
                    $(this).prop('selected', false);

                    if (parseInt(vals[0]) >= min && (parseInt(vals[1]) <= max || max == -1)) {
                        $(this).prop('selected', true);
                    }
                })
                opt.trigger('change.select2')
            } else {
                $(this).closest('.product-rent__info').find('.equal').val('')
                if ($(this).hasClass('rendered_min')) {
                    $(this).parent().parent().find('.rendered_max').find('option:selected').prop('selected', false);
                    var rem = $(this).parent().parent().find('.rendered_max').find('option');
                    $(this).parent().parent().find('.rendered_max').trigger('change.select2');
                    rem.each(function () {
                        $(this).removeAttr('disabled')
                        if (parseInt($(this).data('value')) < min && parseInt($(this).data('value')) != -1) {
                            console.log(min, $(this).data('value'))
                            $(this).prop('disabled', true)
                        }
                    })
                    $(this).parent().parent().find('.rendered_max').select2(
                        {
                            placeholder: '{{ t("search.select") }}',
                            closeOnSelect: true,
                        }
                    );
                }
                var opt = $(("select[data-filter=" + $(this).data('for') + "]")).find('option');
                opt.each(function () {
                    var vals = $(this).text().split('-');
                    $(this).prop('selected', false);

                    if (parseInt(vals[0]) >= min && (parseInt(vals[1]) <= max || max == -1)) {
                        $(this).prop('selected', true);
                    }
                })
                opt.trigger('change.select2')

            }
        })
        {{--function getFilters(a) {--}}
        {{--    var data_type = $(a).data('type');--}}
        {{--    var data_category = $(a).val();--}}
        {{--    var lang = $('#lang').val();--}}
        {{--    $('.new_added').remove();--}}
        {{--    console.log($(".search[data-type=" + data_type + "]"))--}}
        {{--    $.ajax({--}}
        {{--        url: '{{ route('getFilters') }}',--}}
        {{--        type: "GET",--}}
        {{--        data: {--}}
        {{--            type: data_type,--}}
        {{--            category: data_category--}}
        {{--        },--}}
        {{--        headers: {--}}
        {{--            'X-CSRF-Token': '@csrf'--}}
        {{--        },--}}
        {{--        success: function (data) {--}}
        {{--            for (index in data) {--}}
        {{--                var data2 = data[index].option;--}}
        {{--                var options = '';--}}
        {{--                if (data[index].have_interval == 1) {--}}
        {{--                    for (i in data2) {--}}
        {{--                        options += '<option value="' + data2[i].id + '">' + JSON.parse(data2[i].options).min + '-' + JSON.parse(data2[i].options).max + '</option>'--}}
        {{--                    }--}}
        {{--                    $(".search[data-type=" + data_type + "]").append('<div class="new_added product-rent__info"><label for="' + data[index].id + '">' + data[index].title[lang] + ':</label><select  name="with_interval_filter[]" id="' + data[index].id + '">' + options + '<select></div>')--}}
        {{--                } else {--}}
        {{--                    for (i in data2) {--}}
        {{--                        options += '<option value="' + data2[i].id + '">' + JSON.parse(data2[i].options)[lang] + '</option>'--}}
        {{--                    }--}}
        {{--                    $(".search[data-type=" + data_type + "]").append('<div class="new_added product-rent__info"><label  for="' + data[index].id + '">' + data[index].title[lang] + ':</label><select name="without_interval_filter[]" id="' + data[index].id + '">' + options + '<select></div>')--}}
        {{--                }--}}
        {{--            }--}}
        {{--            $('select').styler()--}}
        {{--        },--}}
        {{--    });--}}
        {{--}--}}
    </script>
@endpush
