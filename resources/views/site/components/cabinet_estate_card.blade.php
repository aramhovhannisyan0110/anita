<div class="best-offers__block" style="background-color: {{(!empty($top) && $top )? '#ffffc363':''}}">
    <div style="position: relative " class="apartment-info__img">
        <a href="{{route('page_item',['parent'=>'estates','current'=>$item->url])}}"
           class="best-offers__img">
            @if(!empty($item->galleries->where('poster',1)->first()) && count($item->galleries->where('poster',1)))
                <img
                    src="{{ $item->galleries->where('poster',1)->first()->image(true) }}" alt="">
            @elseif(count($item->galleries))
                <img
                    src="{{ $item->galleries->first()->image(true) }}" alt="">
            @else
                <img
                    src="{{ asset('f/site/img/default.jpg') }}" alt="">
            @endif
            <div class="urgently">
                @if($item->urgent)
                    <span class="urgent">{{ t('app.Շտապ') }}</span>
                @endif
                @if($item->status == 1)
                    <span class="sold">{{ t('app.վաճառված') }}</span>
                @endif
                @if($item->status == 2)
                    <span class="sold">{{ t('app.վարձակալված') }}</span>
                @endif

            </div>
        </a>

    </div>
    <div class="best-offers__box">
        <div>
            <a href="{{route('page_item',['parent'=>'estates','current'=>$item->url])}}">{{$item->title}}</a>
            @if(!empty($item->locations) && !empty(\App\Models\Location::getLocationTitle($item->locations->locations_id??null)))
                <p><i class="fas fa-map-marker-alt"></i>
                    {{ \App\Models\Location::getLocationTitle($item->locations->locations_id) }}
                </p>
            @endif
            <strong>{{($item->price == -1)? t('app.Պայմանագրային')  :number_format($item->price??null ,0, ',', ' ').' $'}} </strong>
        </div>
        <div class="d_flex">
            <div data-id="{{$item->id}}"
                 class="mr-1 best-offers__heart1">
                <a href="javascript:;" id="delete" data-id="{{$item->id}}">
                    <svg  xmlns="http://www.w3.org/2000/svg" width="14.906" height="18.349" viewBox="0 0 14.906 18.349">
                        <path id="Path_11194" data-name="Path 11194" d="M14.859,4.1l-.4-1.214a1.124,1.124,0,0,0-1.067-.773H10V1A1,1,0,0,0,9,0H5.9a1,1,0,0,0-1,1V2.113H1.516a1.124,1.124,0,0,0-1.067.773L.046,4.1A.915.915,0,0,0,.17,4.92.9.9,0,0,0,.9,5.3h.421l.927,11.526A1.662,1.662,0,0,0,3.9,18.348h7.3a1.662,1.662,0,0,0,1.643-1.525L13.768,5.3H14a.905.905,0,0,0,.734-.378.916.916,0,0,0,.124-.819ZM5.972,1.075H8.933V2.113H5.972Zm5.8,15.662a.584.584,0,0,1-.578.537H3.9a.584.584,0,0,1-.578-.537L2.4,5.3H12.7ZM1.133,4.223l.33-1a.056.056,0,0,1,.053-.038H13.389a.056.056,0,0,1,.053.038l.33,1Zm0,0" transform="translate(0 0)" fill="#7a95a8"/>
                        <path id="Path_11195" data-name="Path 11195" d="M268.8,176.341h.028a.536.536,0,0,0,.533-.51l.5-9.69a.535.535,0,1,0-1.068-.056l-.5,9.69a.537.537,0,0,0,.506.565Zm0,0" transform="translate(-258.733 -159.642)" fill="#7a95a8"/>
                        <path id="Path_11196" data-name="Path 11196" d="M106.32,175.836a.536.536,0,0,0,.533.508h.03a.536.536,0,0,0,.5-.566l-.526-9.69a.535.535,0,1,0-1.068.058Zm0,0" transform="translate(-102.022 -159.645)" fill="#7a95a8"/>
                        <path id="Path_11197" data-name="Path 11197" d="M194.789,176.345a.536.536,0,0,0,.535-.538v-9.69a.535.535,0,1,0-1.069,0v9.69A.536.536,0,0,0,194.789,176.345Zm0,0" transform="translate(-187.33 -159.645)" fill="#7a95a8"/>
                    </svg>

                </a>
            </div>
            <div class="best-offers__heart1">
                <a href="{{ route('cabinet.editEstate',['id'=>$item->id]) }}">
                    <svg id="pencil" xmlns="http://www.w3.org/2000/svg" width="15.744" height="15.828" viewBox="0 0 15.744 15.828">
                        <g id="Group_5419" data-name="Group 5419" transform="translate(0 10.922)">
                            <g id="Group_5418" data-name="Group 5418">
                                <path id="Path_11242" data-name="Path 11242" d="M1.574,353.28,0,358.187,4.88,356.6Z" transform="translate(0 -353.28)" fill="#7a95a8"/>
                            </g>
                        </g>
                        <g id="Group_5421" data-name="Group 5421" transform="translate(2.68 2.062)">
                            <g id="Group_5420" data-name="Group 5420" transform="translate(0 0)">
                                <path id="Path_11304" data-name="Path 11304" d="M0,0,10.892-.029l-.013,4.736L-.013,4.736Z" transform="translate(0 7.723) rotate(-45)" fill="#7a95a8"/>
                            </g>
                        </g>
                        <g id="Group_5423" data-name="Group 5423" transform="translate(11.493)">
                            <g id="Group_5422" data-name="Group 5422">
                                <path id="Path_11243" data-name="Path 11243" d="M377.775,2.453,375.57.237a.758.758,0,0,0-1.1,0L373.76.95l3.306,3.324.708-.712A.768.768,0,0,0,377.775,2.453Z" transform="translate(-373.76 0)" fill="#7a95a8"/>
                            </g>
                        </g>
                    </svg>


                </a>
            </div>
        </div>
    </div>

</div>
<div class="best-offers__info d_flex a_items_end j_content_between bg-white p-2">
    @component('site.components.cards_filters_block', ['item'=>$item,'top'=>$top??false])@endcomponent
</div>
