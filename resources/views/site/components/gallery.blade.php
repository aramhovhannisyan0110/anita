@if(count($gallery??[]))
    <div class="photo-gallery d_flex" style="">
        <h2 style="width: 100%">{{t('dynamic.Տեսադարան')}}</h2>
        @foreach($gallery as $image)
            <div class="" style="width: 25%;min-width: 280px;padding: 10px">
                <a style="width: 100%" href="{{ asset('u/gallery/'.$image->image) }}" data-fancybox="gallery" class="">
                    <img width="100%" src="{{ asset('u/gallery/thumbs/'.$image->image) }}" alt="{{ $image->alt }}" title="{{ $image->title }}">
                </a>
            </div>
        @endforeach
        @if (empty($skip_plugins))
            @push('js')
                @js(aApp('fancybox/fancybox.js'))
            @endpush
            @push('css')
                @css(aApp('fancybox/fancybox.css'))
            @endpush
        @endif
    </div>
    <style>
        .photo-gallery {
            flex-wrap: wrap;
            justify-content: flex-start
        }
        @media (max-width: 1239px) {
            .photo-gallery div{
                width: 33.3333%!important;
            }
        }
        @media (max-width: 922px) {
            .photo-gallery div{
                width: 50%!important;
            }
        }
        @media (max-width: 627px) {
            .photo-gallery div{
                width: 100%!important;
            }
        }
    </style>
@endif
