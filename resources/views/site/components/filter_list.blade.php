@if( $filter->have_interval)
    <div class="new_added product-rent__info">
        <label for="">{{$filter->title}}:</label>
        <select multiple="multiple"
                style="display: none"
                data-filter="{{$filter->id}}"
                name="with_interval_filter[{{$filter->id}}][]"
                id="">
            @if(count($filter->option))
                @foreach($filter->option->sortBy('sort') as $option)
                    <option
                        {{ (array_key_exists($filter->id,$selected_with) && ($type->id == $selected_tp))? (in_array($option->id,$selected_with[$filter->id])?'selected':'') :''}}
                        value="{{$option->id}}">{{(!empty($option->options))?json_decode($option->options)->min:null}}
                        -{{(!empty($option->options))?  ((json_decode($option->options)->max == -1)? t('search.more') : json_decode($option->options)->max):null}}</option>
                @endforeach
            @endif
        </select>
        <input type="hidden" class="equal"
               {{ (array_key_exists($filter->id,$selected_with_min) && array_key_exists($filter->id,$selected_with_max)  && ($type->id == $selected_tp) )? ((int)$selected_with_min[$filter->id] == (int)$selected_with_max[$filter->id])?'value='.$selected_with_max[$filter->id].'': '' :'' }}
               name="equal[{{$filter->id}}]">
        <div class="rendered">
            <div>
                <select
                    class="rendered_min for_select2_closable"
                    name="rendered_min[{{ $filter->id }}]"
                    id=""
                    data-for="{{$filter->id}}">
                    @if(count($filter->option))
                        @foreach($filter->option->sortBy('sort') as $option)
                            <option
                                {{ (array_key_exists($filter->id,$selected_with_min)  && ($type->id == $selected_tp))? ((int)$selected_with_min[$filter->id] == (int)json_decode($option->options)->min)?'selected': '' :'' }}
                                data-value="{{(!empty($option->options))?json_decode($option->options)->min:null}}"
                                value="{{json_decode($option->options)->min}}">{{(!empty($option->options))?  ((json_decode($option->options)->min == 0)? t('search.no_min') : json_decode($option->options)->min):null}}
                            </option>
                        @endforeach
                    @endif
                </select>
            </div>
            <div>
                <select
                    class="rendered_max for_select2_closable"
                    name="rendered_max[{{ $filter->id }}]"
                    id=""
                    data-for="{{$filter->id}}">
                    <option
                        data-value="-1"
                        value="-1">
                        {{t('search.no_max')}}
                    </option>
                    @if(count($filter->option))
                        @foreach($filter->option->sortBy('sort') as $option)
                            @if(json_decode($option->options)->max != -1)
                                <option
                                    {{ (array_key_exists($filter->id,$selected_with_max)  && ($type->id == $selected_tp))? ((int)$selected_with_max[$filter->id] == (int)json_decode($option->options)->max)?'selected': '' :'' }}
                                    data-value="{{(!empty($option->options))?json_decode($option->options)->max:null}}"
                                    value="{{json_decode($option->options)->max}}">{{(!empty($option->options))?  ((json_decode($option->options)->max == -1)? t('search.more') : json_decode($option->options)->max):null}}</option>

                            @endif
                        @endforeach
                    @endif
                </select>
            </div>
        </div>
    </div>
@else
    <div class="new_added product-rent__info">
        <label for="">{{$filter->title}}:</label>
        <select class="for_select2"
                multiple="multiple"
                name="without_interval_filter[{{$filter->id}}][]"
                id="">
            @if(count($filter->option))
                @foreach($filter->option as $option)
                    <option
                        {{ (array_key_exists($filter->id,$selected_without) && ($type->id == $selected_tp))? (in_array($option->id,$selected_without[$filter->id])?'selected':'') :''}}
                        value="{{$option->id}}">{{(!empty($option->options))?json_decode($option->options)->{app()->getLocale()}:null}}</option>
                @endforeach
            @endif
        </select>
    </div>
@endif
