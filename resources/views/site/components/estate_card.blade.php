<div class="best-offers__block" style="background-color: {{(!empty($top) && $top )? '#ffffc363':''}}">
    <div style="position: relative " class="apartment-info__img">
        <a href="{{route('page_item',['parent'=>'estates','current'=>$item->url])}}"
           class="best-offers__img">
            @if(!empty($item->galleries->where('poster',1)->first()) && count($item->galleries->where('poster',1)))
                <img
                    src="{{ $item->galleries->where('poster',1)->first()->image(true) }}" alt="">
            @elseif(count($item->galleries))
                <img
                    src="{{ $item->galleries->first()->image(true) }}" alt="">
            @else
                <img
                    src="{{ asset('f/site/img/default.jpg') }}" alt="">
            @endif
            <div class="urgently">
                @if($item->urgent)
                    <span class="urgent">{{ t('app.Շտապ') }}</span>
                @endif
                @if($item->status == 1)
                    <span class="sold">{{ t('app.վաճառված') }}</span>
                @endif
                @if($item->status == 2)
                    <span class="sold">{{ t('app.վարձակալված') }}</span>
                @endif

            </div>
        </a>
        <div data-id="{{$item->id}}"
             class="card_heart best-offers__heart {{ (in_array($item->id,$favorites))?'active-heart':'' }}">
            <a href="javascript:;">
                <svg xmlns="http://www.w3.org/2000/svg" width="20" style="margin-right: -1px"
                     height="21.99"
                     viewBox="0 0 24.716 18">
                    <path id="heart"
                          d="M22.756,2.152A6.646,6.646,0,0,0,17.812,0a6.218,6.218,0,0,0-3.884,1.341,7.945,7.945,0,0,0-1.57,1.639,7.941,7.941,0,0,0-1.57-1.639A6.217,6.217,0,0,0,6.9,0,6.647,6.647,0,0,0,1.961,2.152,7.726,7.726,0,0,0,0,7.428,9.2,9.2,0,0,0,2.452,13.45a52.273,52.273,0,0,0,6.136,5.76c.85.725,1.814,1.546,2.815,2.421a1.451,1.451,0,0,0,1.911,0c1-.875,1.965-1.7,2.816-2.422a52.244,52.244,0,0,0,6.136-5.759,9.2,9.2,0,0,0,2.451-6.022,7.725,7.725,0,0,0-1.961-5.276Zm0,0"
                          transform="translate(0)" fill="#d9e2e9"/>
                </svg>
            </a>
        </div>
    </div>
    <div class="best-offers__box">
        <div>
            <a href="{{route('page_item',['parent'=>'estates','current'=>$item->url])}}">{{$item->title}}</a>
            @if(!empty($item->locations) && !empty(\App\Models\Location::getLocationTitle($item->locations->locations_id??null)))
                <p><i class="fas fa-map-marker-alt"></i>
                    {{ \App\Models\Location::getLocationTitle($item->locations->locations_id) }}
                </p>
            @endif
            <strong>{{($item->price == -1)? t('app.Պայմանագրային')  :number_format($item->price??null ,0, ',', ' ').' $'}} </strong>

        </div>
        <div class="best-offers__info d_flex a_items_end j_content_between">
            @component('site.components.cards_filters_block', ['item'=>$item,'top'=>$top??false])@endcomponent
        </div>
    </div>
</div>
