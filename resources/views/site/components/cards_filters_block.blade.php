<table>
    @if(!empty($item->estate_filter) && count($item->estate_filter))
        @foreach($item->estate_filter as $filter)
            @if($filter->filter->to_card)
                @if($filter->filter->have_interval)
                    @if(!empty($filter->value))
                        <tr>
                            <td>
                                <div><span class="opt"  style="background-color: {{(!empty($top) && $top )? '#ffffc363':''}}">{{$filter->filter->title}} <span class="delimeter">:</span><span class="dotess"> ..........................................................................................................</span></span>
                                </div>
                            </td>
                            <td>
                                <div>
                                    <b>{{$filter->value}}{!! (!empty($filter->filter->metric) && (int)substr($filter->filter->metric, -1)!=0)?substr($filter->filter->metric, 0, -1).'<sup>'.intval(substr($filter->filter->metric, -1)).'</sup>':((!empty($filter->filter->metric))?$filter->filter->metric:'')  !!}<span class="delimeter">, &nbsp;&nbsp;</span></b>
                                </div>
                            </td>
                        </tr>
                    @endif
                @else
                    @if(!empty($filter->filter->option) && count($filter->filter->option))
                        @foreach($filter->filter->option as $option)
                            @if($filter->option_id == $option->id)
                                <tr>
                                    <td data-val="{{$filter->option_id}}">
                                        <div>
                                            <span class="opt"  style="background-color: {{(!empty($top) && $top )? '#ffffc363':''}}">{{$filter->filter->title}} <span class="delimeter">:</span> <span class="dotess"> ..........................................................................................................</span></span>
                                        </div>
                                    </td>
                                    <td>
                                        <div>
                                            <b>{{(json_decode($option->options)->{app()->getLocale()})??null}}{!! (!empty($filter->filter->metric) && (int)substr($filter->filter->metric, -1)!=0)?substr($filter->filter->metric, 0, -1).'<sup>'.intval(substr($filter->filter->metric, -1)).'</sup>':((!empty($filter->filter->metric))?$filter->filter->metric:'')  !!}<span class="delimeter">, &nbsp;&nbsp;</span></b>
                                        </div>
                                    </td>
                                </tr>
                            @endif
                        @endforeach
                    @endif
                @endif
            @endif
        @endforeach
    @endif
    <tr>
        <td>
            <div><span class="opt"  style="background-color: {{(!empty($top) && $top )? '#ffffc363':''}}"> {{ t('app.Կոդ:') }} <span class="delimeter">:</span><span class="dotess"> ..........................................................................................................</span></span>
            </div>
        </td>
        <td><b>{{$item->code??null}}</b></td>
    </tr>
</table>
