<div class="tips-articles__box">
    <div class="tips-articles__info">
        <div>
            <a href="{{route('page_item',['parent'=>$parent_news->url,'current'=>$item->url])}}"
               class="tips-articles__img">
                <img src="{{asset('u/blog/'.$item->image)}}"   alt="{{ $item->image_alt }}" title="{{ $item->image_title }}">
            </a>
            <div class="tips-articles__text">
                <a href="{{route('page_item',['parent'=>$parent_news->url,'current'=>$item->url])}}">{{$item->title}}</a>
                <p>
                    {!! $item->short_desc !!}
                </p>
            </div>
        </div>
        <div class="">
            <div class="tips-articles__href d_flex a_items_center j_content_between">
                <p><i style="font-size: 16px;margin-right: 5px;"
                      class="far fa-clock"></i>{{date('d',strtotime($item->date))}} {{t('months.'.date('M',strtotime($item->date)).'')}} {{date('Y',strtotime($item->date))}}
                </p>
                <a href="{{route('page_item',['parent'=>$parent_news->url,'current'=>$item->url])}}">{{t('app.Դիտել ավելին')}}</a>
            </div>
        </div>
    </div>
</div>
