@extends('site.notifications.layout')
@section('content')
    <p><strong>Раздел:</strong> Контакты </p>
    <p><strong>Эл.почта:</strong> {{ $mail??null }}</p>
    <p><strong>Имя:</strong> {{ $name??null }}</p>
    <p><strong>Телефон:</strong> {{ $phone??null }}</p>
    <p><strong>Тема:</strong> {{ $theme??null }}</p>
    <div>
        <strong>Сообщение:</strong>
        <p>{{ $text??null }}</p>
    </div>
@endsection
