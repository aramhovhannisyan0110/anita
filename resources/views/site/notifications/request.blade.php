@extends('site.notifications.layout')
@section('content')
    <p><strong>Раздел:</strong> Страница недвижимостьи {{ $block }}</p>
    <p><strong>Код недвижимостьи:</strong> {{ $code??null }}</p>
    <p><strong>Эл.почта:</strong> {{ $mail??null }}</p>
    <p><strong>Имя:</strong> {{ $name??null }}</p>
    <p><strong>Мобильный:</strong> {{ $phone??null }}</p>
    <p><strong>Домашний:</strong> {{ $alt_phone??null }}</p>

@endsection
