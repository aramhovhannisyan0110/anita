@extends('admin.layouts.app')
@section('titleSuffix')| <a href="{!! route('admin.locations.add',['parent_id'=>$parent_id??0,'deep'=>$deep??0]) !!}"
                            class="text-cyan"><i class="mdi mdi-plus-box"></i> добавить</a>@endsection
@section('content')
    <div class="name my-2">
        <span style="pointer-events: none!important;"> {{ !empty($loc->name)?$loc->name:'Локация-'.$deep }} </span> <span style="display: none;font-size: inherit!important;" class="edt_name icon-btn edit"></span>
    </div>
    <form id="name" action="{{route('admin.locations.changeLocationName')}}" method="post" style="display: none" class="my-4">
        @csrf
        <input type="hidden" name="deep" value="{{ $deep??0 }}">
        <div class="w-25 d-inline-block">
{{--            @dd($loc)--}}
            @bylang(['id'=>'form_title', 'tp_classes'=>'little-p', 'title'=>'Название'])
            <input type="text" name="name[{!! $iso !!}]" class="form-control" placeholder="Название"
                   value="{{ old('name.'.$iso, tr($loc, 'name' , $iso)) }}">
            @endbylang
{{--            <input type="text" class="form-control" name="name" value="{{ $name??'Локация-'.$deep }}">--}}
        </div>
        <button type="submit" class="btn btn-info">Изменить</button>
    </form>
    @if(count($items))
        <div class="card">
            <div class="table-responsive">
                <table class="table table-striped m-b-0 columns-middle">
                    <thead>
                    <tr>
                        <th>Название</th>
                        <th>Статус</th>
                        <th>Действие</th>
                    </tr>
                    </thead>
                    <tbody class="table-sortable" data-action="{{ route('admin.locations.sort') }}">
                    @foreach($items as $item)
                        <tr class="item-row" data-id="{!! $item->id !!}">
                            <td class="item-title">{{ $item->a('title') }}</td>
                            @if($item->active)
                                <td class="text-success">Активно</td>
                            @else
                                <td class="text-danger">Неактивно</td>
                            @endif
                            <td>
                                <a href="{{ route('admin.locations.edit', ['id'=>$item->id]) }}"
                                   {!! tooltip('Редактировать') !!} class="icon-btn edit"></a>
                                @if($deep == 0 || $deep == 1)
                                <a href="{{ route('admin.locations.main', ['parent_id'=>$item->id,'deep'=>$deep+1]) }}" {!! tooltip('Сублокации') !!} ><i
                                        style="font-size: 20px" class="fas fa-map-marker-alt"></i></a>
                                @endif
                                    {{--                                <a href="{{ route('admin.gallery', ['gallery'=>'locations_item', 'key'=>$item->id]) }}" {!! tooltip('Галерея') !!} class="icon-btn gallery"></a>--}}
                                <span class="d-inline-block" style="margin-left:4px;" data-toggle="modal"
                                      data-target="#itemDeleteModal"><a href="javascript:void(0)"
                                                                        class="icon-btn delete" {!! tooltip('Удалить') !!}></a></span>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    @else
        <h4 class="text-danger"> @lang('admin/all.empty') </h4>
    @endif
    @modal(['id'=>'itemDeleteModal', 'centered'=>true, 'loader'=>true,
        'saveBtn'=>'Удалить',
        'saveBtnClass'=>'btn-danger',
        'closeBtn' => 'Отменить',
        'form'=>['id'=>'itemDeleteForm', 'action'=>'javascript:void(0)']])
    @slot('title') Удаление локации @endslot
    <input type="hidden" id="pdf-item-id">
    <p class="font-14">Вы действительно хотите удалить локацию со всеми своими сублокацияами?</p>
    @endmodal
@endsection
@push('css')
    <style>
        .edt_name:hover{
            cursor: pointer;
        }
        .name:hover .edt_name{
            display: inline!important;
        }
    </style>
@endpush
@push('js')
    <script>
        var itemId = $('#pdf-item-id'),
            blocked = false,
            modal = $('#itemDeleteModal');
        loader = modal.find('.modal-loader');

        function modalError() {
            loader.removeClass('shown');
            blocked = false;
            toastr.error('Возникла проблема!');
            modal.modal('hide');
        }

        modal.on('show.bs.modal', function (e) {
            if (blocked) return false;
            var $this = $(this),
                button = $(e.relatedTarget),
                thisItemRow = button.parents('.item-row');
            itemId.val(thisItemRow.data('id'));
        }).on('hide.bs.modal', function (e) {
            if (blocked) return false;
        });
        $('#itemDeleteForm').on('submit', function () {
            if (blocked) return false;
            blocked = true;
            var thisItemId = itemId.val();
            if (thisItemId && thisItemId.match(/^[1-9][0-9]{0,9}$/)) {
                loader.addClass('shown');
                $.ajax({
                    url: '{!! route('admin.locations.delete') !!}',
                    type: 'post',
                    dataType: 'json',
                    data: {
                        _token: csrf,
                        _method: 'delete',
                        item_id: thisItemId,
                    },
                    error: function (e) {
                        modalError();
                        console.log(e.responseText);
                    },
                    success: function (e) {
                        if (e.success) {
                            loader.removeClass('shown');
                            blocked = false;
                            toastr.success('Локация удалена');
                            modal.modal('hide');
                            $('.item-row[data-id="' + thisItemId + '"]').fadeOut(function () {
                                $(this).remove();
                            });
                        } else modalError();
                    }
                });
            } else modalError();
        });

        $('.edt_name').click(function () {
            $('#name').toggle()
        })


    </script>
@endpush
