@extends('admin.layouts.app')
@section('titleSuffix')
    |
    <div style="display: inline-block">
        <select class="form-control" onchange="location = this.value;">
            <option value="javascript:void(0)">----</option>
            @foreach($categories as $category)
                @foreach($types as $type)
                    <option
                        {{ ($selected_cat == $category->id && $selected_tp == $type->id)?'selected':'' }} value="{{ route('admin.estates.main',['category' => $category->id,'type' => $type->id]) }}">{{$category->title.'-'.$type->title}}</option>
                @endforeach
            @endforeach
        </select>
    </div>
    |
    <a href="{!! route('admin.estates.add',['category' => $selected_cat,'type' => $selected_tp]) !!}" class="text-cyan"><i
            class="mdi mdi-plus-box"></i>добавить</a>
    |
    <form method="get" action="" class="d-inline-flex align-items-baseline">
        <label for="code" class="text-nowrap mr-2">Поиск по коду</label>
        <input class="form-control" placeholder="Введите код" type="text" id="code" value="{{ request('code') }}"
               name="code">
    </form>
@endsection

@section('content')
    @if(count($items))
        <div class="card">
            <div class="table-responsive">
                <table class="table table-striped m-b-0 columns-middle">
                    <thead>
                    <tr>
                        <th>Иконка</th>
                        <th>Название</th>
                        <th>Статус</th>
                        <th>Пользователь</th>
                        <th>Действие</th>
                    </tr>
                    </thead>
                    <tbody class="table-sortable" data-action="{{ route('admin.estates.sort') }}">
                    @foreach($items as $item)
                        <tr class="item-row" data-id="{!! $item->id !!}">
                            <td class="item-title">
                                @if(!empty($item->galleries->where('poster',1)->first()) && count($item->galleries->where('poster',1)))
                                    <img width="100px"
                                         src="{{ $item->galleries->where('poster',1)->first()->image(true) }}" alt="">
                                @elseif(count($item->galleries))
                                    <img width="100px"
                                         src="{{ $item->galleries->first()->image(true) }}" alt="">
                                @else
                                    <img width="100px"
                                         src="{{ asset('f/site/img/default.jpg') }}" alt="">
                                @endif
                            </td>
                            <td class="item-title">
                                {!!  $item->a('title') . '<br>Код : ' . $item->code . '<br>Дата : ' . $item->created_at !!}
                            </td>
                            @if($item->active)
                                <td class="text-success">Активно</td>
                            @else
                                <td class="text-danger">Неактивно</td>
                            @endif
                            @if(!empty($item->user))
                                <td class="text-success">{{$item->user->name}}</td>
                            @else
                                <td class="text-danger">Admin</td>
                            @endif
                            <td>
                                <a href="{{ route('admin.estates.edit', ['id'=>$item->id]) }}"
                                   {!! tooltip('Редактировать') !!} class="icon-btn edit"></a>
{{--                                <a href="{{ route('admin.gallery', ['gallery'=>'estates', 'key'=>$item->id]) }}"--}}
{{--                                   {!! tooltip('Галерея') !!} class="icon-btn gallery"></a>--}}
                                <span class="d-inline-block" style="margin-left:4px;" data-toggle="modal"
                                      data-target="#itemDeleteModal"><a href="javascript:void(0)"
                                                                        class="icon-btn delete" {!! tooltip('Удалить') !!}></a></span>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                {{$items->links()}}
            </div>
        </div>
    @else
        <h4 class="text-danger"> @lang('admin/all.empty') </h4>
    @endif
    @modal(['id'=>'itemDeleteModal', 'centered'=>true, 'loader'=>true,
        'saveBtn'=>'Удалить',
        'saveBtnClass'=>'btn-danger',
        'closeBtn' => 'Отменить',
        'form'=>['id'=>'itemDeleteForm', 'action'=>'javascript:void(0)']])
    @slot('title') Удаление типа недвижимости @endslot
    <input type="hidden" id="pdf-item-id">
    <p class="font-14">Вы действительно хотите удалить данный тип недвижимости?</p>
    @endmodal
@endsection
@push('css')
@endpush
@push('js')
    <script>
        var itemId = $('#pdf-item-id'),
            blocked = false,
            modal = $('#itemDeleteModal');
        loader = modal.find('.modal-loader');

        function modalError() {
            loader.removeClass('shown');
            blocked = false;
            toastr.error('Возникла проблема!');
            modal.modal('hide');
        }

        modal.on('show.bs.modal', function (e) {
            if (blocked) return false;
            var $this = $(this),
                button = $(e.relatedTarget),
                thisItemRow = button.parents('.item-row');
            itemId.val(thisItemRow.data('id'));
        }).on('hide.bs.modal', function (e) {
            if (blocked) return false;
        });
        $('#itemDeleteForm').on('submit', function () {
            if (blocked) return false;
            blocked = true;
            var thisItemId = itemId.val();
            if (thisItemId && thisItemId.match(/^[1-9][0-9]{0,9}$/)) {
                loader.addClass('shown');
                $.ajax({
                    url: '{!! route('admin.estates.delete') !!}',
                    type: 'post',
                    dataType: 'json',
                    data: {
                        _token: csrf,
                        _method: 'delete',
                        item_id: thisItemId,
                    },
                    error: function (e) {
                        modalError();
                        console.log(e.responseText);
                    },
                    success: function (e) {
                        if (e.success) {
                            loader.removeClass('shown');
                            blocked = false;
                            toastr.success('Недвижимость удалена');
                            modal.modal('hide');
                            $('.item-row[data-id="' + thisItemId + '"]').fadeOut(function () {
                                $(this).remove();
                            });
                        } else modalError();
                    }
                });
            } else modalError();
        });
    </script>
@endpush
