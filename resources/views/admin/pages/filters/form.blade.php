@extends('admin.layouts.app')
@section('content')
    <form action="{!! $edit?route('admin.filters.edit', ['id'=>$item->id]):route('admin.filters.add') !!}" method="post"
          enctype="multipart/form-data">
        <input type="hidden" name="price_filter" value="{{$edit?$item->price_filter:($price_filter??0)}}">
        @csrf @method($edit?'patch':'put')
        @if ($errors->any())
            <div class="alert alert-danger" role="alert">
                @foreach ($errors->all() as $error)
                    <p>{{ $error }}</p>
                @endforeach
            </div>
        @endif
        <div class="row">
            <div class="col-12 col-lg-6">
                <div class="card">
                    @bylang(['id'=>'form_title', 'tp_classes'=>'little-p', 'title'=>'Название'])
                    <input type="text" name="title[{!! $iso !!}]" class="form-control" placeholder="Название"
                           value="{{ old('title.'.$iso, tr($item, 'title', $iso)) }}">
                    @endbylang
                </div>
                <div class="card">
                    <div class="c-title">Комментарии для админ-панеля</div>
                    <div class="c-body">
                        <input type="text" name="comment" class="form-control" placeholder="Комментарии"
                               value="{{ old('comment', $item->comment??null) }}">
                    </div>
                </div>
                <div class="card px-3 pt-3">
                    <div class="row cstm-input">
                        <div class="col-12 p-b-5">
                            <input class="labelauty-reverse toggle-bottom-input on-unchecked" type="checkbox"
                                   name="generate_url" value="1"
                                   data-labelauty="Вставить ссылку вручную" {!! oldCheck('generate_url', $edit?false:true) !!}>
                            <div class="bottom-input">
                                <input type="text" style="margin-top:3px;" name="url" class="form-control" id="form_url"
                                       placeholder="Ссылка" value="{{ old('url', $item->url??null) }}">
                            </div>
                        </div>
                    </div>
                    @labelauty(['id'=>'active', 'label'=>'Неактивно|Активно', 'checked'=>oldCheck('active', ($edit && empty($item->active))?false:true)])@endlabelauty
                </div>

            </div>

            <div class="col-12 col-lg-6">
                <div class="card">
                    <div class="c-title">Изоброжение 1:1 (40x40)</div>
                    @if (!empty($item->image))
                        <div class="p-2 text-center">
                            <img src="{{ asset('u/filters/'.$item->image) }}" alt="" class="img-responsive">
                        </div>
                    @endif
                    <div class="c-body">
                        @file(['name'=>'image'])@endfile
                    </div>
                </div>

                @if(!$price_filter)
                    <div class="card">
                        <div class="c-title">Тип фильтра</div>
                        <div class="c-body" style="display: flex;justify-content: space-between;flex-wrap: wrap">
                            @labelauty(['id'=>'type', 'label'=>'Глобальный|Глобальный', 'checked'=>oldCheck('type', ($edit && empty($item->type) || $price_filter)?false:true)])@endlabelauty
                            @labelauty(['id'=>'have_interval', 'label'=>'С интервалом|С интервалом', 'checked'=>oldCheck('have_interval', ($edit && empty($item->have_interval))?false:true)])@endlabelauty
                            @labelauty(['id'=>'to_card', 'label'=>'Показывать на карточке|Показывать на карточке', 'checked'=>oldCheck('to_card', ($edit && empty($item->to_card))?false:true)])@endlabelauty
                            @labelauty(['id'=>'is_required', 'label'=>'Обязательно для заполнения|Обязательно для заполнения', 'checked'=>oldCheck('is_required', ($edit && empty($item->is_required))?false:true)])@endlabelauty
                        </div>
                    </div>
            @else
                <input type="hidden" name="type" value="0">
                <input type="hidden" name="have_interval" value="1">
                <input type="hidden" name="to_card" value="0">
                <input type="hidden" name="is_required" value="1">
            @endif
                <div class="card">
                    @bylang(['id'=>'metric', 'tp_classes'=>'little-p', 'title'=>'Метрическое значение'])
                    <input type="text" name="metric[{!! $iso !!}]" class="form-control"
                           placeholder="Метрическое значение"
                           value="{{ old('metric.'.$iso, tr($item, 'metric', $iso)) }}">
                    @endbylang
                </div>
            </div>

        </div>
        <div class="row select2_cont">
            @if($price_filter)
                <input type="hidden" name="related[]" value="{{$relation}}">
            @endif
            @if(!empty($categories) && !empty($types) && !$price_filter)
                <div class="col-12">
                    <div class="card">
                        <div class="c-title">Категории-типы</div>
                        <div class="c-body" style="display: flex;justify-content: space-between">
                            <select class="categories form-control" name="related[]" multiple="multiple">
                                @foreach($categories as $category)
                                    @foreach($types as $type)
                                        @if($edit)
                                            @php($ids = $category->id.'-'.$type->id)
                                            <option
                                                {{in_array($ids,$related)?'selected':''}} value="{{$category->id.'-'.$type->id}}">{{$category->a('title').'-'.$type->a('title')}}</option>
                                        @else
                                            <option
                                                value="{{$category->id.'-'.$type->id}}">{{$category->a('title').'-'.$type->a('title')}}</option>
                                        @endif
                                    @endforeach
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
            @endif

        </div>
        @if(!$price_filter)
        <div class="row mt-4 without-interval {{($item->have_interval??null) || !$edit ?'hidden':''}} ">
            <div class="col-sm-12">
                <div class="card card-underline" style="margin:0;">
                    <div class="c-title">
                        Критерии
                    </div>
                    @bylang(['id'=>'criteria-container', 'tp_classes'=>'little-p', 'title'=>'Название'])
                    @if(!empty($item->option) && count($item->option) && !$item->have_interval)
                        @foreach($item->option as $option)
                            <div class="inputs_for_index my-2" style="display: flex">
                                <div class="col-11" data-index="{{$loop->index}}">
                                    <input placeholder="Введите название"
                                           class="form-control characteristics-inputs"
                                           name="filter_options[{{$option->id}}][{!! $iso !!}]"
                                           type="text"
                                           data-index="{{$loop->index}}"
                                           value="{{ json_decode($option->options)->{$iso}??null }}">
                                </div>
                                <div class="col-1  text-center delete-criterion-item">
                                    <i class="fa fa-trash " style="vertical-align: middle;"></i>
                                </div>
                            </div>
                        @endforeach
                    @endif
                    @endbylang
                    <div class="char-add-row text-center">
                        <i class="icon-btn add " onclick="addCriterionRow()"></i>

                    </div>
                </div>
                <!--end .card -->
            </div>
        </div>
        @endif
        <div class="row mt-4 with-interval {{($item->have_interval??null) || !$edit?'':'hidden'}}">
            <div class="col-sm-12">
                <div class="card card-underline" style="margin:0;">
                    <div class="c-title">Критерии</div>
                    <div class="c-body">
                        <table class="table table-bordered">
                            <thead>
                            <tr class="d-flex">
                                <td class="col-5">
                                    Мин. Значение
                                </td>
                                <td class="col-5">
                                    Макс. Значение
                                </td>
                                <td class="col-2">
                                    Удалить
                                </td>
                            </tr>
                            </thead>
                            <tbody class="intervaled-criteria-container">
                            @if(!empty($item->option) && count($item->option) && $item->have_interval)
                                @foreach($item->option as  $option)
                                    @if(json_decode($option->options)->max != -1)
                                        <tr class="added-criterion-row d-flex">
                                            <td class="col-5">
                                                <input readonly placeholder="Мин. значение"
                                                       class="form-control characteristics-inputs min"
                                                       name="filter_options[{{$option->id}}][min]"
                                                       type="number"
                                                       value="{{ json_decode($option->options)->min??null }}">
                                            </td>
                                            <td class="col-5">
                                                <input {{ !$loop->last?'readonly':''}} placeholder="Макс. значение"
                                                       class="form-control characteristics-inputs max"
                                                       name="filter_options[{{$option->id}}][max]"
                                                       type="number"
                                                       onkeyup="valid()"
                                                       value="{{ json_decode($option->options)->max??null }}">

                                            </td>
                                            <td class="text-center col-2 ">
                                                <a class="icon-btn delete delete2 {{ $loop->last?'delete-criterion-item2':''}} "></a>
                                            </td>
                                        </tr>
                                    @endif
                                @endforeach
                            @endif
                            </tbody>
                        </table>
                        <div class="w-full text-center">
                            <i class="icon-btn add char-add-row  text-center" onclick="addCriterionRow2()"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <style>
            .select2, .select2_cont {
                display: {{ ($edit && $item->type) || !$edit?'none':'' }};
            }

            .hidden {
                display: none;
            }

            .delete-criterion-item:hover {
                cursor: pointer;
                color: #990c03;
            }

            .delete-criterion-item2 {
                color: red !important;
            }

            .icon-btn:hover {
                color: darkgreen;
                cursor: pointer;
            }

            .select2-search__field {
                min-width: 300px !important;
            }
        </style>
        @seo(['item'=>$item])@endseo
        <div class="col-12 save-btn-fixed">
            <button type="submit"></button>
        </div>
    </form>
@endsection
@push('js')
    <script>
        var isos = '{!! json_encode($isos) !!}';
        isos = JSON.parse(isos);

        function charRow(index, iso) {
            return ' <div  class="inputs_for_index my-2" style="display: flex"> <div class="col-11 " data-index="' + index + '"> <input placeholder="Введите название" class="form-control characteristics-inputs" name="filter_options[' + index + '][' + iso + ']" type="text" value="" data-index="' + index + '"></div>' + '<div class="col-1  text-center delete-criterion-item"><i class="fa fa-trash"></i> </div></div>';
        }

        $(document).on('click', '.delete-criterion-item', function () {
            var dataIndex = $(this).prev().data('index');
            console.log(dataIndex)
            for (var j = 0; j < isos.length; j++) {
                $('.characteristics-inputs').each(function () {
                    if ($(this).data('index') == dataIndex) {
                        $(this).parent().parent().remove();
                    }
                })
            }
        });

        function addCriterionRow() {
            var index = 0;
            $('.characteristics-inputs').each(function () {

                if ($(this).data('index') > index) {
                    index = $(this).data('index');
                }
            });
            index++;
            for (var j = 0; j < isos.length; j++) {
                $('#criteria-container_' + isos[j]).append(charRow(index, isos[j]))
            }
        }

        var charContainer2 = $('.intervaled-criteria-container');

        function charRow2(index, min = 0) {
            return '<tr class="added-criterion-row d-flex">' +
                '<td class="col-5">' +
                '<input readonly placeholder="Введите Значение" class=" form-control characteristics-inputs min" name="filter_options[new-' + index + '][min]" type="number" value="' + min + '">' +
                '</td>' +
                '<td class="col-5">' +
                '<input placeholder="Введите Значение" class=" form-control characteristics-inputs max" min="' + min + '" name="filter_options[new-' + index + '][max]" type="number" value="" onkeyup="valid()">' +
                '</td>' +
                '<td class="text-center col-2">' +
                '<a class="icon-btn delete delete2 delete-criterion-item2"></a>' +
                '</td>' +
                '</tr>';
        }

        $(document).on('click', '.delete-criterion-item2', function () {
            $(this).parents('tr').remove();
            charContainer2.find('.max').last().removeAttr('readonly');
            charContainer2.find('.delete2').last().addClass('delete-criterion-item2');
        });

        function addCriterionRow2() {

            var index = charContainer2.find('tr').length;
            var min = charContainer2.find('.max').last().val();
            charContainer2.find('.max').last().attr('readonly', 'readonly');
            charContainer2.find('.delete2').last().removeClass('delete-criterion-item2');
            charContainer2.append(charRow2(index, min));
        }

        function valid() {
            var val1 = parseInt(charContainer2.find('.min').last().val());
            var val2 = parseInt(charContainer2.find('.max').last().val());
            console.log(val1, val2)
            if (val2 <= val1) {
                charContainer2.find('.max').last().css('border', '1px solid red')
                $('.char-add-row').css('pointer-events', 'none')
            } else {
                charContainer2.find('.max').last().css('border', 'none')
                $('.char-add-row').css('pointer-events', 'all')

            }
        };

        $('#have_interval').click(function () {
            if ($(this).is(':checked')) {
                $('.without-interval').hide();
                $('.without-interval').find('.inputs_for_index').remove();
                $('.with-interval').show();

            } else {
                $('.without-interval').show();
                $('.with-interval').hide();
                $('.with-interval').find('.added-criterion-row').remove();
            }
        });
        $('#type').click(function () {
            if ($(this).is(':checked')) {
                $('.select2').hide();
                $('.select2_cont').hide();
            } else {
                $('.select2').show();
                $('.select2_cont').show();

            }
        });
        {{--        var charContainer = $('.criteria-container');--}}
        {{--        function charRow(index) {--}}
        {{--            return '<tr class="added-criterion-row d-flex">' +--}}
        {{--                '<td class="col-10">' +--}}
        {{--                '<input placeholder="Введите Значение" class=" form-control characteristics-inputs" name="filter_options[' + index + '][value]" type="text" value="">' +--}}
        {{--                '</td>' +--}}
        {{--                '<td class="text-center col-2">' +--}}
        {{--                '<a class="icon-btn delete delete-criterion-item"></a>' +--}}
        {{--                '</td>' +--}}
        {{--                '</tr>';--}}
        {{--        }--}}
        {{--        $(document).on('click', '.delete-criterion-item', function () {--}}
        {{--            $(this).parents('tr').remove();--}}
        {{--        });--}}
        {{--        function addCriterionRow() {--}}
        {{--            var index = charContainer.find('tr').length;--}}
        {{--            charContainer.append(charRow(index));--}}
        {{--        }--}}

        $(document).ready(function () {
            $('.categories').select2(
                {
                    placeholder: '{{ t("search.select") }}',
                    closeOnSelect: false,
                }
            ).on('select2:not(selected)', e => $(e.currentTarget).data('scrolltop', $('.select2-results__options').scrollTop()))
                .on('select2:not(selected)', e => $('.select2-results__options').scrollTop($(e.currentTarget).data('scrolltop')));
        });
    </script>
    @ckeditor
@endpush
