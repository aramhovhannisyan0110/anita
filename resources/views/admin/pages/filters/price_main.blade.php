@extends('admin.layouts.app')
@section('titleSuffix')@endsection
@section('content')
    @if(count($items))
        <div class="card">
            <div class="table-responsive">
                <table class="table table-striped m-b-0 columns-middle">
                    <thead>
                    <tr>
                        <th>Иконка</th>
                        <th>Название</th>
                        <th>Комментарии</th>
                        <th>Действие</th>
                    </tr>
                    </thead>
                    <tbody class="" data-action="{{ route('admin.filters.sort') }}">
                    @foreach($items as $item)
                        <tr class="item-row" data-id="{{$item['filter']->id??null}}">
                            <td class="item-title">
                                @if(!empty($item['filter']))
                                <img src="{{asset('u/filters/'.$item['filter']->image)}}" alt="">
                            @endif
                            </td>
                            <td class="item-title">{{ $item['title'] }}</td>
                            <td class="item-title">{{ $item['filter']?$item['filter']->comment:'' }}</td>
                            <td>
                                @if(!empty($item['filter']))
                                    <a href="{{ route('admin.filters.edit', ['id'=>$item['filter']->id,'price_filter'=>1]) }}"
                                       {!! tooltip('Редактировать') !!} class="icon-btn edit"></a>
                                @else
                                    <a href="{{ route('admin.filters.add',['price_filter'=>1,'relation'=>$item['relation']]) }}"
                                       {!! tooltip('Редактировать') !!} class="icon-btn edit"></a>
                                @endif
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    @else
        <h4 class="text-danger"> @lang('admin/all.empty') </h4>
    @endif
    @modal(['id'=>'itemDeleteModal', 'centered'=>true, 'loader'=>true,
        'saveBtn'=>'Удалить',
        'saveBtnClass'=>'btn-danger',
        'closeBtn' => 'Отменить',
        'form'=>['id'=>'itemDeleteForm', 'action'=>'javascript:void(0)']])
    @slot('title') Удаление типа недвижимости @endslot
    <input type="hidden" id="pdf-item-id">
    <p class="font-14">Вы действительно хотите удалить данный тип недвижимости?</p>
    @endmodal
@endsection
@push('css')
@endpush
@push('js')
    <script>
        var itemId = $('#pdf-item-id'),
            blocked = false,
            modal = $('#itemDeleteModal');
        loader = modal.find('.modal-loader');

        function modalError() {
            loader.removeClass('shown');
            blocked = false;
            toastr.error('Возникла проблема!');
            modal.modal('hide');
        }

        modal.on('show.bs.modal', function (e) {
            if (blocked) return false;
            var $this = $(this),
                button = $(e.relatedTarget),
                thisItemRow = button.parents('.item-row');
            itemId.val(thisItemRow.data('id'));
        }).on('hide.bs.modal', function (e) {
            if (blocked) return false;
        });
        $('#itemDeleteForm').on('submit', function () {
            if (blocked) return false;
            blocked = true;
            var thisItemId = itemId.val();
            if (thisItemId && thisItemId.match(/^[1-9][0-9]{0,9}$/)) {
                loader.addClass('shown');
                $.ajax({
                    url: '{!! route('admin.filters.delete') !!}',
                    type: 'post',
                    dataType: 'json',
                    data: {
                        _token: csrf,
                        _method: 'delete',
                        item_id: thisItemId,
                    },
                    error: function (e) {
                        modalError();
                        console.log(e.responseText);
                    },
                    success: function (e) {
                        if (e.success) {
                            loader.removeClass('shown');
                            blocked = false;
                            toastr.success('Тип недвижимости удален');
                            modal.modal('hide');
                            $('.item-row[data-id="' + thisItemId + '"]').fadeOut(function () {
                                $(this).remove();
                            });
                        } else modalError();
                    }
                });
            } else modalError();
        });
    </script>
@endpush
