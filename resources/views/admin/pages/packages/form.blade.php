@extends('admin.layouts.app')
@section('content')
    <form action="{!! $edit?route('admin.packages.edit', ['id'=>$item->id]):route('admin.packages.add') !!}"
          method="post"
          enctype="multipart/form-data">
        <input type="hidden" name="user_type" value="{{$edit? $item->user_type : $user_type??1}}">
        <input type="hidden" name="package" value="{{$edit? $item->package : $package_type??1}}">
        @csrf @method($edit?'patch':'put')
        @if ($errors->any())
            <div class="alert alert-danger" role="alert">
                @foreach ($errors->all() as $error)
                    <p>{{ $error }}</p>
                @endforeach
            </div>
        @endif
        <div class="row">
            <div class="col-12 col-lg-6">
                <div class="card">
                    <div class="c-title">
                        Сервис
                    </div>
                    <div class="c-body">
                        @if($package_type == 1)
                            <input type="number" name="count" class="form-control my-2"
                                   placeholder="Количество oбъявлений"
                                   value="{{ old('count', $item->count??null) }}">
                        @else
                            <input type="number" name="day" class="form-control my-2" placeholder="Количество дней"
                                   value="{{ old('day', $item->day??null) }}">
                        @endif

                        <input type="number" name="price" class="form-control mb-2" placeholder="Сумма"
                               value="{{ old('price', $item->price??null) }}">
                        @if($package_type == 1)
                            @bylang(['id'=>'form_desc', 'tp_classes'=>'little-p', 'title'=>'Описание'])
                            <textarea name="desc[{!! $iso !!}]" class="form-control my-2" rows="6"
                                      placeholder="Описание">{{ old('desc.'.$iso, tr($item, 'desc', $iso)) }}</textarea>
                            @endbylang
                        @endif
                    </div>

                </div>
                <div class="card px-3 pt-3">
                    @labelauty(['id'=>'active', 'label'=>'Неактивно|Активно', 'checked'=>oldCheck('active', ($edit && empty($item->active))?false:true)])@endlabelauty
                </div>
            </div>

        </div>
        <div class="col-12 save-btn-fixed">
            <button type="submit"></button>
        </div>

    </form>
@endsection
@push('js')
    @ckeditor
@endpush
