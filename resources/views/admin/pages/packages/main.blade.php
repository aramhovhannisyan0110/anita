@extends('admin.layouts.app')
@section('titleSuffix') @endsection
@section('content')

    <ul class="nav nav-tabs justify-content-end">
        <li><a class="px-3 py-2 d-block active" data-toggle="tab" href="#home">Личность</a></li>
        <li><a class="px-3 py-2 d-block" data-toggle="tab" href="#menu1">Компания</a></li>
    </ul>
    <div class="tab-content">
        <div id="home" class="tab-pane fade in active show p-2">
            <div class="py-1 d-flex justify-content-end">
                <a href="{!! route('admin.packages.add',['package_type'=>$package_type??1,'user_type' => 1]) !!}"
                   class="text-cyan"><i class="mdi mdi-plus-box"></i> Добавить сервис  <span class="text-danger">"{{ $package??null }}"</span>  для личностьей</a>
            </div>
            @if(count($user_items))
                <div class="card">
                    <div class="table-responsive">
                        <table class="table table-striped m-b-0 columns-middle">
                            <thead>
                            <tr>
                                @if($package_type == 1)
                                    <th>Количество обявлений</th>
                                @else
                                    <th>Количество дней</th>
                                @endif
                                <th>Стоимость</th>
                                <th>Статус</th>
                                <th>Действие</th>
                            </tr>
                            </thead>
                            <tbody class="table-sortable" data-action="{{ route('admin.packages.sort') }}">
                            @foreach($user_items as $item)
                                <tr class="item-row" data-id="{!! $item->id !!}">
                                    @if($package_type == 1)
                                        <td class="item-title">{{ $item->count }}</td>
                                    @else
                                        <td class="item-title">{{ $item->day }}</td>
                                    @endif
                                    <td>
                                        {{ $item->price }}
                                    </td>
                                    @if($item->active)
                                        <td class="text-success">Активно</td>
                                    @else
                                        <td class="text-danger">Неактивно</td>
                                    @endif
                                    <td>
                                        <a href="{{ route('admin.packages.edit', ['id'=>$item->id]) }}"
                                           {!! tooltip('Редактировать') !!} class="icon-btn edit"></a>
                                        <span class="d-inline-block" style="margin-left:4px;" data-toggle="modal"
                                              data-target="#itemDeleteModal"><a href="javascript:void(0)"
                                                                                class="icon-btn delete" {!! tooltip('Удалить') !!}></a></span>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            @else
                <h4 class="text-danger text-center py-5">  @lang('admin/all.empty')  </h4>
            @endif
        </div>
        <div id="menu1" class="tab-pane fade p-2">
            <div class="py-1 d-flex justify-content-end">
                <a href="{!! route('admin.packages.add',['package_type'=>$package_type??1,'user_type' => 2]) !!}"
                   class="text-cyan"><i class="mdi mdi-plus-box"></i> Добавить сервис <span class="text-danger">"{{ $package??null }}"</span> для компаний</a>
            </div>
            @if(count($company_items))
                <div class="card">
                    <div class="table-responsive">
                        <table class="table table-striped m-b-0 columns-middle">
                            <thead>
                            <tr>
                                <th>Название</th>
                                <th>Дата</th>
                                <th>Статус</th>
                                <th>Действие</th>
                            </tr>
                            </thead>
                            <tbody class="table-sortable" data-action="{{ route('admin.packages.sort') }}">
                            @foreach($company_items as $item)
                                <tr class="item-row" data-id="{!! $item->id !!}">
                                    @if($package_type == 1)
                                        <td class="item-title">{{ $item->count }}</td>
                                    @else
                                        <td class="item-title">{{ $item->day }}</td>
                                    @endif
                                    <td>
                                        {{$item->price}}
                                    </td>
                                    @if($item->active)
                                        <td class="text-success">Активно</td>
                                    @else
                                        <td class="text-danger">Неактивно</td>
                                    @endif
                                    <td>
                                        <a href="{{ route('admin.packages.edit', ['id'=>$item->id]) }}"
                                           {!! tooltip('Редактировать') !!} class="icon-btn edit"></a>
                                        <span class="d-inline-block" style="margin-left:4px;" data-toggle="modal"
                                              data-target="#itemDeleteModal"><a href="javascript:void(0)"
                                                                                class="icon-btn delete" {!! tooltip('Удалить') !!}></a></span>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            @else
                <h4 class="text-danger  text-center py-5">  @lang('admin/all.empty')  </h4>
            @endif
        </div>
    </div>



    @modal(['id'=>'itemDeleteModal', 'centered'=>true, 'loader'=>true,
        'saveBtn'=>'Удалить',
        'saveBtnClass'=>'btn-danger',
        'closeBtn' => 'Отменить',
        'form'=>['id'=>'itemDeleteForm', 'action'=>'javascript:void(0)']])
    @slot('title') Удаление новости @endslot
    <input type="hidden" id="pdf-item-id">
    <p class="font-14">Вы действительно хотите удалить данная новость?</p>
    @endmodal
@endsection
@push('css')
    <style>
        .nav-tabs > li > a {
            border-top-left-radius: 7px;
            border-top-right-radius: 7px;
            border: 1px solid #80808063;
            border-bottom: unset;
        }

        .nav-tabs > li {
            margin-bottom: -2px;
        }

        .nav-tabs > li:first-child {
            margin-right: 5px;
        }

        .tab-content {
            border: 1px solid #80808063;
        }

        .nav-tabs > li > a.active {
            border-bottom: 1px solid #eeeeee
        }
    </style>
@endpush
@push('js')
    <script>
        var itemId = $('#pdf-item-id'),
            blocked = false,
            modal = $('#itemDeleteModal');
        loader = modal.find('.modal-loader');

        function modalError() {
            loader.removeClass('shown');
            blocked = false;
            toastr.error('Возникла проблема!');
            modal.modal('hide');
        }

        modal.on('show.bs.modal', function (e) {
            if (blocked) return false;
            var $this = $(this),
                button = $(e.relatedTarget),
                thisItemRow = button.parents('.item-row');
            itemId.val(thisItemRow.data('id'));
        }).on('hide.bs.modal', function (e) {
            if (blocked) return false;
        });
        $('#itemDeleteForm').on('submit', function () {
            if (blocked) return false;
            blocked = true;
            var thisItemId = itemId.val();
            if (thisItemId && thisItemId.match(/^[1-9][0-9]{0,9}$/)) {
                loader.addClass('shown');
                $.ajax({
                    url: '{!! route('admin.packages.delete') !!}',
                    type: 'post',
                    dataType: 'json',
                    data: {
                        _token: csrf,
                        _method: 'delete',
                        item_id: thisItemId,
                    },
                    error: function (e) {
                        modalError();
                        console.log(e.responseText);
                    },
                    success: function (e) {
                        if (e.success) {
                            loader.removeClass('shown');
                            blocked = false;
                            toastr.success('Блог удален');
                            modal.modal('hide');
                            $('.item-row[data-id="' + thisItemId + '"]').fadeOut(function () {
                                $(this).remove();
                            });
                        } else modalError();
                    }
                });
            } else modalError();
        });
    </script>
@endpush
