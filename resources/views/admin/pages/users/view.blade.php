@extends('admin.layouts.app')
@section('content')
    <div class="card">
        <div class="card-body">
            <div class="view-line"><span class="view-label">Имя:</span> {{ $item->name }}</div>
            <div class="view-line"><span class="view-label">Эл.почта:</span> {{ $item->email }} @if($item->verification) <span class="text-danger">(не подтверждена)</span> @else <span class="text-success">(подтверждена)</span> @endif</div>
            <div class="view-line"><span class="view-label">Телефон:</span> {{ $item->phone }}</div>
            <div class="view-line"><span class="view-label">Адрес:</span> {{ $item->address }}</div>
            <div class="view-line"><span class="view-label">Дата регистрации:</span> {{ $item->created_at->format('d.m.Y H:i') }}</div>
            <div class="view-line"><span class="view-label">Статус:</span> @if($item->active) <span class="text-success">активно</span> @else <span class="text-danger">блокирован</span> @endif</div>
            <div class="view-line"><span class="view-label">Добавленные:</span> {{ $item->added }}</div>
            <div class="view-line"><span class="view-label">Доступные:</span> {{ (int)$item->can_add - (int)$item->added }}</div>
            <div class="view-line"><span class="view-label">Активные:</span> {{ count($announcements_list['active']) }}</div>
            <div class="view-line"><span class="view-label">Ожидаемые модерации:</span> {{ count($announcements_list['pending']) }}</div>
            <div class="view-line"><span class="view-label">Архивированные:</span> {{ count($announcements_list['archive']) }}</div>
            <div class="view-line"><span class="view-label">Отклоненные:</span> {{ count($announcements_list['declined']) }}</div>
            <div class="m-t-10">
                <button type="button" data-toggle="modal" data-target="#blockUserModal" class="btn btn-{{ $item->active?'danger':'success' }}">{{ $item->active?'Блокировать':'Разблокировать' }}</button>
            </div>
        </div>
    </div>
    @bannerBlock(['title'=>'Обявления'])


    <ul class="nav nav-tabs">
        <li><a class="active" data-toggle="tab" href="#menu2">Ожидаемые модерации</a></li>
        <li><a data-toggle="tab" href="#home">Активные</a></li>
        <li><a data-toggle="tab" href="#menu1">Архивированные</a></li>
        <li><a data-toggle="tab" href="#menu3">Отклонённые</a></li>

    </ul>
    <div class="tab-content">
        <div id="menu2" class="tab-pane fade in active show">
            @if(count($announcements_list['pending']))
                <div class="card">
                    <div class="table-responsive">
                        <table class="table table-striped m-b-0 columns-middle">
                            <thead>
                            <tr>
                                <th>Иконка</th>
                                <th>Название</th>
                                <th>Пользователь</th>
                                <th>Статус</th>
                                <th>Действие</th>
                            </tr>
                            </thead>
                            <tbody class="table-sortable" data-action="{{ route('admin.announcements.sort') }}">
                            @foreach($announcements_list['pending'] as $announcement)
                                <tr class="item-row" data-id="{!! $announcement->id !!}">
                                    <td class="item-title">
                                        @if(!empty($announcement->galleries->where('poster',1)->first()) && count($announcement->galleries->where('poster',1)))
                                            <img width="100px"
                                                 src="{{ $announcement->galleries->where('poster',1)->first()->image(true) }}" alt="">
                                        @elseif(count($announcement->galleries))
                                            <img width="100px"
                                                 src="{{ $announcement->galleries->first()->image(true) }}" alt="">
                                        @else
                                            <img width="100px"
                                                 src="{{ asset('f/site/img/default.jpg') }}" alt="">
                                        @endif
                                    </td>
                                    <td class="item-title">{{ $announcement->title }}</td>
                                    @if(!empty($announcement->user))
                                        <td class="text-success">{{$announcement->user->name}}</td>
                                    @else
                                        <td class="text-danger">Admin</td>
                                    @endif
                                    @if($announcement->active)
                                        <td class="text-success">Активно</td>
                                    @else
                                        <td class="text-danger">Неактивно</td>
                                    @endif
                                    <td>
                                        <a href="{{ route('admin.announcements.edit', ['id'=>$announcement->id]) }}"
                                           {!! tooltip('Редактировать') !!} class="icon-btn edit"></a>
                                        <span class="d-inline-block" style="margin-left:4px;" data-toggle="modal"
                                              data-target="#itemDeleteModal"><a href="javascript:void(0)"
                                                                                class="icon-btn delete" {!! tooltip('Удалить') !!}></a></span>
                                        <a href="{{ route('admin.announcements.changeStatus', ['id'=>$announcement->id, 'status'=> \App\Models\Estate::STATUS_ACTIVE ]) }}"
                                           {!! tooltip('Одобрить') !!} style="font-size: 20px" class="ml-1 fas fa-check-square text-success"></a>
                                        <a href="{{ route('admin.announcements.changeStatus', ['id'=>$announcement->id, 'status'=> \App\Models\Estate::STATUS_DECLINED ]) }}"
                                           {!! tooltip('Отклонить') !!} style="font-size: 20px" class="ml-1 far fa-window-close text-danger"></a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            @else
                <h4 class="text-danger"> @lang('admin/all.empty') </h4>
            @endif
        </div>

        <div id="home" class="tab-pane fade ">
            @if(count($announcements_list['active']))
                <div class="card">
                    <div class="table-responsive">
                        <table class="table table-striped m-b-0 columns-middle">
                            <thead>
                            <tr>
                                <th>Иконка</th>
                                <th>Название</th>
                                <th>Пользователь</th>
                                <th>Статус</th>
                                <th>Действие</th>
                            </tr>
                            </thead>
                            <tbody class="table-sortable" data-action="{{ route('admin.announcements.sort') }}">
                            @foreach($announcements_list['active'] as $announcement)
                                <tr class="item-row" data-id="{!! $announcement->id !!}">
                                    <td class="item-title">
                                        @if(!empty($announcement->galleries->where('poster',1)->first()) && count($announcement->galleries->where('poster',1)))
                                            <img width="100px"
                                                 src="{{ $announcement->galleries->where('poster',1)->first()->image(true) }}" alt="">
                                        @elseif(count($announcement->galleries))
                                            <img width="100px"
                                                 src="{{ $announcement->galleries->first()->image(true) }}" alt="">
                                        @else
                                            <img width="100px"
                                                 src="{{ asset('f/site/img/default.jpg') }}" alt="">
                                        @endif
                                    </td>
                                    <td class="item-title">{{ $announcement->title }}</td>
                                    @if(!empty($announcement->user))
                                        <td class="text-success">{{$announcement->user->name}}</td>
                                    @else
                                        <td class="text-danger">Admin</td>
                                    @endif
                                    @if($announcement->active)
                                        <td class="text-success">Активно</td>
                                    @else
                                        <td class="text-danger">Неактивно</td>
                                    @endif
                                    <td>
                                        <a href="{{ route('admin.announcements.edit', ['id'=>$announcement->id]) }}"
                                           {!! tooltip('Редактировать') !!} class="icon-btn edit"></a>
                                        <span class="d-inline-block" style="margin-left:4px;" data-toggle="modal"
                                              data-target="#itemDeleteModal"><a href="javascript:void(0)"
                                                                                class="icon-btn delete" {!! tooltip('Удалить') !!}></a></span>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            @else
                <h4 class="text-danger"> @lang('admin/all.empty') </h4>
            @endif
        </div>
        <div id="menu1" class="tab-pane fade">
            @if(count($announcements_list['archive']))
                <div class="card">
                    <div class="table-responsive">
                        <table class="table table-striped m-b-0 columns-middle">
                            <thead>
                            <tr>
                                <th>Иконка</th>
                                <th>Название</th>
                                <th>Пользователь</th>
                                <th>Статус</th>
                                <th>Действие</th>
                            </tr>
                            </thead>
                            <tbody class="table-sortable" data-action="{{ route('admin.estates.sort') }}">
                            @foreach($announcements_list['archive'] as $announcement)
                                <tr class="item-row" data-id="{!! $announcement->id !!}">
                                    <td class="item-title">
                                        @if(!empty($announcement->galleries->where('poster',1)->first()) && count($announcement->galleries->where('poster',1)))
                                            <img width="100px"
                                                 src="{{ $announcement->galleries->where('poster',1)->first()->image(true) }}" alt="">
                                        @elseif(count($announcement->galleries))
                                            <img width="100px"
                                                 src="{{ $announcement->galleries->first()->image(true) }}" alt="">
                                        @else
                                            <img width="100px"
                                                 src="{{ asset('f/site/img/default.jpg') }}" alt="">
                                        @endif
                                    </td>
                                    <td class="item-title">{{ $announcement->title }}</td>
                                    @if(!empty($item->user))
                                        <td class="text-success">{{$announcement->user->name}}</td>
                                    @else
                                        <td class="text-danger">Admin</td>
                                    @endif
                                    @if($announcement->active)
                                        <td class="text-success">Активно</td>
                                    @else
                                        <td class="text-danger">Неактивно</td>
                                    @endif
                                    <td>
                                        <a href="{{ route('admin.announcements.edit', ['id'=>$announcement->id]) }}"
                                           {!! tooltip('Редактировать') !!} class="icon-btn edit"></a>
                                        <span class="d-inline-block" style="margin-left:4px;" data-toggle="modal"
                                              data-target="#itemDeleteModal"><a href="javascript:void(0)"
                                                                                class="icon-btn delete" {!! tooltip('Удалить') !!}></a></span>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            @else
                <h4 class="text-danger"> @lang('admin/all.empty') </h4>
            @endif
        </div>
        <div id="menu3" class="tab-pane fade">
            @if(count($announcements_list['declined']))
                <div class="card">
                    <div class="table-responsive">
                        <table class="table table-striped m-b-0 columns-middle">
                            <thead>
                            <tr>
                                <th>Иконка</th>
                                <th>Название</th>
                                <th>Пользователь</th>
                                <th>Статус</th>
                                <th>Действие</th>
                            </tr>
                            </thead>
                            <tbody class="table-sortable" data-action="{{ route('admin.estates.sort') }}">
                            @foreach($announcements_list['declined'] as $announcement)
                                <tr class="item-row" data-id="{!! $announcement->id !!}">
                                    <td class="item-title">
                                        @if(!empty($announcement->galleries->where('poster',1)->first()) && count($announcement->galleries->where('poster',1)))
                                            <img width="100px"
                                                 src="{{ $announcement->galleries->where('poster',1)->first()->image(true) }}" alt="">
                                        @elseif(count($announcement->galleries))
                                            <img width="100px"
                                                 src="{{ $announcement->galleries->first()->image(true) }}" alt="">
                                        @else
                                            <img width="100px"
                                                 src="{{ asset('f/site/img/default.jpg') }}" alt="">
                                        @endif
                                    </td>
                                    <td class="item-title">{{ $announcement->title }}</td>
                                    @if(!empty($announcement->user))
                                        <td class="text-success">{{$announcement->user->name}}</td>
                                    @else
                                        <td class="text-danger">Admin</td>
                                    @endif
                                    @if($announcement->active)
                                        <td class="text-success">Активно</td>
                                    @else
                                        <td class="text-danger">Неактивно</td>
                                    @endif
                                    <td>
                                        <a href="{{ route('admin.announcements.edit', ['id'=>$announcement->id]) }}"
                                           {!! tooltip('Редактировать') !!} class="icon-btn edit"></a>
                                        <span class="d-inline-block" style="margin-left:4px;" data-toggle="modal"
                                              data-target="#itemDeleteModal"><a href="javascript:void(0)"
                                                                                class="icon-btn delete" {!! tooltip('Удалить') !!}></a></span>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            @else
                <h4 class="text-danger"> @lang('admin/all.empty') </h4>
            @endif
        </div>
    </div>

    @endbannerBlock
    @modal(['id'=>'blockUserModal', 'centered'=>true,
        'saveBtn'=>$item->active?'Блокировать':'Разблокировать',
        'saveBtnClass'=>'btn-'.($item->active?'danger':'success'),
        'closeBtn' => 'Отменить',
        'form'=>['method'=>'post','action'=>route('admin.users.toggleActive')]])
    @slot('title')Блокировка пользователя@endslot
    <input type="hidden" name="active" value="{{ $item->active?0:1 }}">
    <input type="hidden" name="id" value="{{ $item->id }}">
    @csrf @method('patch')
    <p class="font-14">Вы действительно хотите {{ $item->active?'блокировать':'разблокировать' }} данного пользователя?</p>
    @endmodal
    @modal(['id'=>'itemDeleteModal', 'centered'=>true, 'loader'=>true,
        'saveBtn'=>'Удалить',
        'saveBtnClass'=>'btn-danger',
        'closeBtn' => 'Отменить',
        'form'=>['id'=>'itemDeleteForm', 'action'=>'javascript:void(0)']])
    @slot('title') Удаление типа недвижимости @endslot
    <input type="hidden" id="pdf-item-id">
    <p class="font-14">Вы действительно хотите удалить данный тип недвижимости?</p>
    @endmodal
@endsection
@push('css')
    <style>
        .nav-tabs li a{
            padding: 10px 15px;
            display: inline-block;
            margin-bottom: -1px;
            border-top-left-radius: 5px;
            border-top-right-radius: 5px;
        }
        .nav-tabs li a.active{
            background: white;

        }
        h4{
            padding: 35px;
            text-align: center;
        }
    </style>
@endpush
@push('js')
    <script>
        var itemId = $('#pdf-item-id'),
            blocked = false,
            modal = $('#itemDeleteModal');
        loader = modal.find('.modal-loader');

        function modalError() {
            loader.removeClass('shown');
            blocked = false;
            toastr.error('Возникла проблема!');
            modal.modal('hide');
        }

        modal.on('show.bs.modal', function (e) {
            if (blocked) return false;
            var $this = $(this),
                button = $(e.relatedTarget),
                thisItemRow = button.parents('.item-row');
            itemId.val(thisItemRow.data('id'));
        }).on('hide.bs.modal', function (e) {
            if (blocked) return false;
        });
        $('#itemDeleteForm').on('submit', function () {
            if (blocked) return false;
            blocked = true;
            var thisItemId = itemId.val();
            if (thisItemId && thisItemId.match(/^[1-9][0-9]{0,9}$/)) {
                loader.addClass('shown');
                $.ajax({
                    url: '{!! route('admin.announcements.delete') !!}',
                    type: 'post',
                    dataType: 'json',
                    data: {
                        _token: csrf,
                        _method: 'delete',
                        item_id: thisItemId,
                    },
                    error: function (e) {
                        modalError();
                        console.log(e.responseText);
                    },
                    success: function (e) {
                        if (e.success) {
                            loader.removeClass('shown');
                            blocked = false;
                            toastr.success('Недвижимость удалена');
                            modal.modal('hide');
                            $('.item-row[data-id="' + thisItemId + '"]').fadeOut(function () {
                                $(this).remove();
                            });
                        } else modalError();
                    }
                });
            } else modalError();
        });
    </script>
@endpush
