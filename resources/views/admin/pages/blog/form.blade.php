@extends('admin.layouts.app')
@section('content')
    <form action="{!! $edit?route('admin.blog.edit', ['id'=>$item->id]):route('admin.blog.add') !!}" method="post"
          enctype="multipart/form-data">
        @csrf @method($edit?'patch':'put')
        @if ($errors->any())
            <div class="alert alert-danger" role="alert">
                @foreach ($errors->all() as $error)
                    <p>{{ $error }}</p>
                @endforeach
            </div>
        @endif
        <div class="row">
            <div class="col-12 col-lg-6">
                <div class="card">
                    @bylang(['id'=>'form_title', 'tp_classes'=>'little-p', 'title'=>'Название'])
                    <input type="text" name="title[{!! $iso !!}]" class="form-control" placeholder="Название"
                           value="{{ old('title.'.$iso, tr($item, 'title', $iso)) }}">
                    @endbylang


                </div>
                <div class="card">
                    <div class="c-title">Дата</div>
                    <div class="c-body">
                        <input type="date" name="date" class="form-control" placeholder="Дата"
                               value="{{ old('date', $item->date??null) }}">
                    </div>
                </div>
                <div class="card px-3 pt-3">
                    <div class="row cstm-input">
                        <div class="col-12 p-b-5">
                            <input class="labelauty-reverse toggle-bottom-input on-unchecked" type="checkbox"
                                   name="generate_url" value="1"
                                   data-labelauty="Вставить ссылку вручную" {!! oldCheck('generate_url', $edit?false:true) !!}>
                            <div class="bottom-input">
                                <input type="text" style="margin-top:3px;" name="url" class="form-control" id="form_url"
                                       placeholder="Ссылка" value="{{ old('url', $item->url??null) }}">
                            </div>
                        </div>
                    </div>
                    @labelauty(['id'=>'active', 'label'=>'Неактивно|Активно', 'checked'=>oldCheck('active', ($edit && empty($item->active))?false:true)])@endlabelauty
                </div>
            </div>
            <div class="col-12 col-lg-6">
                <div class="card">
                    <div class="c-title">Изоброжение 16:9 (1440x768)</div>
                    @if (!empty($item->image))
                        <div class=" item-container w-100" data-id="{{ $item->id }}">
                            <div class="gallery-grid">
                                <img width="100%" src="{{ asset('u/blog/'.$item->image) }}" class="">
                                <div class="gallery-absolute">
                                    <div class="gallery-item-actions">
                                        <a href="javascript:void(0)" class="gallery-item-action gallery-item-edit"
                                           data-toggle="modal" data-target="#itemEditModal"><i
                                                class="fas fa-pencil-alt"></i></a>
                                    </div>
                                    <div><a href="{{ asset('u/blog/'.$item->image) }}" data-fancybox="gallery"
                                            class="gallery-item-show"><i class="fas fa-search-plus"></i></a></div>
                                </div>
                            </div>
                        </div>
                    @endif
                    <div class="c-body">
                        @file(['name'=>'image'])@endfile
                    </div>
                </div>
            </div>
            <div class="col-6">
                <div class="card">
                    @bylang(['id'=>'form_content', 'tp_classes'=>'little-p', 'title'=>'Кототкое описание'])
                    <textarea class="form-control form-textarea ckeditor"
                              name="short_desc[{!! $iso !!}]">{!! old('short_desc.'.$iso, tr($item, 'short_desc', $iso)) !!}</textarea>
                    @endbylang
                </div>
            </div>
            <div class="col-6">
                <div class="card">
                    @bylang(['id'=>'form_content2', 'tp_classes'=>'little-p', 'title'=>'Описание'])
                    <textarea name="desc[{!! $iso !!}]"
                              class="ckeditor">{!! old('desc.'.$iso, tr($item, 'desc', $iso)) !!}</textarea>
                    @endbylang
                </div>
            </div>

        </div>
        @seo(['item'=>$item])@endseo
        <div class="col-12 save-btn-fixed">
            <button type="submit"></button>
        </div>
    </form>
@endsection
@modal(['id'=>'itemEditModal', 'centered'=>true, 'loader'=>true,
   'saveBtn'=>'Редактировать',
   'saveBtnClass'=>'btn-success',
   'closeBtn' => 'Отменить',
   'form'=>['id'=>'itemEditForm', 'action'=>'javascript:void(0)']])
@slot('title')Редактирование изоброжении@endslot
<input type="hidden" id="edit-item-id" value="{{ $item->id??null }}">
@bylang(['title'=>'SEO', 'id'=>'gallery-bylang'])
<input type="text" name="alt[{!! $iso !!}]" id="edit-alt-{{ $iso }}"
       value="{{ old('image_alt.'.$iso, tr($item, 'image_alt', $iso)) }}" class="form-control" placeholder="Alt">
<input type="text" name="title[{!! $iso !!}]" id="edit-title-{{ $iso }}"
       value="{{ old('image_title.'.$iso, tr($item, 'image_title', $iso)) }}" class="form-control mt-2" placeholder="Title">
@endbylang
@endmodal
@push('css')
    @css(aApp('fancybox/fancybox.css'))
@endpush
@push('js')
    @js(aApp('fancybox/fancybox.js'))
    @ckeditor
    <script>
        var blocked = false,
            isos = {!! json_encode($isos, JSON_UNESCAPED_UNICODE) !!},
            editModal = $('#itemEditModal'),
            galleryBylangFirst = $('#gallery-bylang .bylang-nav-tabs .nav-item:first-child>.nav-link');
        editModal.on('show.bs.modal', function (e) {
            $('#smartSelect').removeAttr('href')
        });
        editModal.on('hide.bs.modal', function (e) {
            $('#smartSelect').attr('href', $('#smartSelect').data('href'))
        })

        var editModalError = function (loader) {
            loader.removeClass('shown');
            blocked = false;
            toastr.error('Возникла проблема!');
            editModal.modal('hide');
        };
        $('#itemEditForm').on('submit', function () {
            if (blocked) return false;
            blocked = true;
            var loader = editModal.find('.modal-loader');
            var thisItemId = $(this).find('#edit-item-id').val();
            if (thisItemId && thisItemId.match(/^[1-9][0-9]{0,9}$/)) {
                var alts = {},
                    titles = {};
                for (var i in isos) {
                    var iso = isos[i];
                    alts[iso] = $.trim($('#edit-alt-' + iso).val());
                    titles[iso] = $.trim($('#edit-title-' + iso).val());
                }
                loader.addClass('shown');
                $.ajax({
                    url: '{!! route('admin.blog.edit_image') !!}',
                    type: 'post',
                    dataType: 'json',
                    data: {
                        _token: csrf,
                        _method: 'patch',
                        item_id: thisItemId,
                        image_alt: alts,
                        image_title: titles
                    },
                    error: function (e) {
                        console.error(e.responseText);
                        editModalError(loader);
                    },
                    success: function (e) {
                        if (e.success) {
                            loader.removeClass('shown');
                            blocked = false;
                            toastr.success('Изменения сохранены.');
                            editModal.modal('hide');
                            // images[thisItemId] = {
                            //     alt: alts,
                            //     title: titles
                            // }
                        } else editModalError(loader);
                    }
                });
            } else editModalError(loader);
        });
    </script>
@endpush
