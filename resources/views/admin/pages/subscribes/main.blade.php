@extends('admin.layouts.app')
@section('titleSuffix')| <a href="{!! route('admin.main_slider.add') !!}" class="text-cyan"><i class="mdi mdi-plus-box"></i> добавить</a>@endsection
@section('content')
    @if(count($items))
        <div class="card ">
            <div class="d-flex justify-content-end p-2"><button class="download_excel btn-success" style=" outline: none!important;cursor: pointer">Export Excel</button></div>
            <div class="table-responsive">
                <table class="table table-striped m-b-0 columns-middle" id="table2excel">
                    <thead>
                    <tr class="noExl">
                        <th>N</th>
                        <th>Email</th>
                        <th>Действие</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($items->sortByDesc('id') as $item)
                        <tr class="item-row" data-id="{!! $item->id !!}">
                            <td class="item-title">
                                {{$loop->index+1}}
                            </td>

                            <td>
                                {{$item->email}}
                               </td>
                            <td>
                                <span class="d-inline-block" style="margin-left:4px;" data-toggle="modal"
                                      data-target="#itemDeleteModal"><a href="javascript:void(0)"
                                                                        class="icon-btn delete" {!! tooltip('Удалить') !!}></a></span>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    @else
        <h4 class="text-danger">@lang('admin/all.empty')</h4>
    @endif
    @modal(['id'=>'itemDeleteModal', 'centered'=>true, 'loader'=>true,
          'saveBtn'=>'Удалить',
          'saveBtnClass'=>'btn-danger',
          'closeBtn' => 'Отменить',
          'form'=>['id'=>'itemDeleteForm', 'action'=>'javascript:void(0)']])
    @slot('title') Удаление типа недвижимости @endslot
    <input type="hidden" id="pdf-item-id">
    <p class="font-14">Вы действительно хотите удалить данный тип недвижимости?</p>
    @endmodal
@endsection
@push('js')
    <script>
        var itemId = $('#pdf-item-id'),
            blocked = false,
            modal = $('#itemDeleteModal');
        loader = modal.find('.modal-loader');

        function modalError() {
            loader.removeClass('shown');
            blocked = false;
            toastr.error('Возникла проблема!');
            modal.modal('hide');
        }

        modal.on('show.bs.modal', function (e) {
            if (blocked) return false;
            var $this = $(this),
                button = $(e.relatedTarget),
                thisItemRow = button.parents('.item-row');
            itemId.val(thisItemRow.data('id'));
        }).on('hide.bs.modal', function (e) {
            if (blocked) return false;
        });
        $('#itemDeleteForm').on('submit', function () {
            if (blocked) return false;
            blocked = true;
            var thisItemId = itemId.val();
            if (thisItemId && thisItemId.match(/^[1-9][0-9]{0,9}$/)) {
                loader.addClass('shown');
                $.ajax({
                    url: '{!! route('admin.subscribes.delete') !!}',
                    type: 'post',
                    dataType: 'json',
                    data: {
                        _token: csrf,
                        _method: 'delete',
                        item_id: thisItemId,
                    },
                    error: function (e) {
                        modalError();
                        console.log(e.responseText);
                    },
                    success: function (e) {
                        if (e.success) {
                            loader.removeClass('shown');
                            blocked = false;
                            toastr.success('Недвижимость удалена');
                            modal.modal('hide');
                            $('.item-row[data-id="' + thisItemId + '"]').fadeOut(function () {
                                $(this).remove();
                            });
                        } else modalError();
                    }
                });
            } else modalError();
        });
    </script>
    <script type="text/javascript" src="{{asset('f/admin/js/jquery.table2excel.min.js')}}"></script>
    <script>
        $('.download_excel').click(function () {
            $("#table2excel").table2excel({
                exclude:".noExl",
                name:"Подписки",
                filename:"Подписки",//do not include extension
                fileext:".xls" // file extension
            });
        })


    </script>
@endpush
