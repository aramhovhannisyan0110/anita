@extends('admin.layouts.app')
@section('titleSuffix')
{{--    <form method="get" action="" class="d-inline-flex align-items-baseline">--}}
{{--        <label for="code" class="text-nowrap mr-2">Поиск по коду</label>--}}
{{--        <input class="form-control" placeholder="Введите код" type="text" id="code" value="{{ request('code') }}"--}}
{{--               name="code">--}}
{{--    </form>--}}
@endsection

@section('content')
        <ul class="nav nav-tabs">
            <li><a class="{{ (!$active || $active === 'pending')?'active':'' }}" data-toggle="tab" href="#menu2">Ожидаемые модерации</a></li>
            <li><a class="{{ ($active === 'active')?'active':'' }}" data-toggle="tab" href="#home">Активные</a></li>
            <li><a class="{{ ($active === 'archive')?'active':'' }}" data-toggle="tab" href="#menu1">Архивированные</a></li>
            <li><a class="{{ ($active === 'declined')?'active':'' }}" data-toggle="tab" href="#menu3">Отклонённые</a></li>
        </ul>
        <div class="tab-content">
            <div id="menu2" class="tab-pane fade {{ (!$active || $active === 'pending')?'in active show':'' }} ">
                @if(count($pending_items))
                    <div class="card">
                        <div class="table-responsive">
                            <table class="table table-striped m-b-0 columns-middle">
                                <thead>
                                <tr>
                                    <th>Иконка</th>
                                    <th>Название</th>
                                    <th>Пользователь</th>
                                    <th>Статус</th>
                                    <th>Действие</th>
                                </tr>
                                </thead>
                                <tbody class="table-sortable" data-action="{{ route('admin.announcements.sort') }}">
                                @foreach($pending_items as $item)
                                    <tr class="item-row" data-id="{!! $item->id !!}">
                                        <td class="item-title">
                                            @if(!empty($item->galleries->where('poster',1)->first()) && count($item->galleries->where('poster',1)))
                                                <img width="100px"
                                                     src="{{ $item->galleries->where('poster',1)->first()->image(true) }}" alt="">
                                            @elseif(count($item->galleries))
                                                <img width="100px"
                                                     src="{{ $item->galleries->first()->image(true) }}" alt="">
                                            @else
                                                <img width="100px"
                                                     src="{{ asset('f/site/img/default.jpg') }}" alt="">
                                            @endif
                                        </td>
                                        <td class="item-title">{{ $item->title }}</td>
                                        @if(!empty($item->user))
                                            <td class="text-success">{{$item->user->name}}</td>
                                        @else
                                            <td class="text-danger">Admin</td>
                                        @endif
                                        @if($item->active)
                                            <td class="text-success">Активно</td>
                                        @else
                                            <td class="text-danger">Неактивно</td>
                                        @endif
                                        <td>
                                            <a href="{{ route('admin.announcements.edit', ['id'=>$item->id]) }}"
                                               {!! tooltip('Редактировать') !!} class="icon-btn edit"></a>
                                            <span class="d-inline-block" style="margin-left:4px;" data-toggle="modal"
                                                  data-target="#itemDeleteModal"><a href="javascript:void(0)"
                                                                                    class="icon-btn delete" {!! tooltip('Удалить') !!}></a></span>
                                            <a href="{{ route('admin.announcements.changeStatus', ['id'=>$item->id, 'status'=> \App\Models\Estate::STATUS_ACTIVE ]) }}"
                                               {!! tooltip('Одобрить') !!} style="font-size: 20px" class="ml-1 fas fa-check-square text-success"></a>
                                            <a href="{{ route('admin.announcements.changeStatus', ['id'=>$item->id, 'status'=> \App\Models\Estate::STATUS_DECLINED ]) }}"
                                               {!! tooltip('Отклонить') !!} style="font-size: 20px" class="ml-1 far fa-window-close text-danger"></a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                    {{$pending_items->appends(['active' => 'pending'])->links()}}
                @else
                    <h4 class="text-danger"> @lang('admin/all.empty') </h4>
                @endif
            </div>
            <div id="home" class="tab-pane fade {{ ($active === 'active')?'in active show':'' }}">
                @if(count($active_items))
                <div class="card">
                    <div class="table-responsive">
                        <table class="table table-striped m-b-0 columns-middle">
                            <thead>
                            <tr>
                                <th>Иконка</th>
                                <th>Название</th>
                                <th>Пользователь</th>
                                <th>Статус</th>
                                <th>Действие</th>
                            </tr>
                            </thead>
                            <tbody class="table-sortable" data-action="{{ route('admin.announcements.sort') }}">
                            @foreach($active_items as $item)
                                <tr class="item-row" data-id="{!! $item->id !!}">
                                    <td class="item-title">
                                        @if(!empty($item->galleries->where('poster',1)->first()) && count($item->galleries->where('poster',1)))
                                            <img width="100px"
                                                 src="{{ $item->galleries->where('poster',1)->first()->image(true) }}" alt="">
                                        @elseif(count($item->galleries))
                                            <img width="100px"
                                                 src="{{ $item->galleries->first()->image(true) }}" alt="">
                                        @else
                                            <img width="100px"
                                                 src="{{ asset('f/site/img/default.jpg') }}" alt="">
                                        @endif
                                    </td>
                                    <td class="item-title">{{ $item->title }}</td>
                                    @if(!empty($item->user))
                                        <td class="text-success">{{$item->user->name}}</td>
                                    @else
                                        <td class="text-danger">Admin</td>
                                    @endif
                                    @if($item->active)
                                        <td class="text-success">Активно</td>
                                    @else
                                        <td class="text-danger">Неактивно</td>
                                    @endif
                                    <td>
                                        <a href="{{ route('admin.announcements.edit', ['id'=>$item->id]) }}"
                                           {!! tooltip('Редактировать') !!} class="icon-btn edit"></a>
                                        <span class="d-inline-block" style="margin-left:4px;" data-toggle="modal"
                                              data-target="#itemDeleteModal"><a href="javascript:void(0)"
                                                                                class="icon-btn delete" {!! tooltip('Удалить') !!}></a></span>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                {{$active_items->appends(['active' => 'active'])->links()}}
                @else
                    <h4 class="text-danger"> @lang('admin/all.empty') </h4>
                @endif
            </div>
            <div id="menu1" class="tab-pane fade {{ ($active === 'archive')?'in active show':'' }}">
                @if(count($archive_items))
                    <div class="card">
                        <div class="table-responsive">
                            <table class="table table-striped m-b-0 columns-middle">
                                <thead>
                                <tr>
                                    <th>Иконка</th>
                                    <th>Название</th>
                                    <th>Пользователь</th>
                                    <th>Статус</th>
                                    <th>Действие</th>
                                </tr>
                                </thead>
                                <tbody class="table-sortable" data-action="{{ route('admin.estates.sort') }}">
                                @foreach($archive_items as $item)
                                    <tr class="item-row" data-id="{!! $item->id !!}">
                                        <td class="item-title">
                                            @if(!empty($item->galleries->where('poster',1)->first()) && count($item->galleries->where('poster',1)))
                                                <img width="100px"
                                                     src="{{ $item->galleries->where('poster',1)->first()->image(true) }}" alt="">
                                            @elseif(count($item->galleries))
                                                <img width="100px"
                                                     src="{{ $item->galleries->first()->image(true) }}" alt="">
                                            @else
                                                <img width="100px"
                                                     src="{{ asset('f/site/img/default.jpg') }}" alt="">
                                            @endif
                                        </td>
                                        <td class="item-title">{{ $item->title }}</td>
                                        @if(!empty($item->user))
                                            <td class="text-success">{{$item->user->name}}</td>
                                        @else
                                            <td class="text-danger">Admin</td>
                                        @endif
                                        @if($item->active)
                                            <td class="text-success">Активно</td>
                                        @else
                                            <td class="text-danger">Неактивно</td>
                                        @endif
                                        <td>
                                            <a href="{{ route('admin.announcements.edit', ['id'=>$item->id]) }}"
                                               {!! tooltip('Редактировать') !!} class="icon-btn edit"></a>
                                            <span class="d-inline-block" style="margin-left:4px;" data-toggle="modal"
                                                  data-target="#itemDeleteModal"><a href="javascript:void(0)"
                                                                                    class="icon-btn delete" {!! tooltip('Удалить') !!}></a></span>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                    {{$archive_items->appends(['active' => 'archive'])->links()}}
                @else
                    <h4 class="text-danger"> @lang('admin/all.empty') </h4>
                @endif
            </div>
            <div id="menu3" class="tab-pane fade {{ ($active === 'declined')?'in active show':'' }}">
                @if(count($declined_items))
                    <div class="card">
                        <div class="table-responsive">
                            <table class="table table-striped m-b-0 columns-middle">
                                <thead>
                                <tr>
                                    <th>Иконка</th>
                                    <th>Название</th>
                                    <th>Пользователь</th>
                                    <th>Статус</th>
                                    <th>Действие</th>
                                </tr>
                                </thead>
                                <tbody class="table-sortable" data-action="{{ route('admin.estates.sort') }}">
                                @foreach($declined_items as $item)
                                    <tr class="item-row" data-id="{!! $item->id !!}">
                                        <td class="item-title">
                                            @if(!empty($item->galleries->where('poster',1)->first()) && count($item->galleries->where('poster',1)))
                                                <img width="100px"
                                                     src="{{ $item->galleries->where('poster',1)->first()->image(true) }}" alt="">
                                            @elseif(count($item->galleries))
                                                <img width="100px"
                                                     src="{{ $item->galleries->first()->image(true) }}" alt="">
                                            @else
                                                <img width="100px"
                                                     src="{{ asset('f/site/img/default.jpg') }}" alt="">
                                            @endif
                                        </td>
                                        <td class="item-title">{{ $item->title }}</td>
                                        @if(!empty($item->user))
                                            <td class="text-success">{{$item->user->name}}</td>
                                        @else
                                            <td class="text-danger">Admin</td>
                                        @endif
                                        @if($item->active)
                                            <td class="text-success">Активно</td>
                                        @else
                                            <td class="text-danger">Неактивно</td>
                                        @endif
                                        <td>
                                            <a href="{{ route('admin.announcements.edit', ['id'=>$item->id]) }}"
                                               {!! tooltip('Редактировать') !!} class="icon-btn edit"></a>
                                            <span class="d-inline-block" style="margin-left:4px;" data-toggle="modal"
                                                  data-target="#itemDeleteModal"><a href="javascript:void(0)"
                                                                                    class="icon-btn delete" {!! tooltip('Удалить') !!}></a></span>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                    {{$declined_items->appends(['active' => 'declined'])->links()}}
                @else
                    <h4 class="text-danger"> @lang('admin/all.empty') </h4>
                @endif
            </div>
        </div>
    @modal(['id'=>'itemDeleteModal', 'centered'=>true, 'loader'=>true,
        'saveBtn'=>'Удалить',
        'saveBtnClass'=>'btn-danger',
        'closeBtn' => 'Отменить',
        'form'=>['id'=>'itemDeleteForm', 'action'=>'javascript:void(0)']])
    @slot('title') Удаление типа недвижимости @endslot
    <input type="hidden" id="pdf-item-id">
    <p class="font-14">Вы действительно хотите удалить данный тип недвижимости?</p>
    @endmodal
@endsection
@push('css')
    <style>
        .nav-tabs li a{
            padding: 10px 15px;
            display: inline-block;
            margin-bottom: -1px;
            border-top-left-radius: 5px;
            border-top-right-radius: 5px;
        }
        .nav-tabs li a.active{
            background: white;

        }
    </style>
@endpush
@push('js')
    <script>
        var itemId = $('#pdf-item-id'),
            blocked = false,
            modal = $('#itemDeleteModal');
        loader = modal.find('.modal-loader');

        function modalError() {
            loader.removeClass('shown');
            blocked = false;
            toastr.error('Возникла проблема!');
            modal.modal('hide');
        }

        modal.on('show.bs.modal', function (e) {
            if (blocked) return false;
            var $this = $(this),
                button = $(e.relatedTarget),
                thisItemRow = button.parents('.item-row');
            itemId.val(thisItemRow.data('id'));
        }).on('hide.bs.modal', function (e) {
            if (blocked) return false;
        });
        $('#itemDeleteForm').on('submit', function () {
            if (blocked) return false;
            blocked = true;
            var thisItemId = itemId.val();
            if (thisItemId && thisItemId.match(/^[1-9][0-9]{0,9}$/)) {
                loader.addClass('shown');
                $.ajax({
                    url: '{!! route('admin.announcements.delete') !!}',
                    type: 'post',
                    dataType: 'json',
                    data: {
                        _token: csrf,
                        _method: 'delete',
                        item_id: thisItemId,
                    },
                    error: function (e) {
                        modalError();
                        console.log(e.responseText);
                    },
                    success: function (e) {
                        if (e.success) {
                            loader.removeClass('shown');
                            blocked = false;
                            toastr.success('Недвижимость удалена');
                            modal.modal('hide');
                            $('.item-row[data-id="' + thisItemId + '"]').fadeOut(function () {
                                $(this).remove();
                            });
                        } else modalError();
                    }
                });
            } else modalError();
        });
    </script>
@endpush
