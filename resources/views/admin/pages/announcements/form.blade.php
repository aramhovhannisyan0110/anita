@extends('admin.layouts.app')
@section('content')
    @if(!empty($item->user))
        <div>
            <table class="mb-3" style="background: #fff;border: 1px solid #7e7e7e;min-width: 300px">
                <tr style="border-bottom: 1px solid #7e7e7e;">
                    <th class="text-center py-1" colspan="2">Данные пользователя</th>
                </tr>
                <tr style="border-bottom: 1px solid #7e7e7e">
                    <td><span>Имя:</span></td>
                    <td><span>{{$item->user->name}}</span></td>
                </tr>
                @if(!empty($item->user->email))
                <tr style="border-bottom: 1px solid #7e7e7e">
                    <td><span>Эл.почта:</span></td>
                    <td><span>{{$item->user->email}}</span></td>
                </tr>
                @endif
                @if(!empty($item->user->phone))
                <tr style="border-bottom: 1px solid #7e7e7e">
                    <td><span>Телефон:</span></td>
                    <td><span>{{$item->user->phone}}</span></td>
                </tr>
                @endif
                @if(!empty($item->user->address))
                <tr style="border-bottom: 1px solid #7e7e7e">
                    <td><span>Адрес:</span></td>
                    <td><span>{{$item->user->address}}</span></td>
                </tr>
                @endif
                <tr style="border-bottom: 1px solid #7e7e7e">
                    <td><span>Тип:</span></td>
                    <td><span>{{$item->user->user_type == 1?'Личность':'Агент'}}</span></td>
                </tr>
            </table>
        </div>
    @endif
    <form
        action="{!! $edit?route('admin.announcements.edit', ['id'=>$item->id]):route('admin.announcements.add',['category' => $selected_cat,'type' => $selected_tp]) !!}"
        method="post" enctype="multipart/form-data">
        @csrf @method($edit?'patch':'put')
        @if ($errors->any())
            <div class="alert alert-danger" role="alert">
                @foreach ($errors->all() as $error)
                    <p>{{ $error }}</p>
                @endforeach

            </div>
        @endif
        <div class="row">
            <div class="col-12 col-lg-6">
                <div class="card">
                    @bylang(['id'=>'form_title', 'tp_classes'=>'little-p', 'title'=>'Название'])
                    <input type="text" name="title[{!! $iso !!}]" class="form-control" placeholder="Название"
                           value="{{ old('title.'.$iso, tr($item, 'title', $iso)) }}">
                    @endbylang
                </div>
                <div class="card">
                    <div class="c-title pb-1 pt-2">
                        Код
                    </div>
                    <div class="c-body">
                        <input type="text" name="code" class="form-control" placeholder="Код"
                               value="{{ old('code', $item->code??null) }}">
                    </div>
                </div>

                <div class="card">
                    <div class="c-title pb-1 pt-2">
                        Цена
                    </div>
                    <div class="c-body">
                        <div class="w-100 ">
                            @labelauty(['id'=>'price_filter', 'label'=>'Договорная|Договорная', 'checked'=>oldCheck('price_filter', ($edit &&  ($item->price != -1))?false:true)])@endlabelauty
                        </div>
                        @if($edit)
                            @if(count($item->estate_filter))
                                @foreach($item->estate_filter as $val)
                                    @if($val->filter_id == $price_filter->id)
                                        @php($value = $val->value)
                                    @endif
                                @endforeach
                            @endif
                            <div class="card" id="for_price_filter">
                                <div class="c-title">{{$price_filter->title.' ('.$price_filter->metric.')'}}</div>
                                <div class="c-body"
                                     style="display: flex;justify-content: space-between">
                                    <input type="text" name="interval_filter[{{$price_filter->id}}]"
                                           class="form-control  {{$errors->has('interval_filter.'.$price_filter->id)?'error_border':''}}"
                                           placeholder="{{$price_filter->title}}"
                                           value="{{ old('interval_filter.'.$price_filter->id, $value??null) }}"
                                           data-old="{{ old('interval_filter.'.$price_filter->id, $value??null) }}">
                                </div>
                            </div>
                        @else
                            <div class="card">
                                <div class="c-title">{{$price_filter->title.' ('.$price_filter->metric.')'}}</div>
                                <div class="c-body"
                                     style="display: flex;justify-content: space-between">
                                    <input type="text" name="interval_filter[{{$price_filter->id}}]"
                                           class="form-control   {{$errors->has('interval_filter.'.$price_filter->id)?'error_border':''}}"
                                           placeholder="{{$price_filter->title}}"
                                           value="{{ old('interval_filter.'.$price_filter->id, $value??null) }}"
                                           data-old="{{ old('interval_filter.'.$price_filter->id, $value??null) }}">

                                </div>
                            </div>
                        @endif
                    </div>
                </div>

            </div>
            <div class="col-12 col-lg-6">
                <div class="card px-3 pt-3">
                    <div class="row cstm-input">
                        <div class="col-12 p-b-5">
                            <input class="labelauty-reverse toggle-bottom-input on-unchecked" type="checkbox"
                                   name="generate_url"
                                   data-labelauty="Вставить ссылку вручную" {!! oldCheck('generate_url', $edit ?false : true) !!}>
                            <div class="bottom-input">
                                <input type="text" style="margin-top:3px;" name="url" class="form-control" id="form_url"
                                       placeholder="Ссылка" value="{{ old('url', $item->url??null) }}">
                            </div>
                        </div>
                    </div>
                    <div class="d-flex">
                        @labelauty(['id'=>'active','class'=>'mr-2', 'label'=>'Неактивно|Активно', 'checked'=>oldCheck('active', ($edit && empty($item->active))?false:true)])@endlabelauty
                        @labelauty(['id'=>'top','class'=>' danger-top', 'label'=>'Не показывать в топе|Показывать в топе', 'checked'=>oldCheck('top', ($edit && empty($item->top))?false:true)])@endlabelauty
                    </div>
                    <style>
                        .danger-top input.labelauty.custom-labelauty:checked + label {
                            background-color: #ff0c0c;
                        }

                        .danger-top input.labelauty.custom-labelauty:checked:not([disabled]) + label:hover {
                            background-color: #d00000;
                        }
                    </style>
                </div>
                <div class="card">
                    <div class="address">
                        @if(!empty($regions) && count($regions))
                            <div class="col-12">
                                <div class="card ">
                                    <div class="c-title">Выберите местоположение</div>
                                    <div class="c-body">
                                        <select id="demo-3b" name="region[0]" id="types_ids">
                                            @foreach($regions as $region)
                                                <optgroup label="{{$region->title}}">
                                                    @foreach($region->childes as $cat)
                                                        <option value="" disabled
                                                                data-level="1"> {{$cat->title}}  </option>
                                                        @foreach($cat->childes as $c)
                                                            <option
                                                                {{ (old('region.0')==$c->id) ? 'selected' : ((!empty($connected_regions) &&  in_array($c->id,$connected_regions)) ? 'selected' : '')  }} value="{{$c->id}}"
                                                                data-level="2"> {{$c->title}}  </option>
                                                        @endforeach
                                                    @endforeach
                                                </optgroup>
                                            @endforeach
                                        </select>
                                    </div>

                                </div>
                            </div>

                        @endif
                    </div>
                    @bylang(['id'=>'form_title4', 'tp_classes'=>'little-p', 'title'=>'Адрес'])
                    <input type="text" name="address[{!! $iso !!}]" class="form-control" placeholder="Адрес"
                           value="{{ old('address.'.$iso, tr($item, 'address', $iso)) }}">
                    @endbylang
                </div>
            </div>

            <div class="col-12 col-lg-6">
                <div class="card">
                    @bylang(['id'=>'form_title1', 'tp_classes'=>'little-p', 'title'=>'Описание'])
                    <textarea type="text" name="description[{!! $iso !!}]" class="form-control ckeditor"
                              placeholder="Описание"
                    >{{ old('description.'.$iso, tr($item, 'description', $iso)) }}</textarea>
                    @endbylang
                </div>
            </div>
            <div class="col-12 col-lg-6">
                <div class="card">
                    @bylang(['id'=>'form_title2', 'tp_classes'=>'little-p', 'title'=>'Краткое Описание'])
                    <textarea type="text" name="short_description[{!! $iso !!}]" class="form-control ckeditor"
                              placeholder="Краткое Описание"
                    >{{ old('short_description.'.$iso, tr($item, 'short_description', $iso)) }}</textarea>
                    @endbylang
                </div>
            </div>
            <div class="col-12">
                <div class="card">
                    <div class="c-title pb-1 pt-2">
                        Ссылки
                    </div>
                    <div class="c-body">
                        <input type="text" name="iframe" class="form-control my-2" placeholder="Карта"
                               value="{{ old('iframe', $item->iframe??null) }}">
                        <input type="text" name="youtube" class="form-control" placeholder="Youtube"
                               value="{{ old('youtube', $item->youtube??null) }}">
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="card col-12">
                <div class="c-title pb-1 pt-2">
                    Фильтры
                </div>
                <div class="c-body ">
                    <div class="col-12">
                        <div class="row">
                            @if(!empty($global_filters))
                                @foreach($global_filters as $filter)
                                    @component('admin.components.admin_filters',['filter'=>$filter,'edit'=>$edit,'item'=>$item]) @endcomponent
                                @endforeach
                            @endif

                        </div>
                    </div>
                    <div class="col-12">
                        <div class="row">
                            @if(!empty($local_filters))
                                @foreach($local_filters as $filter)
                                    @if(!($filter->price_filter))
                                        @component('admin.components.admin_filters',['filter'=>$filter,'edit'=>$edit,'item'=>$item]) @endcomponent
                                    @endif
                                @endforeach
                            @endif

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="card col-12">
                <div class="c-title">
                    Параметры
                </div>
                <div class="c-body row">
                    <div class="col-12 col-lg-6">
                        @labelauty(['id'=>'block_1', 'label'=>'Не показывать в первом блоке|Показать в первом блоке', 'checked'=>oldCheck('block_1', ($edit && empty($item->block_1))?false:true)])@endlabelauty
                        @labelauty(['id'=>'block_2', 'label'=>'Не показывать во втором блоке|Показать во втором блоке', 'checked'=>oldCheck('block_2', ($edit && empty($item->block_2))?false:true)])@endlabelauty
                        @labelauty(['id'=>'block_3', 'label'=>'Не показывать в третьем блоке|Показать в третьем блоке', 'checked'=>oldCheck('block_3', ($edit && empty($item->block_3))?false:true)])@endlabelauty
                        @labelauty(['id'=>'urgent', 'label'=>'Обыкновенный|Срочный', 'checked'=>oldCheck('urgent', ($edit && empty($item->urgent))?false:true)])@endlabelauty
                        @labelauty(['id'=>'new', 'label'=>'Обычная|Новостройка', 'checked'=>oldCheck('new', ($edit && empty($item->new))?false:true)])@endlabelauty
                    </div>
                    <div class="col-12 col-lg-6">
                        <table>
                            <tr>
                                <th colspan="2" style="text-align: center">Статус</th>
                            </tr>
                            <tr>
                                <td><label for="free">Свободно</label></td>
                                <td><input type="radio" id="free" name="status"
                                           {{(!empty($item->status) && $item->status== 0)?'checked':''}} value="0"></td>
                            </tr>
                            <tr>
                                <td><label for="sold">Продано</label></td>
                                <td><input type="radio" id="sold" name="status"
                                           {{(!empty($item->status) && $item->status== 1)?'checked':''}} value="1"></td>
                            </tr>
                            <tr>
                                <td><label for="rent">Сдано в аренду</label></td>
                                <td><input type="radio" id="rent" name="status"
                                           {{  (!empty($item->status) && $item->status== 2)?'checked':''}} value="2">
                                </td>

                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        @seo(['item'=>$item])@endseo

        <div class="card">
            <div class="c-title">Добавить изображения</div>
            <div class="c-body">
                <div class="form-group" style="margin-top:15px;position: relative;min-height: 65px; height: 100%">
                    {{--            <label for="exampleInputFile">File input</label>--}}
                    <input type="file" name="images[]" class="demo" multiple
                           data-jpreview-container="#demo-1-container">

                    <div id="demo-1-container" class="jpreview-container"></div>
                </div>
                <input type="hidden" name="gallery" value="estates">
                <div class="col-12 save-btn-fixed">
                    <button type="submit"></button>
                </div>
            </div>
        </div>

    </form>
    <div class="row">
        @if(!empty($gallery_images) && !empty($gallery_images->where('poster',1)->first()) && count($gallery_images->where('poster',1)))
            <div class="col-12 col-lg-4">
                <div class="card">
                    <div class="c-title">Главное изображение</div>
                    <div class="c-body"><img id="for_home" class="w-100"
                                             src="{{ $gallery_images->where('poster',1)->first()->image(true) }}"
                                             alt=""></div>
                </div>

            </div>
        @elseif(!empty($gallery_images) && count($gallery_images))
            <div class="col-12 col-lg-4">

                <div class="card">
                    <div class="c-title">Главное изображение</div>
                    <div class="c-body"><img id="for_home" class="w-100"
                                             src="{{ $gallery_images->first()->image(true) }}" alt=""></div>
                </div>

            </div>
        @else
            <div class="col-12 col-lg-4">

                <div class="card">
                    <div class="c-title">Главное изображение</div>
                    <div class="c-body"><img id="for_home" class="w-100"
                                             src="{{ asset('f/site/img/default.jpg') }}" alt=""></div>
                </div>

            </div>
        @endif
        @if(!empty($gallery_images))
            <div class="col-12 col-lg-8">
                @component('admin.components.gallery',['gallery_images'=>$gallery_images,'show_home'=>true])@endcomponent
            </div>
        @endif
    </div>

@endsection
@push('css')
    <link href="{{asset('f/admin/smartSelect/smartSelect.min.css')}}"
          data-href="{{asset('f/admin/smartSelect/smartSelect.min.css')}}" id="smartSelect" media="all" rel="stylesheet"
          type="text/css">
    @css(aApp('fancybox/fancybox.css'))
    @css(aApp('multiupload/jpreview.css'))
    <style>
        input[type="file"] {
            display: block;
        }

        .imageThumb {
            max-height: 75px;
            border: 2px solid;
            padding: 1px;
            cursor: pointer;
        }

        .pip {
            display: inline-block;
            margin: 10px 10px 0 0;
        }

        .remove {
            display: block;
            background: #444;
            border: 1px solid black;
            color: white;
            text-align: center;
            cursor: pointer;
        }

        .remove:hover {
            background: white;
            color: black;
        }

        table * {
            cursor: pointer
        }

        table td {
            padding: 5px;
        }

        .error_border {
            border: 1px solid red !important;
        }

        input[type="radio"] {
            background-color: #fff;
            background-image: -webkit-linear-gradient(0deg, transparent 20%, hsla(0, 0%, 100%, .7), transparent 80%), -webkit-linear-gradient(90deg, transparent 20%, hsla(0, 0%, 100%, .7), transparent 80%);
            border-radius: 10px;
            box-shadow: inset 0 1px 1px hsla(0, 0%, 100%, .8), 0 0 0 1px hsl(108, 50%, 49%), 0 2px 3px hsla(108, 50%, 49%, 0.6), 0 4px 3px hsla(108, 50%, 49%, 0.45), 0 6px 6px hsla(0, 8%, 62%, 0), 0 10px 6px hsla(0, 0%, 0%, 0);
            cursor: pointer;
            display: inline-block;
            height: 15px;
            margin-right: 15px;
            position: relative;
            width: 15px;
            -webkit-appearance: none;
        }

        input[type="radio"]:after {
            border-radius: 25px;
            box-shadow: inset 0 0 0 1px hsl(108, 100%, 31%), 0 1px 1px hsla(0, 0%, 100%, 0.8);
            content: '';
            display: block;
            height: 9px;
            left: 3px;
            position: relative;
            top: 3px;
            width: 9px;
        }

        input[type="radio"]:checked:after {
            background-color: #56ba3e;
            box-shadow: inset 0 0 0 1px hsla(108, 6%, 13%, 0.32), inset 0 2px 2px hsla(0, 0%, 100%, .4), 0 1px 1px hsla(0, 0%, 100%, .8), 0 0 2px 2px hsl(108, 100%, 50%);
        }
    </style>
@endpush
@push('js')
    @ckeditor
    @js(aApp('fancybox/fancybox.js'))
    @js(aApp('multiupload/bootstrap-prettyfile.js'))
    @js(aApp('multiupload/jpreview.js'))
    @js(aApp('multiupload/index.js'))
    <script>
        $('.demo').prettyFile();
        $('.demo').jPreview();


    </script>
    <script>
        function checkPrice() {
            var price_filter = parseInt($('#for_price_filter').find('input').data('old'));
            if ($('#price_filter').is(':checked')) {
                $('#for_price_filter').hide();
                $('#for_price_filter').find('input').val(-1)
            } else {
                $('#for_price_filter').show();
                $('#for_price_filter').find('input').val((!isNaN(price_filter)) ? price_filter : '')
            }
        }

        $("select#demo-3b").smartselect({
            toolbar: false,
            defaultView: 'root+selected',
            multiple: false,
            text: {
                selectLabel: 'Выбрать...'
            },
        });
        $(document).ready(function () {
            checkPrice();
            $('#price_filter').on('click', function () {
                checkPrice()
            })
        })
    </script>
@endpush
