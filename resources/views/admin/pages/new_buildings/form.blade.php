@extends('admin.layouts.app')
@section('content')
    <form action="{!! $edit?route('admin.new_buildings.edit', ['id'=>$item->id]):route('admin.new_buildings.add') !!}"
          method="post"
          enctype="multipart/form-data"
          id="form_content">
        @csrf @method($edit?'patch':'put')
        @if ($errors->any())
            <div class="alert alert-danger" role="alert">
                @foreach ($errors->all() as $error)
                    <p>{{ $error }}</p>
                @endforeach
            </div>
        @endif
        <div class="row">
            <div class="col-12 col-lg-6">
                <div class="card">
                    @bylang(['id'=>'form_title', 'tp_classes'=>'little-p', 'title'=>'Название'])
                    <input type="text" name="title[{!! $iso !!}]" class="form-control" placeholder="Название"
                           value="{{ old('title.'.$iso, tr($item, 'title', $iso)) }}">
                    @endbylang


                </div>
                <div class="card">
                    @bylang(['id'=>'form_title1', 'tp_classes'=>'little-p', 'title'=>'Владелец'])
                    <input type="text" name="owner[{!! $iso !!}]" class="form-control" placeholder="Владелец"
                           value="{{ old('owner.'.$iso, tr($item, 'owner', $iso)) }}">
                    @endbylang
                </div>
                <div class="card px-3 pt-3">
                    <div class="row cstm-input">
                        <div class="col-12 p-b-5">
                            <input class="labelauty-reverse toggle-bottom-input on-unchecked" type="checkbox"
                                   name="generate_url" value="1"
                                   data-labelauty="Вставить ссылку вручную" {!! oldCheck('generate_url', $edit?false:true) !!}>
                            <div class="bottom-input">
                                <input type="text" style="margin-top:3px;" name="url" class="form-control" id="form_url"
                                       placeholder="Ссылка" value="{{ old('url', $item->url??null) }}">
                            </div>
                        </div>
                    </div>
                    @labelauty(['id'=>'active', 'label'=>'Неактивно|Активно', 'checked'=>oldCheck('active', ($edit && empty($item->active))?false:true)])@endlabelauty
                </div>
            </div>
            <div class="col-12 col-lg-6">
                <div class="card">
                    @bylang(['id'=>'form_content', 'tp_classes'=>'little-p', 'title'=>'Условия продажи'])
                    <textarea class="form-control form-textarea ckeditor"
                              name="conditions[{!! $iso !!}]">{!! old('conditions.'.$iso, tr($item, 'conditions', $iso)) !!}</textarea>
                    @endbylang
                </div>
            </div>
            <div class="col-12 col-lg-6">
                <div class="card">
                    @bylang(['id'=>'form_content2', 'tp_classes'=>'little-p', 'title'=>'Описание'])
                    <textarea name="description[{!! $iso !!}]"
                              class="ckeditor">{!! old('description.'.$iso, tr($item, 'description', $iso)) !!}</textarea>
                    @endbylang
                </div>
            </div>
            <div class="col-12 col-lg-6">
                <div class="card">
                    <div class="c-title">Ссылка на видео(Youtube)</div>
                    <div class="c-body">
                        <input type="text" name="youtube" class="form-control" placeholder="Youtube"
                               value="{{ old('youtube', $item->youtube??null) }}">
                    </div>
                </div>
                <div class="card">
                    <div class="c-title">Телефон</div>
                    <div class="c-body">
                        <input type="text" name="phone" class="form-control" placeholder="Телефон"
                               value="{{ old('phone', $item->phone??null) }}">
                    </div>
                </div>
                <div class="card">
                    <div class="c-title">Эл.почта</div>
                    <div class="c-body">
                        <input type="text" name="email" class="form-control" placeholder="Эл.почта"
                               value="{{ old('email', $item->email??null) }}">
                    </div>
                </div>
                <div class="card">
                    <div class="c-title">Адрес</div>
                    <div class="c-body">
                        <input type="text" name="address" class="form-control" placeholder="Адрес"
                               value="{{ old('address', $item->address??null) }}">
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12 col-lg-6">
                <div class="card">
                    @bylang(['id'=>'form_title3', 'tp_classes'=>'little-p', 'title'=>'Название правого банера'])
                    <input type="text" name="banner_title[{!! $iso !!}]" class="form-control" placeholder="Название"
                           value="{{ old('banner_title.'.$iso, tr($item, 'banner_title', $iso)) }}">
                    @endbylang
                </div>
                <div class="card">
                    <div class="c-title">Ссылка банера</div>
                    <div class="c-body">
                        <input type="text" name="banner_url" class="form-control" placeholder="Ссылка"
                               value="{{ old('banner_url', $item->banner_url??null) }}">
                    </div>
                </div>
                <div class="card">
                    @bylang(['id'=>'form_content4', 'tp_classes'=>'little-p', 'title'=>'Описание правого банера'])
                    <textarea name="banner_text[{!! $iso !!}]"
                              class="ckeditor">{!! old('banner_text.'.$iso, tr($item, 'banner_text', $iso)) !!}</textarea>
                    @endbylang
                </div>
            </div>
            <div class="col-12 col-lg-6">
                <div class="card">
                    <div class="c-title">Изоброжение 3:4 (360x520)</div>
                    @if (!empty($item->banner_image))
                        <div class=" item-container w-100" data-id="{{ $item->id }}">
                            <div class="gallery-grid">
                                <img width="100%" src="{{ asset('u/new_buildings/'.$item->banner_image) }}" class="">
                                <div class="gallery-absolute">
                                    <div class="gallery-item-actions">
                                        <a href="javascript:void(0)" class="gallery-item-action gallery-item-edit"
                                           data-toggle="modal" data-target="#itemEditModal"><i
                                                class="fas fa-pencil-alt"></i></a>
                                    </div>
                                    <div><a href="{{ asset('u/blog/'.$item->image) }}" data-fancybox="gallery"
                                            class="gallery-item-show"><i class="fas fa-search-plus"></i></a></div>
                                </div>
                            </div>
                        </div>
                    @endif
                    <div class="c-body">
                        @file(['name'=>'banner_image'])@endfile
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="card col-12">
                <div
                    class="c-title font-segeo font-13 font-bold mb-3">Выберите местоположение
                </div>
                <div class="c-body">
                    <div class="little-p">
                        <div id="map" style="width: 100%; height: 400px"></div>
                        <div class="mt-2" style="display: none">
                            <input type="text" name="lat" class="form-control map-inp lat"
                                   placeholder="Широта" maxlength="20"
                                   value="{{ old('lat', $item->lat??null) }}">
                            <input type="text" name="lng" class="form-control map-inp lng mt-2"
                                   placeholder="Долгота" maxlength="20"
                                   value="{{ old('lng', $item->lng??null) }}">
                            <button type="button" class="btn btn-secondary mt-2 show-on-map">
                                Показать
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="card">
            <div class="c-title">Связанные обявления</div>
            <div class="c-body">
                <div>
                    @if(count($item->estates))
                        <table class="table table-striped m-b-0 columns-middle">
                            <thead>
                            <tr>
                                <th>Название</th>
                                <th>Код</th>
                                <th>Действие</th>
                            </tr>
                            </thead>
                            <tbody id="for_content2">
                            @foreach($item->estates as $estate)
                                @component('admin.components.for_add_to_new',['estate'=>$estate,'checked'=>true])@endcomponent
                            @endforeach
                            </tbody>
                        </table>
                    @endif
                </div>
            </div>
        </div>
        <div class="card">
            <div class="c-title">Связать новые</div>
            <div class="c-body">
                <input type="text" id="by_code" placeholder="Введите код">
                <button id="search" type="button">Поиск по коду</button>
                <table class="table table-striped m-b-0 columns-middle">
                    <thead>
                    <tr>
                        <th>Название</th>
                        <th>Код</th>
                        <th>Действие</th>
                    </tr>
                    </thead>
                    <tbody id="for_content">

                    </tbody>
                </table>
            </div>
        </div>
        <div class="content">

        </div>

        @seo(['item'=>$item])@endseo
        <div class="col-12 save-btn-fixed">
            <button type="button" id="submit_form"></button>
        </div>
        <input type="hidden" name="estates" id="ids">
    </form>







@endsection

@push('css')
    @css(aApp('fancybox/fancybox.css'))
@endpush
@push('js')
    @js(aApp('fancybox/fancybox.js'))
    @ckeditor

    <script>
        $(document).on('click', '#submit_form', function () {

            var selected = $('#for_content').find('input:checked')
            var selected2 = $('#for_content2').find('input:checked')
            var arr = [];
            if (selected.length) {
                selected.each(function () {
                    arr.push($(this).val());
                })
            } if (selected2.length) {
                selected2.each(function () {
                    arr.push($(this).val());
                })
            }
            $('#ids').val(arr)
            $('#form_content').submit()
        })
        $('#search').on('click', function () {
            var code = $('#by_code').val()
            $.ajax({
                url: '{!! route('admin.new_buildings.search_estate') !!}',
                type: 'post',
                dataType: 'json',
                data: {
                    _token: csrf,
                    code: code,
                },
                error: function (e) {
                    console.log('error');
                },
                success: function (e) {
                    $('#for_content').html(e)
                }
            });
        })

    </script>














    <script src="https://api-maps.yandex.ru/2.1/?apikey=ce752946-5050-4a17-9d27-624b2dc71d8b&lang=ru_RU"></script>

    <script>
        (function () {
            ymaps.ready(init);
            var myMap, tempPlacemark = null,
                latInput = $('.map-inp.lat'), longInput = $('.map-inp.lng'),
                myPlacemark = {}, placemarkData = {};

            function showOnMap(position) {
                if (tempPlacemark !== null) {
                    myMap.geoObjects.remove(tempPlacemark);
                    tempPlacemark = null;
                    latInput.val('');
                    longInput.val('');
                }
                tempPlacemark = new ymaps.Placemark(position)
                myMap.geoObjects.add(tempPlacemark);
                latInput.val(position[0]);
                longInput.val(position[1]);
            }

            function init() {
                myMap = new ymaps.Map("map", {
                    center: [40.2, 44.5],
                    zoom: 10,
                });
                @if($edit)
                    tempPlacemark = new ymaps.Placemark([{!! $item->lat !!}, {!! $item->lng !!}])
                myMap.geoObjects.add(tempPlacemark);
                @endif

                myMap.events.add('click', function (e) {
                    showOnMap(e.get('coords'));
                });
                $('.show-on-map').on('click', function () {
                    var lat = $.trim(latInput.val()),
                        long = $.trim(longInput.val());
                    if (lat != '' && long != '' && !isNaN(lat) && !isNaN(long)) {
                        showOnMap([lat, long]);
                        myMap.setCenter([lat, long], myMap.getZoom(), {duration: 300});

                    }
                });
            }
        })();
    </script>
    <script>
        var blocked = false,
            isos = {!! json_encode($isos, JSON_UNESCAPED_UNICODE) !!},
            editModal = $('#itemEditModal'),
            galleryBylangFirst = $('#gallery-bylang .bylang-nav-tabs .nav-item:first-child>.nav-link');
        editModal.on('show.bs.modal', function (e) {
            $('#smartSelect').removeAttr('href')
        });
        editModal.on('hide.bs.modal', function (e) {
            $('#smartSelect').attr('href', $('#smartSelect').data('href'))
        })

        var editModalError = function (loader) {
            loader.removeClass('shown');
            blocked = false;
            toastr.error('Возникла проблема!');
            editModal.modal('hide');
        };
        $('#itemEditForm').on('submit', function () {
            if (blocked) return false;
            blocked = true;
            var loader = editModal.find('.modal-loader');
            var thisItemId = $(this).find('#edit-item-id').val();
            if (thisItemId && thisItemId.match(/^[1-9][0-9]{0,9}$/)) {
                var alts = {},
                    titles = {};
                for (var i in isos) {
                    var iso = isos[i];
                    alts[iso] = $.trim($('#edit-alt-' + iso).val());
                    titles[iso] = $.trim($('#edit-title-' + iso).val());
                }
                loader.addClass('shown');
                $.ajax({
                    url: '{!! route('admin.blog.edit_image') !!}',
                    type: 'post',
                    dataType: 'json',
                    data: {
                        _token: csrf,
                        _method: 'patch',
                        item_id: thisItemId,
                        image_alt: alts,
                        image_title: titles
                    },
                    error: function (e) {
                        console.error(e.responseText);
                        editModalError(loader);
                    },
                    success: function (e) {
                        if (e.success) {
                            loader.removeClass('shown');
                            blocked = false;
                            toastr.success('Изменения сохранены.');
                            editModal.modal('hide');
                            // images[thisItemId] = {
                            //     alt: alts,
                            //     title: titles
                            // }
                        } else editModalError(loader);
                    }
                });
            } else editModalError(loader);
        });
    </script>
@endpush
