<div class="col-12 col-lg-6 col-xl-4 ">
    @if($filter->have_interval)

        @if($edit)
            @if(count($item->estate_filter))
                @foreach($item->estate_filter as $val)
                    @if($val->filter_id == $filter->id)
                        @php($value = $val->value)
                    @endif
                @endforeach
            @endif
            <div class="card">
                <div
                    class="c-title font-segeo font-13 font-bold mb-2">{{$filter->title}}{{!empty($filter->metric)?' ('.$filter->metric.')':''}}</div>
                <div class="c-body"
                     style="display: flex;flex-direction:column;justify-content: space-between">
                    <input type="text" name="interval_filter[{{$filter->id}}]"
                           class="form-control interval_filter {{$errors->has('interval_filter.'.$filter->id)?'error_border':''}}"
                           placeholder="{{$filter->title}}"
                           value="{{ old('interval_filter.'.$filter->id, $value??null) }}"
                           data-filter="{{$filter->id}}">
                    <div style="color: #aa2222;font-size: 12px" class=" font-segeo  font-bold mt-1">
                        {{$errors->has('without_interval_filter.'.$filter->id)?$errors->first('without_interval_filter.'.$filter->id):''}}
                    </div>
                </div>
            </div>
        @else
            <div class="card">
                <div
                    class="c-title font-segeo font-13 font-bold mb-2">{{$filter->title}}{{!empty($filter->metric)?' ('.$filter->metric.')':''}}</div>
                <div class="c-body"
                     style="display: flex;flex-direction:column;justify-content: space-between">
                    <input type="text" name="interval_filter[{{$filter->id}}]"
                           class="form-control interval_filter {{$errors->has('interval_filter.'.$filter->id)?'error_border':''}}"
                           placeholder="{{$filter->title}}"
                           data-filter="{{$filter->id}}"
                           value="{{ old('interval_filter.'.$filter->id, $value??null) }}">
                    <div style="color: #aa2222;font-size: 12px" class=" font-segeo  font-bold mt-1">
                        {{$errors->has('interval_filter.'.$filter->id)?$errors->first('interval_filter.'.$filter->id):''}}
                    </div>
                </div>
            </div>
        @endif
    @else
        <div class="card mb-5">
            <div
                class="c-title  font-segeo font-13 font-bold mb-2">{{$filter->title}}{{!empty($filter->metric)?' ('.$filter->metric.')':''}}
            </div>
            <div class="c-body d_flex flex-column"
                 style="display: flex;flex-direction:column;justify-content: space-between">
                <select class="form-control without_interval_filter  {{$errors->has('without_interval_filter.'.$filter->id)?'error_border':''}}"
                        name="without_interval_filter[{{$filter->id}}]" id=""
                        data-filter="{{$filter->id}}"
                        data-title="{{$filter->title}}">
                    <option value="">---</option>
                    @if(!empty($filter->option))
                        @foreach($filter->option as $option)
                            @if($edit)
                                @if(!empty($item->estate_filter))
                                    @foreach($item->estate_filter as $val)
                                        @if($val->option_id == $option->id)
                                            @php($value2 = $option->id)
                                        @endif
                                    @endforeach
                                    <option
                                        {{(old('without_interval_filter.'.$filter->id) == $option->id) ? 'selected' : (isset($value2) && ($value2 == $option->id)?'selected':'')  }} value="{{$option->id}}">{{(isset(json_decode($option->options)->{app()->getLocale()}))?json_decode($option->options)->{app()->getLocale()}:''}}</option>
                                @endif
                            @else
                                <option
                                    value="{{$option->id}}" {{ (old('without_interval_filter.'.$filter->id) == $option->id) ? 'selected' : '' }}>{{(isset(json_decode($option->options)->{app()->getLocale()}))?json_decode($option->options)->{app()->getLocale()}:''}}</option>
                            @endif
                        @endforeach
                    @endif
                </select>
                <div style="color: #aa2222;font-size: 12px" class=" font-segeo  font-bold mt-1">
                    {{$errors->has('without_interval_filter.'.$filter->id)?$errors->first('without_interval_filter.'.$filter->id):''}}
                </div>
            </div>
        </div>
    @endif
</div>
