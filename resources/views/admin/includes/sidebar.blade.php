@alink(['url'=>route('admin.types.main'), 'icon'=>'fa fa-drafting-compass', 'title'=>'Типы'])@endalink
@alink(['url'=>route('admin.categories.main'), 'icon'=>'fas fa-bezier-curve', 'title'=>'Категории'])@endalink
@alink(['url'=>route('admin.locations.main'), 'icon'=>'fas fa-map-marked-alt', 'title'=>'Локации']) @endalink
@alink(['url'=>route('admin.filters.main'), 'icon'=>'fa fa-filter', 'title'=>'Фильтры'])@endalink
@alink(['url'=>route('admin.filters.list'), 'icon'=>'fas fa-hand-holding-usd', 'title'=>'Фильтры цен'])@endalink
@alink(['url'=>route('admin.estates.main'), 'icon'=>'fas fa-map-pin', 'title'=>'Недвижимости'])@endalink
@alink(['icon'=>'fas fa-file-invoice-dollar', 'title'=>'Платные сервисы'])
    @alink(['url'=>route('admin.packages.main',['package_type'=>2]), 'icon'=>'fas fa-sort-amount-up', 'title'=>'Показывать в топе'])@endalink
    @alink(['url'=>route('admin.packages.main',['package_type'=>3]), 'icon'=>'fas fa-fast-forward', 'title'=>'Прикрепить стикер "Срочно"'])@endalink
    @alink(['url'=>route('admin.packages.main',['package_type'=>4]), 'icon'=>'fas fa-home', 'title'=>'Показывать на главной'])@endalink
    @alink(['url'=>route('admin.packages.main',['package_type'=>1]), 'icon'=>'fas fa-sort-numeric-up', 'title'=>'Количества ограничений'])@endalink
@endalink
@alink(['icon'=>'fas fa-users', 'title'=>'Пользователи'])
    @alink(['url'=>route('admin.users.list',['user_type'=>1]), 'icon'=>'fas fa-user', 'title'=>'Личностьи'])@endalink
    @alink(['url'=>route('admin.users.list',['user_type'=>2]), 'icon'=>'fas fa-user-tie', 'title'=>'Агенты'])@endalink
@endalink
@alink(['url'=>route('admin.announcements.main'), 'icon'=>'fas fa-bullhorn', 'title'=>'Обявления'])@endalink
@alink(['url'=>route('admin.new_buildings.main'), 'icon'=>'fas fa-city', 'title'=>'Новостройки'])@endalink
@alink(['url'=>route('admin.pages.main'), 'icon'=>'mdi mdi-receipt', 'title'=>'Страницы'])@endalink
@alink(['url'=>route('admin.languages.main'), 'icon'=>'mdi mdi-translate', 'title'=>'Языки']) @endalink
@alink(['url'=>route('admin.main_slider.main'), 'icon'=>'fa fa-images', 'title'=>'Слайдеры']) @endalink
@alink(['url'=>route('admin.banners', ['page'=>'info']), 'icon'=>'fas fa-info-circle', 'title'=>'Информация']) @endalink
@alink(['url'=>route('admin.blog.main'), 'icon'=>'fa fa-blog', 'title'=>'Блог'])@endalink
@alink(['url'=>route('admin.services.main'), 'icon'=>'fa fa-cog', 'title'=>'Сервисы'])@endalink
@alink(['url'=>route('admin.subscribes'), 'icon'=>'fas fa-bell', 'title'=>'Подписки'])@endalink
