<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsToEstatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('estates', function (Blueprint $table) {
            //
            $table->integer('block_1')->default(0);
            $table->integer('block_2')->default(0);
            $table->integer('block_3')->default(0);
            $table->integer('urgent')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('estates', function (Blueprint $table) {
            $table->dropColumn('block_1');
            $table->dropColumn('block_2');
            $table->dropColumn('block_3');
            $table->dropColumn('urgent');
        });
    }
}
