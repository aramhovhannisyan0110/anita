<?php

namespace App\Models;

use App\Http\Traits\HasTranslations;
use App\Http\Traits\Sortable;
use App\Http\Traits\UrlUnique;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\File;

class Package extends Model
{
    use  Sortable, UrlUnique, HasTranslations;
    public $translatable = ['desc'];
    public $timestamps = false;
    public const PACKAGE_COUNT = 1;
    public const PACKAGE_TOP = 2;
    public const PACKAGE_URGENT = 3;
    public const PACKAGE_HOME = 4;
    public const PACKAGES = [
        self::PACKAGE_COUNT => 'Количество',
        self::PACKAGE_TOP => 'Топ',
        self::PACKAGE_URGENT => 'Срочно',
        self::PACKAGE_HOME => 'На главную',
    ];    public static function adminList($package_type)
    {
        return self::select('id', 'package', 'count', 'user_type','price','day', 'active')->where(['package'=>$package_type])->sort()->get();
    }

    public static function getItem($id)
    {
        $result = self::where('id', $id)->first();
        if (!$result) abort(404);
        return $result;
    }
    public static function getTopPackages($user_type)
    {
        return self::where(['user_type'=>$user_type,'package'=>self::PACKAGE_TOP])->get();
    }
    public static function getCountPackages($user_type)
    {
        return self::where(['user_type'=>$user_type,'package'=>self::PACKAGE_COUNT])->get();
    }
    public static function getUrgentPackages($user_type)
    {
        return self::where(['user_type'=>$user_type,'package'=>self::PACKAGE_URGENT])->get();
    }
    public static function getHomePackages($user_type)
    {
        return self::where(['user_type'=>$user_type,'package'=>self::PACKAGE_HOME])->get();
    }
    public static function action($model, $inputs)
    {
        if (empty($model)) {
            $model = new self;
            $action = 'add';
            $ignore = false;
        } else {
            $action = 'edit';
            $ignore = $model->id;
        }
        $model['active'] = !empty($inputs['active']) ? 1 : 0;
        $model['user_type'] = !empty($inputs['user_type']) ? $inputs['user_type'] : 0;
        $model['count'] = !empty($inputs['count']) ? $inputs['count'] : 0;
        $model['price'] = !empty($inputs['price']) ? $inputs['price'] : 0;
        $model['day'] = !empty($inputs['day']) ? $inputs['day'] : 0;
        $model['package'] = !empty($inputs['package']) ? $inputs['package'] : 1;
        $model['active'] = !empty($inputs['active']) ? 1 : 0;
        merge_model($inputs, $model, ['desc']);
        return $model->save();
    }
    public static function deleteItem($model)
    {
        return $model->delete();
    }
}
