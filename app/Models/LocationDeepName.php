<?php

namespace App\Models;

use App\Http\Traits\HasTranslations;
use App\Http\Traits\Sortable;
use App\Http\Traits\UrlUnique;
use App\Services\PageManager\Facades\PageManager;
use Illuminate\Database\Eloquent\Model;

class LocationDeepName extends Model
{
    use HasTranslations, Sortable, UrlUnique;
    public $translatable = ['name'];
    protected $table = 'location_deeps_name';
    protected $fillable = ['name','deep'];
    public static function action($model, $inputs) {
//        self::clearCaches();
        if (empty($model)) {
            $model = new self;
        }
        merge_model($inputs, $model, ['name']);
//        $resizes = [
//            [
//                'width'=>40,
//                'height'=>40,
//                'upsize'=>true,
//            ]
//        ];
//        if($image = upload_image('image', 'u/locations/', $resizes, ($action=='edit' && !empty($model->image))?$model->image:false)) $model->image = $image;
        return $model->save();
    }
}
