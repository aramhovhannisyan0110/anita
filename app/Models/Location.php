<?php

namespace App\Models;

use App\Http\Traits\HasTranslations;
use App\Http\Traits\Sortable;
use App\Http\Traits\UrlUnique;
use App\Services\PageManager\Facades\PageManager;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\File;

class Location extends Model
{
    use HasTranslations, Sortable, UrlUnique;
    public $translatable = ['title', 'seo_title', 'seo_description', 'seo_keywords'];
    public static function adminList($parent_id = null){
        return self::select('id', 'deep','parent_id','type', 'active','title')->where('parent_id',$parent_id)->sort()->get();
    }
    public  function childes(){
        return self::hasMany(Location::class, 'parent_id','id');
    }
    public  function parent(){
        return self::hasOne(Location::class, 'id','parent_id');
    }
    private static function cacheKey(){
        return 'locations';
    }
    private static function clearCaches(){
        Cache::forget(self::cacheKey());
    }
    public static function getFilters(){
        return Cache::rememberForever(self::cacheKey(), function(){
            return self::where(['active', 1])->sort()->get();
        });
    }
    public static function getItem($id){
        $result = self::where('id',$id)->with('parent')->first();
        if (!$result) $result = false;
        return $result;
    }
    public static function getLocations($parent_id = 0){
        return self::where('parent_id',$parent_id)->with(['childes'=>function($q){
            $q->with("childes");
        }])->sort()->get();
    }
    public static function getLocationTitle($ids){
        $ids = json_decode($ids);
        $title='';
        if(!empty($ids)){
//            foreach($ids as $k=>$id){
//                if($k>0){
            $id = $ids[0]??null;
                    if($id != ''){
                        if (Location::getItem($id)){
                            $title=$title.' '.Location::getItem($id)->parent->title.', '.Location::getItem($id)->title;
                        }
                    }
//                }
//            }
        }
        return $title;
    }
    public static function action($model, $inputs) {
        self::clearCaches();
        if (empty($model)) {
            $model = new self;
            $action = 'add';
            $model['parent_id'] = $inputs['parent_id'];
            $model['deep'] = $inputs['deep'];
            $ignore = false;
        }
        else {
            $action = 'edit';
            $ignore = $model['id'];
        }
        if (!empty($inputs['generate_url'])) {
            $url = self::url_unique($inputs['generated_url'], $ignore);
            if (PageManager::inUsedRoutes($url) && $url!=$model->url) $url = $url.'-2';
            $length = mb_strlen($url, 'UTF-8');
            if($length==1) $url = '-'.$url.'-';
            else if ($length==2) $url=$url.'-';
        }
        else {
            $url = $inputs['url'];
        }
        $model['url'] = $url;
        $model['active'] = !empty($inputs['active'])?1:0;
        $model['type'] = !empty($inputs['type'])?1:0;
        merge_model($inputs, $model, ['title', 'seo_title', 'seo_description', 'seo_keywords']);
//        $resizes = [
//            [
//                'width'=>40,
//                'height'=>40,
//                'upsize'=>true,
//            ]
//        ];
//        if($image = upload_image('image', 'u/locations/', $resizes, ($action=='edit' && !empty($model->image))?$model->image:false)) $model->image = $image;
        return $model->save();
    }
    public static function deleteItem($model){
//        $path = public_path('u/filters/');
//        if (!empty($model->image)) File::delete($path.$model->image);
        EstateLocation::where('location_id',$model->id)->delete();

        return $model->delete();
    }
}
