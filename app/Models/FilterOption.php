<?php

namespace App\Models;

use App\Http\Traits\HasTranslations;
use Illuminate\Database\Eloquent\Model;

class FilterOption extends Model
{
    //
    use HasTranslations;
    protected $fillable = ['options','filter_id','have_interval'];
    protected  $table = 'filter_options';
}
