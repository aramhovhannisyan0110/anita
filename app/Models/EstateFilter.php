<?php

namespace App\Models;

use App\Http\Traits\HasTranslations;
use Illuminate\Database\Eloquent\Model;

class EstateFilter extends Model
{
    use HasTranslations;
    protected $fillable = ['estate_id','filter_id','option_id','value'];
    public function filter(){
        return self::hasOne(Filter::class,'id','filter_id');
    }

}
