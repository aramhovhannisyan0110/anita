<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class NewBuildingEstates extends Model
{
    protected $table = 'new_building_estates';
    protected $fillable = ['estate_id','new_building_id'];
    public $timestamps =false;
}
