<?php


namespace App\Models;


use App\Http\Traits\HasTranslations;
use App\Http\Traits\Sortable;
use App\Http\Traits\UrlUnique;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\File;

class NewBuilding extends Model
{
    use HasTranslations, Sortable, UrlUnique;

    public $translatable = ['title', 'owner', 'conditions', 'description', 'banner_title', 'banner_text', 'seo_title', 'seo_description', 'seo_keywords'];

    public static function adminList()
    {
        return self::select('id', 'title', 'owner', 'active')->sort()->get();
    }

    public function estates()
    {
        return self::belongsToMany(Estate::class, 'new_building_estates');
    }

    private static function cacheKey()
    {
        return 'new_building';
    }

    public function gallery()
    {
        return self::hasMany(Gallery::class, 'key', 'id')->where('gallery', 'buildings');
    }

    public static function getItems($id = null)
    {
        $result = self::where('id', '<>', $id)->with('gallery')->sort()->get();
        if (!$result) abort(404);
        return $result;
    }

    private static function clearCaches()
    {
        Cache::forget(self::cacheKey());
    }

    public static function getItem($id)
    {
        $result = self::where('id', $id)->with('estates')->first();
        if (!$result) abort(404);
        return $result;
    }

    public static function getBlog()
    {
        return Cache::rememberForever(self::cacheKey(), function () {
            return self::where('active', 1)->sort()->get();
        });
    }

    public static function getItemSite($url)
    {
        return self::where(['url' => $url, 'active' => 1])->with('gallery')->with(['estates' => function ($q) {
            $q->with(['type', 'galleries', 'locations', 'estate_filter' => function ($q) {
                return $q->with(['filter' => function ($item) {
                    return $item->with('option');
                }
                ]);
            }]);
        }])->firstOrFail();


    }
//        public static function action_image($model, $inputs){
//        self::clearCaches();
//        merge_model($inputs, $model, ['image_alt','image_title']);
//        if($model->save()) return true;
//        return false;
//
//    }
    public static function action($model, $inputs)
    {
        self::clearCaches();
        if (empty($model)) {
            $model = new self;
            $action = 'add';
            $ignore = false;
        } else {
            $action = 'edit';
            $ignore = $model->id;
        }
        $model['active'] = !empty($inputs['active']) ? 1 : 0;
        if (!empty($inputs['generate_url'])) {
            $url = self::url_unique($inputs['generated_url'], $ignore);
        } else {
            $url = $inputs['url'];
        }
        $model['url'] = $url;
        $model['lat'] = $inputs['lat'] ?? null;
        $model['lng'] = $inputs['lng'] ?? null;
        $model['banner_url'] = $inputs['banner_url'] ?? null;
        $model['phone'] = $inputs['phone'] ?? null;
        $model['email'] = $inputs['email'] ?? null;
        $model['address'] = $inputs['address'] ?? null;
        $model['youtube'] = preg_match('%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i', $inputs['youtube'], $video) ? $video[1] : $inputs['youtube'];;
        merge_model($inputs, $model, ['title', 'owner', 'conditions', 'description', 'banner_title', 'banner_text', 'seo_title', 'seo_keywords', 'seo_description']);
        $resizes = [
            [
                'width' => 360,
                'height' => 520,
                'upsize' => true,
                'method' => 'resize'
            ]
        ];
        if ($image = upload_image('banner_image', 'u/new_buildings/', $resizes, ($action == 'edit' && !empty($model->banner_image)) ? $model->banner_image : false)) $model->banner_image = $image;
        NewBuildingEstates::where('new_building_id', $model->id)->delete();
        if (!empty($inputs['estates'])) {
            $estates = explode(',', $inputs['estates']);
            foreach ($estates as $estate) {
                if (!empty($estate) && !(NewBuildingEstates::where(['new_building_id' => $model->id, 'estate_id' => $estate])->first())) {
                    $es_model = new NewBuildingEstates();
                    $es_model->new_building_id = $model['id'];
                    $es_model->estate_id = $estate;
                    $es_model->save();
                }
            }
        }


        return $model->save();
    }


    public static function deleteItem($model)
    {
        $path = public_path('u/new_buildings/');
        if (!empty($model->banner_image)) File::delete($path . $model->banner_image);
        self::clearCaches();
        return $model->delete();
    }
}
