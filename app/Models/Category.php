<?php

namespace App\Models;

use App\Http\Traits\HasTranslations;
use App\Http\Traits\Sortable;
use App\Http\Traits\UrlUnique;
use App\Services\PageManager\Facades\PageManager;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\File;

class Category extends Model
{
    use HasTranslations, Sortable, UrlUnique;
    public $translatable = ['title', 'seo_title', 'seo_description', 'seo_keywords'];
    public static function adminList(){
        return self::select('id', 'image', 'active','title')->sort()->get();
    }
    public static function getFirst(){
        return self::where("active",1)->sort()->first();
    }
    private static function cacheKey(){
        return 'Categories';
    }
    private static function clearCaches(){
        Cache::forget(self::cacheKey());
    }
    public static function getCategories(){
        return Cache::rememberForever(self::cacheKey(), function(){
            return self::where('active', 1)->sort()->get();
        });
    }
    public static function getItem($id){
        $result = self::where('id',$id)->first();
        if (!$result) abort(404);
        return $result;
    }
    public static function action($model, $inputs) {
        self::clearCaches();
        if (empty($model)) {
            $model = new self;
            $action = 'add';
            $ignore = false;
        }
        else {
            $action = 'edit';
            $ignore = $model->id;
        }
        if (!empty($inputs['generate_url'])) {
            $url = self::url_unique($inputs['generated_url'], $ignore);
            if (PageManager::inUsedRoutes($url) && $url!=$model->url) $url = $url.'-2';
            $length = mb_strlen($url, 'UTF-8');
            if($length==1) $url = '-'.$url.'-';
            else if ($length==2) $url=$url.'-';
        }
        else {
            $url = $inputs['url'];
        }
        $model['url'] = $url;
        $model['active'] = !empty($inputs['active'])?1:0;
        $model['new_building'] = !empty($inputs['new_building'])?1:0;
        merge_model($inputs, $model, ['title', 'seo_title', 'seo_description', 'seo_keywords']);
        $resizes = [
            [
                'width'=>40,
                'height'=>40,
                'upsize'=>true,
            ]
        ];
        if($image = upload_image('image', 'u/categories/', $resizes, ($action=='edit' && !empty($model->image))?$model->image:false)) $model->image = $image;
        return $model->save();
    }
    public static function deleteItem($model){
        self::clearCaches();
        $path = public_path('u/categories/');
        if (!empty($model->image)) File::delete($path.$model->image);
        return $model->delete();
    }
}
