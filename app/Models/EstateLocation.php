<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EstateLocation extends Model
{
    //
    protected $table= 'location_estate';
    protected $fillable = ['estate_id','locations_id','location_id'];
}
