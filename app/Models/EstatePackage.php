<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EstatePackage extends Model
{
    //
    protected $fillable = [
        'package_id',
        'user_id',
        'count',
        'active',
        'exp_date',
        'estate_id'
    ];
}
