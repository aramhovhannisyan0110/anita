<?php

namespace App\Models;

use App\Http\Controllers\Admin\GalleriesController;
use App\Http\Traits\HasTranslations;
use App\Http\Traits\Sortable;
use App\Http\Traits\UrlUnique;
use App\Services\PageManager\Facades\PageManager;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\File;

class Estate extends Model
{
    public const STATUS_DECLINED = 0;
    public const STATUS_ACTIVE = 1;
    public const STATUS_ARCHIVE = 2;
    public const STATUS_PENDING = 3;
    public const STATUSES = [
        self::STATUS_DECLINED => 'declined',
        self::STATUS_ACTIVE => 'active',
        self::STATUS_ARCHIVE => 'archive',
        self::STATUS_PENDING => 'pending',
    ];
    use HasTranslations, Sortable, UrlUnique;

    public $translatable = ['title', 'seo_title', 'image_alt', 'image_title', 'seo_description', 'seo_keywords', 'address', 'description', 'short_description','user_address', 'user_name' ];

    public static function adminList($category = 0, $type = 0, $code = false)
    {

        return $code ? self::where([['code', 'LIKE', "%{$code}%"]])->sort()->paginate(30) : self::where(['category_id' => $category, 'type_id' => $type])->sort()->paginate(30);
    }
    public static function search($code)
    {
        return self::where([['code', 'LIKE', "%{$code}%"],['active',1],['active_status',self::STATUS_ACTIVE]])->sort()->get();
    }

    public function user()
    {
        return self::hasOne(User::class, 'id', 'user_id');
    }

    public static function getAnnouncements($status)
    {
        return self::where('active_status', $status)->with('user')->orderBy('id','DESC')->paginate(30, ['*'], "page_$status");
    }

    private static function cacheKey()
    {
        return 'estates';
    }

    public function estate_filter()
    {
        return self::hasMany(EstateFilter::class, 'estate_id', 'id');
    }

    public function galleries()
    {
        return self::hasMany(Gallery::class, 'key', 'id')->where('gallery', 'estates');
    }

    public function type()
    {
        return self::hasOne(Type::class, 'id', 'type_id');
    }

    private static function clearCaches()
    {
        Cache::forget(self::cacheKey());
    }

    public function locations()
    {
        return self::belongsTo(EstateLocation::class, 'id', 'estate_id');
    }

    public static function getBlock($block)
    {
        return self::where(["block_$block" => 1, 'active' => 1])->with(['type', 'galleries', 'estate_filter' => function ($q) {
            return $q->with('filter');
        }])->sort()->get();
    }

    public static function getEstates($category, $type)
    {
        return self::where(["category_id" => $category, "type_id" => $type, 'active' => 1])->with(['type', 'galleries', 'locations', 'estate_filter' => function ($q) {
            return $q->with(['filter' => function ($item) {
                return $item->with('option');
            }]);
        }])->sort()->get();
    }

    public static function getEstatesInSingle($category, $type)
    {
        return self::where(["category_id" => $category, "type_id" => $type, 'active' => 1])->with(['type', 'galleries', 'locations', 'estate_filter' => function ($q) {
            return $q->with(['filter' => function ($item) {
                return $item->with('option');
            }]);
        }])->inRandomOrder()->limit(6)->get();
    }

    public static function getFilteredEstates($category, $type)
    {
        return self::where(["category_id" => $category, "type_id" => $type, 'active' => 1]);
    }

    public static function getFavorites($ids)
    {
        return self::whereIn("id", $ids)->where('active', 1)->with(['type', 'galleries', 'locations', 'estate_filter' => function ($q) {
            return $q->with(['filter' => function ($item) {
                return $item->with('option');
            }]);
        }])->sort()->get();
    }

    public static function getItem($id)
    {
        $result = self::where('id', $id)->with(['type', 'galleries', 'locations', 'user', 'estate_filter' => function ($q) {
            return $q->with(['filter' => function ($item) {
                return $item->with('option');
            }]);
        }])->first();
        if (!$result) abort(404);
        return $result;
    }

    public static function getItemSite($url)
    {
        $result = self::where(['active' => 1, 'url' => $url])->with(['estate_filter', 'galleries','user','locations'])->first();
        if (!$result) abort(404);
        return $result;
    }

    public static function action_image($model, $inputs)
    {
        self::clearCaches();
        merge_model($inputs, $model, ['image_alt', 'image_title']);
        if ($model->save()) return true;
        return false;

    }

    public static function action($model, $inputs)
    {
        self::clearCaches();
        if (empty($model)) {
            $model = new self;
            $action = 'add';
            $ignore = false;
        } else {
            $action = 'edit';
            $ignore = $model->id;
        }
        if (!empty($inputs['generate_url'])) {
            $url = self::url_unique($inputs['generated_url'], $ignore);
            if (PageManager::inUsedRoutes($url) && $url != $model->url) $url = $url . '-2';
            $length = mb_strlen($url, 'UTF-8');
            if ($length == 1) $url = '-' . $url . '-';
            else if ($length == 2) $url = $url . '-';
        } else {
            $url = $inputs['url'];
        }

        $model['url'] = $url;
        $model['active'] = !empty($inputs['active']) ? 1 : 0;
        $model['block_1'] = !empty($inputs['block_1']) ? 1 : 0;
        $model['block_2'] = !empty($inputs['block_2']) ? 1 : 0;
        $model['block_3'] = !empty($inputs['block_3']) ? 1 : 0;
        $model['urgent'] = !empty($inputs['urgent']) ? 1 : 0;
        $model['new'] = !empty($inputs['new']) ? 1 : 0;
        $model['top'] = !empty($inputs['top']) ? 1 : 0;
        $model['status'] = !empty($inputs['status']) ? $inputs['status'] : 0;
        $model['active_status'] = self::STATUS_ACTIVE;
        $model['lat'] = $inputs['lat'] ?? null;
        $model['lng'] = $inputs['lng'] ?? null;
        $model['youtube'] = $inputs['youtube'] ?? null;
        $model['exp_date'] = Carbon::now()->addDays(30);
        $model['code'] = $inputs['code'] ?? Carbon::now()->format('Ymd') . random_int(100, 999);
        if (!empty($inputs['user_phones']) && count($inputs['user_phones'])){
            $model['user_phones'] = json_encode($inputs['user_phones']);
        }

        merge_model($inputs, $model, ['title', 'image_alt', 'image_title', 'category_id', 'type_id', 'address','user_name','user_address', 'description', 'short_description', 'seo_title', 'seo_description', 'seo_keywords', 'price']);
        $resizes = [
            [
                'width' => 800,
                'height' => 500,
                'upsize' => true,
            ]
        ];
//        if ($image = upload_image('image', 'u/estates/', $resizes, ($action == 'edit' && !empty($model->image)) ? $model->image : false,1)) $model->image = $image;
        $model->save();
        $gallery = [];
//        dd($inputs['images']);
        if (!empty($inputs['images'])) {
            $gallery['key'] = $model->id;
            $gallery['gallery'] = 'estates';
            $gallery['images'] = $inputs['images'];
            $gallery_images = new GalleriesController();
            $gallery_images->addEstatesGallery($gallery);
        }
        $model->estate_filter()->delete();
        EstateFilter::where('estate_id', $model->id)->delete();
        if (!empty($inputs['interval_filter'])) {
            foreach ($inputs['interval_filter'] as $k => $option) {
                if (FilterOption::where('options->min', '<=', (int)$option)->where('options->max', '>', (int)$option)->where('filter_id', $k)->exists()) {
                    $opt = FilterOption::where('options->min', '<=', (int)$option)->where('options->max', '>', (int)$option)->where('filter_id', $k)->first();
                } else {
                    $opt = $item = FilterOption::where('options->max', "-1")->where('filter_id', $k)->first();
                }
                if (Filter::where('id', $k)->firstOrFail()->price_filter) {
                    Estate::where('id', $model['id'])->update(['price' => $option]);
                }
                $data['filter_id'] = $k;
                $data['option_id'] = $opt->id ?? null;
                $data['value'] = $option;
                $data['estate_id'] = $model['id'];
                EstateFilter::create($data);
            }
        }
        EstateLocation::where('estate_id', $model->id)->delete();
        if (!empty($inputs['without_interval_filter'])) {
            foreach ($inputs['without_interval_filter'] as $k => $option) {
                if (!empty($option)) {
                    $data['option_id'] = $option;
                    $data['filter_id'] = $k;
                    $data['estate_id'] = $model['id'];
                    $data['value'] = '';
                    EstateFilter::create($data);
                }
            }
        }
        if (!empty($inputs['region'])) {
            $data['location_ids'] = [];
            foreach ($inputs['region'] as $region) {
                $data['locations_id'][] = $region;
            }
            $data['locations_id'] = json_encode($data['locations_id']);
            $data['location_id'] = $inputs['region'][0];
            $data['estate_id'] = $model->id;
            EstateLocation::create($data);
        }
        return $model;
    }

    public static function actionCabinet($model, $inputs)
    {
        self::clearCaches();
        if (empty($model)) {
            $model = new self;
            $action = 'add';
            $ignore = false;
        } else {
            $action = 'edit';
            $ignore = $model->id;
        }
        if (!empty($inputs['generate_url'])) {
            $url = self::url_unique($inputs['generated_url'], $ignore);
            if (PageManager::inUsedRoutes($url) && $url != $model->url) $url = $url . '-2';
            $length = mb_strlen($url, 'UTF-8');
            if ($length == 1) $url = '-' . $url . '-';
            else if ($length == 2) $url = $url . '-';
        } else {
            $url = $inputs['url'];
        }

        $model['url'] = $url;
        $model['active'] = 0;
        if ($action === 'add'){
            $model['block_1'] = 0;
            $model['block_2'] = 0;
            $model['block_3'] = 0;
            $model['urgent'] = 0;
            $model['new'] = 0;
            $model['top'] = 0;
            $model['exp_date'] = Carbon::now()->addDays(30);
            $model['user_id'] = Auth::id() ?? null;
            $model['category_id'] = $inputs['category_id'];
            $model['type_id'] = $inputs['type_id'];
        }
        $model['active_status'] = self::STATUS_PENDING;
        $model['lat'] = $inputs['lat'] ?? null;
        $model['lng'] = $inputs['lng'] ?? null;
        $model['youtube'] = preg_match('%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i', $inputs['youtube'], $video)?$video[1]:$inputs['youtube'];;
        $model['code'] = $inputs['code'] ?? Carbon::now()->format('Ymd') . random_int(100, 999);
        merge_model($inputs, $model, ['title', 'image_alt', 'image_title', 'address', 'description', 'short_description', 'seo_title', 'seo_description', 'seo_keywords']);
        $resizes = [
            [
                'width' => 800,
                'height' => 500,
                'upsize' => true,
            ]
        ];
        if ($image = upload_image('image', 'u/estates/', $resizes, ($action == 'edit' && !empty($model->image)) ? $model->image : false, 1)) $model->image = $image;
        $model->save();
        $gallery = [];
        if (!empty($inputs['images'])) {
            $gallery['key'] = $model->id;
            $gallery['gallery'] = 'estates';
            $gallery['images'] = $inputs['images'];
            $gallery_images = new GalleriesController();
            $gallery_images->addEstatesGallery($gallery);
        }
        $model->estate_filter()->delete();
        EstateFilter::where('estate_id', $model->id)->delete();
        if (!empty($inputs['interval_filter'])) {
            foreach ($inputs['interval_filter'] as $k => $option) {
                if (FilterOption::where('options->min', '<=', (int)$option)->where('options->max', '>', (int)$option)->where('filter_id', $k)->exists()) {
                    $opt = FilterOption::where('options->min', '<=', (int)$option)->where('options->max', '>', (int)$option)->where('filter_id', $k)->first();
                } else {
                    $opt = $item = FilterOption::where('options->max', "-1")->where('filter_id', $k)->first();
                }
                if (Filter::where('id', $k)->firstOrFail()->price_filter) {
                    Estate::where('id', $model['id'])->update(['price' => $option]);
                }
                $data['filter_id'] = $k;
                $data['option_id'] = $opt->id ?? null;
                $data['value'] = $option;
                $data['estate_id'] = $model['id'];
                EstateFilter::create($data);
            }
        }
        EstateLocation::where('estate_id', $model->id)->delete();
        if (!empty($inputs['without_interval_filter'])) {
            foreach ($inputs['without_interval_filter'] as $k => $option) {
                if (!empty($option)) {
                    $data['option_id'] = $option;
                    $data['filter_id'] = $k;
                    $data['estate_id'] = $model['id'];
                    $data['value'] = '';
                    EstateFilter::create($data);
                }
            }
        }
        if (!empty($inputs['region'])) {
            $data['location_ids'] = [];
            foreach ($inputs['region'] as $region) {
                $data['locations_id'][] = $region;
            }
            $data['locations_id'] = json_encode($data['locations_id']);
            $data['location_id'] = $inputs['region'][0];
            $data['estate_id'] = $model->id;
            EstateLocation::create($data);
        }
        return $model;
    }

    public static function deleteItem($model)
    {
        EstateLocation::where('estate_id', $model->id)->delete();
        EstateFilter::where('estate_id', $model->id)->delete();
        $galleries = Gallery::where(['key' => $model->id, 'gallery' => 'estates'])->get();
        if (!empty($galleries) && count($galleries)) {
            $path2 = public_path('u/gallery/');
            $path_thumbs2 = public_path('u/gallery/thumbs/');
            foreach ($galleries as $gallery) {
                File::delete($path2 . $gallery->image);
                File::delete($path_thumbs2 . $gallery->image);
            }
        }
        $path = public_path('u/estates/');
        if (!empty($model->image)) File::delete($path . $model->image);
        return $model->delete();
    }
}
