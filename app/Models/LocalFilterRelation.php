<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LocalFilterRelation extends Model
{
    protected $fillable = ['filter_id','type_id','category_id','category_type'];
    protected $table = 'local_filter_relations';
    public static function getRelated($id){
            return self::where('filter_id', $id)->get();
    }
}
