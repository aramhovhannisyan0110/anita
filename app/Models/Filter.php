<?php

namespace App\Models;

use App\Http\Traits\HasTranslations;
use App\Http\Traits\Sortable;
use App\Http\Traits\UrlUnique;
use App\Services\PageManager\Facades\PageManager;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\File;

class Filter extends Model
{
    use HasTranslations, Sortable, UrlUnique;
    public $translatable = ['title', 'seo_title', 'seo_description', 'seo_keywords', 'metric'];

    public function category()
    {
        return self::belongsToMany(Category::class, 'local_filter_relations', 'filter_id', 'category_id');
    }

    public function types()
    {
        return self::belongsToMany(Type::class, 'local_filter_relations', 'filter_id', 'type_id');
    }

    public function option()
    {
        return self::hasMany(FilterOption::class, 'filter_id', 'id');
    }

    public static function global()
    {
        return self::where(['type'=> 1,'price_filter'=>0])->with('option')->get();
    }
    public static function local($category, $type)
    {
        $ids = LocalFilterRelation::where('category_type', $category . '-' . $type)->pluck('filter_id')->toArray();
        return self::whereIn('id', $ids)->where(['type'=>0])->with('option')->get();
    }
    public static function price($category, $type)
    {
        $ids = LocalFilterRelation::where('category_type', $category . '-' . $type)->pluck('filter_id')->toArray();
        return self::whereIn('id', $ids)->where(['type'=>0,'price_filter'=>0])->with('option')->get();
    }
    public static function adminList()
    {
        return self::select('id', 'image', 'type', 'active', 'title','comment')->where(['price_filter'=>0])->sort()->get();
    }

    private static function cacheKey()
    {
        return 'filters';
    }

    private static function clearCaches()
    {
        Cache::forget(self::cacheKey());
    }

    public static function getFilters()
    {
        return Cache::rememberForever(self::cacheKey(), function () {
            return self::where(['active', 1])->sort()->get();
        });
    }

    public static function getItem($id)
    {
        $result = self::where('id', $id)->with('option')->with(['category'])->first();
//        dd( $result = self::where('id',$id)->with('option'));
//        $ids = $result->category->pluck('id')->toArray();
//        $result->whereIn('category_id',$ids)->get();
        if (!$result) abort(404);
        return $result;
    }

    public static function action($model, $inputs)
    {
//        dd($inputs);
        self::clearCaches();
        if (empty($model)) {
            $model = new self;
            $action = 'add';
            $ignore = false;
        } else {
            $action = 'edit';
            $ignore = $model->id;
        }
        if (!empty($inputs['generate_url'])) {
            $url = self::url_unique($inputs['generated_url'], $ignore);
            if (PageManager::inUsedRoutes($url) && $url != $model->url) $url = $url . '-2';
            $length = mb_strlen($url, 'UTF-8');
            if ($length == 1) $url = '-' . $url . '-';
            else if ($length == 2) $url = $url . '-';
        } else {
            $url = $inputs['url'];
        }
        $model['url'] = $url;
        $model['active'] = !empty($inputs['active']) ? 1 : 0;
        $model['price_filter'] = $inputs['price_filter'];
        $model['type'] = !empty($inputs['type']) ? 1 : 0;
        $model['to_card'] = !empty($inputs['to_card']) ? 1 : 0;
        $model['have_interval'] = !empty($inputs['have_interval']) ? 1 : 0;
        $model['is_required'] = !empty($inputs['is_required']) ? 1 : 0;
        $model['comment'] = !empty($inputs['comment']) ? $inputs['comment'] : '';
        merge_model($inputs, $model, ['title', 'seo_title', 'seo_description', 'seo_keywords', 'metric']);
        $resizes = [
            [
                'width' => 40,
                'height' => 40,
                'upsize' => true,
            ]
        ];
        if ($image = upload_image('image', 'u/filters/', $resizes, ($action == 'edit' && !empty($model->image)) ? $model->image : false)) $model->image = $image;
        $model->save();
//        $model->option()->delete();
        $option_ids = [];
        if (!empty($inputs['filter_options'])) {
            $sort = 0;
            foreach ($inputs['filter_options'] as $k => $option) {
                if (FilterOption::where('id', $k)->exists()) {
                    $filter_opt = FilterOption::where('id', $k)->first();
                    if(isset($option['min'])){
                        $option['min']=(int)$option['min'];
                        $option['max']=(int)$option['max'];
                    }
                    $filter_opt['options'] = json_encode($option);
                    $filter_opt['sort'] = $sort;
                    $filter_opt->save();
                    $option_ids[] = $k;
                } else {
                    if(isset($option['min'])){
                        $option['min']=(int)$option['min'];
                        $option['max']=(int)$option['max'];
                    }
                    $data['options'] = json_encode($option);
                    $data['filter_id'] = $model['id'];
                    $data['have_interval'] = $model['have_interval'];
                    $data['sort'] = $sort;
                    $new = FilterOption::create($data);
                    $option_ids[] = $new->id;
                }
                $sort++;
            }


            if(!empty($inputs['have_interval'])){
                if(FilterOption::where('options->max',"-1")->where('filter_id',$model['id'])->exists()){
                    $item = FilterOption::where('options->max',"-1")->where('filter_id',$model['id'])->first();
                }else{
                    $item = new FilterOption();
                }
                $option=[];
                $option['min']=(int)end($inputs['filter_options'])['max'];
                $option['max']=-1;
                $item['options'] = json_encode($option);
                $item['filter_id'] = $model['id'];
                $item['sort'] = $sort;
                $item['have_interval'] = $model['have_interval'];
                $item->save();
                $option_ids[] = $item->id;
            }
            FilterOption::where('filter_id', $model->id)->whereNotIn('id',$option_ids)->delete();
        }else{
            $model->option()->delete();
        }
        LocalFilterRelation::where('filter_id', $model['id'])->delete();
        if (!empty($inputs['related']) && empty($inputs['type'])) {
            foreach ($inputs['related'] as $relations) {
                $all = $relations;
                $relations = explode('-', $relations);
                $data['category_id'] = $relations[0] ?? null;
                $data['type_id'] = $relations[1] ?? null;
                $data['filter_id'] = $model['id'];
                $data['category_type'] = $all;
                if ((int)$model['price_filter'] === 1){
                    $deleted = Filter::where([['price_filter',1],['id','<>',$model['id']]])->whereHas('category',function ($q) use($data){
                        return $q->where('category_id',$data['category_id']);
                    })->whereHas('types',function ($q) use($data){
                        return $q->where('type_id',$data['type_id']);
                    })->pluck('id');
                    if (!empty($deleted) && count($deleted)){
                        LocalFilterRelation::where(['type_id'=>$data['type_id'],'category_id'=>$data['category_id']])->whereIn('filter_id',$deleted)->delete();
                    }
                    Filter::whereIn('id',$deleted)->delete();
                }
                LocalFilterRelation::create($data);
            }
        }
        return $model;
    }

    public static function deleteItem($model)
    {
        EstateFilter::where('filter_id',$model->id)->delete();
        LocalFilterRelation::where('filter_id',$model->id)->delete();
        $path = public_path('u/filters/');
        if (!empty($model->image)) File::delete($path . $model->image);
        $model->option()->delete();
        return $model->delete();
    }
}
