<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\File;

class Subscribe extends Model
{

    protected $fillable = ['email'];
    public $timestamps = false;
    public static function action($model, $inputs) {
        if (empty($model)) {
            $model = new self;
        }
        merge_model($inputs, $model, ['email']);
        return $model->save();
    }
    public static function deleteItem($model)
    {
        return $model->delete();
    }
}
