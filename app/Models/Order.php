<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Order extends Model
{
    const ORDER_STATUS_ACCEPTED = 1;
    const ORDER_STATUS_PENDING = 0;
    const ORDER_STATUS_DECLINED = -1;

    public static function createOrder($packages, $estateId = null, $userId = null)
    {
        $order = new static();
        $order->amount = 0;

        if (!count($packages) || !$order->save()) {
            return null;
        }

        foreach ($packages as $package) {
            $order->amount += (int)$package->price;
            $order->packages()->attach($package->id, [
                'estate_id' => $estateId,
                'user_id' => Auth::id()
            ]);
        }

        if (!$order->save()) {
            return null;
        }

        return $order;
    }

    public function setAccepted()
    {
        $this->status = static::ORDER_STATUS_ACCEPTED;

        return $this->save();
    }

    public function setDeclined()
    {
        $this->status = static::ORDER_STATUS_DECLINED;

        return $this->save();
    }

    public function packages()
    {
        return $this->belongsToMany(Package::class, 'ordered_packages', 'order_id', 'package_id')->withPivot([
            'user_id',
            'estate_id'
        ]);
    }
}
