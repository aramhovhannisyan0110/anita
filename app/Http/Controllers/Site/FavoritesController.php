<?php

namespace App\Http\Controllers\Site;

use App\Http\Controllers\Controller;
use App\Models\Favorite;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;


class FavoritesController extends Controller
{
    public function actionFavorite(Request $request)
    {
        if ($request->has('item_id') && $request->has('item_id') != null){
            $user = Auth::id();
            if (Favorite::where(['user_id'=>$user,'estate_id'=>$request->item_id])->first()){
                Favorite::where(['user_id'=>$user,'estate_id'=>$request->item_id])->delete();
                return response()->json(['success'=>true,'deleted'=>true,'estate_id'=>$request->item_id]);
            }else{
                $fav = new Favorite();
                $fav->user_id = $user;
                $fav->estate_id = $request->item_id;
                $fav->save();
                return response()->json(['success'=>true,'deleted'=>false,'estate_id'=>$request->item_id]);
            }
        }
        else{
            abort(404);
        }
    }
}
