<?php

namespace App\Http\Controllers\Site;

use App\Mail\Contact;
use App\Models\Banner;
use App\Models\Blog;
use App\Models\Category;
use App\Models\Estate;
use App\Models\EstateFilter;
use App\Models\Favorite;
use App\Models\Filter;
use App\Models\Gallery;
use App\Models\Location;
use App\Models\MainSlide;
use App\Models\NewBuilding;
use App\Models\Page;
use App\Models\Service;
use App\Models\Subscribe;
use App\Models\Type;
use App\Models\VideoGallery;
use App\Services\PageManager\StaticPages;
use Illuminate\Http\Request;
use Illuminate\Session\TokenMismatchException;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;

class AppController extends BaseController
{
    use StaticPages;

    protected function static_home($page)
    {
        $data = [];
        $data['current_page'] = $page['id'];
        $data['seo'] = $this->renderSEO($page);
        $data['news'] = Blog::getBlog()->take(4);
        $data['parent_news'] = Page::getStaticPage('blog');
        $data['new_buildings'] = NewBuilding::getItems();
        $data['types'] = Type::getTypes();
        $data['categories'] = Category::getCategories();
        $data['slides'] = MainSlide::getSlides();
        $data['block_1'] = Estate::getBlock(1)->take(8);
        $data['block_2'] = Estate::getBlock(2)->take(8);
        $data['block_3'] = Estate::getBlock(3)->take(8);
        $data['new_buildings_parents'] = Page::getStaticPage('newBuildings')->url;
        if (Auth::check()){
            $data['favorites'] = Favorite::where('user_id',Auth::id())->pluck('estate_id')->toArray();
        }else{
            $data['favorites'] = isset($_COOKIE['favorite']) ? explode(',', $_COOKIE['favorite']) : [];
        }
        return view('site.pages.home', $data);
    }

    protected function static_about($page)
    {
        $data = [];
        $data['current_page'] = $page['id'];
        $data['banners'] = Banner::get('about');
        $data['gallery'] = Gallery::get('about');
        $data['video_gallery'] = VideoGallery::get('about');
        $data['seo'] = $this->renderSEO($page);
        return view('site.pages.about_company', $data);
    }

    protected function static_contact($page)
    {
        $data = [];
        $data['current_page'] = $page['id'];
        $data['banners'] = Banner::get('contact');
        $data['seo'] = $this->renderSEO($page);
        return view('site.pages.contact', $data);
    }

    protected function static_blog($page)
    {
        $data = [];
        $data['current_page'] = $page['id'];
        $data['page'] = $page;
        $data['items'] = Blog::getBlog()->paginate(12);
        $data['seo'] = $this->renderSEO($page);
        return view('site.pages.blog', $data);
    }

    protected function static_favorites($page)
    {
        if (Auth::check()){
            $data['favorites'] = Favorite::where('user_id',Auth::id())->pluck('estate_id')->toArray();
        }else{
            $data['favorites'] = isset($_COOKIE['favorite']) ? explode(',', $_COOKIE['favorite']) : [];
        }
        $ids = $data['favorites'];
        $data['items'] = Estate::getFavorites($ids);
        $data['current_page'] = $page['id'];
        $data['seo'] = $this->renderSEO($page);
        return view('site.pages.favorites', $data);
    }

    protected function static_services($page)
    {
        $data['items'] = Service::getItems();
        $data['content'] = Banner::get('services');
        $data['current_page'] = $page['id'];
        $data['seo'] = $this->renderSEO($page);
        return view('site.pages.services', $data);
    }
    protected function static_newBuildings($page)
    {
        $data['items'] = NewBuilding::getItems(0);
        $data['content'] = Banner::get('services');
        $data['current_page'] = $page['id'];
        $data['page'] = $page;
        $data['seo'] = $this->renderSEO($page);
        return view('site.pages.new_buildings', $data);
    }

    public function page_item($parent = null, $current = null)
    {
        if ($parent != null && $parent != 'estates') {
            $parent_page = Page::getItemSite($parent);
        }
        if (isset($parent_page) && $parent_page != null && $parent_page->static == 'blog') {
            $data['item'] = Blog::getItemSite($current);
            $data['page'] = $parent_page;
            $data['alts'] = Blog::getBlog()->where('id', '<>', $data['item']['id'])->take(4);
            $data['current_page'] = $data['item']['id'];
            $data['seo'] = $this->renderSEO($data['item']);
            $view = 'blog_item';
        }
        elseif (isset($parent_page) && $parent_page != null && $parent_page->static == 'newBuildings') {
            $data['item'] = NewBuilding::getItemSite($current);
            $data['page'] = $parent_page;
            $data['alts'] = NewBuilding::getItems($data['item']['id'])->take(4);
            $data['current_page'] = $data['page']['id'];
            $data['seo'] = $this->renderSEO($data['item']);
            if (Auth::check()){
                $data['favorites'] = Favorite::where('user_id',Auth::id())->pluck('estate_id')->toArray();
            }else{
                $data['favorites'] = isset($_COOKIE['favorite']) ? explode(',', $_COOKIE['favorite']) : [];
            }            $view = 'new_buildings_item';
        } elseif ($parent == 'estates' && $current != null) {
            $data['item'] = Estate::getItemSite($current);
            $type = Type::getItem($data['item']->type_id);
            $data['seo'] = $this->renderSEO($data['item']);
            $category = Category::getItem($data['item']->category_id);
            $data['part'] = $type->title . '. ' . $category->title;
            $data['alts'] = Estate::getEstatesInSingle($category->id, $type->id);
            $data['galleries'] = Gallery::get('estates', $data['item']['id']);
            if (Auth::check()){
                $data['favorites'] = Favorite::where('user_id',Auth::id())->pluck('estate_id')->toArray();
            }else{
                $data['favorites'] = isset($_COOKIE['favorite']) ? explode(',', $_COOKIE['favorite']) : [];
            }            $view = 'estate_item';
        } else {
            abort(404);
        }

        return view("site.pages.$view", $data);
    }

    protected function dynamic_page($page)
    {
        $data = [];
        $data['current_page'] = $page['id'];
        $data['page'] = $page;
        $data['gallery'] = Gallery::get('pages', $page->id);
        $data['video_gallery'] = VideoGallery::get('pages', $page->id);
        $data['seo'] = $this->renderSEO($page);
        return view('site.pages.dynamic_page', $data);
    }

    /**
     * @throws TokenMismatchException
     */
    public function sendMail(Request $request)
    {
        $redirect = redirect(page('contact') . '#contact-form');
        $validator = Validator::make($request->all(), [
            'mail' => 'required|string|mail|max:255',
            'name' => 'nullable|string|max:255',
            'theme' => 'nullable|string|max:255',
            'phone' => 'required|string|phone|max:255',
            'message' => 'required|string|max:1000'
        ], [
            'required' => t('auth.required'),
            'string' => t('auth.required'),
            'max' => t('auth.max'),
            'mail' => t('auth.invalid email'),
            'phone' => t('auth.invalid phone'),
        ]);
        if ($validator->fails()) {
            return $redirect->withErrors($validator)->withInput();
        }
        $email = $this->shared['info']->contact_email->email;
        if (!$email || !is_email($email)) return $redirect->withErrors(['global' => __('app.internal error')])->withInput();
        try {
            Mail::to($email)->send(new Contact($request->only('mail', 'phone', 'message', 'name', 'theme')));

        } catch (\Exception $exception) {
            return $redirect->withErrors(['global' => __('app.internal error')])->withInput();
        }

        return $redirect->with(['message_sent' => true]);
    }

    public function sendRequest(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'mail' => 'required|string|mail|max:255',
            'name' => 'nullable|string|max:255',
            'alt_phone' => 'nullable|string|phone|max:255',
            'phone' => 'required|string|phone|max:255',
        ], [
            'required' => t('auth.required'),
            'string' => t('auth.required'),
            'max' => t('auth.max'),
            'mail' => t('auth.invalid email'),
            'phone' => t('auth.invalid phone'),
        ]);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }
        $email = $this->shared['info']->contact_email->email;
        if (!$email || !is_email($email)) return redirect()->back()->withErrors(['global' => __('app.internal error')])->withInput();
        try {
            Mail::to($email)->send(new \App\Mail\Request($request->only('mail', 'phone', 'alt_phone', 'name', 'block', 'code')));
        } catch (\Exception $exception) {
            return redirect()->back()->withErrors(['global' => __('app.internal error')])->withInput();
        }
        return redirect()->back()->with(['message_sent' => true]);
    }

    public function estateSearch(Request $request)
    {
        $tp = Type::getItem($request->types);
        $ct = Category::getItem($request->categories);
        $data['items'] = Estate::getEstates($request->categories, $request->types);
        $data['seo'] = $this->renderSEO($tp);
        $data['seo2'] = $this->renderSEO($ct);
        $type = $tp->title;
        $category = $ct->title;
        $data['part'] = $type . '. ' . $category;
        $data['slides'] = MainSlide::getSlides();
        $data['global_filters'] = Filter::global();
        $data['local_filters'] = Filter::local($request->categories, $request->types);
        $data['selected_cat'] = $request->categories;
        $data['selected_tp'] = $request->types;
        $data['new'] = (Category::getItem($request->categories)->new_building) ? true : false;
        $data['categories'] = Category::getCategories();
        $data['types'] = Type::getTypes();
        $data['regions'] = Location::getLocations();
        if (Auth::check()){
            $data['favorites'] = Favorite::where('user_id',Auth::id())->pluck('estate_id')->toArray();
        }else{
            $data['favorites'] = isset($_COOKIE['favorite']) ? explode(',', $_COOKIE['favorite']) : [];
        }
        return view('site.pages.general_search', $data);
    }

    public function estateFilterSearch(Request $request)
    {
        DB::enableQueryLog();
        if (!empty($request->code)) {
            $data['items'] = Estate::where('code', 'like', "%$request->code%");
        } else {
            $data['items'] = Estate::getFilteredEstates($request->category, $request->type);
            $data['items']->select('estates.*');
            if ($request->has('status')) {
                $data['items']->whereIn('status', $request->status);
            }
            if ($request->has('new') && $request->new == 'on') {
                $data['items']->where('new', 1);
            }
            if ($request->has('region') && !$request->has('regions')) {
                $locates = Location::getLocations($request->region);
                if (!empty($locates) && count($locates)) {
                    $items = [];
                    foreach ($locates as $locate) {
                        if (!empty($locate->childes) && count($locate->childes)) {
                            foreach ($locate->childes as $item) {
                                $items[] = $item->id;
                            }
                        }
                    }
                    $data['items']->whereHas('locations', function ($q) use ($items) {
                        $q->whereIn('location_id', $items);
                    });
                }
            } elseif ($request->has('regions') && count($request->regions)) {
                $data['items']->whereHas('locations', function ($q) use ($request) {
                    $q->whereIn('location_id', $request->regions);
                });
            }

            if (!empty($request->equal)) {
                foreach ($request->equal as $key => $equal) {
                    if (!empty($equal)) {

                        $data['items']->where(function ($query)  use ($key,$equal) {
                            $query->whereHas('estate_filter', function ($q) use ($key) {
                                $q->where('filter_id', $key);
                            },'=','0')->orWhereHas('estate_filter', function ($q) use ($key, $equal) {
                                $q->where('filter_id', $key)->where('value', $equal);
                            })->orWhereHas('estate_filter', function ($q) use ($key) {
                                $q->where(['filter_id'=>$key,'value'=>-1]);
                            });
                        });

                    }
                }
            }



            $items =clone  $data['items'];
            $state_ids  = $items->select('id')->pluck('id');

            foreach (['without_interval_filter', 'with_interval_filter'] as $filterKey) if ($request->has($filterKey) && count($request->{$filterKey})) {

                foreach ($request->{$filterKey} as $key => $filter) {




                    // $estate_filters =  EstateFilter::query()->where

                    // ->where('filter_id' , $key)
                    // ->whereIn('option_id', $filter)
                    // ->whereIn('estate_id', $state_ids )->select('estate_id')->pluck('estate_id');

                    // $data['items']->whereIn('id', $estate_filters);


                    // $estate_filters =  EstateFilter::query()->where( function ($q)  use ($key, $equal,$filter) {
                        // $q->where('filter_id', '<>',$key);
                    // } )->orWhere(
                        // function ($q) use ($key, $filter) {
                            // $q->where('filter_id', $key)->whereIn('option_id', $filter);
                        // })
                    // ->whereIn('estate_id', $state_ids )
                    // ->select('estate_id')
                    // ->groupBy('estate_id')
                    // ->pluck('estate_id');

                    // $data['items']->whereIn('id', $estate_filters);

                    // $data['items']->where(function ($query)  use ($key, $equal,$filter) {
                    //     $query->whereHas('estate_filter', function ($q) use ($key, $filter) {
                    //         $q->where('filter_id', $key);
                    //     }, '=', '0')->orWhereHas('estate_filter', function ($q) use ($key, $filter) {
                    //         $q->where('filter_id', $key)->whereIn('option_id', $filter);
                    //     });
                    // });
                    $filterCriteria[] = $filter;
                }
            }
            if (!empty($filterCriteria)) {
                foreach ($filterCriteria as $index => $filterCriterion) {
                    $tableName = 'filter'.$index;
                    $data['items']->join('estate_filters as '.$tableName, function($join) use ($tableName,$filterCriteria) {
                        $join->on($tableName.'.estate_id', 'estates.id');
                    });
                    $data['items']->whereIn($tableName.'.option_id', $filterCriterion);
                }
            }

        }




        if ($request->has('sort') && $request->sort == 'price_asc') {
            $data['items']->orderBy('price', 'ASC');
            $data['sort'] = 1;
        }

        elseif ($request->has('sort') && $request->sort == 'price_desc') {
            $data['items']->orderBy('price', 'DESC');
            $data['sort'] = 2;
        }
        elseif ($request->has('sort') && $request->sort == 'date_desc') {
            $data['items']->orderBy('created_at', 'DESC');
            $data['sort'] = 3;
        } elseif ($request->has('sort') && $request->sort == 'date_asc')  {
            $data['sort'] = 0;
            $data['items']->orderBy('created_at', 'ASC');
        }else{
            // $data['items']->sort();
        }

        $tops = $data['items']->get();
        $data['total_count'] = count($tops);
        $data['tops'] = $tops->where('top', 1)->shuffle()->take(9);
        $data['items'] = $data['items']->where('top', 0)->limit(200)->paginate(30);
        $tp = Type::getItem($request->type);
        $ct = Category::getItem($request->category);
        $data['seo'] = $this->renderSEO($tp);
        $data['seo2'] = $this->renderSEO($ct);
        $type = $tp->title;
        $category = $ct->title;
        $data['part'] = $type . '. ' . $category;
        $data['slides'] = MainSlide::getSlides();
        $data['global_filters'] = Filter::global();
        $data['local_filters'] = Filter::local($request->category, $request->type);
        $data['selected_cat'] = $request->category;
        $data['selected_tp'] = $request->type;
        $data['new'] = (Category::getItem($request->category)->new_building) ? true : false;
        $data['selected_regions'] = $request->regions;
        $data['selected_region'] = $request->region;
        $data['code'] = $request->code ?? null;
        $data['status'] = $request->status ?? [];
        $data['selected_with_min'] = $request->rendered_min ?? [];
        $data['selected_with_max'] = $request->rendered_max ?? [];
        $data['selected_without'] = $request->without_interval_filter ?? [];
        $data['selected_with'] = $request->with_interval_filter ?? [];
        $data['categories'] = Category::getCategories();
        $data['types'] = Type::getTypes();
        $data['regions'] = Location::getLocations();
        // dump(DB::getQueryLog());
        if (Auth::check()){
            $data['favorites'] = Favorite::where('user_id',Auth::id())->pluck('estate_id')->toArray();
        }else{
            $data['favorites'] = isset($_COOKIE['favorite']) ? explode(',', $_COOKIE['favorite']) : [];
        }        return view('site.pages.estates_list', $data);
    }
    public function getFilters(Request $request)
    {
        $data = Filter::local($request->category, $request->type);
        return json_decode($data);
    }


}

