<?php

namespace App\Http\Controllers\Site\Cabinet;

use App\Http\Controllers\Controller;
use App\Models\Estate;
use App\Models\Order;
use App\Models\Package;
use App\Models\User;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class PaymentController extends Controller
{
    protected const param = [
        'ClientID' => '379ca74d-8f1b-4bf4-a196-d977f6cb8b57',
        'Username' => '3d19541048',
        'Password' => 'lazY2k',
        'returnUrl' => 'https://www.atlass.am/paymentCheck',
    ];

    public static function payment()
    {
        $paymentCode = self::getPaymentCode();
        if (!$paymentCode) {
            return \redirect()->back();
        }

        return redirect('https://servicestest.ameriabank.am/VPOS/Payments/Pay?id=' . $paymentCode . '&lang=am');
        /*        $res = json_decode($res->getBody());
                if ((int)$res->errorCode === 0) {
                    Notify::success('donate.success');
                    return redirect($res->formUrl);
                } else {

                    Notify::error(t('donate.system error'));
                    return redirect()->route('donate');
                }*/
    }

    public static function getPaymentCode(Order $order)
    {
        $client = new Client([
            'headers' => ['Content-Type' => 'application/json']
        ]);
        $orderData = [
            'ClientID' => '379ca74d-8f1b-4bf4-a196-d977f6cb8b57',
            'Username' => '3d19541048',
            'Password' => 'lazY2k',
            'BackURL' => 'https://www.atlass.am/paymentCheck',
            'Currency' => '051',
            'Description' => 'Payment for packages',
            'OrderID' => $order->id,
            'Amount' => 10/*$order->amount*/, // Testovi rejimic helneluc es mas@ khanes commentic
        ];
        $res = $client->post('https://servicestest.ameriabank.am/VPOS/api/VPOS/InitPayment',
            [
                'body' => json_encode($orderData)
            ]
        );
        $res = json_decode($res->getBody());

        if ($res->ResponseCode == 1) return $res->PaymentID;

        return false;
    }

    public function paymentCheck(Request $request)
    {
        $this->validate($request, [
            'orderID' => 'required|exists:orders,id',
            'paymentID' => 'required|string'
        ]);


        if ($request->input('resposneCode') == '00') {

            $client = new Client([
                'headers' => ['Content-Type' => 'application/json']
            ]);
            $orderData = [
                'PaymentID' => $request->paymentID,
                'Username' => '3d19541048',
                'Password' => 'lazY2k',

            ];
            $res = $client->post('https://servicestest.ameriabank.am/VPOS/api/VPOS/GetPaymentDetails',
                [
                    'body' => json_encode($orderData)
                ]
            );
            $res = json_decode($res->getBody());
            if ($res->ResponseCode == '00'){
                $order = Order::query()->where('id', $request->input('orderID'))->first();
                $order->setAccepted();
                $order->status = 1;
                $count_pack = false;
;
                foreach ($order->packages as $package) {
                    if ($package->package == Package::PACKAGE_COUNT) {
                        $user = Auth::user();
                        $user->can_add += $package->count;
                        $count_pack = true;
                        $user->save();
                    } elseif ($package->package == Package::PACKAGE_TOP) {
                        if($estate = Estate::where('id',$package->pivot->estate_id)->first()){
                            $estate->top = 1;
                            $estate->save();
                        }
                    } elseif ($package->package == Package::PACKAGE_HOME) {
                        if($estate = Estate::where('id',$package->pivot->estate_id)->first()){
                            $estate->block_1 = 1;
                            $estate->save();
                        }
                    } elseif ($package->package == Package::PACKAGE_URGENT) {
                        if($estate = Estate::where('id',$package->pivot->estate_id)->first()){
                            $estate->urgent = 1;
                            $estate->save();
                        }
                    }
                }

                $order->save();

                if ($count_pack){
                    return redirect()->route('cabinet.paid')->with(['payment'=>'success']);
                }

                return redirect()->route('cabinet.announcements')->with(['payment'=>'success']);
            }
        } else {
            return redirect()->route('cabinet')->withErrors(['global'=>true]);
        }
    }

    private function validator($inputs, $edit = false)
    {
        $rules = [
            'name' => 'string|required|max:255',
            'surname' => 'string|required|max:255',
            'email' => 'email|required|max:255',
            'phone' => 'string|required|max:255',
        ];

        return Validator::make($inputs, $rules, [
            'name.required' => t('donate.name required'),
            'name.string' => t('donate.name format'),
            'name.max' => t('donate.name max'),
            'surname.required' => t('donate.surname required'),
            'surname.string' => t('donate.surname format'),
            'surname.max' => t('donate.surname max'),
            'email.required' => t('donate.email required'),
            'email.email' => t('donate.email email'),
            'email.max' => t('donate.email max'),
            'phone.required' => t('donate.phone required'),
            'phone.max' => t('donate.phone max'),
            'phone.string' => t('donate.phone format'),
        ]);
    }
}
