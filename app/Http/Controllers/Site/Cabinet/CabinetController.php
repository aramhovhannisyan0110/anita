<?php

namespace App\Http\Controllers\Site\Cabinet;

use App\Http\Controllers\Site\BaseController;
use App\Models\Estate;
use App\Models\EstatePackage;
use App\Models\Package;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class CabinetController extends BaseController
{
    public function main()
    {
        return $this->settings();
//        $data = [];
//        $data['user'] = User::auth();
//        $data['seo'] = $this->staticSEO(__('cabinet.profile settings'));
//        return view('site.cabinet.cabinet', $data);
    }
    public function history()
    {
        $data = [];
        $data['user'] = User::query()->where('id', Auth::id())->with(['orders' => function($q) {
            $q->orderBy('id','DESC');
        }])->first();
        $data['seo'] = $this->staticSEO(__('cabinet.profile settings'));
        $data['active'] = 'history';

        return view('site.cabinet.history', $data);
    }

    public function announcements()
    {
        $data = [];
        $data['active'] = 'announcements';
        $data['user'] = User::auth();
        $data['favorites'] = [];
        $data['tops'] = Package::getTopPackages(Auth::user()->user_type);
        $data['homes'] = Package::getHomePackages(Auth::user()->user_type);
        $data['urgents'] = Package::getUrgentPackages(Auth::user()->user_type);
        $data['active_announcements'] = Estate::where(['active_status' => Estate::STATUS_ACTIVE, 'user_id' => $data['user']->id])->get();
        $data['urgent'] = [];
        $data['top'] = [];
        $data['home'] = [];
        if (count($data['active_announcements'])) {
            foreach ($data['active_announcements'] as $act_packs) {
                $packs = EstatePackage::where('estate_id', $act_packs->id)->get();
                if (!empty($packs) && count($packs)) {
                    foreach ($packs as $pack) {
                        $package = Package::where('id', $pack->package_id)->first();

                        if ((int)$package->package === Package::PACKAGE_URGENT) {
                            $data['urgent'][$act_packs->id][] = $pack->exp_date;
                        } elseif ((int)$package->package === Package::PACKAGE_HOME) {
                            $data['home'][$act_packs->id][] = $pack->exp_date;
                        } elseif ((int)$package->package === Package::PACKAGE_TOP) {
                            $data['top'][$act_packs->id][] = $pack->exp_date;
                        }
                    }
                }
                if (!empty($data['urgent'][$act_packs->id])) {
                    usort($data['urgent'][$act_packs->id], function ($a, $b) {
                        $dateTimestamp1 = strtotime($a);
                        $dateTimestamp2 = strtotime($b);

                        return $dateTimestamp1 < $dateTimestamp2 ? -1 : 1;
                    });
                    $data['urgent'][$act_packs->id] = $data['urgent'][$act_packs->id][count($data['urgent'][$act_packs->id]) - 1];
                }
                if (!empty($data['home'][$act_packs->id])){
                    usort($data['home'][$act_packs->id], function ($a, $b) {
                        $dateTimestamp1 = strtotime($a);
                        $dateTimestamp2 = strtotime($b);

                        return $dateTimestamp1 < $dateTimestamp2 ? -1 : 1;
                    });

                    $data['home'][$act_packs->id] = $data['home'][$act_packs->id][count($data['home'][$act_packs->id]) - 1];

                }
                if (!empty($data['top'][$act_packs->id])) {

                    usort($data['top'][$act_packs->id], function ($a, $b) {
                        $dateTimestamp1 = strtotime($a);
                        $dateTimestamp2 = strtotime($b);

                        return $dateTimestamp1 < $dateTimestamp2 ? -1 : 1;
                    });

                    $data['top'][$act_packs->id] = $data['top'][$act_packs->id][count($data['top'][$act_packs->id]) - 1];
                }
            }
        }

        $data['archive_announcements'] = Estate::where(['active_status' => Estate::STATUS_ARCHIVE, 'user_id' => $data['user']->id])->get();
        $data['pending_announcements'] = Estate::where(['active_status' => Estate::STATUS_PENDING, 'user_id' => $data['user']->id])->get();
        $data['declined_announcements'] = Estate::where(['active_status' => Estate::STATUS_DECLINED, 'user_id' => $data['user']->id])->get();
        $data['seo'] = $this->staticSEO(__('cabinet.announcements'));
        return view('site.cabinet.announcements', $data);
    }

    public function paid()
    {
        $data = [];
        $data['active'] = 'paid';
        $data['user'] = User::auth();
        $data['top_packages'] = Package::getTopPackages($data['user']->user_type);
        $data['count_packages'] = Package::getCountPackages($data['user']->user_type);
        $data['urgent_packages'] = Package::getUrgentPackages($data['user']->user_type);
        $data['home_packages'] = Package::getHomePackages($data['user']->user_type);
        $data['seo'] = $this->staticSEO(__('cabinet.profile settings'));
        return view('site.cabinet.paid_packages', $data);
    }

    public function settings()
    {
        $data = [];
        $data['active'] = 'settings';
        $data['user'] = User::where(['id'=>Auth::id()])->with('estates')->firstOrFail();
        $data['estates'] = $estates = $data['user']->estates;
        $data['announcements_list'] = [
            'active' => $estates->where('active_status', Estate::STATUS_ACTIVE),
            'archive' => $estates->where('active_status', Estate::STATUS_ARCHIVE),
            'declined' => $estates->where('active_status', Estate::STATUS_DECLINED),
            'pending' => $estates->where('active_status', Estate::STATUS_PENDING),
        ];        $data['seo'] = $this->staticSEO(__('cabinet.profile settings'));
        return view('site.cabinet.cabinet', $data);
    }

    public function personal(Request $request)
    {
        $user = User::auth();
        $inputs = $request->all();
        $rules = [
            'name' => 'required|string|max:255',
            'phone' => 'required|string|phone|max:255',
            'phone_2' => 'nullable|string|phone|max:255',
            'phone_3' => 'nullable|string|phone|max:255',
            'viber' => 'nullable|string|phone|max:255',
            'whatsapp' => 'nullable|string|phone|max:255',
            'address' => 'nullable|string|max:255',
        ];
        $messages = [
            'required' => t('cabinet.errors.required'),
            'string' => t('cabinet.errors.required'),
            'max' => t('cabinet.errors.max'),
            'phone' => t('cabinet.errors.invalid phone'),

        ];
        if (!empty($request['password']) && !empty($request['password_confirmation']) && !empty($request['current_password'])) {
            $rules['password'] = 'required|string|min:5|confirmed';
            $rules['password_confirmation'] = 'required_with:password|same:password';
            $rules['current_password'] = ['required', 'string', function ($attribute, $value, $fail) use ($user) {
                if (!Hash::check($value, $user->password)) {
                    $fail(__('auth.invalid password'));
                }
            }];
            $messages['new_password.required'] = t('cabinet.Введите новый пароль');
            $messages['new_password.string'] = t('cabinet.Введите новый пароль');
            $messages['new_password.min'] = t('cabinet.Новый пароль должен содержать мин. :min символов');
            $messages['new_password.confirmed'] = t('cabinet.Новый пароль и подверждение не совпадают');
        }
        Validator::make($inputs, $rules, $messages)->validate();

        $user->email = $request->input('email');
        if (!empty($request['password']) && !empty($request['password_confirmation']) && !empty($request['current_password'])) {
            $user->password = Hash::make($request->input('password'));
        }
        $user->name = $request->input('name');
        $user->phone = $request->input('phone');
        $user->phone_2 = $request->input('phone_2');
        $user->phone_3 = $request->input('phone_3');
        $user->viber = $request->input('viber');
        $user->whatsapp = $request->input('whatsapp');
        $user->address = $request->input('address');
        $user->save();
        Auth::login($user);
        return redirect()->route('cabinet.settings')->with(['personal.success' => true]);
    }

}
