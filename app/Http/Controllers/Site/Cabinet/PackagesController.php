<?php

namespace App\Http\Controllers\Site\Cabinet;

use App\Http\Controllers\Controller;
use App\Models\Order;
use App\Models\Package;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;

class PackagesController extends Controller
{
    public function buyPackage(Request $request)
    {
        $this->validate($request, [
            'package_id' => 'required|exists:packages,id'
        ]);

        $package = Package::query()->where('id', $request->input('package_id'))->firstOrFail();
        $user = auth()->user();

        $order = Order::createOrder(Arr::wrap($package), null, $user->id);

        if (!$order) {
            // order stexcveluc inch vor sxala exel, error petqa uxarkes het
            return redirect()->back();
        }

        $paymentCode = PaymentController::getPaymentCode($order);

        return redirect('https://servicestest.ameriabank.am/VPOS/Payments/Pay?id=' . $paymentCode . '');
    }
}
