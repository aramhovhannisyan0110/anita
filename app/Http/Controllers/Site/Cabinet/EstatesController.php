<?php

namespace App\Http\Controllers\Site\Cabinet;

use App\Http\Controllers\Site\BaseController;
use App\Models\Category;
use App\Models\Estate;
use App\Models\EstatePackage;
use App\Models\Filter;
use App\Models\Gallery;
use App\Models\Location;
use App\Models\Order;
use App\Models\Package;
use App\Models\Type;
use App\Services\Notify\Facades\Notify;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class EstatesController extends BaseController
{
    public function addEstate(Request $request, $step)
    {
        $data = [];
        if ($step == 1) {
            $user = Auth::user();
            if (((int)$user->can_add - (int)$user->added) <= 0) {
                return redirect()->route('cabinet.settings')->with([
                    'personal.count_error' => true,
                    ]);
            }
            $data['types'] = Type::getTypes();
            $data['categories'] = Category::getCategories();

            return view('site.cabinet.addStep1', $data);
        }
        if ($step == 2) {
            $inputs = $request->all();
            $rules = [
                'category' => 'required|string|max:255',
                'type' => 'required|string|max:255',
            ];
            $messages = [
                'category.required' => t('errors.category required'),
                'category.string' => t('errors.wrong value of category'),
                'category.max' => t('errors.value of category must be less than 255'),
                'type.required' => t('errors.type required'),
                'type.string' => t('errors.wrong value of type'),
                'type.max' => t('errors.value of type must be less than 255'),
            ];
            $validator = Validator::make($inputs, $rules, $messages);
            if ($validator->fails()) {
                return redirect(route('cabinet.addEstate', ['step' => 1]))
                    ->withErrors($validator, 'messages');
            } else {
                $data['type'] = Type::getItem($request->type);
                $data['category'] = Category::getItem($request->category);
                $data['edit'] = false;
                $data['selected_cat'] = '';
                $data['selected_tp'] = '';
                $data['price_filter'] = '';
                $data['edit'] = false;
                $data['global_filters'] = Filter::global();
                $data['local_filters'] = Filter::local($request->category, $request->type);
                $data['price_filter'] = Filter::local($request->category, $request->type)->where('price_filter', 1)->first();
                $data['selected_cat'] = $request->category;
                $data['selected_tp'] = $request->type;
                $data['categories'] = Category::getCategories();
                $data['types'] = Type::getTypes();
                $data['regions'] = Location::getLocations();
                $type = $data['type']->title;
                $category = $data['category']->title;
                $data['part'] = $type . '. ' . $category;

                return view('site.cabinet.addStep2', $data);
            }

        }

        return abort(404);
    }

    public function addEstateFinish()
    {
        return view('site.cabinet.addEstateFinish');
    }

    public function delete(Request $request)
    {


        $id = $request->input('id');
        $result = [
            'success' => false,
            'id' => $id
        ];
        if ($id && is_id($id)) {
            $page = Estate::where('id', $id)->first();
            if ($page && Estate::deleteItem($page)) $result['success'] = true;
        }
        return response()->json($result);
    }
    public function poster(Request $request)
    {
        $id = $request->input('item_id');
        $response = ['success' => false];
        if (is_id($id) && $item = Gallery::where('id', $id)->first()) {
            $values = ['poster' => $request->poster];
            Gallery::poster($item, $values);
            $response['success'] = true;
        }

        return response()->json($response);
    }

    public function get_packages(Request $request)
    {
        $packages = [];

        if ($request->has('home_package') && $request->input('home_package') != null) {
            if ($home = Package::where('id', $request->home_package)->first()) {
                $dataHome = [
                    'package_id' => $request->home_package,
                    'user_id' => Auth::id(),
                    'estate_id' => $request->estate_id,
                    'exp_date' => Carbon::now()->addDays($home->day)->format('Y-m-d'),
                    'active' => 0,
                    'count' => $home->count,
                ];
                EstatePackage::create($dataHome);

                $packages[] = $home;
            }
        }
        if ($request->has('top_package') && $request->input('top_package') != null) {
            if ($top = Package::where('id', $request->top_package)->first()) {
                $dataTop = [
                    'package_id' => $request->top_package,
                    'user_id' => Auth::id(),
                    'estate_id' => $request->estate_id,
                    'exp_date' => Carbon::now()->addDays($top->day)->format('Y-m-d'),
                    'active' => 0,
                    'count' => $top->count,
                ];
                EstatePackage::create($dataTop);

                $packages[] = $top;
            }
        }
        if ($request->has('urgent_package') && $request->input('urgent_package') != null) {
            if ($urgent = Package::where('id', $request->urgent_package)->first()) {
                $dataUrgent = [
                    'package_id' => $request->urgent_package,
                    'user_id' => Auth::id(),
                    'estate_id' => $request->estate_id,
                    'exp_date' => Carbon::now()->addDays($urgent->day)->format('Y-m-d'),
                    'active' => 0,
                    'count' => $urgent->count,
                ];
                EstatePackage::create($dataUrgent);

                $packages[] = $urgent;
            }
        }

        $order = Order::createOrder($packages, $request->input('estate_id'));

        $paymentCode = PaymentController::getPaymentCode($order);

        return redirect('https://servicestest.ameriabank.am/VPOS/Payments/Pay?id=' . $paymentCode . '');
    }

    public function edit($id)
    {
        $data['item'] = Estate::getItem($id);
        $data['edit'] = true;
        $data['price_filter'] = Filter::local($data['item']->category_id, $data['item']->type_id)->where('price_filter', 1)->first();
        $data['global_filters'] = Filter::global();
        $data['local_filters'] = Filter::local($data['item']->category_id, $data['item']->type_id);
        $data['regions'] = Location::getLocations();
        $data['gallery_images'] = Gallery::get('estates', ['key' => $id]);
        if ($data['item']->locations != null) {
            $data['connected_regions'] = json_decode($data['item']->locations->locations_id);
        } else {
            $data['connected_regions'] = [];
        }
        $tp = Type::getItem($data['item']->type_id);
        $ct = Category::getItem($data['item']->category_id);
        $type = $tp->title;
        $category = $ct->title;
        $data['part'] = $type . '. ' . $category;

        return view('site.cabinet.addStep2', $data);
    }

    public function editPatch($id, Request $request)
    {
        $item = Estate::getItem($id);
        $validator = $this->validator($request, $id, $item);
        $validator['validator']->validate();
        $validator['inputs']['category_id'] = $item->category_id;
        $validator['inputs']['type_id'] = $item->type_id;
        if (Estate::actionCabinet($item, $validator['inputs'])) {
            Notify::success('Недвижимость успешно редактирована.');

            return redirect()->route('cabinet.announcements', ['id' => $item->id]);
        } else {
            Notify::get('error_occured');

            return redirect()->back()->withInput()->withErrors();
        }
    }

    private function validator($request, $ignore = false, $page = null)
    {
        $inputs = $request->all();
        $unique = $ignore === false ? null : ',' . $ignore;
        if (!empty($inputs['url'])) $inputs['url'] = lower_case($inputs['url']);
        $inputs['generated_url'] = !empty($inputs['title'][$this->urlLang]) ? to_url($inputs['title'][$this->urlLang]) : null;
        $request->merge(['url' => $inputs['url']]);
        $result = [];
        $rules = !$ignore ? [
            'title.*' => 'required|string|max:255',
            'description.*' => 'nullable|string|max:255',
            'price' => 'nullable|numeric',
            'region' => 'required|array',
            'region.*' => 'required|string|max:255',
            'interval_filter.*' => [function ($attribute, $value, $fail) {
                $array = explode('.', $attribute);
                $filter = Filter::where('id', end($array))->firstOrFail();
                if (($filter->is_required) && (empty($value) || $value == 0)) {
                    $fail(t('cabinet.errors.Значение фильтра') . ' "' . $filter->title . '" ' . t('cabinet.errors.обязательно для заполнения'));
                }
            }],
            'without_interval_filter.*' => [function ($attribute, $value, $fail) {
                $array = explode('.', $attribute);
                $filter = Filter::where('id', end($array))->firstOrFail();
                if (($filter->is_required) && empty($value)) {
                    $fail(t('cabinet.errors.Значение фильтра') . ' "' . $filter->title . '" ' . t('cabinet.errors.обязательно для заполнения'));
                }
            }],
        ] : [
            'price' => 'nullable|numeric',
            'image' => 'image',
            'interval_filter.*' => [function ($attribute, $value, $fail) {
                $array = explode('.', $attribute);
                $filter = Filter::where('id', end($array))->firstOrFail();
                if (($filter->is_required) && (empty($value) || $value == 0)) {
                    $fail(t('cabinet.errors.Значение фильтра') . ' "' . $filter->title . '" ' . t('cabinet.errors.обязательно для заполнения'));
                }
            }],
            'without_interval_filter.*' => [function ($attribute, $value, $fail) {
                $array = explode('.', $attribute);
                $filter = Filter::where('id', end($array))->firstOrFail();
                if (($filter->is_required) && empty($value)) {
                    $fail(t('cabinet.errors.Значение фильтра') . ' "' . $filter->title . '" ' . t('cabinet.errors.обязательно для заполнения'));
                }
            }],
            'region' => 'required|array',
            'title.*' => 'required|string|max:255',
            'description.*' => 'nullable|string|max:255',
            'region.*' => 'required|string|max:255',
        ];
        if (empty($inputs['generate_url'])) {
            $rules['url'] = 'required|is_url|string|unique:estates,url' . $unique . '|min:3|nullable';
            if (!$page || $page->url !== $inputs['url']) {
                $rules['url'] .= '|not_in_routes';
            }
        }
        $result['validator'] = Validator::make($inputs, $rules, [
            'generated_url.required_with' => 'Введите название (' . $this->urlLang . ') чтобы сгенерировать URL.',
            'url.required' => 'Введите URL или подставьте галочку "сгенерировать автоматический".',
            'url.is_url' => 'Неправильный URL.',
            'url.unique' => 'URL уже используется.',
            'image.image' => t('cabinet.errors.Неверное изоброжение.'),
            'title.*.required' => t('cabinet.errors.Заполните имя.'),
            'title.*.string' => t('cabinet.errors.Значение имени должно быть строковое.'),
            'title.*.max' => t('cabinet.errors.Значение имени слишком длинное.'),
            'description.*.string' => t('cabinet.errors.Значение имени должно быть строковое.'),
            'description.*.max' => t('cabinet.errors.Значение имени слишком длинное.'),
            'region.*.required' => t('cabinet.errors.Выберите местоположение.'),
            'region.required' => t('cabinet.errors.Выберите местоположение.'),
            'region.*.string' => t('cabinet.errors.Значение местоположении должно быть строковое.'),
            'region.*.max' => t('cabinet.errors.Значение местоположении слишком длинное.'),
            'price.numeric' => t('cabinet.errors.Значение цены должна быть цифра.'),
        ]);
        $result['inputs'] = $inputs;

        return $result;
    }

    public function addPut(Request $request, $category, $type)
    {
        $validator = $this->validator($request);
        $validator['validator']->validate();
        $validator['inputs']['category_id'] = $category;
        $validator['inputs']['type_id'] = $type;
        if ($data = Estate::actionCabinet(null, $validator['inputs'])) {
            Notify::success('Недвижимость успешно добавлена.');

            return view('site.cabinet.addEstateFinish');
        } else {
            Notify::get('error_occured');

            return redirect()->back()->withInput();
        }
    }

    public function gallerySort()
    {
        $ids = Gallery::sortable(true);
        if (!$ids) return response()->json(false);

        return response()->json(true);
    }

    public function galleryDelete(Request $request)
    {
        $result = ['success' => false];
        $id = $request->input('item_id');
        if ($id && is_id($id)) {
            $item = Gallery::where('id', $id)->first();
            if ($item && Gallery::deleteItem($item)) $result['success'] = true;
        }

        return response()->json($result);
    }


}
