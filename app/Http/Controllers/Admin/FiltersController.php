<?php

namespace App\Http\Controllers\Admin;

use App\Models\Category;
use App\Models\Filter;
use App\Models\LocalFilterRelation;
use App\Models\Type;
use App\Services\Notify\Facades\Notify;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class FiltersController extends BaseController
{
    public function main(){
        $data = ['title'=>'Фильтры'];
        $data['items'] = Filter::adminList();
        return view('admin.pages.filters.main', $data);
    }
    public function add($price_filter=0,$relation = 0-0){
        $data = [];
        $data['title'] = 'Добавление нового фильтра';
        $data['back_url'] = $price_filter?route('admin.filters.list') :route('admin.filters.main');
        $data['edit'] = false;
        $data['categories'] = Category::getCategories();
        $data['relation'] = $relation;
        $data['types'] = Type::getTypes();
        $data['price_filter'] = $price_filter;
        return view('admin.pages.filters.form', $data);
    }
    public function add_put(Request $request){
        $validator = $this->validator($request);
        $validator['validator']->validate();
        if(Filter::action(null, $validator['inputs'])) {
            Notify::success('Фильтр успешно добавлен.');
            if ($request->has('price_filter') && $request->price_filter == 1){
                return redirect()->route('admin.filters.list');
            }else{
                return redirect()->route('admin.filters.add');
            }
        }
        else {
            Notify::get('error_occured');
            return redirect()->back()->withInput();
        }
    }
    public function edit($id,$price_filter=0){
        $data = [];
        $data['item'] = Filter::getItem($id);
        $data['title'] = 'Редактирование фильтра';
        $data['back_url'] = $price_filter?route('admin.filters.list') :route('admin.filters.main');
        $data['edit'] = true;
        if (!$price_filter){
            $data['related'] = LocalFilterRelation::getRelated($id)->pluck('category_type')->toArray();
        }else{
            $rel=LocalFilterRelation::getRelated($id)->pluck('category_type')->first();
            $data['relation'] = $rel;
        }
        $data['categories'] = Category::getCategories();
        $data['types'] = Type::getTypes();
        $data['price_filter'] = $price_filter;
        return view('admin.pages.filters.form', $data);
    }
    public function price_filters(){
        $categories = Category::getCategories();
        $types = Type::getTypes();
        $list=[];
        foreach($categories as $category){
            foreach ($types as $type){
                $data=[];
                $data['title'] = $category->title.'-'.$type->title;
                $data['relation'] = $category->id.'-'.$type->id;
                $filter = Filter::where('price_filter',1)->whereHas('category',function ($q) use($category){
                   return $q->where('category_id',$category->id);
                })->whereHas('types',function ($q) use($type){
                    return $q->where('type_id',$type->id);
                })->first();
                $data['filter'] = $filter;
                $list[] = $data;
            }
        }
        return view('admin.pages.filters.price_main',['items'=>$list]);
    }
    public function edit_patch($id, Request $request){
        $item = Filter::getItem($id);
        $validator = $this->validator($request, $id, $item);
        $validator['validator']->validate();
        if(Filter::action($item, $validator['inputs'])) {
            Notify::success('Фильтр успешно редактирован.');
            return redirect()->route('admin.filters.edit', ['id'=>$item->id,'price_filter'=>$item->price_filter]);
        }
        else {
            Notify::get('error_occured');
            return redirect()->back()->withInput();
        }
    }
    public function delete(Request $request) {
        $result = ['success'=>false];
        $id = $request->input('item_id');
        if ($id && is_id($id)) {
            $page = Filter::where('id', $id)->first();
            if ($page && Filter::deleteItem($page)) $result['success'] = true;
        }
        return response()->json($result);
    }
    public function sort(){
        return Filter::sortable();
    }
    private function validator($request, $ignore=false, $page=null)
    {
        $inputs = $request->all();

        if (!empty($inputs['url'])) $inputs['url'] = lower_case($inputs['url']);
        $inputs['generated_url'] = !empty($inputs['title'][$this->urlLang]) ? to_url($inputs['title'][$this->urlLang]) : null;
        $request->merge(['url' => $inputs['url']]);
        $unique = $ignore === false ? null : ',' . $ignore;
        $result = [];
        $rules = !$ignore ? [
            'image' => 'required|image',
            'title.*' => 'required|string|max:255',
            'title' => 'array',
//            'required*' => 'required|string|max:255',
            'generated_url' => 'required_with:generate_url|string|nullable',
        ] : [
            'image' => 'image',
            'title.*' => 'required|string|max:255',
            'generated_url' => 'required_with:generate_url|string|nullable',
        ];
        if (empty($inputs['generate_url'])) {
            $rules['url'] = 'required|is_url|string|unique:filters,url' . $unique . '|min:3|nullable';
            if (!$page || $page->url !== $inputs['url']) {
                $rules['url'] .= '|not_in_routes';
            }
        }
        $result['validator'] = Validator::make($inputs, $rules, [
            'generated_url.required_with' => 'Введите название (' . $this->urlLang . ') чтобы сгенерировать URL.',
            'url.required' => 'Введите URL или подставьте галочку "сгенерировать автоматический".',
            'url.is_url' => 'Неправильный URL.',
            'url.unique' => 'URL уже используется.',
            'url.not_in_routes' => 'URL уже используется.',
            'url.min' => 'URL должен содержать мин. 3 символов.',
            'image.image' => 'Неверное изоброжение.',
            'image.required' => 'Выберите изоброжение.',
            'title.*.required' => 'Заполните имя.',
            'title.string' => 'Значение имени должно быть строковое.',
            'title.max' => 'Значение имени слишком длинное.',
        ]);
        $result['inputs'] = $inputs;
        return $result;
    }
}
