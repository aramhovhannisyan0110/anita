<?php

namespace App\Http\Controllers\Admin;

use App\Models\Estate;
use App\Models\Order;
use App\Services\Notify\Facades\Notify;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UsersController extends BaseController
{
    public function main(){
        $data = ['title'=>'Пользователи'];
        $data['items'] = User::getUsers();
        return view('admin.pages.users.main', $data);
    }

    public function view($id){
        $data = [];
        $data['item'] = User::where(['id'=>$id, 'admin'=>0])->with('estates')->firstOrFail();
        $data['title'] = 'Пользователь "'.$data['item']->email.'"';
        $data['estates'] = $estates = $data['item']->estates;
        $data['announcements_list'] = [
            'active' => $estates->where('active_status', Estate::STATUS_ACTIVE),
            'archive' => $estates->where('active_status', Estate::STATUS_ARCHIVE),
            'declined' => $estates->where('active_status', Estate::STATUS_DECLINED),
            'pending' => $estates->where('active_status', Estate::STATUS_PENDING),
        ];
        return view('admin.pages.users.view', $data);
    }
    public function list($user_type){
        $data = [];
        $data['items'] = User::where(['user_type'=>$user_type, 'admin'=>0])->get();
        $data['title'] = 'Пользовательи';
        return view('admin.pages.users.list', $data);
    }

    public function toggleActive(Request $request){
        $id = $request->input('id');
        $active = $request->input('active');
        if (!is_id($id)) abort(404);
        $item = User::findOrFail($id);
        $item->active = $active?1:0;
        $item->save();
        Notify::success('Изменении сохранены');
        return redirect()->route('admin.users.view', ['id'=>$item->id]);
    }
}
