<?php


namespace App\Http\Controllers\Admin;


use App\Models\Estate;
use App\Models\NewBuilding;
use App\Services\Notify\Facades\Notify;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class NewBuildingController extends BaseController
{
    public function main(){
        $data = ['title'=>'Новостройки'];
        $data['items'] = NewBuilding::adminList();
        return view('admin.pages.new_buildings.main', $data);
    }
    public function add(){
        $data = [];
        $data['title'] = 'Добавление новостройки';
        $data['back_url'] = route('admin.new_buildings.main');
        $data['edit'] = false;
        return view('admin.pages.new_buildings.form', $data);
    }
    public function add_put(Request $request){
        $validator = $this->validator($request);
        $validator['validator']->validate();
        if(NewBuilding::action(null, $validator['inputs'])) {
            Notify::success('Новостройка успешно добавлена.');
            return redirect()->route('admin.new_buildings.add');
        }
        else {
            Notify::get('error_occured');
            return redirect()->back()->withInput();
        }
    }
    public function edit($id){
        $data = [];
        $data['item'] = NewBuilding::getItem($id);
        $data['title'] = 'Редактирование новостройки';
        $data['back_url'] = route('admin.new_buildings.main');
        $data['edit'] = true;
        return view('admin.pages.new_buildings.form', $data);
    }
    public function edit_patch($id, Request $request){
        $item = NewBuilding::getItem($id);
        $validator = $this->validator($request, $item->id);
        $validator['validator']->validate();
        if(NewBuilding::action($item, $validator['inputs'])) {
            Notify::success('Новостройка успешно редактирована.');
            return redirect()->route('admin.new_buildings.edit', ['id'=>$item->id]);
        }
        else {
            Notify::get('error_occured');
            return redirect()->back()->withInput();
        }
    }
//    public function edit_image( Request $request)
//    {
//        $item = Blog::getItem($request->item_id);
//        if (Blog::action_image($item, $request)) {
//            $data['success'] = true;
//        } else {
//            $data['success'] = false;
//
//        }
//        return $data;
//    }
    public function delete(Request $request) {
        $result = ['success'=>false];
        $id = $request->input('item_id');
        if ($id && is_id($id)) {
            $page = NewBuilding::where('id', $id)->first();
            if ($page && NewBuilding::deleteItem($page)) $result['success'] = true;

        }
        return response()->json($result);
    }
    public function sort(){
        return NewBuilding::sortable();
    }
    public function search_estate(Request $request){
        $estates = Estate::search($request->code);
        $response = '';
        foreach ($estates as $estate)
        {
            $response.=view('admin.components.for_add_to_new',compact('estate'));
        }
        return response()->json($response);



//        return NewBuilding::sortable();
    }
    private function validator($request, $ignore=false) {
        $inputs = $request->all();
        if(!empty($inputs['url'])) $inputs['url'] = lower_case($inputs['url']);
        $inputs['generated_url'] = !empty($inputs['title'][$this->urlLang])?to_url($inputs['title'][$this->urlLang]):null;
        $request->merge(['url' => $inputs['url']]);
        $unique = $ignore===false?null:','.$ignore;
        $result = [];
        $rules = [
            'generated_url'=>'required_with:generate_url|string|nullable',
            'title.*'=>'required|string|max:255|nullable',
            'title'=>'required|array',
            'owner'=>'required|array',
            'owner.*'=>'required|string|max:255|nullable',
        ];
        if (empty($inputs['generate_url'])) {
            $rules['url'] = 'required|is_url|string|unique:blogs,url'.$unique.'|nullable';
        }
        if (!$ignore) {
            $rules['banner_image'] = 'required|image';
        }
        else {
            $rules['banner_image'] = 'image|nullable';
        }
        $result['validator'] = Validator::make($inputs, $rules, [
            'generated_url.required_with'=>'Введите название ('.$this->urlLang.') чтобы сгенерировать URL.',
            'url.required'=>'Введите URL или подставьте галочку "сгенерировать автоматический".',
            'url.is_url'=>'Неправильный URL.',
            'url.unique'=>'URL уже используется.',
            'image.image'=>'Неверное изоброжение.',
            'image.required'=>'Выберите изоброжение.'
        ]);
        $result['inputs'] = $inputs;
        return $result;
    }
}
