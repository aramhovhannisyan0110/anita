<?php

namespace App\Http\Controllers\Admin;

use App\Models\HomeList;
use Illuminate\Http\Request;

class HomeListController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\HomeList  $homeList
     * @return \Illuminate\Http\Response
     */
    public function show(HomeList $homeList)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\HomeList  $homeList
     * @return \Illuminate\Http\Response
     */
    public function edit(HomeList $homeList)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\HomeList  $homeList
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, HomeList $homeList)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\HomeList  $homeList
     * @return \Illuminate\Http\Response
     */
    public function destroy(HomeList $homeList)
    {
        //
    }
}
