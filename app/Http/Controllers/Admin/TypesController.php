<?php

namespace App\Http\Controllers\Admin;

use App\Models\Type;
use App\Services\Notify\Facades\Notify;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class TypesController extends BaseController
{
    public function main(){
        $data = ['title'=>'Типы Недвижимостей'];
        $data['items'] = Type::adminList();
        return view('admin.pages.types.main', $data);
    }
    public function add(){
        $data = [];
        $data['title'] = 'Добавление нового типа недвижимости';
        $data['back_url'] = route('admin.types.main');
        $data['edit'] = false;
        return view('admin.pages.types.form', $data);
    }
    public function add_put(Request $request){
        $validator = $this->validator($request);
        $validator['validator']->validate();
        if(Type::action(null, $validator['inputs'])) {
            Notify::success('Тип успешно добавлен.');
            return redirect()->route('admin.types.add');
        }
        else {
            Notify::get('error_occured');
            return redirect()->back()->withInput();
        }
    }
    public function edit($id){
        $data = [];
        $data['item'] = Type::getItem($id);
        $data['title'] = 'Редактирование типа недвижимости';
        $data['back_url'] = route('admin.types.main');
        $data['edit'] = true;
        return view('admin.pages.types.form', $data);
    }
    public function edit_patch($id, Request $request){
        $item = Type::getItem($id);
        $validator = $this->validator($request, $id, $item);
        $validator['validator']->validate();
        if(Type::action($item, $validator['inputs'])) {
            Notify::success('Тип недвижимости успешно редактирован.');
            return redirect()->route('admin.types.edit', ['id'=>$item->id]);
        }
        else {
            Notify::get('error_occured');
            return redirect()->back()->withInput();
        }
    }
    public function delete(Request $request) {
        $result = ['success'=>false];
        $id = $request->input('item_id');
        if ($id && is_id($id)) {
            $page = Type::where('id', $id)->first();
            if ($page && Type::deleteItem($page)) $result['success'] = true;
        }
        return response()->json($result);
    }
    public function sort(){
        return Type::sortable();
    }
//    private function validator($inputs, $edit=false) {
//        $result = [];
//        $rules = !$edit?[
//            'image'=>'required|image',
//            'title>*'=>'required|string|max:255',
//        ]:[
//            'image'=>'image',
//            'title.*'=>'required|string|max:255'
//        ];
//        return Validator::make($inputs, $rules, [
//            'image.image'=>'Неверное изоброжение.',
//            'image.required'=>'Выберите изоброжение.',
//            'title.required'=>'Заполните имя.',
//            'title.string'=>'Значение имени должно быть строковое.',
//            'title.max'=>'Значение имени слишком длинное.',
//        ]);
//    }
    private function validator($request, $ignore=false, $page=null) {
        $inputs = $request->all();

        if(!empty($inputs['url'])) $inputs['url'] = lower_case($inputs['url']);
        $inputs['generated_url'] = !empty($inputs['title'][$this->urlLang])?to_url($inputs['title'][$this->urlLang]):null;
        $request->merge(['url' => $inputs['url']]);
        $unique = $ignore===false?null:','.$ignore;
        $result = [];
        $rules = !$ignore?[
            'image'=>'required|image',
            'title>*'=>'required|string|max:255',
            'generated_url'=>'required_with:generate_url|string|nullable',
        ]:[
            'image'=>'image',
            'title.*'=>'required|string|max:255',
            'generated_url'=>'required_with:generate_url|string|nullable',
        ];
        if (empty($inputs['generate_url'])) {
            $rules['url'] = 'required|is_url|string|unique:types,url'.$unique.'|min:3|nullable';
            if (!$page || $page->url!==$inputs['url']){
                $rules['url'].='|not_in_routes';
            }
        }
        $result['validator'] = Validator::make($inputs, $rules, [
            'generated_url.required_with'=>'Введите название ('.$this->urlLang.') чтобы сгенерировать URL.',
            'url.required'=>'Введите URL или подставьте галочку "сгенерировать автоматический".',
            'url.is_url'=>'Неправильный URL.',
            'url.unique'=>'URL уже используется.',
            'url.not_in_routes'=>'URL уже используется.',
            'url.min'=>'URL должен содержать мин. 3 символов.',
            'image.image'=>'Неверное изоброжение.',
            'image.required'=>'Выберите изоброжение.',
            'title.required'=>'Заполните имя.',
            'title.string'=>'Значение имени должно быть строковое.',
            'title.max'=>'Значение имени слишком длинное.',
        ]);
        $result['inputs'] = $inputs;
        return $result;
    }

}
