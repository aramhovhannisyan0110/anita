<?php

namespace App\Http\Controllers\Admin;

use App\Models\Subscribe;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class SubscribesController extends Controller
{
    public function subscribe(Request $request)
    {
        $inputs = $request->all();
        $data = Validator::make($inputs, [
            'email' => ['required', 'string', 'mail', 'max:255', 'unique:subscribes'],
        ], [
            'required' => t('subscribe_errors.required'),
            'string' => t('subscribe_errors.required'),
            'unique' => t('subscribe_errors.unique'),
            'max' => t('subscribe_errors.max'),
            'mail' => t('subscribe_errors.invalid email'),
        ]);
        $data->validate();
        if (Subscribe::action(null, $request)) {
            return redirect()->back()->withInput(['success' => true]);
        }else{
            return redirect()->back()->withInput(['success' => false]);
        }
    }
    public function delete(Request $request) {
        $result = ['success'=>false];
        $id = $request->input('item_id');
        if ($id && is_id($id)) {
            $page = Subscribe::where('id', $id)->first();
            if ($page && Subscribe::deleteItem($page)) $result['success'] = true;
        }
        return response()->json($result);
    }
}
