<?php

namespace App\Http\Controllers\Admin;

use App\Models\Category;
use App\Models\Estate;
use App\Models\EstateLocation;
use App\Models\Filter;
use App\Models\Gallery;
use App\Models\Location;
use App\Models\Type;
use App\Services\Notify\Facades\Notify;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class EstatesController extends BaseController
{
    public function main(Request $request, $category = null, $type = null)
    {
        if ($category === null || $type === null) {
            $cat = Category::getFirst();
            $tp = Type::getFirst();
            $category = $cat->id;
            $type = $tp->id;
        } else {
            $cat = Category::getItem($category);
            $tp = Type::getItem($type);
        }
        $data = ['title' => $cat->title . '-' . $tp->title];
        $data['categories'] = Category::getCategories();
        $data['types'] = Type::getTypes();
        $data['items'] = $request->has('code') ? Estate::adminList($category, $type, $request->get('code')) : Estate::adminList($category, $type);
        $data['selected_cat'] = ($request->has('code') && $request->get('code') != null) ? null : $category;
        $data['selected_tp'] = ($request->has('code') && $request->get('code') != null) ? null : $type;
        return view('admin.pages.estates.main', $data);
    }

    public function add($category, $type)
    {

        $data = [];
        $data['title'] = 'Добавление новой недвижимостьи';
        $data['back_url'] = route('admin.estates.main', ['category' => $category, 'type' => $type]);
        $data['edit'] = false;
        $data['global_filters'] = Filter::global();
        $data['local_filters'] = Filter::local($category, $type);
        $data['price_filter'] = Filter::local($category, $type)->where('price_filter', 1)->first();
        $data['selected_cat'] = $category;
        $data['selected_tp'] = $type;
        $data['categories'] = Category::getCategories();
        $data['types'] = Type::getTypes();
        $data['regions'] = Location::getLocations();
        return view('admin.pages.estates.form', $data);
    }

    public function add_put(Request $request, $category, $type)
    {
        $validator = $this->validator($request);
        $validator['validator']->validate();
        $validator['inputs']['category_id'] = $category;
        $validator['inputs']['type_id'] = $type;
        if (Estate::action(null, $validator['inputs'])) {
            Notify::success('Недвижимость успешно добавлена.');
            return redirect()->route('admin.estates.add', ['category' => $category, 'type' => $type]);
        } else {
            Notify::get('error_occured');
            return redirect()->back()->withInput();
        }
    }

    public function edit($id)
    {
        $data = [];
        $data['item'] = Estate::getItem($id);
        $data['title'] = 'Редактирование недвижимости';
        $data['back_url'] = route('admin.estates.main', ['category' => $data['item']->category_id, 'type' => $data['item']->type_id]);
        $data['global_filters'] = Filter::global();
        $data['local_filters'] = Filter::local($data['item']->category_id, $data['item']->type_id);
        $data['price_filter'] = Filter::local($data['item']->category_id, $data['item']->type_id)->where('price_filter', 1)->first();
        $data['edit'] = true;
        $data['selected_cat'] = $data['item']->category_id;
        $data['selected_tp'] = $data['item']->type_id;
        $data['categories'] = Category::getCategories();
        $data['types'] = Type::getTypes();
        $data['regions'] = Location::getLocations();
        $data['gallery_images'] = Gallery::get('estates',['key'=>$id]);
        if ($data['item']->locations != null) {
            $data['connected_regions'] = json_decode($data['item']->locations->locations_id);
        } else {
            $data['connected_regions'] = [];
        }
        return view('admin.pages.estates.form', $data);
    }

    public function edit_patch($id, Request $request)
    {
        $item = Estate::getItem($id);
        $validator = $this->validator($request, $id, $item);
        $validator['validator']->validate();
        $validator['inputs']['category_id'] = $item->category_id;
        $validator['inputs']['type_id'] = $item->type_id;
        if (Estate::action($item, $validator['inputs'])) {
            Notify::success('Недвижимость успешно редактирована.');
            return redirect()->route('admin.estates.edit', ['id' => $item->id]);
        } else {
            Notify::get('error_occured');
            return redirect()->back()->withInput()->withErrors();
        }
    }

    public function edit_image(Request $request)
    {
        $item = Estate::getItem($request->item_id);
        if (Estate::action_image($item, $request)) {
            $data['success'] = true;
        } else {
            $data['success'] = false;

        }
        return $data;
    }

    public function delete(Request $request)
    {

        $result = ['success' => false];
        $id = $request->input('item_id');
        if ($id && is_id($id)) {
            $page = Estate::where('id', $id)->first();
            if ($page && Estate::deleteItem($page)) $result['success'] = true;
        }
        return response()->json($result);
    }

    public function sort()
    {
        return Estate::sortable();
    }

    private function validator($request, $ignore = false, $page = null)
    {
        $inputs = $request->all();
        if (!empty($inputs['url'])) $inputs['url'] = lower_case($inputs['url']);
        $inputs['generated_url'] = !empty($inputs['title'][$this->urlLang]) ? to_url($inputs['title'][$this->urlLang]) : null;
        $request->merge(['url' => $inputs['url']]);
        $unique = $ignore === false ? null : ',' . $ignore;
        $result = [];
        $rules = !$ignore ? [

//            'image' => 'required|image',
            'title.*' => 'required|string|max:255',
            'price' => 'nullable|numeric',
            'region' => 'required|array',
            'code' => 'nullable|unique:estates,code|min:6|max:255|string',
            'region.*' => 'required|string|max:255',
            'interval_filter.*' =>  [function($attribute, $value, $fail) {
            $array=explode('.',$attribute);
                $filter=Filter::where('id',end($array))->firstOrFail();
                if(($filter->is_required) && (empty($value) || $value == 0)){
                    $fail('Значение фильтра "'.$filter->title.'" обязательно для заполнения');
                }
            }],
            'without_interval_filter.*' =>  [function($attribute, $value, $fail) {
                $array=explode('.',$attribute);
                $filter=Filter::where('id',end($array))->firstOrFail();
                if(($filter->is_required) && empty($value)){
                    $fail('Значение фильтра "'.$filter->title.'" обязательно для заполнения');
                }
            }],
            'generated_url' => 'required_with:generate_url|string|nullable',
        ] : [
            'price' => 'nullable|numeric',
            'image' => 'image',
            'interval_filter.*' =>  [function($attribute, $value, $fail) {
                $array=explode('.',$attribute);
                $filter=Filter::where('id',end($array))->firstOrFail();
                if(($filter->is_required) && (empty($value) || $value == 0)){
                    $fail('Значение фильтра "'.$filter->title.'" обязательно для заполнения');
                }
            }],
            'without_interval_filter.*' =>  [function($attribute, $value, $fail) {
                $array=explode('.',$attribute);
                $filter=Filter::where('id',end($array))->firstOrFail();
                if(($filter->is_required) && empty($value)){
                    $fail('Значение фильтра "'.$filter->title.'" обязательно для заполнения');
                }
            }],
            'code' => 'nullable|unique:estates,code' . $unique . '|min:6|max:255|string',
            'region' => 'required|array',
            'title.*' => 'required|string|max:255',
            'region.*' => 'required|string|max:255',
            'generated_url' => 'required_with:generate_url|string|nullable',
        ];
        if (empty($inputs['generate_url'])) {
            $rules['url'] = 'required|is_url|string|unique:estates,url' . $unique . '|min:3|nullable';
            if (!$page || $page->url !== $inputs['url']) {
                $rules['url'] .= '|not_in_routes';
            }
        }
        $result['validator'] = Validator::make($inputs, $rules, [

            'generated_url.required_with' => 'Введите название (' . $this->urlLang . ') чтобы сгенерировать URL.',
            'url.required' => 'Введите URL или подставьте галочку "сгенерировать автоматический".',
            'url.is_url' => 'Неправильный URL.',
            'url.unique' => 'URL уже используется.',
            'code.required' => 'Заполните значение кода.',
            'code.string' => 'Неправильный формат код.',
            'code.unique' => 'Код уже используется.',
            'code.min' => 'Значение кода должно быть болше 6-и.',
            'code.max' => 'Значение кода слишком длинное.',
            'url.not_in_routes' => 'URL уже используется.',
            'url.min' => 'URL должен содержать мин. 3 символов.',
            'image.image' => 'Неверное изоброжение.',
            'image.required' => 'Выберите изоброжение.',
            'title.*.required' => 'Заполните имя.',
            'title.*.string' => 'Значение имени должно быть строковое.',
            'title.*.max' => 'Значение имени слишком длинное.',
            'region.*.required' => 'Выберите местоположение.',
            'region.required' => 'Выберите местоположение.',
            'region.*.string' => 'Значение местоположении должно быть строковое.',
            'region.*.max' => 'Значение местоположении слишком длинное.',
            'price.numeric' => 'Значение цены должна быть цифра.',
        ]);
        $result['inputs'] = $inputs;
        return $result;
    }
}
