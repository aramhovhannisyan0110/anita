<?php

namespace App\Http\Controllers\Admin;

use App\Models\Package;
use App\Services\Notify\Facades\Notify;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class PackagesController extends BaseController
{
    public function main($package_type=1){
        $data = ['title'=>'Сервисы'];
        $data['user_items'] = Package::adminList($package_type)->where('user_type',1);
        $data['company_items'] = Package::adminList($package_type)->where('user_type',2);
        $data['package'] = Package::PACKAGES[$package_type];
        $data['package_type'] = $package_type;
        return view('admin.pages.packages.main', $data);
    }
    public function add($package_type=1,$user_type=1){
        $data = [];
        $data['title'] = 'Добавление сервиса "'.Package::PACKAGES[$package_type].'"'.($user_type==1? ' для личностьей' : ' для компаний');
        $data['back_url'] = route('admin.packages.main',['package_type'=>$package_type??1]);
        $data['edit'] = false;
        $data['package_type'] = $package_type;
        $data['user_type'] = $user_type;
        return view('admin.pages.packages.form', $data);
    }
    public function add_put(Request $request){
        $validator = $this->validator($request);
        $validator['validator']->validate();
        if(Package::action(null, $validator['inputs'])) {
            Notify::success('Сервис успешно добавлен.');
            return redirect()->route('admin.packages.add',['package_type'=>$request->package??1,'user_type'=>$request->user_type??1]);
        }
        else {
            Notify::get('error_occured');
            return redirect()->back()->withInput();
        }
    }
    public function edit($id){
        $data = [];
        $data['item'] = Package::getItem($id);
        $data['package_type'] = $data['item']->package;
        $data['user_type'] = $data['item']->user_type;
        $data['title'] = 'Редактирование сервиса';
        $data['back_url'] = route('admin.packages.main');
        $data['edit'] = true;
        return view('admin.pages.packages.form', $data);
    }
    public function edit_patch($id, Request $request){
        $item = Package::getItem($id);
        $validator = $this->validator($request, $item->id);
        $validator['validator']->validate();
        if(Package::action($item, $validator['inputs'])) {
            Notify::success('Сервис успешно редактирован.');
            return redirect()->route('admin.packages.edit', ['id'=>$item->id]);
        }
        else {
            Notify::get('error_occured');
            return redirect()->back()->withInput();
        }
    }
    public function delete(Request $request) {
        $result = ['success'=>false];
        $id = $request->input('item_id');
        if ($id && is_id($id)) {
            $page = Package::where('id', $id)->first();
            if ($page && Package::deleteItem($page)) $result['success'] = true;
        }
        return response()->json($result);
    }
    public function sort(){return Service::sortable();
    }
    private function validator($request, $ignore=false) {
        $inputs = $request->all();
        $rules = [
            'count' => 'nullable|numeric',
            'day' => 'nullable|numeric',
            'desc.*' => 'nullable|string|max:500',
            'price' => 'required|numeric',
            'user_type' => 'required|numeric',
            'package' => 'required|numeric',
        ] ;

        $result['validator'] = Validator::make($inputs, $rules, [
            'required'=>'Заполните поле.',
            'integer'=>'Значение поля должна быть цифра.',
            'max'=>'Перевышен макс. допустимое кол-ество символов.',
        ]);
        $result['inputs'] = $inputs;
        return $result;
    }
}
