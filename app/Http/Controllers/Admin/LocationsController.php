<?php

namespace App\Http\Controllers\Admin;

use App\Models\Location;
use App\Models\LocationDeepName;
use App\Services\Notify\Facades\Notify;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class LocationsController extends BaseController
{
    public function main($parent_id = 0,$deep = 0){
        $data = ['title'=>'Локации'];
        $data['items'] = Location::adminList($parent_id);
        $data['parent_id'] =$parent_id;
        $data['deep'] =$data['items']->first()->deep??$deep;
        $data['loc'] = LocationDeepName::where('deep',$data['deep'])->first()??null;
        return view('admin.pages.locations.main', $data);

    }
    public function add($parent_id = 0,$deep = 0){
        $data = [];
        $data['title'] = 'Добавление новой локации';
        $data['parent_id'] = $parent_id;
        $data['deep'] = $deep;
        $data['back_url'] = route('admin.locations.main',['parent_id'=>$parent_id]);
        $data['edit'] = false;
        return view('admin.pages.locations.form', $data);
    }
    public function add_put(Request $request,$parent_id,$deep){
        $validator = $this->validator($request);
        $validator['validator']->validate();
        $validator['inputs']['parent_id'] = $parent_id;
        $validator['inputs']['deep'] = $deep;
        if(Location::action(null, $validator['inputs'])) {
            Notify::success('Локация успешно добавлена.');
            return redirect()->route('admin.locations.add',['parent_id'=>$parent_id,'deep'=>$deep]);
        }
        else {
            Notify::get('error_occured');
            return redirect()->back()->withInput();
        }
    }
    public function edit($id){
        $data = [];
        $data['item'] = Location::getItem($id);
        $data['title'] = 'Редактирование локации';
        $data['back_url'] = route('admin.locations.main',['parent_id'=>$data['item']['parent_id']]);
        $data['edit'] = true;
        return view('admin.pages.locations.form', $data);
    }
    public function edit_patch($id, Request $request){
        $item = Location::getItem($id);
        $validator = $this->validator($request, $id, $item);
        $validator['validator']->validate();
        if(Location::action($item, $validator['inputs'])) {
            Notify::success('Локация успешно редактирован.');
            return redirect()->route('admin.locations.edit', ['id'=>$item->id]);
        }
        else {
            Notify::get('error_occured');
            return redirect()->back()->withInput();
        }
    }
    public function changeLocationName(Request $request){
        if(LocationDeepName::where('deep',$request->deep)->exists()){
            $model=LocationDeepName::where('deep',$request->deep)->first();
            if(LocationDeepName::action($model, $request)) {
                Notify::success('Имя успешно отредактирована.');
                return redirect()->back();
            }
            else {
                Notify::get('error_occured');
                return redirect()->back()->withInput();
            }
        }else{
            if(LocationDeepName::action(null, $request)) {
                Notify::success('Имя успешно сохранена.');
                return redirect()->back();
            }
            else {
                Notify::get('error_occured');
                return redirect()->back()->withInput();
            }
        }
    }
    public function delete(Request $request) {
        $result = ['success'=>false];
        $id = $request->input('item_id');
        if ($id && is_id($id)) {
            $page = Location::where('id', $id)->first();
            if ($page && Location::deleteItem($page)) $result['success'] = true;
        }
        return response()->json($result);
    }
    public function sort(){
        return Location::sortable();
    }
    public function getLocations(Request $request){
        $data = Location::getLocations($request->parent_id);
//        echo json_decode($data);
        return json_encode($data);
    }
    private function validator($request, $ignore=false, $page=null)
    {
        $inputs = $request->all();

        if (!empty($inputs['url'])) $inputs['url'] = lower_case($inputs['url']);
        $inputs['generated_url'] = !empty($inputs['title'][$this->urlLang]) ? to_url($inputs['title'][$this->urlLang]) : null;
        $request->merge(['url' => $inputs['url']]);
        $unique = $ignore === false ? null : ',' . $ignore;
        $result = [];
        $rules = !$ignore ? [
            'title>*' => 'required|string|max:255',
            'generated_url' => 'required_with:generate_url|string|nullable',
        ] : [
            'title.*' => 'required|string|max:255',
            'generated_url' => 'required_with:generate_url|string|nullable',
        ];
        if (empty($inputs['generate_url'])) {
            $rules['url'] = 'required|is_url|string|unique:locations,url' . $unique . '|min:3|nullable';
            if (!$page || $page->url !== $inputs['url']) {
                $rules['url'] .= '|not_in_routes';
            }
        }
        $result['validator'] = Validator::make($inputs, $rules, [
            'generated_url.required_with' => 'Введите название (' . $this->urlLang . ') чтобы сгенерировать URL.',
            'url.required' => 'Введите URL или подставьте галочку "сгенерировать автоматический".',
            'url.is_url' => 'Неправильный URL.',
            'url.unique' => 'URL уже используется.',
            'url.not_in_routes' => 'URL уже используется.',
            'url.min' => 'URL должен содержать мин. 3 символов.',
            'title.required' => 'Заполните имя.',
            'title.string' => 'Значение имени должно быть строковое.',
            'title.max' => 'Значение имени слишком длинное.',
        ]);
        $result['inputs'] = $inputs;
        return $result;
    }
}
