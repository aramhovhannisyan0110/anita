<?php

namespace App\Http\Middleware;

use App\Models\User;
use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if (Auth::check() && !empty(Auth::user()->user_type) ) {
            return $next($request);
        }elseif (Auth::check() && empty(Auth::user()->user_type)){
            return redirect()->route('chooseUserType',['id'=>Auth::id()]);
        }
        return redirect()->route('cabinet.login');
    }
}
