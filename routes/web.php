<?php
use Illuminate\Support\Facades\Route;
use App\Services\LanguageManager\Facades\LanguageManager;

//region Admin
//region Login
Route::group(['prefix' => config('admin.prefix'), 'middleware'=>'notAdmin'], function (){
    Route::get('login', 'Admin\AuthController@login')->name('admin.login');
    Route::post('login', 'Admin\AuthController@attemptLogin');
    Route::get('password/reset', 'Admin\AuthController@reset')->name('admin.password.reset');
    Route::post('password/reset', 'Admin\AuthController@attemptReset');
    Route::get('password/recover/{email}/{token}', 'Admin\AuthController@recover')->where(['email'=>'[^\/]+', 'token'=>'[^\/]+'])->name('admin.password.recover');
    Route::post('password/recover/{email}/{token}', 'Admin\AuthController@attemptRecover')->where(['email'=>'[^\/]+', 'token'=>'[^\/]+']);
});
//endregion
Route::get('verify-email/{email}/{token}', 'Site\Auth\RegisterController@verifyEmail')->name('verify_email');

Route::middleware('setLocale')->group(function(){
    Route::post('send-mail', 'Site\AppController@sendMail')->name('contacts.send_mail');
});
Route::group(['prefix' => config('admin.prefix'), 'middleware' => 'admin'], function () {
    //region CKFinder
    Route::any('file_browser/connector', '\CKSource\CKFinderBridge\Controller\CKFinderController@requestAction')->name('ckfinder_connector');
    Route::any('file_browser/browser', '\CKSource\CKFinderBridge\Controller\CKFinderController@browserAction')->name('ckfinder_browser');
    //endregion
    Route::name('admin.')->namespace('Admin')->group(function(){
        //region Logout
        Route::post('logout', 'AuthController@logout')->name('logout');
        //endregion
        //region Home Page Redirect
        Route::get('/', 'AuthController@redirectIfAuthenticated');
        //endregion
        //region Dashboard
        Route::get('dashboard', 'DashboardController@main')->name('dashboard');
        //endregion
        //region Languages
        Route::prefix('languages')->name('languages.')->group(function() { $c='LanguagesController@';
            Route::get('', $c.'main')->name('main');
            Route::patch('', $c.'editLanguage');
        });
        //endregion
        //region Pages
        Route::prefix('pages')->name('pages.')->group(function() { $c='PagesController@';
            Route::get('', $c.'main')->name('main');
            Route::get('add', $c.'addPage')->name('add');
            Route::put('add', $c.'addPage_put');
            Route::get('edit/{id}', $c.'editPage')->name('edit');
            Route::patch('edit/{id}', $c.'editPage_patch');
            Route::delete('delete', $c.'deletePage_delete')->middleware('ajax')->name('delete');
            Route::patch('sort', $c.'sort')->middleware('ajax')->name('sort');
        });
        //endregion
        //region Pages
        Route::prefix('new_buildings')->name('new_buildings.')->group(function() { $c='NewBuildingController@';
            Route::get('', $c.'main')->name('main');
            Route::get('add', $c.'add')->name('add');
            Route::put('add', $c.'add_put');
            Route::get('edit/{id}', $c.'edit')->name('edit');
            Route::patch('edit/{id}', $c.'edit_patch');
            Route::delete('delete', $c.'delete')->middleware('ajax')->name('delete');
            Route::patch('sort', $c.'sort')->middleware('ajax')->name('sort');
            Route::post('search', $c.'search_estate')->middleware('ajax')->name('search_estate');
        });
        //endregion
        //region Banners
        Route::match(['get', 'post'], 'banners/{page}', 'BannersController@renderPage')->name('banners');

        Route::prefix('gallery')->group(function(){ $c='GalleriesController@';
            Route::get('{gallery}/{id?}{key?}', $c.'show')->name('gallery');
            Route::put('add', $c.'add')->name('gallery.add');
            Route::patch('edit', $c.'edit')->middleware('ajax')->name('gallery.edit');
            Route::patch('sort', $c.'sort')->middleware('ajax')->name('gallery.sort');
            Route::delete('delete', $c.'delete')->middleware('ajax')->name('gallery.delete');
            Route::post('poster', $c.'poster')->middleware('ajax')->name('gallery.poster');
        });
        //endregion
        //region Users
        Route::prefix('users')->name('users.')->group(function(){ $c='UsersController@';
            Route::get('', $c.'main')->name('main');
            Route::get('{id}', $c.'view')->name('view');
            Route::get('list/{user_type}', $c.'list')->name('list');
            Route::patch('toggle-active', $c.'toggleActive')->name('toggleActive');
        });
        //endregion
        //region Video Galleries
        Route::prefix('video-gallery')->group(function(){ $c='VideoGalleriesController@';
            Route::get('{gallery}/{id?}', $c.'show')->name('video_gallery');
            Route::get('{gallery}/add/{id?}', $c.'add')->name('video_gallery.add');
            Route::put('{gallery}/add/{id?}', $c.'add_put');
            Route::get('{id}/edit', $c.'edit')->name('video_gallery.edit');
            Route::patch('{id}/edit', $c.'edit_patch');
            Route::patch('sort', $c.'sort')->middleware('ajax')->name('video_gallery.sort');
            Route::delete('delete', $c.'delete')->middleware('ajax')->name('video_gallery.delete');
        });
        //endregion
        //region Profile
        Route::prefix('profile')->name('profile.')->group(function() { $c = 'ProfileController@';
            Route::get('', $c.'main')->name('main');
            Route::patch('', $c.'patch');
        });
        //endregion
        //region Translations
        Route::prefix('translations')->name('translations.')->group(function() { $c = 'TranslationsController@';
            Route::get('{locale}', $c.'main')->name('main');
            Route::get('{locale}/{filename}', $c.'edit')->name('edit');
            Route::patch('{locale}/{filename}', $c.'edit_patch')->name('edit');
        });
        //endregion
        //Home Page Sliders
        Route::prefix('main-slider')->name('main_slider.')->group(function() { $c='MainSliderController@';
            Route::get('', $c.'main')->name('main');
            Route::get('add', $c.'add')->name('add');
            Route::put('add', $c.'add_put');
            Route::get('edit/{id}', $c.'edit')->name('edit');
            Route::patch('edit/{id}', $c.'edit_patch');
            Route::delete('delete', $c.'delete')->middleware('ajax')->name('delete');
            Route::patch('sort', $c.'sort')->middleware('ajax')->name('sort');
        });
        //end sliders
        //region types
        Route::prefix('types')->name('types.')->group(function() { $c='TypesController@';
            Route::get('', $c.'main')->name('main');
            Route::get('add', $c.'add')->name('add');
            Route::put('add', $c.'add_put');
            Route::get('edit/{id}', $c.'edit')->name('edit');
            Route::patch('edit/{id}', $c.'edit_patch');
            Route::delete('delete', $c.'delete')->middleware('ajax')->name('delete');
            Route::patch('sort', $c.'sort')->middleware('ajax')->name('sort');
        });
        //endregion
        //region Categories
        Route::prefix('categories')->name('categories.')->group(function() { $c='CategoriesController@';
            Route::get('', $c.'main')->name('main');
            Route::get('add', $c.'add')->name('add');
            Route::put('add', $c.'add_put');
            Route::get('edit/{id}', $c.'edit')->name('edit');
            Route::patch('edit/{id}', $c.'edit_patch');
            Route::delete('delete', $c.'delete')->middleware('ajax')->name('delete');
            Route::patch('sort', $c.'sort')->middleware('ajax')->name('sort');
        });
        //endregion
        // region Announcements
        Route::prefix('announcements')->name('announcements.')->group(function() { $c='AnnouncementsController@';
            Route::get('', $c.'main')->name('main');
            Route::get('add', $c.'add')->name('add');
            Route::get('changeStatus/{id}/{status}', $c.'changeStatus')->name('changeStatus');
            Route::put('add', $c.'add_put');
            Route::get('edit/{id}', $c.'edit')->name('edit');
            Route::patch('edit/{id}', $c.'edit_patch');
            Route::delete('delete', $c.'delete')->middleware('ajax')->name('delete');
            Route::patch('sort', $c.'sort')->middleware('ajax')->name('sort');
        });
        //endregion
        //region Filters
        Route::prefix('filters')->name('filters.')->group(function() { $c='FiltersController@';
            Route::get('', $c.'main')->name('main');
            Route::get('priceFilters', $c.'price_filters')->name('list');
            Route::get('add/{price_filter?}/{relation?}', $c.'add')->name('add');
            Route::put('add', $c.'add_put');
            Route::get('edit/{id}/{price_filter?}', $c.'edit')->name('edit');
            Route::patch('edit/{id}', $c.'edit_patch');
            Route::delete('delete', $c.'delete')->middleware('ajax')->name('delete');
            Route::patch('sort', $c.'sort')->middleware('ajax')->name('sort');
        });
        //endregion
        //region Locations
        Route::prefix('locations')->name('locations.')->group(function() { $c='LocationsController@';
            Route::get('main/{parent_id?}/{deep?}', $c.'main')->name('main');
            Route::get('{parent_id?}/{deep?}/add', $c.'add')->name('add');
            Route::put('{parent_id?}/{deep?}/add', $c.'add_put');
            Route::get('edit/{id}', $c.'edit')->name('edit');
            Route::patch('edit/{id}', $c.'edit_patch');
            Route::post('changeLocationName', $c.'changeLocationName')->name('changeLocationName');
            Route::delete('delete', $c.'delete')->middleware('ajax')->name('delete');
            Route::patch('sort', $c.'sort')->middleware('ajax')->name('sort');
        });
        //endregion
        //region Estates
        Route::prefix('estates')->name('estates.')->group(function() { $c='EstatesController@';
            Route::get('main/{category?}/{type?}/{code?}', $c.'main')->name('main');
            Route::get('{category?}/{type?}/add', $c.'add')->name('add');
            Route::put('{category?}/{type?}/add', $c.'add_put');
            Route::get('edit/{id}', $c.'edit')->name('edit');
            Route::patch('edit/{id}', $c.'edit_patch');
            Route::patch('edit_image', $c.'edit_image')->name('edit_image');
//            Route::post('changeLocationName', $c.'changeLocationName')->name('changeLocationName');
            Route::delete('delete', $c.'delete')->middleware('ajax')->name('delete');
            Route::patch('sort', $c.'sort')->middleware('ajax')->name('sort');
        });
        //endregion
        //region subscribes
        Route::prefix('subscribes')->name('subscribes.')->group(function() { $c='SubscribesController@';
            Route::delete('delete', $c.'delete')->middleware('ajax')->name('delete');
        });
        //endregion
        //region packages
        Route::prefix('packages')->name('packages.')->group(function() { $c='PackagesController@';
            Route::get('/{user_type?}', $c.'main')->name('main');
            Route::get('add/{package_type?}/{user_type?}', $c.'add')->name('add');
            Route::put('add', $c.'add_put');
            Route::get('edit/{id}', $c.'edit')->name('edit');
            Route::patch('edit/{id}', $c.'edit_patch');
            Route::delete('delete', $c.'delete')->middleware('ajax')->name('delete');
            Route::patch('sort', $c.'sort')->middleware('ajax')->name('sort');
        });
        //endregion
        //region Subscribes
            Route::get('subscribers', 'PagesController@subscribers')->name('subscribes');
        //endregion
        //Region services
        Route::prefix('services')->name('services.')->group(function() { $c='ServicesController@';
            Route::get('', $c.'main')->name('main');
            Route::get('add', $c.'add')->name('add');
            Route::put('add', $c.'add_put');
            Route::get('edit/{id}', $c.'edit')->name('edit');
            Route::patch('edit/{id}', $c.'edit_patch');
            Route::delete('delete', $c.'delete')->middleware('ajax')->name('delete');
            Route::patch('sort', $c.'sort')->middleware('ajax')->name('sort');
        });
        //endregion
        //Region blog
        Route::prefix('blog')->name('blog.')->group(function() { $c='BlogController@';
            Route::get('', $c.'main')->name('main');
            Route::get('add', $c.'add')->name('add');
            Route::put('add', $c.'add_put');
            Route::get('edit/{id}', $c.'edit')->name('edit');
            Route::patch('edit/{id}', $c.'edit_patch');
            Route::patch('edit_image', $c.'edit_image')->name('edit_image');
            Route::delete('delete', $c.'delete')->middleware('ajax')->name('delete');
            Route::patch('sort', $c.'sort')->middleware('ajax')->name('sort');
        });

        //home_lists
        Route::prefix('home-list')->name('homeList.')->group(function () {
            $c = 'HomeListController@';
            Route::get('', $c.'index')->name('index');
            Route::get('add', $c.'create')->name('create');
            Route::patch('edit/{id}', $c.'edit');
            Route::delete('delete', $c.'destroy')->name('destroy');
        });

        //endregion
        //regionGetRegions
//        Route::get('getRegions','LocationsController@getLocations')->name('getRegions');
        //endregion
    });
});
//endregion
//region EstateSearch

//
//region Site
Route::get('cabinet/logout', 'Site\Auth\LoginController@logout')->middleware('logged_in')->name('logout');
Route::group(['prefix'=>LanguageManager::getPrefix(), 'middleware'=>'languageManager'], function () {
    include 'cabinet.php';
    Route::get('paymentCheck', 'Site\Cabinet\PaymentController@paymentCheck')->name('paymentCheck');
    Route::get('estateSearch','Site\AppController@estateSearch')->name('estateSearch');
    Route::get('getRegions','Admin\LocationsController@getLocations')->name('getRegions');
    Route::get('getBuildingType','Admin\CategoriesController@getBuildingType')->name('getBuildingType');
    Route::get('search','Site\AppController@estateFilterSearch')->name('search');
    Route::get('getFilters','Site\AppController@getFilters')->name('getFilters');
    Route::get('{url?}', 'Site\AppController@pageManager')->name('page');
    Route::post('send-mail', 'Site\AppController@sendMail')->name('contacts.send_mail');
    Route::post('send-request', 'Site\AppController@sendRequest')->name('contacts.send_request');
    Route::post('subscribe', 'Admin\SubscribesController@subscribe')->name('subscribe');
    Route::get('{parent?}/{current}', 'Site\AppController@page_item')->name('page_item');
    Route::post('auth/favorites', 'Site\FavoritesController@actionFavorite')->middleware('ajax')->name('auth.favorites');
});
//endregion
