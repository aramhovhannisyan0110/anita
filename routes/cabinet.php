<?php

use Illuminate\Support\Facades\Route;

Route::group(['prefix' => 'cabinet'], function () {
    Route::get('register', 'Site\Auth\RegisterController@showRegistrationForm')->name('register');
    Route::post('login', 'Site\Auth\LoginController@login')->name('login.post');
    Route::get('login', 'Site\Auth\LoginController@showLoginForm')->name('cabinet.login');
    Route::post('register', 'Site\Auth\RegisterController@register')->name('register.post');
    Route::post('reset', 'Site\Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
    Route::get('userType/{id}', 'Site\Cabinet\ProfileController@chooseUserType')->name('chooseUserType');
    Route::post('userType', 'Site\Cabinet\ProfileController@chooseUserTypeSubmit')->name('chooseUserType.post');
    Route::post('password/reset/{email}/{token}', 'Site\Auth\ResetPasswordController@reset')->name('password.update');
    Route::get('login/facebook', 'Site\CustomerLoginController@facebook')->name('cabinet.facebook.login');
    Route::get('login/facebook/redirect', 'Site\CustomerLoginController@facebookRedirect')->name('cabinet.facebook.redirect');
    Route::get('login/google', 'Site\CustomerLoginController@gmail')->name('cabinet.gmail.login');
    Route::get('login/google/redirect', 'Site\CustomerLoginController@gmailRedirect')->name('cabinet.gmail.redirect');
    Route::get('login/mailru', 'Site\CustomerLoginController@mailru')->name('cabinet.mailru.login');
    Route::get('login/mailru/redirect', 'Site\CustomerLoginController@mailruRedirect')->name('cabinet.mailru.redirect');
    Route::group(['middleware'=>'guest'],function(){
        Route::post('profile/personal','Site\Cabinet\CabinetController@personal')->name('cabinet.personal');
        Route::get('', 'Site\Cabinet\CabinetController@main')->name('cabinet');
        Route::get('/announcements', 'Site\Cabinet\CabinetController@announcements')->name('cabinet.announcements');
        Route::get('/addEstate/{step}/{category?}/{type?}', 'Site\Cabinet\EstatesController@addEstate')->name('cabinet.addEstate');
        Route::get('/addEstateFinish', 'Site\Cabinet\EstatesController@addEstateFinish')->name('cabinet.addEstateFinish');
        Route::get('/edit/{id}', 'Site\Cabinet\EstatesController@edit')->name('cabinet.editEstate');
        Route::get('/paid', 'Site\Cabinet\CabinetController@paid')->name('cabinet.paid');
        Route::get('/settings', 'Site\Cabinet\CabinetController@settings')->name('cabinet.settings');
        Route::get('/buy_package', 'Site\Cabinet\PackagesController@buyPackage')->name('cabinet.buy_package');
        Route::get('/history', 'Site\Cabinet\CabinetController@history')->name('cabinet.history');
        Route::put('/estates/add/{category}/{type}', 'Site\Cabinet\EstatesController@addPut')->name('cabinet.estates.add');
        Route::patch('/estate/edit/{id}', 'Site\Cabinet\EstatesController@editPatch')->name('cabinet.estates.edit');
        Route::patch('sort', 'Site\Cabinet\EstatesController@gallerySort')->middleware('ajax')->name('cabinet.gallery.sort');
        Route::delete('delete', 'Site\Cabinet\EstatesController@galleryDelete')->middleware('ajax')->name('cabinet.gallery.delete');
        Route::post('poster', 'Site\Cabinet\EstatesController@poster')->middleware('ajax')->name('cabinet.gallery.poster');
        Route::post('get_packages', 'Site\Cabinet\EstatesController@get_packages')->name('cabinet.estates.get_packages');
        Route::delete('delete', 'Site\Cabinet\EstatesController@delete')->middleware('ajax')->name('cabinet.delete');

    });

});
