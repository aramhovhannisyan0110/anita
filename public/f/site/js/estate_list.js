console.log(window.selected_region)
$(document).ready(function () {
    $('.for_select2_sort').select2();
    setGridTypeByAram();

    getLocations(parseInt(window.selected_region));
    $(document).on('change', '.rendered select', function () {
        var min = parseInt($(this).parent().parent().find('.rendered_min').find('option:selected').data('value'));
        var max = parseInt($(this).parent().parent().find('.rendered_max').find('option:selected').data('value'));
        if (min == max) {
            $(this).closest('.product-rent__info').find('.equal').val(min);
            var opt = $(("select[data-filter=" + $(this).data('for') + "]")).find('option');
            opt.each(function () {
                var vals = $(this).text().split('-');
                $(this).prop('selected', false);

                if (parseInt(vals[0]) >= min && (parseInt(vals[1]) <= max || max == -1)) {
                    $(this).prop('selected', true);
                }
            })
            opt.trigger('change.select2')
        } else {
            $(this).closest('.product-rent__info').find('.equal').val('')
            if ($(this).hasClass('rendered_min')) {
                $(this).parent().parent().find('.rendered_max').find('option:selected').prop('selected', false);
                var rem = $(this).parent().parent().find('.rendered_max').find('option');
                $(this).parent().parent().find('.rendered_max').trigger('change.select2');
                rem.each(function () {
                    $(this).removeAttr('disabled')
                    if (parseInt($(this).data('value')) < min && parseInt($(this).data('value')) != -1) {
                        console.log(min, $(this).data('value'))
                        $(this).prop('disabled', true)
                    }
                })
                $(this).parent().parent().find('.rendered_max').select2(
                    {
                        placeholder: window.select_place,
                        closeOnSelect: true,
                    }
                );
            }
            var opt = $(("select[data-filter=" + $(this).data('for') + "]")).find('option');
            opt.each(function () {
                var vals = $(this).text().split('-');
                $(this).prop('selected', false);

                if (parseInt(vals[0]) >= min && (parseInt(vals[1]) <= max || max == -1)) {
                    $(this).prop('selected', true);
                }
            })
            opt.trigger('change.select2')

        }
    })


    $('.for_select2').select2(
        {
            placeholder: window.select_place,
            closeOnSelect: false,
            // language: {
            //     noResults: function () {
            //         return '<button id="no-results-btn" onclick="noResultsButtonClicked()">NortyrtyrtyResult Found</a>';
            //     },
            // },
            // escapeMarkup: function (markup) {
            //     return markup;
            // },
        }
    )
        // .on('select2:not(selected)', e => $(e.currentTarget).data('scrolltop', $('.select2-results__options').scrollTop()))
        // .on('select2:not(selected)', e => $('.select2-results__options').scrollTop($(e.currentTarget).data('scrolltop')));
    $('.for_select2_closable').select2(
            {
                placeholder: window.select_place,
                closeOnSelect: true,
            }
        )
    $(document).on('focus', '.select2-selection.select2-selection--single', function (e) {
        $(this).closest(".select2-container").siblings('select:enabled').select2('open');
    });
    $('.for_select2_closable').on('select2:closing', function (e) {
        $(e.target).data("select2").$selection.one('focus focusin', function (e) {
            e.stopPropagation();
        });
    });


    $(window).resize(function () {
        filters_init()
    })
    filters_init();

    function filters_init() {
        if ($(window).width() <= 1300) {
            $(".popup-request-call__info").append($('.product-list__rent').remove())
            $('.for_select2').select2(
                {
                    placeholder: window.select_place,
                    closeOnSelect: false,
                }
            ).on('select2:not(selected)', e => $(e.currentTarget).data('scrolltop', $('.select2-results__options').scrollTop()))
                .on('select2:not(selected)', e => $('.select2-results__options').scrollTop($(e.currentTarget).data('scrolltop')));
            $('.for_select2_closable').select2(
                {
                    placeholder: window.select_place,
                    closeOnSelect: true,
                }
            )
        } else {
            $(".product-list__right").append($('.product-list__rent').remove())
            $('.for_select2').select2(
                {
                    placeholder: window.select_place,
                    closeOnSelect: false,
                }
            ).on('select2:not(selected)', e => $(e.currentTarget).data('scrolltop', $('.select2-results__options').scrollTop()))
                .on('select2:not(selected)', e => $('.select2-results__options').scrollTop($(e.currentTarget).data('scrolltop')));
            $('.for_select2_closable').select2(
                {
                    placeholder: window.select_place,
                    closeOnSelect: true,
                }
            )
        }
    }

});
$(document).on('change', '.rendered select', function () {
    var min = parseInt($(this).parent().parent().find('.rendered_min').find('option:selected').data('value'));
    var max = parseInt($(this).parent().parent().find('.rendered_max').find('option:selected').data('value'));
    if (min == max) {
        $(this).closest('.product-rent__info').find('.equal').val(min);
        var opt = $(("select[data-filter=" + $(this).data('for') + "]")).find('option');
        opt.each(function () {
            var vals = $(this).text().split('-');
            $(this).prop('selected', false);

            if (parseInt(vals[0]) >= min && (parseInt(vals[1]) <= max || max == -1)) {
                $(this).prop('selected', true);
            }
        })
        opt.trigger('change.select2')
    } else {
        $(this).closest('.product-rent__info').find('.equal').val('')
        if ($(this).hasClass('rendered_min')) {
            $(this).parent().parent().find('.rendered_max').find('option:selected').prop('selected', false);
            var rem = $(this).parent().parent().find('.rendered_max').find('option');
            $(this).parent().parent().find('.rendered_max').trigger('change.select2');
            rem.each(function () {
                $(this).removeAttr('disabled')
                if (parseInt($(this).data('value')) < min && parseInt($(this).data('value')) != -1) {
                    $(this).prop('disabled', true)
                }
            })
        }
        var opt = $(document).find(("select[data-filter=" + $(this).data('for') + "]")).find('option');
        opt.each(function () {
            var vals = $(this).text().split('-');
            $(this).prop('selected', false);
            if (parseInt(vals[0]) >= min && (parseInt(vals[1]) <= max || max == -1)) {
                $(this).prop('selected', true);
            }
        })
        opt.trigger('change.select2')

    }
})
$(document).on('click', '.select2-results__group', function () {
    var item = $(this).parent().attr('aria-label');
    var opt = $(document).find('optgroup[label=' + item + ']');
    if ($(this).next().find("li[aria-selected='true']").length == $(this).next().find('li').length) {
        $(this).next().find('li').each(function () {
            $(this).attr('aria-selected', false)
        })
    } else {
        $(this).next().find('li').each(function () {
            $(this).attr('aria-selected', true)
        })
    }
    if (opt.find("option:selected").length == opt.find('option').length) {
        opt.find('option').each(function () {
            $(this).prop('selected', false)
        })
    } else {
        opt.find('option').each(function () {
            $(this).prop('selected', true)
        })
    }
    opt.parent().trigger('change.select2');


    //
});
// $(function () {
//     $(".items__min").slice(0, 8).show();
//     $("#loadMore").on('click', function (e) {
//         e.preventDefault();
//         $(".items__min:hidden").slice(0, 8).slideDown();
//         if ($(".items__min:hidden").length == 0) {
//             $("#loadMore" + "").fadeOut('slow');
//         }
//
//     });
// });
// $(function () {
//     $(".items").slice(0, 8).show();
//     $("#loadmach").on('click', function (e) {
//         e.preventDefault();
//         $(".items:hidden").slice(0, 8).slideDown();
//         if ($(".items:hidden").length == 0) {
//             $("#loadmach" + "").fadeOut('slow');
//         }
//
//     });
// });

function getFilters(a) {
    var data_type = $(a).data('type');
    var data_category = $(a).val();
    var lang = $('#lang').val();
    getBuildingType(data_category)
    $('.new_added').remove();
    $.ajax({
        url: window.get_filters,
        type: "GET",
        data: {
            type: data_type,
            category: data_category
        },
        headers: {
            'X-CSRF-Token': window.token
        },
        success: function (data) {
            for (index in data) {
                var data2 = data[index].option;
                var options = '';
                var options_min = '';
                var options_max = '';
                if (data[index].have_interval == 1) {
                    for (i in data2) {
                        options += '<option  value="' + data2[i].id + '">' + JSON.parse(data2[i].options).min + '-' + JSON.parse(data2[i].options).max + '</option>'
                        if (parseInt(JSON.parse(data2[i].options).min) == 0) {
                            options_min += '<option data-value = "' + JSON.parse(data2[i].options).min + '"  value="' + data2[i].id + '">'+window.no_min+'</option>'
                        } else {
                            options_min += '<option data-value = "' + JSON.parse(data2[i].options).min + '"  value="' + data2[i].id + '">' + JSON.parse(data2[i].options).min + '</option>'
                        }
                        options_max += '<option data-value = "' + JSON.parse(data2[i].options).max + '"  value="' + data2[i].id + '">' + JSON.parse(data2[i].options).max + '</option>'
                    }
                    $(".search[data-type=" + data_type + "]").append('<div class="new_added product-rent__info "><label for="' + data[index].id + '">' + data[index].title[lang] + ':</label><select style = "display:none" data-filter = "' + data[index].id + '"  multiple name="without_interval_filter[]" class="" >' + options + '</select><input type="hidden" class="equal" name="equal[' + data[index].id + ']"><div class="rendered"><div><select class="rendered_min for_select2_closable" name="" id="" data-for="' + data[index].id + '">' + options_min + '</select></div><div><select class="rendered_max for_select2_closable" name="" id="" data-for="' + data[index].id + '"> <option data-value="-1" value="-1"> '+window.no_max+' </option>' + options_max + '</select></div></div></div>')
                } else {
                    for (i in data2) {
                        options += '<option value="' + data2[i].id + '">' + JSON.parse(data2[i].options)[lang] + '</option>'
                    }
                    $(".search[data-type=" + data_type + "]").append('<div class="new_added product-rent__info"><label  for="' + data[index].id + '">' + data[index].title[lang] + ':</label><select  multiple name="without_interval_filter[]"  class="for_select2" >' + options + '<select></div>')
                }
            }
            $('.new_added .for_select2_closable').select2(
                {
                    placeholder: window.select_place,
                    closeOnSelect: true
                }
            );
            $('.new_added .for_select2').select2(
                {
                    placeholder: window.select_place,
                    closeOnSelect: false
                }
            );

        },
    });
}

function getLocations(a) {
    var lang = $('#lang').val();
    var selected_regions = JSON.parse(window.selected_regions);
    if (typeof a == "object") {
        // var data_type = $(a).data('type');
        var parent = $(a).val();
    } else {
        var parent = a;
    }
    var options = '';
    var optgroup = '';
    $(".for-opt").find('optgroup').remove();
    $.ajax({
        url: window.get_regions,
        type: "GET",
        data: {
            parent_id: parent
        },
        headers: {
            'X-CSRF-Token': window.token
        },
        success: function (data) {
            data = JSON.parse(data);
            if (data != '') {
                for (index in data) {
                    options = '';
                    for (opt in data[index].childes) {
                        options += '<option ' + (((typeof(selected_regions) != "undefined" && selected_regions !== null) && selected_regions.includes("" + data[index].childes[opt].id + "")) ? "selected" : "") + ' value="' + data[index].childes[opt].id + '">' + data[index].childes[opt].title[lang] + '</option>'
                    }
                    optgroup += '<optgroup label="' + data[index].title[lang] + '">' + options + '</optgroup>'
                }
                $(".for-opt").append(optgroup)
                $(".new_region").select2({
                        placeholder: window.select_place,
                        closeOnSelect: false,
                        // language: {
                        //     noResults: function () {
                        //         return '<button id="no-results-btn" onclick="noResultsButtonClicked()">NortyrtyrtyResult Found</a>';
                        //     },
                        // },
                        // escapeMarkup: function (markup) {
                        //     return markup;
                        // },
                    }
                ).on('select2:not(selected)', e => $(e.currentTarget).data('scrolltop', $('.select2-results__options').scrollTop()))
                    .on('select2:not(selected)', e => $('.select2-results__options').scrollTop($(e.currentTarget).data('scrolltop')));

            }
        },
    });
}

function setGridTypeByAram() {
    if ($(window).width() > 756) {
        $('.sale-apartment__info').removeClass('verticalCard')
    } else {
        $('.sale-apartment__info').addClass('verticalCard')
    }
}

$(document).resize(function () {
    setGridTypeByAram();
})

function getBuildingType(a) {
    $.ajax({
        url: window.get_building_type,
        type: "GET",
        data: {
            category: a,
        },
        headers: {
            'X-CSRF-Token': window.token
        },
        success: function (data) {
            if (data) {
                $('.new_building').show()
            } else {
                $('.new_building').hide()

            }
        },
    });
}
